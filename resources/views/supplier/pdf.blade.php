<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style type="text/css">
        .text-center {
          text-align: center;
        }
        .font-size{
            font-size: 25px;
        }
        .margin-bot{
            margin-bottom: 10px;
        }
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
        }
        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }
        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
          padding: 8px;
          line-height: 1.42857143;
          vertical-align: top;
          border-top: 1px solid #ddd;
        }

    </style>
  </head>
  <body>
        <div class="content">
            <div class="content-inner">
                <hr>
                <div class="row">
                    <div class="col-6 col-md-6">
                        <h4>Purchase Order</h4>
                    </div>
                    <div class="col-6 col-md-6">
                        {{ $outlet->company_name }} <br>
                        {{ $outlet->street1 }}&nbsp;{{ $outlet->street2 }}&nbsp;{{ $outlet->city }}&nbsp;{{ $outlet->postcode }}&nbsp;{{ $outlet->state }}&nbsp;{{ $outlet->country }} <br>
                        {{ $outlet->telephone }}
                    </div>
                </div>
                <hr>
                <div class="row mt-3">
                    <div class="col-6 col-md-6">
                        <p>To : {{ $company_name }} <br>
                            {{ $address }}
                        </p>
                    </div>
                    <div class="col-6 col-md-6">
                        {{ $date }}
                    </div>
                </div>
                
                <table class="table table-bordered">
                    <tr>
                        <td>Index</td>
                        <td>Product Code</td>
                        <td>Product Name</td>
                        <td>Quantity</td>
                    </tr>
                    @foreach($listItem as $items)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $items['product_code'] }}</td>
                        <td>{{ $items['product_name'] }}</td>
                        <td>{{ $items['pivot']['quantity'] }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3"> Total Quantity</td>
                        <td>{{ $total_quantity }}</td>
                    </tr>
                </table>

                @if($remarks)
                    <p>Remarks : {{ $remarks }}</p>
                @endif
                Thank You,
                <br/>
                <i>VGO</i>
            </div>
        </div>
  </body>
</html>
<div class="content">
	<div class="content-inner">
		<hr>
		<div class="row">
			<div class="col-6 col-md-6">
				<h4>Purchase Order</h4>
			</div>
			<div class="col-6 col-md-6">
				{{ $data->outlet->company_name }} <br>
				{{ $data->outlet->street1 }}&nbsp;{{ $data->outlet->street2 }}&nbsp;{{ $data->outlet->city }}&nbsp;{{ $data->outlet->postcode }}&nbsp;{{ $data->outlet->state }}&nbsp;{{ $data->outlet->country }} <br>
				{{ $data->outlet->telephone }}
			</div>
		</div>
		<hr>
		<div class="row mt-3">
			<div class="col-6 col-md-6">
				<p>To : {{ $data->company_name }} <br>
					{{ $data->address }}
				</p>
			</div>
			<div class="col-6 col-md-6">
				{{ $data->date }}
			</div>
		</div>
	    
	    <table class="table table-bordered">
	    	<tr>
	    		<td>Index</td>
	    		<td>Product Code</td>
	    		<td>Product Name</td>
	    		<td>Quantity</td>
	    	</tr>
	    	@foreach($data->listItem as $items)
	    	<tr>
	    		<td>{{ $loop->index + 1 }}</td>
	    		<td>{{ $items['product_code'] }}</td>
	    		<td>{{ $items['product_name'] }}</td>
	    		<td>{{ $items['pivot']['quantity'] }}</td>
	    	</tr>
	    	@endforeach
	    	<tr>
	    		<td colspan="3"> Total Quantity</td>
	    		<td>{{ $data->total_quantity }}</td>
	    	</tr>
	    </table>

	    @if($data->remarks)
	    	<p>Remarks : {{ $data->remarks }}</p>
    	@endif
	    Thank You,
	    <br/>
	    <i>VGO</i>
	</div>
</div>
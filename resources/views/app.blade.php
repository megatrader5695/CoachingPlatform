<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Amirul Fazlan,  Mohd Aznielrul">
        <meta name="description" content="Laravel Vue SPA">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>
        
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/customize.css') }}" rel="stylesheet">

        <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
        <script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>

        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
        <!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
        <script src="{{ asset('js/jquery-ui.js') }}"></script>

        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'siteName'  => config('app.name'),
                'apiDomain' => config('app.url').'/api'
            ]) !!}
        </script>
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">  
        
        <div id="app">
            <app></app>
        </div>

        <!-- <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap/js/popper.min.js') }}"></script>
        <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('js/waves.js') }}"></script>
        <script src="{{ asset('js/sidebarmenu.js') }}"></script>
        <script src="{{ asset('plugins/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
        <script src="{{ asset('js/custom.min.js') }}"></script>
        <script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('plugins/raphael/raphael-min.js') }}"></script> -->

        <!-- <script src="{{ asset('plugins/morrisjs/morris.min.js') }}"></script> -->
        <!-- <script src="{{ asset('js/dashboard1.js') }}"></script> -->
        <!-- <script src="{{ asset('plugins/styleswitcher/jQuery.style.switcher.js') }}"></script> -->

        <!-- WebPack -->
        <script src="{{ mix('/js/app.js') }}"></script>

    </body>
</html>

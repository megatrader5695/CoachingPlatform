<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style type="text/css">
        .btn-primary{
                color: #fff;
                background-color: #007bff;
                border-color: #007bff;
        }
        .btn{
            display: inline-block;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: 0.5rem 0.75rem;
            font-size: 0.875rem;
            line-height: 1.25;
            -webkit-transition: all 0.15s ease-in-out;
            transition: all 0.15s ease-in-out;
        }

    </style>
  </head>
  <body>
    Hello <i>{{ $item->receiver }}</i>,
    <p>New Staff Verification. Application details as attached below.</p>
     
    <div>
    <p><b>Name:</b>&nbsp;{{ $item->name }}</p>
    <p><b>Email:</b>&nbsp;{{ $item->email }}</p>
    <p><b>Contact:</b>&nbsp;{{ $item->contact }}</p>
    </div>
     
    <p>Please verify the staff detail and approve. If not please ignore this mail</p>

    <p>
        <a href="https://stag.vgoaquatic.com" class="btn btn-primary">Verify Staff</a>
    </p>
     
    Thank You,
    <br/>
    <i>VGO</i>
  </body>
</html>
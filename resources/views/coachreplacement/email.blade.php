<div class="content">
	<div class="content-inner">
		<hr>
		<div class="row">
			<div class="col-12 col-md-12">
				Hi {{ $data->coach_name }},
			</div>
			<br>
			<div class="col-12 col-md-12">
				You have been assign to replace class on {{ $data->day }} ({{ $data->date }}) at {{ $data->start }} to {{ $data->end }}.
			</div>
		</div>
	</div>
</div>
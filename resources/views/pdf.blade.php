<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style type="text/css">
        .text-center {
          text-align: center;
        }
        .font-size{
            font-size: 25px;
        }
        .margin-bot{
            margin-bottom: 10px;
        }
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
        }
        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #000 !important;
        }
        .table > thead > tr > th,
        .table > tbody > tr > th,
        .table > tfoot > tr > th,
        .table > thead > tr > td,
        .table > tbody > tr > td,
        .table > tfoot > tr > td {
          padding: 8px;
          line-height: 1.42857143;
          vertical-align: top;
          border-top: 1px solid #ddd;
        }

    </style>
  </head>
  <body>
    <div class="text-center margin-bot font-size">VGO Aquatic</div>
    <h3>Driver Name : {{ $detail->name }}</h3>
    <h3>Date : {{ $detail->receive_date }}</h3>
    <h3>Remark : {{ $detail->remark }}</h3>

    <table class="table table-striped table-bordered">
        <tr>
            <th class="text-center">No</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total Price</th>
        </tr>
        <?php $i=1 ?>
        @forelse ($product as $products)
        <tr class="jump-response">
            <td class="text-center">{{ $i }}</td>
            <td>{{ $products->product_name }}</td>
            <td>RM{{ $products->price }}</td>
            <td>RM{{ $products->quantity }}</td>
            <td>RM{{ $products->price_quantity }}</td>
        </tr>
        <?php $i++; ?>
        @empty
        <tr>
            <td colspan="4">Looks like there is no products available.</td>
        </tr>
        @endforelse
        <tr>
            <td colspan="2">Total</td>
            <td>RM{{ $detail->total_quantity }}</td>
            <td>RM{{ $detail->total_price }}</td>
        </tr>
    </table>
  </body>
</html>
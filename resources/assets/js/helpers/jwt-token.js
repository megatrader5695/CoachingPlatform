export default {
  setToken(token) {
    window.localStorage.setItem("jwt_token", token);
  },
  setRole(id) {
    window.localStorage.setItem("role", id);
  },
  setRoleId(id) {
    window.localStorage.setItem("role_id", JSON.stringify(id));
  },
  getToken() {
    return window.localStorage.getItem("jwt_token");
  },
  getRole() {
    return window.localStorage.getItem("role");
  },
  getRoleId() {
    return JSON.parse(window.localStorage.getItem("role_id"));
  },
  removeToken() {
    window.localStorage.removeItem("jwt_token");
    window.localStorage.removeItem("role_id");
  }
};

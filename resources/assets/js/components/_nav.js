export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'fa fa-tachometer',
      role: [2]
    },
    {
      name: 'Profile',
      url: '/profile',
      icon: 'icon-user',
      role: [2]
    },
    {
      name: 'Outlet',
      url: '/outlet',
      icon: 'icon-map',
      role: [3]
    },
    {
      name: 'Super Admin',
      icon: 'icon-user',
      role: [4],
      children: [
        {
          name: 'Management',
          url: '/super-admin'
        },
        {
          name: 'Create Super Admin',
          url: '/super-admin/create'
        }
      ]
    },
    {
      name: 'Staff Admin',
      icon: 'icon-people',
      role: [5],
      children: [
        {
          name: 'Management',
          url: '/staff'
        },
        {
          name: 'Create Staff',
          url: '/staff/create'
        },
        {
          name: 'Staff Approval',
          url: '/staff/approval'
        }
      ]
    },
    {
      name: 'Coach',
      icon: 'icon-people',
      role: [6],
      children: [
        {
          name: 'Management',
          url: '/coach'
        },
        {
          name: 'Create Coach',
          url: '/coach/new'
        },
        {
          name: 'Coach Approval',
          url: '/coach/approval'
        },
        {
          name: 'To Do List',
          url: '/todolist'
        }
      ]
    },
    {
      name: 'Member',
      icon: 'fa fa-user-o',
      role: [7],
      children: [
        {
          name: 'Management',
          url: '/member'
        },
        {
          name: 'Register',
          url: '/member/create'
        }
      ]
    },
    {
      name: 'Customer',
      icon: 'icon-people',
      role: [8],
      children: [
        {
          name: 'Management',
          url: '/customer'
        },
        {
          name: 'Create Customer',
          url: '/customer/create'
        }
      ]
    },
    // {
    //   name: "Swim Member",
    //   icon: "fa fa-tint",
    //   role: [2, 3],
    //   children: [
    //     {
    //       name: "Management",
    //       url: "/swim"
    //     },
    //     {
    //       name: "Create Swim Member",
    //       url: "/swim/create"
    //     }
    //   ]
    // },
    // {
    //   name: "Gym Member",
    //   icon: "fa fa-user-o",
    //   role: [2, 3],
    //   children: [
    //     {
    //       name: "Management",
    //       url: "/gym"
    //     },
    //     {
    //       name: "Create Gym Member",
    //       url: "/gym/create"
    //     }
    //   ]
    // },
    {
      name: 'Leave',
      icon: 'fa fa-ambulance',
      role: [9],
      children: [
        {
          name: 'Leave Management',
          url: '/leave'
        },
        {
          name: 'Apply Leave',
          url: '/leave/apply'
        },
        {
          name: 'History Leave',
          url: '/leave/history'
        }
      ]
    },
    {
      name: 'Claim',
      icon: 'fa fa-money',
      role: [10],
      children: [
        {
          name: 'Claim Management',
          url: '/claim'
        },
        {
          name: 'Apply Claim',
          url: '/claim/apply'
        },
        {
          name: 'History Claim',
          url: '/claim/history'
        }
      ]
    },
    {
      name: 'Package',
      icon: 'fa fa-gift',
      role: [11],
      children: [

        {
          name: 'Active Package',
          url: '/package'
        },
        {
          name: 'Inactive Package',
          url: '/package/inactive'
        }

      ]
    },

    {
      name: 'Inventory',
      icon: 'fa fa-list',
      role: [12],
      children: [

        {
          name: 'Management',
          url: '/inventory'
        },
        {
          name: 'Category',
          url: '/inventory-category'
        },
        {
          name: 'Stock Receive',
          url: '/inventory/receive'
        },
        {
          name: 'Stock Return',
          url: '/inventory/return'
        }


      ]
    },

    {
      name: 'Class',
      url: '/class',
      icon: 'fa fa-id-card-o',
      role: [13]
    },
    // {
    //   name: 'Syllabus',
    //   url: '/syllabus',
    //   icon: 'fa fa-id-card-o',
    //   role: [2, 3]
    // },
    {
      name: 'Feedback / Rating Management',
      url: '/feedback/management',
      icon: 'fa fa-id-card-o',
      role: [14]
    },

    {
      name: 'Master Data',
      icon: 'fa fa-briefcase',
      role: [16],
      children: [
        {
          name: 'Syllabus',
          url: '/master-data/syllabus'
        },
        {
          name: 'Package',
          url: '/master-data/package'
        },
        {
          name: 'Exams',
          url: '/master-data/exams'
        },
        {
          name: 'Coach',
          url: '/coach/config'
        },
        {
          name: 'E-mail template',
          url: '/master-data/email'
        },
        {
          name: 'Contract',
          url: '/master-data/contract'
      },
        {
          name: 'Leave Config',
          url: '/leave/config'
        },
        {
          name: 'Claim Config',
          url: '/claim/config'
        },
        {
          name: 'Calendar',
          url: '/calendar'
        },
        {
          name: 'Voucher',
          url: '/master-data/voucher'
        },
        {
          name: 'Notice',
          url: '/master-data/notice'
        },
        {
          name: 'Banner Management',
          url: '/master-data/banner'
        },
        {
          name: 'Health',
          url: '/master-data/health'
        },
        {
          name: 'Badge',
          url: '/master-data/badge'
        },
        {
          name: 'Questionnaire',
          url: '/master-data/questionnaire'
        },
        {
          name: 'Department',
          url: '/master-data/department'
        }
        ,
        {
          name: 'Roles and Permission',
          url: '/master-data/rolespermission'
        }
      ]
    },
    {
      name: 'Feedback / Rating',
      url: '/feedback',
      icon: 'fa fa-id-card-o',
      role: [17]
    },
    {
      name: 'Supplier',
      icon: 'fa fa-list',
      role: [18],
      children: [

        {
          name: 'Management',
          url: '/supplier'
        },
        {
          name: 'Item',
          url: '/supplier-item'
        },
        {
          name: 'Order',
          url: '/supplier-order'
        },
      ]
    },
    {
      name: 'Career',
      icon: 'fa fa-user-o',
      role: [19],
      children: [

        {
          name: 'Management',
          url: '/career'
        },
      ]
    },
  ]
};

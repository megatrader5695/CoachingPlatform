import $ from 'jquery';
import Vue from 'vue';
import VueNoty from 'vuejs-noty';
import axios from 'axios';
import BootstrapVue from 'bootstrap-vue';
import Snotify from 'vue-snotify';

Vue.use(BootstrapVue);
window.$ = window.jQuery = $;
window.axios = axios;
require('bootstrap');

Vue.use(VueNoty, {
    progressBar: false,
    layout: 'topRight',
    theme: 'bootstrap-v4',
    timeout: 3000
});

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

// Calendar Component
import fullCalendar from 'vue-fullcalendar';
Vue.component('full-calendar', fullCalendar);

import router from './router';
import store from './store/index';
import App from './components/App.vue';
import jwtToken from './helpers/jwt-token';

axios.interceptors.request.use(
    config => {
        config.headers['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
        config.headers['X-Requested-With'] = 'XMLHttpRequest';

        if (jwtToken.getToken()) {
            config.headers['Authorization'] = 'Bearer ' + jwtToken.getToken();
        }

        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        let errorResponseData = error.response.data;

        const errors = ['token_invalid', 'token_expired', 'token_not_provided'];

        if (errorResponseData.error && errors.includes(errorResponseData.error)) {
            store.dispatch('unsetAuthUser').then(() => {
                jwtToken.removeToken();
                router.push({ name: 'login' });
            });
        }

        return Promise.reject(error);
    }
);

Vue.use(Snotify);
Vue.use(require('vue-moment'));

//import 'bootstrap/dist/css/bootstrap.min.css'
import * as uiv from 'uiv';

Vue.use(uiv);

// Typeahead Module
import Autocomplete from 'v-autocomplete';

// You need a specific loader for CSS files like https://github.com/webpack/css-loader
import 'v-autocomplete/dist/v-autocomplete.css';
Vue.use(Autocomplete);

import VueClockPicker from '@pencilpix/vue2-clock-picker';
import '@pencilpix/vue2-clock-picker/dist/vue2-clock-picker.min.css';

//Vue Notification
import Notifications from 'vue-notification';
Vue.use(Notifications);

//Vue wsyig
import wysiwyg from 'vue-wysiwyg';
Vue.use(wysiwyg, {});

//SweetAlert

import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

// Vue Good Table
import VueGoodTable from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css';
Vue.use(VueGoodTable);

// Vue Tables 2
import { ServerTable, ClientTable, Event } from 'vue-tables-2';
Vue.use(ClientTable, {}, false, 'bootstrap4');

// Vue the mask
import VueTheMask from 'vue-the-mask';
Vue.use(VueTheMask);

// Filter
Vue.filter('capitalize', function(value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
});

Vue.filter('time', function(value) {
    var hour = value.substring(0, 2);
    var minutes = value.substring(3, 5);
    var format = '';
    if (hour > 12) {
        hour -= 12;
        format = 'p.m';
    } else if(hour == 12){
        format = 'p.m';
    } else{
        format = 'a.m';
    }
    return hour + ':' + minutes + ' ' + format;
});

import moment from 'moment';

Vue.filter('age', function(value) {
    return moment(value).fromNow(true);
});

Vue.filter('currency', function(value) {
    if (!value) return '';
    let val = (value/1).toFixed(2).replace(',', ',')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
});

Vue.filter('money', function(value) {
    if (!value) return '';
    return parseFloat(value).toFixed(2);
});

Vue.filter('description', function(value) {
    return value.substring(1, 100);
});


// select cool
import VueSelect from 'vue-cool-select'
Vue.use(VueSelect, {
  theme: 'bootstrap' // or 'material-design'
})

// vue-select
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

// signature
import VueSignaturePad from 'vue-signature-pad';
Vue.use(VueSignaturePad);

// rating star
import StarRating from 'vue-star-rating';
Vue.component('star-rating', StarRating);

//component
Vue.component('page-header', {
    template: `<div class="page-header">
                <h4><slot></slot></h4>
                <slot name="button"></slot>
              </div>`
});

Vue.component('app', App);

const app = new Vue({
    router,
    store
}).$mount('#app');

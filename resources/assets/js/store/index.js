import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		auth
	},
	state: {
		reservedData: {}
	},
	mutations: {
		//showing passed with payload, represented as num
		save: (state, data) => {
			state.reservedData = data;
		}
	},
	strict: true
});

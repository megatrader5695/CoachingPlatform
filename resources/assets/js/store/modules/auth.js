/*
|--------------------------------------------------------------------------
| Mutation Types
|--------------------------------------------------------------------------
*/
export const SET_USER = "SET_USER";
export const UNSET_USER = "UNSET_USER";

/*
|--------------------------------------------------------------------------
| Initial State
|--------------------------------------------------------------------------
*/
const initialState = {
  name: null,
  email: null,
  contact: null,
  designation: null,
  id: null
};

/*
|--------------------------------------------------------------------------
| Mutations
|--------------------------------------------------------------------------
*/
const mutations = {
  [SET_USER](state, payload) {
    state.id = payload.user.id;
    state.name = payload.user.name;
    state.email = payload.user.email;
    state.contact = payload.user.contact;
    state.icNumber = payload.user.icNumber;
    state.designation = payload.user.designation;
  },
  [UNSET_USER](state, payload) {
    state.name = null;
    state.email = null;
  }
};

/*
|--------------------------------------------------------------------------
| Actions
|--------------------------------------------------------------------------
*/
const actions = {
  setAuthUser: (context, user) => {
    context.commit(SET_USER, { user });
  },
  unsetAuthUser: context => {
    context.commit(UNSET_USER);
  }
};

/*
|--------------------------------------------------------------------------
| Getters
|--------------------------------------------------------------------------
*/
const getters = {
  isLoggedIn: state => {
    return !!(state.name && state.email);
  }
};

const getRole = {
  isLoggedIn: state => {
    return !!state.role_id;
  }
};

/*
|--------------------------------------------------------------------------
| Export the module
|--------------------------------------------------------------------------
*/
export default {
  state: initialState,
  mutations,
  actions,
  getters
};

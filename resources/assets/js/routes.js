import Full from './components/containers/Full.vue';

import Home from './components/home/Home.vue';
import Login from './components/login/Login.vue';
import ProfileWrapper from './components/profile/ProfileWrapper.vue';
import Profile from './components/profile/Profile.vue';
import EditProfile from './components/profile/edit-profile/EditProfile.vue';
import EditPassword from './components/profile/edit-password/EditPassword.vue';
import OutletWrapper from './components/outlet/OutletWrapper.vue';
import Outlet from './components/outlet/Outlet.vue';
import CreateOutlet from './components/outlet/create-outlet/CreateOutlet.vue';
import ViewOutlet from './components/outlet/view-outlet/ViewOutlet.vue';
import EditOutlet from './components/outlet/edit-outlet/EditOutlet.vue';
import SuperAdmin from './components/super-admin/SuperAdmin.vue';
import ViewSuperAdmin from './components/super-admin/view-profile/ViewSuperAdmin.vue';
import SuperAdminWrapper from './components/super-admin/SuperAdminWrapper.vue';
import EditSuperAdmin from './components/super-admin/edit-profile/EditSuperAdmin.vue';
import AssignOutlet from './components/super-admin/assign-outlet/AssignOutlet.vue';
import LeaveWrapper from './components/leave/LeaveWrapper.vue';
import Leave from './components/leave/Leave.vue';
import LeaveConfig from './components/leave/config/LeaveConfig.vue';
import Claim from './components/claim/Claim.vue';
import ClaimWrapper from './components/claim/ClaimWrapper.vue';
import ClaimConfig from './components/claim/config/ClaimConfig.vue';
import Package from './components/package/Package.vue';
import PackageWrapper from './components/package/PackageWrapper.vue';
import PackageCreate from './components/package/PackageCreate.vue';
import InactivePackage from './components/package/InactivePackage.vue';
import Timeslot from './components/timeslot/Timeslot.vue';
import viewPackage from './components/package/PackageView.vue';
import CreateSuperAdmin from './components/super-admin/Create2.vue';

export default [
	{
		path: '/',
		name: 'login',
		component: Login,
		meta: { requiresGuest: true }
	},
	{
		path: '/profile',
		component: Full,
		children: [
			{
				path: '',
				name: 'profile',
				component: Profile,
				meta: { requiresAuth: true }
			},
			{
				path: 'edit-profile',
				name: 'profile.editProfile',
				component: EditProfile,
				meta: { requiresAuth: true }
			},
			{
				path: 'edit-password',
				name: 'profile.editPassword',
				component: EditPassword,
				meta: { requiresAuth: true }
			},
			{
				path: '*',
				redirect: {
					name: 'profile'
				}
			}
		]
	},
	{
		path: '/outlet',
		component: Full,
		children: [
			{
				path: '',
				name: 'outlet',
				component: Outlet,
				meta: { requiresAuth: true }
			},
			{
				path: 'create',
				name: 'outlet.create',
				component: CreateOutlet,
				meta: { requiresAuth: true }
			},
			{
				path: '/outlet/:id',
				name: 'outlet.view',
				component: ViewOutlet,
				meta: { requiresAuth: true }
			},
			{
				path: '/outlet/:id/edit',
				name: 'outlet.edit',
				component: EditOutlet,
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/super-admin',
		component: Full,
		children: [
			{
				path: '/super-admin/create',
				name: 'superadmin.register',
				component: CreateSuperAdmin,
				meta: { requiresAuth: true }
			},
			{
				path: '',
				component: Full,
				component: require('./components/super-admin/SuperAdmin.vue'),
				meta: { requiresAuth: true, systemAuth: true }
			},
			{
				path: '/super-admin/:id/',
				name: 'superadmin.view',
				component: ViewSuperAdmin,
				meta: { requiresAuth: true }
			},
			{
				path: '/super-admin/:id/edit',
				name: 'superadmin.edit',
				component: EditSuperAdmin,
				meta: { requiresAuth: true }
			},
			{
				path: '/super-admin/:id/outlet',
				name: 'superadmin.outlet',
				component: AssignOutlet,
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/staff/login',
		name: 'staff.login',
		component: require('./components/staff/login/Login.vue')
	},
	{
		path: '/staff/register',
		name: 'staff.register',
		component: require('./components/staff/login/Register.vue')
	},
	{
		path: '/staff/signature/:id',
		name: 'staff.signature',
		component: require('./components/staff/Signature.vue')
	},
	{
		path: '/staff/signatures/:id',
		name: 'staff.signatures',
		component: require('./components/staff/SignatureSocial.vue')
	},
	{
		path: '/staff',
		component: Full,
		children: [
			{
				path: '',
				name: 'staff',
				component: require('./components/staff/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/staff/approval',
				name: 'staff.approval',
				component: require('./components/staff/Approval.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/staff/approval/:id',
				name: 'staff.approval.view',
				component: require('./components/staff/ApprovalView.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/staff/create',
				name: 'staff.create',
				component: require('./components/staff/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/staff/:id',
				name: 'staff.view',
				component: require('./components/staff/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/staff/:id/edit',
				name: 'staff.edit',
				component: require('./components/staff/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/coach/login',
		name: 'coach.login',
		component: require('./components/coach/login/Login.vue')
	},
	{
		path: '/coach/register',
		name: 'coach.register',
		component: require('./components/coach/login/Register.vue')
	},
	{
		path: '/coach',
		component: Full,
		children: [
			{
				path: '',
				name: 'coach',
				component: require('./components/coach/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/coach/new',
				name: 'coach.create',
				component: require('./components/coach/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/coach/approval',
				name: 'coach.approval',
				component: require('./components/coach/Approval.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/coach/config',
				name: 'coach.config',
				component: require('./components/coach/Config.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/coach/config/create',
				name: 'coach.config.create',
				component: require('./components/coach/ConfigCreate.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/coach/approval/:id',
				name: 'coach.approval.view',
				component: require('./components/coach/ApprovalView.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/coach/:id/edit',
				name: 'coach.edit',
				component: require('./components/coach/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/coach/:id',
				name: 'coach.view',
				component: require('./components/coach/View.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/todolist',
		component: Full,
		children: [
			{
				path: '',
				name: 'todolist',
				component: require('./components/todolist/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/todolist/create',
				name: 'todolist.create',
				component: require('./components/todolist/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/todolist/:id/edit',
				name: 'todolist.edit',
				component: require('./components/todolist/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/todolist/:id',
				name: 'todolist.view',
				component: require('./components/todolist/View.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/leave',
		component: Full,
		children: [
			{
				path: '',
				name: 'leave',
				component: Leave,
				meta: { requiresAuth: true }
			},
			{
				path: '/leave/config/new',
				name: 'leave.config.create',
				component: require('./components/leave/config/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'history/:id',
				name: 'leave.history.show',
				component: require('./components/leave/history/Show.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'history/:id/view',
				name: 'leave.history.view',
				component: require('./components/leave/history/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'config',
				name: 'leave.config',
				component: LeaveConfig,
				meta: { requiresAuth: true }
			},
			{
				path: 'apply',
				name: 'leave.apply',
				component: require('./components/leave/apply/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'history',
				name: 'leave.history',
				component: require('./components/leave/history/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'config/:id',
				name: 'leave.config.show',
				component: require('./components/leave/config/Show.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/leave/config/:id/edit',
				name: 'leave.config.edit',
				component: require('./components/leave/config/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/leave/config/assign/:id',
				name: 'leave.config.assign',
				component: require('./components/leave/config/Assign.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/claim',
		component: Full,
		children: [
			{
				path: '',
				name: 'claim',
				component: Claim,
				meta: { requiresAuth: true }
			},
			{
				path: '/claim/create',
				name: 'claim.create',
				component: require('./components/claim/config/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/claim/config',
				name: 'claim.config',
				component: ClaimConfig,
				meta: { requiresAuth: true }
			},
			{
				path: 'apply',
				name: 'claim.apply',
				component: require('./components/claim/apply/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'history/:id',
				name: 'claim.history.show',
				component: require('./components/claim/history/Show.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'history/:id/view',
				name: 'claim.history.view',
				component: require('./components/claim/history/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/claim/config/:id/edit',
				name: 'claim.config.edit',
				component: require('./components/claim/config/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/claim/config/:id',
				name: 'claim.config.assign',
				component: require('./components/claim/config/Assign.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: 'history',
				name: 'claim.history',
				component: require('./components/claim/history/Index.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/package',
		component: Full,
		children: [
			{
				path: '',
				name: 'package',
				component: Package,
				meta: { requiresAuth: true }
			},
			{
				path: '/package/inactive',
				name: 'package.inactive',
				component: InactivePackage,
				meta: {
						requiresAuth: true
					}
			},
			{
				path: '/package/create',
				name: 'package.create',
				component: PackageCreate,
				meta: { requiresAuth: true }
			},
			{
				path: '/package/:id',
				name: 'package.view',
				component: viewPackage,
				meta: { requiresAuth: true }
			},
			{
				path: '/package/:id/edit',
				name: 'package.edit',
				component: require('./components/package/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/package/:id/image',
				name: 'package.image',
				component: require('./components/package/Image.vue'),
				meta: { requiresAuth: true }
			}
		]
	},

	{
		path: '/inventory',
		component: Full,
		children: [
			{
				path: '',
				name: 'inventory',
				component: require('./components/inventory/Inventory.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/receive',
				name: 'inventory.receive',
				component: require('./components/inventory/ReceiveInventory.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/return',
				name: 'inventory.return',
				component: require('./components/inventory/ReturnInventory.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/create',
				name: 'inventory.create',
				component: require('./components/inventory/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/:id',
				name: 'inventory.view',
				component: require('./components/inventory/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/:id/edit',
				name: 'inventory.edit',
				component: require('./components/inventory/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/receive/create',
				name: 'inventory.receive.create',
				component: require('./components/inventory/receive/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/receive/:id',
				name: 'inventory.receive.view',
				component: require('./components/inventory/receive/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/return/create',
				name: 'inventory.return.create',
				component: require('./components/inventory/return/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory/return/:id',
				name: 'inventory.return.view',
				component: require('./components/inventory/return/View.vue'),
				meta: { requiresAuth: true }
			}
		]
	},

	{
		path: '/inventory-category',
		component: Full,
		children: [
			{
				path: '',
				name: 'inventory.category',
				component: require('./components/inventory/category/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory-category/create',
				name: 'inventory.category.create',
				component: require('./components/inventory/category/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory-category/:id',
				name: 'inventory.category.view',
				component: require('./components/inventory/category/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/inventory-category/:id/edit',
				name: 'inventory.category.edit',
				component: require('./components/inventory/category/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},

	{
		path: '/timeslot',
		component: Full,
		children: [
			{
				path: '',
				name: 'timeslot',
				component: Timeslot,
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/pos',
		beforeEnter(to, from, next) {
			// console.log(window.location.hostname);
			if(window.location.hostname == 'stag.vgoaquatic.com'){
				window.open('http://stagpos.vgoaquatic.com', '_blank');
			}else{
				window.open('http://pos.vgoaquatic.com', '_blank');
			}
			
		}
	},
	{
		path: '/attendance',
		beforeEnter(to, from, next) {
			window.open('https://attend.vgoaquatic.com', '_blank');
		}
	},
	{
		path: '/tv',
		beforeEnter(to, from, next) {
			window.open('https://tv.vgoaquatic.com', '_blank');
		}
	},
	{
		path: '/socialauth',
		name: 'socialauth',
		component:  require('./components/login/SocialAuth.vue'),
		meta: { requiresAuth: false }
	},
	{
		path: '/customer',

		component: Full,
		children: [
			{
				path: '',
				name: 'customer.index',
				component: require('./components/customer/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/customer/create',
				name: 'customer.create',
				component: require('./components/customer/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/customer/:id',
				name: 'customer.view',
				component: require('./components/customer/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/customer/:id/edit',
				name: 'customer.edit',
				component: require('./components/customer/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/customer/:id/payment',
				name: 'customer.payment',
				component: require('./components/customer/Payment.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/swim',
		component: Full,
		children: [
			{
				path: '',
				name: 'swim.index',
				component: require('./components/swim/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/swim/create',
				name: 'swim.create',
				component: require('./components/swim/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/swim/:id',
				name: 'swim.view',
				component: require('./components/swim/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/swim/:id/edit',
				name: 'swim.edit',
				component: require('./components/swim/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/gym',
		component: Full,
		children: [
			{
				path: '',
				name: 'gym.index',
				component: require('./components/gym/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/gym/create',
				name: 'gym.create',
				component: require('./components/gym/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/gym/:id',
				name: 'gym.view',
				component: require('./components/gym/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/gym/:id/edit',
				name: 'gym.edit',
				component: require('./components/gym/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/calendar',
		component: Full,
		children: [
			{
				path: '',
				name: 'calendar',
				component: require('./components/calendar/Outlet.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/calendar/setting',
				name: 'calendar.config',
				component: require('./components/calendar/Setting.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/calendar/:id',
				name: 'calendar.outlet',
				component: require('./components/calendar/Index.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/class',
		component: Full,
		children: [
			{
				path: '',
				name: 'class',
				component: require('./components/class/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/class/:outlet/:day',
				name: 'class.view',
				component: require('./components/class/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/class/:day/:id/detail',
				name: 'class.view.day',
				component: require('./components/class/Details.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/class/:day/:id/edit',
				name: 'class.view.day.edit',
				component: require('./components/class/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/class/:day/:id/replace',
				name: 'class.view.day.replace',
				component: require('./components/class/ReplacementForm.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/class/:day/:id/replace/coach',
				name: 'class.view.day.replace.coach',
				component: require('./components/class/Coach.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/class/create',
				name: 'class.create',
				component: require('./components/class/Create.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/syllabus',
		component: Full,
		children: [
			{
				path: '',
				name: 'syllabus',
				component: require('./components/syllabus/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/syllabus/create',
				name: 'syllabus.create',
				component: require('./components/syllabus/Create.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/dashboard',
		component: Full,
		children: [
			{
				path: '',
				name: 'dashboard',
				component: require('./components/dashboard/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/annual_sales/overview',
				name: 'dashboard.monthly_sales',
				component: require('./components/dashboard/monthly_sales_overview.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/monthly_sales/overview',
				name: 'dashboard.Transactions',
				component: require('./components/dashboard/Transactions.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/Transactions/Invoice_no',
				name: 'dashboard.Invoice',
				component: require('./components/dashboard/Invoice.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/SwimMember',
				name: 'dashboard.SwimMember',
				component: require('./components/dashboard/SwimMember.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/member',
		component: Full,
		children: [
			{
				path: '',
				name: 'member',
				component: require('./components/member/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/create',
				name: 'member.create',
				component: require('./components/member/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id',
				name: 'member.view',
				component: require('./components/member/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/edit',
				name: 'member.edit',
				component: require('./components/member/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/package',
				name: 'member.package',
				component: require('./components/member/Package.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/:memberid/editpackage',
				name: 'member.editpackage',
				component: require('./components/member/Editpackage.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:memberid/:id/editstatus/:class',
				name: 'member.editstatus',
				component: require('./components/member/Editstatus.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/transfer',
				name: 'member.transfer',
				component: require('./components/member/Merge.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/addClass/:packuserId',
				name: 'member.addClass',
				component: require('./components/member/AddClass.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/:package/:class/:package_user_id/attendance',
				name: 'member.attendance',
				component: require('./components/member/Attendance.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:package_user_id/replacement_attachments',
				name: 'member.attendance.replacement_attachments',
				component: require('./components/member/ReplacementAttachments.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/questionnaire',
				name: 'member.questionnaire',
				component: require('./components/member/Questionnaire.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/progress',
				name: 'member.progress',
				component: require('./components/member/ProgressMember.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:id/update/payment_log',
				name: 'member.updatepaymentlog',
				component: require('./components/member/Updatepaymentlog.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/member/:memberid/:id/create_order',
				name: 'member.manualcreateorder',
				component: require('./components/member/Createorder.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/changepassword',
		component: Full,
		children: [
			{
				path: '/changepassword/:id',
				name: 'change.password',
				component: require('./components/changepassword/Edit.vue')
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/email',
				name: 'email-template',
				component: require('./components/email/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/email/create',
				name: 'email-template.create',
				component: require('./components/email/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/email/:id',
				name: 'email-template.view',
				component: require('./components/email/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/email/:id/edit',
				name: 'email-template.edit',
				component: require('./components/email/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/voucher',
				name: 'voucher',
				component: require('./components/voucher/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/voucher/create',
				name: 'voucher.create',
				component: require('./components/voucher/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/voucher/:id',
				name: 'voucher.view',
				component: require('./components/voucher/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/voucher/:id/edit',
				name: 'voucher.edit',
				component: require('./components/voucher/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/notice',
				name: 'notice',
				component: require('./components/notice/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/notice/create',
				name: 'notice.create',
				component: require('./components/notice/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/notice/:id',
				name: 'notice.view',
				component: require('./components/notice/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/notice/:id/edit',
				name: 'notice.edit',
				component: require('./components/notice/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/contract',

				name: 'contract',
				component: require('./components/contract/Index.vue'),
				meta: { requiresAuth: true }
			},

			{
				path: '/master-data/contract/create',
				name: 'contract.create',
				component: require('./components/contract/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/contract/:id',
				name: 'contract.view',
				component: require('./components/contract/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/contract/:id/edit',
				name: 'contract.edit',
				component: require('./components/contract/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/banner',
				name: 'banner',
				component: require('./components/banner/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/banner/create',
				name: 'banner.create',
				component: require('./components/banner/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/banner/:id',
				name: 'banner.view',
				component: require('./components/banner/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/banner/:id/edit',
				name: 'banner.edit',
				component: require('./components/banner/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/health',

				name: 'health',
				component: require('./components/health/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/health/create',
				name: 'health.create',
				component: require('./components/health/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/health/:id',
				name: 'health.view',
				component: require('./components/health/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/health/:id/edit',
				name: 'health.edit',
				component: require('./components/health/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/badge',

				name: 'badge',
				component: require('./components/badge/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/badge/create',
				name: 'badge.create',
				component: require('./components/badge/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/badge/:id',
				name: 'badge.view',
				component: require('./components/badge/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/badge/:id/edit',
				name: 'badge.edit',
				component: require('./components/badge/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/questionnaire',

				name: 'questionnaire',
				component: require('./components/questionnaire/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/questionnaire/:id',
				name: 'questionnaire.showAll',
				component: require('./components/questionnaire/View.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/department',

				name: 'department',
				component: require('./components/department/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/department/create',
				name: 'department.create',
				component: require('./components/department/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/department/:id/edit',
				name: 'department.edit',
				component: require('./components/department/Edit.vue'),
				meta: { requiresAuth: true }
			}
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/maximum-student',
				name: 'maximum-student',
				component: require('./components/master-data/StudentLimit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/package',
				name: 'master-data-package',
				component: require('./components/master-data/Package.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/exams',
				name: 'master-data-exams',
				component: require('./components/master-data/exams.vue'),
				meta: { requiresAuth: true }
			},
			{
                path: '/exams/:id/ques/index',
                name: 'exams-question.index',
                component: require('./components/master-data/IndexExamsQuestion.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/master-data/syllabus',
                name: 'master-data-syllabus',
                component: require('./components/master-data/Syllabus.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/syllabus/:syllabus/ques/:id/edit',
                name: 'syllabus-question.edit',
                component: require('./components/master-data/EditSyllabusQuestion.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/exams/ques/:id/edit',
                name: 'exams-question.edit',
                component: require('./components/master-data/EditExamsQuestion.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/syllabus/:id/edit',
                name: 'syllabus.edit',
                component: require('./components/master-data/EditSyllabus.vue'),
                meta: { requiresAuth: true }
            },
            // {
            //     path: '/syllabus/Create',
            //     name: 'syllabus-new.create',
            //     component: require('./components/master-data/CreateSyllabus.vue'),
            //     meta: { requiresAuth: true }
            // },
            {
                path: '/syllabus/:id/ques/index',
                name: 'syllabus-question.index',
                component: require('./components/master-data/IndexSyllabusQuestion.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/syllabus/:id/ques/create',
                name: 'syllabus-question.create',
                component: require('./components/master-data/CreateSyllabusQuestion.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/exams/:id/ques/create',
                name: 'exams-question.create',
                component: require('./components/master-data/CreateExamsQuestion.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/syllabus/:syllabus/question/:id/index',
                name: 'question-option.index',
                component: require('./components/master-data/IndexQuestionOption.vue'),
                meta: { requiresAuth: true }
            },
            {
                path: '/syllabus/:syllabus/question/:id/option/create',
                name: 'question-option.create',
                component: require('./components/master-data/CreateQuestionOption.vue'),
                meta: { requiresAuth: true }
            }
		]
	},
	{
		path: '/master-data',
		component: Full,
		children: [
			{
				path: '/master-data/rolespermission',

				name: 'rolespermission',
				component: require('./components/rolespermission/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/master-data/rolespermission/:id',
				name: 'rolespermission.edit',
				component: require('./components/rolespermission/Edit.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/feedback',
		component: Full,
		children: [
			{
				path: '',
				name: 'feedback.index',
				component: require('./components/feedback/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/feedback/create',
				name: 'feedback.create',
				component: require('./components/feedback/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/feedback/management',
				name: 'feedback.management.index',
				component: require('./components/feedback/management/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/feedback/management/:id/edit',
				name: 'feedback.management.edit',
				component: require('./components/feedback/management/Edit.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/feedback/management/:id',
				name: 'feedback.management.view',
				component: require('./components/feedback/management/rating/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/rating/create',
				name: 'rating.create',
				component: require('./components/rating/Create.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/supplier',
		component: Full,
		children: [
			{
				path: '',
				name: 'supplier.index',
				component: require('./components/supplier/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/supplier/create',
				name: 'supplier.create',
				component: require('./components/supplier/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/supplier/:id',
				name: 'supplier.view',
				component: require('./components/supplier/View.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/supplier/:id/edit',
				name: 'supplier.edit',
				component: require('./components/supplier/Edit.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/supplier-item',
		component: Full,
		children: [
			{
				path: '',
				name: 'supplier-item.index',
				component: require('./components/supplier/items/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/supplier-item/create',
				name: 'supplier-item.create',
				component: require('./components/supplier/items/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/supplier-item/:id/edit',
				name: 'supplier-item.edit',
				component: require('./components/supplier/items/Edit.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/supplier-order',
		component: Full,
		children: [
			{
				path: '',
				name: 'supplier-order.index',
				component: require('./components/supplier/orders/Index.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/supplier-order/create',
				name: 'supplier-order.create',
				component: require('./components/supplier/orders/Create.vue'),
				meta: { requiresAuth: true }
			},
			{
				path: '/supplier-order/:id/',
				name: 'supplier-order.view',
				component: require('./components/supplier/orders/View.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
	{
		path: '/career',
		component: Full,
		children: [
			{
				path: '',
				name: 'career.index',
				component: require('./components/career/Index.vue'),
				meta: { requiresAuth: true }
			},
		]
	},
];

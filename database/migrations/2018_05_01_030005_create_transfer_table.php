<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfer', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->unsigned()->nullable();
            $table->foreign('member_id')->references('id')->on('users');

            $table->integer('classroom_id')->unsigned()->nullable();
            $table->foreign('classroom_id')->references('id')->on('classrooms');

            $table->date('date');

            $table->enum('inOrOut', ['in', 'out']);


            $table->timestamps();
        });


        Schema::table('classroom_user', function (Blueprint $table) {
            $table->enum('status', ['active', 'waiting'])->default('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfer');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveColumnToUsesr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_user', function (Blueprint $table) {
            $table->dropColumn(['emergency_name','emergency_relationship', 'emergency_contact']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('emergency_name');
            $table->string('emergency_relationship');
            $table->string('emergency_contact');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

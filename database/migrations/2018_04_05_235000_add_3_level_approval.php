<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add3LevelApproval extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('leave_user', function (Blueprint $table) {
            $table->boolean('firstApproval')->default(0);
            $table->boolean('secondApproval')->default(0);
            $table->boolean('thirdApproval')->default(0);
        });

        Schema::table('claim_user', function (Blueprint $table) {
            $table->boolean('firstApproval')->default(0);
            $table->boolean('secondApproval')->default(0);
            $table->boolean('thirdApproval')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

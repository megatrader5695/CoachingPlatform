<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('transfers', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('member_id')->unsigned()->nullable();
            $table->foreign('member_id')->references('id')->on('users');

            $table->integer('fromClassroom')->unsigned()->nullable();
            $table->foreign('fromClassroom')->references('id')->on('classrooms');

            $table->integer('toClassroom')->unsigned()->nullable();
            $table->foreign('toClassroom')->references('id')->on('classrooms');


            $table->date('fromDate');
            $table->date('toDate');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replacements');
    }
}

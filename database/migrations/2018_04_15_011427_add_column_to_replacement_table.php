<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToReplacementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('replacements', function (Blueprint $table) {

            $table->boolean('confirmed')->default(0);
            $table->integer('fromUser')->unsigned()->nullable();
            $table->foreign('fromUser')->references('id')->on('users');
            $table->integer('toUser')->unsigned()->nullable();
            $table->foreign('toUser')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveNesscColToBridge extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['swim','stroke', 'progress', 'badge', 'deposit', 'class']);
        });

        Schema::table('package_user', function (Blueprint $table) {
            $table->enum('swim', ['White', 'Blue', 'Red', 'Yellow', 'Green'])->nullable();
            $table->enum('stroke', ['White', 'Blue', 'Red', 'Yellow', 'Green'])->nullable();
            $table->integer('progress')->nullable();
            $table->string('badge')->nullable();
            $table->enum('class', ['Swim', 'Gym', 'Stroke']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

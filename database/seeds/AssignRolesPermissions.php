<?php

use Illuminate\Database\Seeder;
use App\RolesPermission;
use App\Role;

class AssignRolesPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RolesPermission::truncate();

        $superadmin = Role::findOrFail(2);

        $permission1 = [
           2,
           5,
           6,
           7,
           8,
           9,
           10,
           11,
           12,
           13,
           14,
           15,
           16,
           18,
           19
        ];

        $superadmin->permissions()->sync($permission1);

        $sysadmin = Role::findOrFail(1);

        $permission2 = [
           2,
           3,
           4,
        ];

        $sysadmin->permissions()->sync($permission2);
    }
}

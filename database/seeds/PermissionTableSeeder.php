<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();

        $permissions = [
           'dashboard',
           'profile',
           'outlet',
           'superadmin',
           'staff',
           'coach',
           'member',
           'customer',
           'leave',
           'claim',
           'package',
           'inventory',
           'class',
           'feedback / rating management',
           'pos / tv / attendance',
           'masterdata',
           'feedback / rating',
           'supplier',
           'career'
        ];


        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}

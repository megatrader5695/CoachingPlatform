<?php

// return [
// 	'default_zkbio' => 'http://vgoaquatic.asuscomm.com:8098/api/',
// 	'default_zkbio_token' => '62C26268C2',
// ];

return [
	'default_zkbio' => env("ZKBIO_URL", "true"),
	'default_zkbio_token' => env("ZKBIO_ACCESS_TOKEN", "true"),
];
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveUserDetail extends Model
{
    protected $table = 'leave_user_detail';

    protected $fillable = ['user_id', 'leave_id', 'days', 'carryfoward'];

    public function leave()
    {
    	return $this->belongsTo('App\Leave');
    }
}

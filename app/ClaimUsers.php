<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimUsers extends Model
{
    protected $table = 'claim_user';

    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function claim()
    {
    	return $this->belongsTo('App\Claim');
    }

    public function scopeNotDisapprove($query)
    {
        return $query->where('approve', '!=', 2);
    }
}

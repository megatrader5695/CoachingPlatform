<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrentLogin extends Model
{
    protected $table = 'current_outlet_login';

    protected $fillable = [
        'user_id', 'outlet_id', 'session_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Package;
use App\PackageUser;
use App\Attendance;
use App\Order;
use App\ClassroomUser;
use App\Classroom;
use App\User;
use Carbon\Carbon;
use DB;
use stdClass;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ManualAddMemberToDevice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manualAddMember:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manual manualAddMember:api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |------------------------------------------------------------------------------------
        | Auto terminate
        |------------------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $startOfDay = Carbon::now()->startOfDay();
        $endOfDay = Carbon::today()->endOfDay();

        $packageUsers = PackageUser::with('classroom')
                        ->where('status','active')
                        ->get();

        $attendances = Attendance::whereBetween('clock_in_start',[$startOfDay,$endOfDay])->get();

        foreach ($packageUsers as $packageUser) {

            $classroomUsers = ClassroomUser::where('package_user_id',$packageUser->id)->get();

            foreach ($classroomUsers as $classroomUser) {

                $classrooms = Classroom::find($classroomUser->classroom_id);

                $userDetails = User::find($packageUser->user_id);

                if ($classrooms) {

                    if ($packageUser->outstanding > 0) {

                        $year = Carbon::now()->format('Y-m-d');
                        $start_time = $year.' 03:00:00';
                        $end_time = $year.' 03:01:00';
                        $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                        $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                    } else {

                        $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classrooms->start)));
                        $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classrooms->end)));


                    }

                    $accLevelIds = '402880276ce125e4016ce12701cf0377';

                    $details = new stdClass;
                    $details->pin = $userDetails->id;
                    $details->name = $userDetails->name;
                    if($userDetails->gender == 'Male') {
                        $gender = 'M';
                    } else {
                        $gender = 'F';
                    }
                    $details->gender = $gender;
                    $details->accLevelIds = $accLevelIds;
                    $details->accStartTime = $clock_in_start;
                    $details->accEndTime = $clock_in_end;

                    $data = json_encode($details);

                    
                }
            }

            
        }
        
        DB::commit();
    }
}

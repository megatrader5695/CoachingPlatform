<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PackageUser;
use App\Order;
use App\OrderSubscription;
use Carbon\Carbon;
use DB;

class UpdateMonthlyPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:monthlyPayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update single monthly_payment to order accordingly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        //$package_ids = ['60','64','69','84','85','86','87','88','89','90'];
        $package_users = PackageUser::whereHas('order')->with('packages')->where('status','active')->get();

        $fat = [];
        $test_paid= [];
        $test_unpaid =[];
        foreach ($package_users as $key => $pu) {
            if ($pu->status == 'active'){
                $orders = Order::whereHas('package_user', function($q){
                                $q->where('status', '=', 'active');
                            })->where([['package_user_id', $pu->id],['package_id',$pu->package_id]])->get();
                if($orders != ''){
                    
                    foreach ($orders as $key => $order) {
                        
                        if($order->package_depo > 0 || $key == 0 && $order->package_depo == 0){

                            $order->monthly_payment = Carbon::parse($pu->start_date)->startOfMonth()->format('Y-m-d');
                            $order->save();
                            
                        }else{
                            
                            $index = $key-1;

                            if ($index != '-1') {
                                
                                $order->monthly_payment = Carbon::parse($orders[$index]->monthly_payment)->addMonths($pu->packages->subscription)->startOfMonth()->format('Y-m-d');
                                $current_month = Carbon::now()->startOfMonth()->format('Y-m-d');

                               if($order->monthly_payment > $current_month){
                                   $order->delete();
                               }else{
                                   $order->save();
                               }

                            }
                            
                        }
                    } 

                    $last_order = Order::whereHas('package_user', function($q){
                                    $q->where('status', '=', 'active');
                                })->where([['package_user_id', $pu->id],['package_id',$pu->package_id]])->orderbyDesc('id')->first();

                    if($last_order != ''){

                    $OrderSubscription = OrderSubscription::where('package_user_id', $pu->id)->first();
                    $OrderSubscription->order_id = $last_order->id;
                    $OrderSubscription->month = Carbon::parse($last_order->monthly_payment)->format('n');       
                    $OrderSubscription->year = carbon::parse($last_order->monthly_payment)->format('Y');
                    $OrderSubscription->save();
                    //dd($OrderSubscription);
                    }

                }
            }
        }//dd($id_arr);
        DB::commit();

        echo 'success run';
    }
}

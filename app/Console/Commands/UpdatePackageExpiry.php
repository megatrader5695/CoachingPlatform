<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Temporary;
use App\Package;
use App\PackageUser;
use App\Order;
use App\OrderSubscription;
use App\UserChangePackage;
use App\ClassroomUser;
use Carbon\Carbon;
use DB;

class UpdatePackageExpiry extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:packageuserexpiry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Order Subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        $package_user = PackageUser::whereHas('packages', function($q){
                                $q->where('status', '=', 'active');
                            })->whereHas('order')->with('packages')->where('status','active')->get();
        $unpaid = ['1','2'];

        foreach ($package_user as $pu) {

            $orders = Order::whereHas('package', function($q){
                                    $q->where('status', '=', 'active');
                                })
                        ->with('package')->where([['package_user_id',$pu->id],['status','0']])->get()->last();
                        
            if(!$orders){

                $orders = Order::whereHas('package', function($q){
                                    $q->where('status', '=', 'active');
                                })
                        ->with('package')->where('package_user_id',$pu->id)->whereIn('status',$unpaid)->first();

                if($orders->status == '2'){
                    //dd($pu);
                   $pu->expiry = Carbon::parse($orders->monthly_payment)->startOfMonth()->format('Y-m-d'); 
                   $pu->outstanding = $orders->package_depo + $orders->package_price;
                   $pu->save(); 
                   
                }elseif($orders->status == '1'){
                    $pu->expiry = Carbon::parse($orders->monthly_payment)->startOfMonth()->format('Y-m-d'); 
                    $pu->outstanding = Order::whereHas('package', function($q){
                                        $q->where('status', '=', 'active');
                                    })
                            ->with('package')->where('package_user_id',$pu->id)->whereIn('status',$unpaid)->sum('package_total');
                    $pu->save();
                }
                
            }else{

            $pu->expiry = Carbon::parse($orders->monthly_payment)->addMonths($orders->package->subscription)->startOfMonth()->format('Y-m-d'); 
            $pu->outstanding = Order::whereHas('package', function($q){
                                    $q->where('status', '=', 'active');
                                })
                        ->with('package')->where('package_user_id',$pu->id)->whereIn('status',$unpaid)->sum('package_total');
            
            $pu->save();

            }
  
        }
        DB::commit();

        echo 'success run';
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Attendance;
use DB;

class DeleteGymAbsent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:gymAbsent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete data if gym member is absent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        $attendance = Attendance::classNull()->absent()->forceDelete();
        if (!$attendance) {
            DB::rollback();
        }

        DB::commit();
    }
}

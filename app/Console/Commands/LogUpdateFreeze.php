<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Package;
use App\OrderSubscription;
use App\ClassroomUser;
use App\PackageUser;
use App\Order;
use App\UserChangePackage;
use App\Transaction;
use App\Outlet;
use Carbon\Carbon;
use DB;

class LogUpdateFreeze extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:updatefreeze';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Freeze During MCO';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | Create new order record
        | Update table Order Subscription
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $feb_unpaid_orders = Order::where([['status','1'],['package_id','60'],['outlet_id','24'],['monthly_payment','2020-02-01'],['package_depo','0'],['registration_fee','0']])->get();

        // dd(count($feb_unpaid_orders));

        foreach ($feb_unpaid_orders as $valued) {

          $value = Order::where('id',$valued->id)->first();
          $check_subscription = Package::where('id',$value->package_id)->first();

          $total_price = $value->package_price;
          $total_deduction = round($total_price/$check_subscription->subscription,1);
          $total_after_deduction = round($total_price-$total_deduction,1);
          $new_monthly_payment = '2020-04-01';

          $value->package_price = number_format($total_after_deduction,2, '.', '');
          $value->package_total = number_format($total_after_deduction,2, '.', '');
          $value->save();

          $Insertorder = [
            'member_name' => $value->member_name,
            'cust_id' => $value->cust_id,
            'package_id' => $value->package_id,
            'outlet_id' => $value->outlet_id,
            'package_user_id' => $value->package_user_id,
            'package_price' => number_format($total_deduction,2, '.', ''),
            'package_depo' => '0',
            'registration_fee' => '0',
            'registration_fee_status' => $value->registration_fee_status,
            'freeze_price' => '',
            'package_total' => number_format($total_deduction,2, '.', ''),
            'monthly_payment' => $new_monthly_payment,
            'status' => '0',
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
          ];

          $new_order = Order::create($Insertorder);

          if($new_order){

            $outlet_prefix = Outlet::find($new_order->outlet_id);

            $invoice_number = DB::select("SELECT invoice_no FROM `transactions` WHERE invoice_no is NOT NULL AND outlet_id = $new_order->outlet_id order by invoice_no desc limit 1");

            if(empty($invoice_number)){

              $inv_no = $outlet_prefix->prefix."0000000001";

            } else {

              $inv_no = $invoice_number[0]->invoice_no;
              $inv_no++;
              $str_length = 10;
              $inv_no = substr("0000000000{$inv_no}", -$str_length);
              $inv_no = $outlet_prefix->prefix.$inv_no;

            }

            $new_txn = [
                'cust_id' => $new_order->cust_id,
                'outlet_id' => $new_order->outlet_id,
                'cashier_id' => '1',
                'order_id' => $new_order->id,
                'product_id' => '0',
                'voucher_id' => '50',
                'quantity' => '1',
                'amount_paid' => '0.00',
                'deposit_use' => '',
                'voucher_value' => number_format($total_deduction,2, '.', ''),
                'invoice_total' => '0.00',
                'sst_package' => '0.00',
                'payment_method' => 'Cash',
                'status' => '0',
                'only_reg_fee' => '0',
                'package_id' => $new_order->package_id,
                'item_price' => number_format($total_deduction,2, '.', ''),
                'package_depo' => '0',
                'reg_fee' => '0',
                'invoice_no' => $inv_no,
                'remarks' => 'MCO FREEZE APRIL',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];

            $transaction_created = Transaction::create($new_txn);
            // dd($value,$new_order,$transaction_created);

            $next_order = Order::where([['package_user_id',$valued->package_user_id],['monthly_payment','2020-05-01']])->first();
            if(!$next_order){

              $future_order = [
                'member_name' => $value->member_name,
                'cust_id' => $value->cust_id,
                'package_id' => $value->package_id,
                'outlet_id' => $value->outlet_id,
                'package_user_id' => $value->package_user_id,
                'package_price' => $check_subscription->price,
                'package_depo' => '0',
                'registration_fee' => '0',
                'registration_fee_status' => $value->registration_fee_status,
                'freeze_price' => '',
                'package_total' => $check_subscription->price,
                'monthly_payment' => '2020-05-01',
                'status' => '1',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
              ];

              $create_future_order = Order::create($future_order);
            }
          }
        }

        DB::commit();
        echo 'success run feb unpaid';
        /*
        |--------------------------------------------------------------------------
        | Create new order record
        | Update table Order Subscription
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $feb_paid_orders = Order::where([['status','0'],['package_id','60'],['outlet_id','24'],['monthly_payment','2020-02-01'],['package_depo','0'],['registration_fee','0']])->get();
        // dd(count($feb_paid_orders));

        foreach ($feb_paid_orders as $get_paid) {

          $feb_paid = Order::where('id',$get_paid->id)->first();

          $new_order = Order::where([['package_user_id',$feb_paid->package_user_id],['status','1'],['package_id','60'],['outlet_id','24'],['monthly_payment','2020-05-01'],['package_depo','0'],['registration_fee','0']])->first();

          if(!$new_order){
            
            $check_subscription = Package::where('id',$feb_paid->package_id)->first();

            $total_price = $feb_paid->package_price;
            $total_deduction = round($total_price/$check_subscription->subscription,1);
            $total_after_deduction = round($total_price-$total_deduction,1);
            $new_monthly_payment = '2020-05-01';

            // $feb_paid->package_price = number_format($total_after_deduction,2, '.', '');
            // $feb_paid->package_total = number_format($total_after_deduction,2, '.', '');
            // $feb_paid->save();

            $Insertorder = [
              'member_name' => $feb_paid->member_name,
              'cust_id' => $feb_paid->cust_id,
              'package_id' => $feb_paid->package_id,
              'outlet_id' => $feb_paid->outlet_id,
              'package_user_id' => $feb_paid->package_user_id,
              'package_price' => number_format($total_deduction,2, '.', ''),
              'package_depo' => '0',
              'registration_fee' => '0',
              'registration_fee_status' => $feb_paid->registration_fee_status,
              'freeze_price' => '',
              'package_total' => number_format($total_deduction,2, '.', ''),
              'monthly_payment' => $new_monthly_payment,
              'status' => '0',
              'created_at' => carbon::now(),
              'updated_at' => carbon::now(),
            ];
            
            $Neworder = [
              'member_name' => $feb_paid->member_name,
              'cust_id' => $feb_paid->cust_id,
              'package_id' => $feb_paid->package_id,
              'outlet_id' => $feb_paid->outlet_id,
              'package_user_id' => $feb_paid->package_user_id,
              'package_price' => number_format($total_after_deduction,2, '.', ''),
              'package_depo' => '0',
              'registration_fee' => '0',
              'registration_fee_status' => $feb_paid->registration_fee_status,
              'freeze_price' => '',
              'package_total' => number_format($total_after_deduction,2, '.', ''),
              'monthly_payment' => $new_monthly_payment,
              'status' => '1',
              'created_at' => carbon::now(),
              'updated_at' => carbon::now(),
            ];


          $paid_order = Order::create($Insertorder);
          $unpaid_order = Order::create($Neworder);
          // dd($paid_order,$unpaid_order);
          if($paid_order){

            $outlet_prefix = Outlet::find($paid_order->outlet_id);

            $invoice_number = DB::select("SELECT invoice_no FROM `transactions` WHERE invoice_no is NOT NULL AND outlet_id = $paid_order->outlet_id order by invoice_no desc limit 1");

            if(empty($invoice_number)){

              $inv_no = $outlet_prefix->prefix."0000000001";

            } else {

              $inv_no = $invoice_number[0]->invoice_no;
              $inv_no++;
              $str_length = 10;
              $inv_no = substr("0000000000{$inv_no}", -$str_length);
              $inv_no = $outlet_prefix->prefix.$inv_no;

            }

            $new_txn = [
                'cust_id' => $paid_order->cust_id,
                'outlet_id' => $paid_order->outlet_id,
                'cashier_id' => '1',
                'order_id' => $paid_order->id,
                'product_id' => '0',
                'voucher_id' => '50',
                'quantity' => '1',
                'amount_paid' => '0.00',
                'deposit_use' => '',
                'voucher_value' => number_format($total_deduction,2, '.', ''),
                'invoice_total' => '0.00',
                'sst_package' => '0.00',
                'payment_method' => 'Cash',
                'status' => '0',
                'only_reg_fee' => '0',
                'package_id' => $paid_order->package_id,
                'item_price' => number_format($total_deduction,2, '.', ''),
                'package_depo' => '0',
                'reg_fee' => '0',
                'invoice_no' => $inv_no,
                'remarks' => 'MCO FREEZE APRIL',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];

            $transaction_created = Transaction::create($new_txn);
          }
          // dd($feb_paid,$paid_order,$unpaid_order,$transaction_created);

          }elseif ($new_order) {
            // dd($new_order->monthly_payment);
            $check_subscription = Package::where('id',$new_order->package_id)->first();

            $total_price = $new_order->package_price;
            $total_deduction = round($total_price/$check_subscription->subscription,1);
            $total_after_deduction = round($total_price-$total_deduction,1);
            $new_monthly_payment = $new_order->monthly_payment;

            $new_order->package_price = number_format($total_deduction,2, '.', '');
            $new_order->package_total = number_format($total_deduction,2, '.', '');
            $new_order->status = '0';
            $new_order->save();
            

            $outlet_prefix = Outlet::find($new_order->outlet_id);

            $invoice_number = DB::select("SELECT invoice_no FROM `transactions` WHERE invoice_no is NOT NULL AND outlet_id = $new_order->outlet_id order by invoice_no desc limit 1");

            if(empty($invoice_number)){

              $inv_no = $outlet_prefix->prefix."0000000001";

            } else {

              $inv_no = $invoice_number[0]->invoice_no;
              $inv_no++;
              $str_length = 10;
              $inv_no = substr("0000000000{$inv_no}", -$str_length);
              $inv_no = $outlet_prefix->prefix.$inv_no;

            }
            // dd($inv_no);
            $new_txn = [
                'cust_id' => $new_order->cust_id,
                'outlet_id' => $new_order->outlet_id,
                'cashier_id' => '1',
                'order_id' => $new_order->id,
                'product_id' => '0',
                'voucher_id' => '50',
                'quantity' => '1',
                'amount_paid' => '0.00',
                'deposit_use' => '',
                'voucher_value' => number_format($total_deduction,2, '.', ''),
                'invoice_total' => '0.00',
                'sst_package' => '0.00',
                'payment_method' => 'Cash',
                'status' => '0',
                'only_reg_fee' => '0',
                'package_id' => $new_order->package_id,
                'item_price' => number_format($total_deduction,2, '.', ''),
                'package_depo' => '0',
                'reg_fee' => '0',
                'invoice_no' => $inv_no,
                'remarks' => 'MCO FREEZE APRIL',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];

            $transaction_created = Transaction::create($new_txn);

            // $classroom = ClassroomUser::where('package_user_id',$package_user->id)->first();
            // dd($classroom);
            if($transaction_created){

                $Insertorder = [
                  'member_name' => $new_order->member_name,
                  'cust_id' => $new_order->cust_id,
                  'package_id' => $new_order->package_id,
                  'outlet_id' => $new_order->outlet_id,
                  'package_user_id' => $new_order->package_user_id,
                  'package_price' => number_format($total_after_deduction,2, '.', ''),
                  'package_depo' => '0',
                  'registration_fee' => '0',
                  'registration_fee_status' => $new_order->registration_fee_status,
                  'freeze_price' => '',
                  'package_total' => number_format($total_after_deduction,2, '.', ''),
                  'monthly_payment' => $new_monthly_payment,
                  'status' => '1',
                  'created_at' => carbon::now(),
                  'updated_at' => carbon::now(),
                ];

                // dd($new_order,$new_txn, $Insertorder);
                $create_new_order = Order::create($Insertorder);
            }
          }
        }

        DB::commit();
        echo 'success run feb paid';

        /*
        |--------------------------------------------------------------------------
        | Create new order record
        | Update table Order Subscription
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $currentDate = Carbon::parse('2020-04-01')->startofMonth()->format('Y-m-d');
        // dd($currentDate);


        //dd($currentDate);
        $unpaid_orders = Order::where([['status','1'],['monthly_payment',$currentDate],['registration_fee','0'],['package_depo','0']])->get();

        // dd(count($unpaid_orders)); 
        foreach ($unpaid_orders as $mco_order) {

          $check_subscription = Package::where('id',$mco_order->package_id)->first();

          if($check_subscription->subscription == '1'){

            $outlet_prefix = Outlet::find($mco_order->outlet_id);

            $invoice_number = DB::select("SELECT invoice_no FROM `transactions` WHERE invoice_no is NOT NULL AND outlet_id = $mco_order->outlet_id order by invoice_no desc limit 1");

            if(empty($invoice_number)){

              $inv_no = $outlet_prefix->prefix."0000000001";

            } else {

              $inv_no = $invoice_number[0]->invoice_no;
              $inv_no++;
              $str_length = 10;
              $inv_no = substr("0000000000{$inv_no}", -$str_length);
              $inv_no = $outlet_prefix->prefix.$inv_no;

            }
            // dd($inv_no);
            $new_txn = [
                'cust_id' => $mco_order->cust_id,
                'outlet_id' => $mco_order->outlet_id,
                'cashier_id' => '1',
                'order_id' => $mco_order->id,
                'product_id' => '0',
                'voucher_id' => '50',
                'quantity' => '1',
                'amount_paid' => '0.00',
                'deposit_use' => '',
                'voucher_value' => $mco_order->package_price,
                'invoice_total' => '0.00',
                'sst_package' => '0.00',
                'payment_method' => 'Cash',
                'status' => '0',
                'only_reg_fee' => '0',
                'package_id' => $mco_order->package_id,
                'item_price' => $mco_order->package_price,
                'package_depo' => '0',
                'reg_fee' => '0',
                'invoice_no' => $inv_no,
                'remarks' => 'MCO FREEZE APRIL',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];

            $transaction_created = Transaction::create($new_txn);

            // $classroom = ClassroomUser::where('package_user_id',$package_user->id)->first();
            // dd($classroom);
            if($transaction_created){
                $mco_order->status = '0';
                $mco_order->save();
            }
  
          }elseif ($check_subscription->subscription == '3') {

            $get_mco_order = Order::where('id',$mco_order->id)->first();

            $total_price = $mco_order->package_price;
            $total_deduction = round($total_price/$check_subscription->subscription,1);
            $total_after_deduction = round($total_price-$total_deduction,1);
            $new_monthly_payment = $mco_order->monthly_payment;

            $get_mco_order->package_price = number_format($total_deduction,2, '.', '');
            $get_mco_order->package_total = number_format($total_deduction,2, '.', '');
            $get_mco_order->status = '0';
            $get_mco_order->save();
            

            $outlet_prefix = Outlet::find($mco_order->outlet_id);

            $invoice_number = DB::select("SELECT invoice_no FROM `transactions` WHERE invoice_no is NOT NULL AND outlet_id = $mco_order->outlet_id order by invoice_no desc limit 1");

            if(empty($invoice_number)){

              $inv_no = $outlet_prefix->prefix."0000000001";

            } else {

              $inv_no = $invoice_number[0]->invoice_no;
              $inv_no++;
              $str_length = 10;
              $inv_no = substr("0000000000{$inv_no}", -$str_length);
              $inv_no = $outlet_prefix->prefix.$inv_no;

            }
            // dd($inv_no);
            $new_txn = [
                'cust_id' => $mco_order->cust_id,
                'outlet_id' => $mco_order->outlet_id,
                'cashier_id' => '1',
                'order_id' => $mco_order->id,
                'product_id' => '0',
                'voucher_id' => '50',
                'quantity' => '1',
                'amount_paid' => '0.00',
                'deposit_use' => '',
                'voucher_value' => number_format($total_deduction,2, '.', ''),
                'invoice_total' => '0.00',
                'sst_package' => '0.00',
                'payment_method' => 'Cash',
                'status' => '0',
                'only_reg_fee' => '0',
                'package_id' => $mco_order->package_id,
                'item_price' => number_format($total_deduction,2, '.', ''),
                'package_depo' => '0',
                'reg_fee' => '0',
                'invoice_no' => $inv_no,
                'remarks' => 'MCO FREEZE APRIL',
                'created_at' => carbon::now(),
                'updated_at' => carbon::now(),
            ];

            $transaction_created = Transaction::create($new_txn);

            // $classroom = ClassroomUser::where('package_user_id',$package_user->id)->first();
            // dd($classroom);
            if($transaction_created){

                $Insertorder = [
                  'member_name' => $mco_order->member_name,
                  'cust_id' => $mco_order->cust_id,
                  'package_id' => $mco_order->package_id,
                  'outlet_id' => $mco_order->outlet_id,
                  'package_user_id' => $mco_order->package_user_id,
                  'package_price' => number_format($total_after_deduction,2, '.', ''),
                  'package_depo' => '0',
                  'registration_fee' => '0',
                  'registration_fee_status' => $mco_order->registration_fee_status,
                  'freeze_price' => '',
                  'package_total' => number_format($total_after_deduction,2, '.', ''),
                  'monthly_payment' => carbon::parse($mco_order->monthly_payment)->addMonth(1)->startofMonth(),
                  'status' => '1',
                  'created_at' => carbon::now(),
                  'updated_at' => carbon::now(),
                ];

                // dd($mco_order,$new_txn, $Insertorder);
                $new_order = Order::create($Insertorder);


                $mco_order->status = '0';
                $mco_order->save();
            }
            
          }

        }
        DB::commit();
        echo 'success run for APRIL';
    }
}

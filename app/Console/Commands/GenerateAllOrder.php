<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PackageUser;
use App\Order;
use App\User;
use App\OrderSubscription;
use Carbon\Carbon;
use DB;

class GenerateAllOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:allorder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manually generate Orders From Package Users Start Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        $id_arr = ['1002003','1002005','1002007','1002009','1002011','1002013','1002015','1002017','1002018','1002020','1002022','1002024','1002026','1002028','1002041','1002047','1002050','1002054','1002058','1002060','1002077','1002079','1002081','1002083','1002085','1002087','1002090','1002092','1002093','1002096','1002098','1002102','1002104','1002112','1002115','1002117','1002119','1002126','1002127','1002129','1002131','1002134','1002137','1002150','1002151','1002153','1002157','1002159','1002163','1002166','1002174','1002176','1002178','1002180','1002182','1002184','1002185','1002186','1002188','1002190','1002192','1002194','1002196','1002198','1002200','1002202','1002231','1002232','1002234','1002235','1002237','1002239','1002241','1002243','1002244','1002245','1002246','1002248','1002249','1002250','1002252','1002254','1002255','1002256','1002257','1002259','1002261','1002264','1002266','1002319','1002321','1002322','1002324','1002326','1002328','1002330','1002332','1002334','1002335','1002337','1002340','1002341','1002342','1002343','1002345','1002346','1002348','1002350','1002352','1002353','1002356','1002358','1002359','1002361','1002362','1002364','1002366','1002367','1002368','1002370','1002372','1002373','1002375','1002376','1002378','1002379','1002380','1002382','1002383','1002385','1002386','1002388','1002390','1002392','1002394','1002397','1002398','1002400','1002401','1002404','1002406','1002408','1002410','1002412','1002413','1002416','1002418','1002420','1002421','1002422','1002423','1002438','1002440','1002442','1002454','1002456','1002464','1002466','1002468','1002469','1002471','1002473','1002475','1002476','1002477','1002479','1002481','1002483','1002484','1002485','1002487','1002488','1002490','1002492','1002494','1002496','1002499','1002501','1002503','1002505','1002507','1002509','1002510','1002511','1002512','1002514','1002515','1002518','1002520','1002521','1002523','1002524','1002526','1002528','1002531','1002533','1002535','1002538','1002540','1002542','1002544'];
        $package_users = PackageUser::with('packages','user')->whereIn('user_id',$id_arr)->get();
        
        $fat = [];
        foreach ($package_users as $key => $pu) {

            if($pu->user->user_id == ''){
                dd($pu->user->name);
            }
            if ($pu->status == 'active') {
                
                $current_date = Carbon::now();
                $start_date = new Carbon($pu->start_date);
                $expiry = new Carbon($pu->expiry);
                $diffInMonths = $start_date->diffInMonths($current_date, false) + 1;
                $countOrder = ceil($diffInMonths / $pu->packages->subscription);
                $index = $countOrder;

                for ($i=1; $i <= $index ; $i++) {

                    $latestOrder = Order::where('package_user_id', $pu->id)->orderByDesc('monthly_payment')->first();
                    

                    if ($latestOrder) {
                        //dd('hi');
                        $getSingleOrder = Order::where('package_user_id', $pu->id)->latest()->first();
                        $newExpiry = new Carbon($latestOrder->monthly_payment);
                        $insertOrder = [
                            'package_user_id'         => $getSingleOrder->package_user_id,
                            'package_price'           => $getSingleOrder->package_price,
                            'package_depo'            => '0',
                            'registration_fee'        => '0',
                            'registration_fee_status' => '1',
                            'package_total'           => $getSingleOrder->package_price,
                            'status'                  => '1',
                            'member_name'             => $getSingleOrder->member_name,
                            'cust_id'                 => $getSingleOrder->cust_id,
                            'package_id'              => $getSingleOrder->package_id,
                            'outlet_id'               => $getSingleOrder->outlet_id,
                            'monthly_payment'         => $newExpiry->addMonths($pu->packages->subscription)
                        ];

                        $orders = Order::create($insertOrder);
                        if (!$orders) {
                            DB::rollback();
                        }

                        $sub_month = carbon::parse($orders->monthly_payment)->addMonth(1);

                        $pu->expiry = $sub_month;;
                        $pu->save();
                        if (!$pu) {
                            DB::rollback();
                        }

                        $condition = [
                            'package_user_id'   => $pu->id
                        ];

                        $insert = [
                            'order_id'  => $orders->id,
                            'month'     => $sub_month->month,
                            'year'      => $sub_month->year,
                        ];

                        $orderSubscsription = OrderSubscription::updateOrCreate($condition, $insert);
                        if (!$orderSubscsription) {
                            DB::rollback();
                        }
                    }else{

                        $insertOrder = [
                            'package_user_id'   => $pu->id,
                            'package_price'     => $pu->packages->price,
                            'package_depo'      => $pu->packages->deposit,
                            'registration_fee'  => $pu->packages->registration_fee,
                            'registration_fee_status' => '1',
                            'package_total'     => $pu->packages->price+$pu->packages->deposit+$pu->packages->registration_fee,
                            'status'            => '0',
                            'member_name'       => $pu->user->name,
                            'cust_id'           => $pu->user->user_id,
                            'package_id'        => $pu->package_id,
                            'outlet_id'         => $pu->outlet_id,
                            'monthly_payment'   => $start_date->startOfMonth(),
                        ];

                        $orders = Order::create($insertOrder);
                        if (!$orders) {
                            DB::rollback();
                        }

                        $sub_month = carbon::parse($orders->monthly_payment)->addMonth(1);

                        $pu->expiry = $sub_month;
                        $pu->save();
                        if (!$pu) {
                            DB::rollback();
                        }

                        $condition = [
                            'package_user_id'   => $pu->id
                        ];

                        $insert = [
                            'order_id'  => $orders->id,
                            'month'     => $sub_month->month,
                            'year'      => $sub_month->year,
                        ];

                        $orderSubscsription = OrderSubscription::updateOrCreate($condition, $insert);
                        if (!$orderSubscsription) {
                            DB::rollback();
                        }
                    }
                }
            }
        }
        DB::commit();

        echo 'success run';
    }
}
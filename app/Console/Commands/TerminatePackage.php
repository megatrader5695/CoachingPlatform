<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Package;
use App\PackageUser;
use App\Attendance;
use App\Order;
use App\ClassroomUser;
use App\User;
use Carbon\Carbon;
use DB;

class TerminatePackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'terminate:package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Terminate package for private package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |------------------------------------------------------------------------------------
        | Auto terminate
        |------------------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $classroom_users = ClassroomUser::all();

        foreach ($classroom_users as $classroom_user) {
            $count = Attendance::where([
                ['user_id', $classroom_user->user_id],
                ['package_id', $classroom_user->package_id],
                ['class_id', $classroom_user->classroom_id],
                ['status', 'present']
            ])
            ->count();

            if (isset($classroom_user->package_id)) {

                $package = Package::find($classroom_user->package_id);
                $packageUsers = PackageUser::find($classroom_user->package_user_id);

                $date = Carbon::now()->startOfMonth();
                
                if($packageUsers){
                    // dd();
                    if($count >= $package->access){
                        if ($package->type === 'Private') {

                            $user = User::find($classroom_user->user_id);
                            $package_user =  $user->packages()->where('package_user.id', $classroom_user->package_user_id)->first();
                            $package_user->pivot->status = 'terminate';
                            $package_user->pivot->save();

                            $classroom_user->delete();

                            // DB::table('classroom_user')->where([
                            //     ['id', $classroom_user->id]
                            // ])->update([
                            //     'status'        => 'inactive',
                            // ]);
                        }
                    }

                    if ($packageUsers->expiry == $date->format('Y-m-d')) {

                        if ($package->type === 'Private') {

                            $user = User::find($classroom_user->user_id);

                            $package_user =  $user->packages()->where('package_user.id', $classroom_user->package_user_id)->first();
                            $package_user->pivot->status = 'terminate';
                            $package_user->pivot->save();

                            $classroom_user->delete();

                            // DB::table('classroom_user')->where([
                            //     ['id', $classroom_user->id]
                            // ])->update([
                            //     'status'        => 'inactive',
                            // ]);
                        }

                    }
                }  

            }
        }
        // dd($test);
        DB::commit();
        echo 'success run';
    }
}

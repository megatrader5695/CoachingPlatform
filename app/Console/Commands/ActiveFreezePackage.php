<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Package;
use App\PackageUser;
use App\Attendance;
use App\Order;
use App\OrderSubscription;
use App\ClassroomUser;
use App\User;
use App\UserChangePackage;
use Carbon\Carbon;
use DB;

class ActiveFreezePackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activate:freeze';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto active freeze package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |------------------------------------------------------------------------------------
        | Auto terminate
        |------------------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $currentMonth = Carbon::now();

        $minusOneMonth = carbon::parse($currentMonth)->subMonths(1);

        $userPackageStatusFreezes = UserChangePackage::where('status','freeze')
                                    ->where('end_date',$minusOneMonth->endOfMonth()->format('Y-m-d'))
                                    ->get();

        // Freeze to Active where status is freeze (end of freeze)
        foreach ($userPackageStatusFreezes as $userPackageStatusFreeze) {
            $packageUser = PackageUser::find($userPackageStatusFreeze->package_user_id);

            $packageUser->status = 'active';
            $packageUser->updated_at = Carbon::now();
            $packageUser->save();
        }
        

        // Freeze to Active where status is Active (manual change freeze to active)
        $userPackageStatusActives = UserChangePackage::where('status','active')
                                    ->where('start_date',$currentMonth->startOfMonth()->format('Y-m-d'))
                                    ->get();

        foreach ($userPackageStatusActives as $userPackageStatusActive) {
            $packageUser = PackageUser::find($userPackageStatusActive->package_user_id);

            $packageUser->status = 'active';
            $packageUser->updated_at = Carbon::now();
            $packageUser->save();
        }


        // Active to Freeze where status is Freeze (the freeze date is in future)
        $userPackageStatusToFreezes = UserChangePackage::where('status','freeze')
                                    ->where('start_date',$currentMonth->startOfMonth()->format('Y-m-d'))
                                    ->get();

        foreach ($userPackageStatusToFreezes as $userPackageStatusToFreeze) {
            $packageUser = PackageUser::find($userPackageStatusToFreeze->package_user_id);

            $packageUser->status = 'freeze';
            $packageUser->updated_at = Carbon::now();
            $packageUser->save();
        }
        
        
        DB::commit();
        echo 'success run';
    }
}

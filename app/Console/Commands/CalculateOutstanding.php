<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Order;
use App\User;
use App\PackageUser;

class CalculateOutstanding extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calculate:outstanding';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate outstanding';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | update table package_user
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $reset_outstanding = PackageUser::query()->update(['outstanding' => '0.00']);
        // dd($reset_outstanding);
        $customers = User::where('role_id', 5)->get();      // get all customer
        // $customers = User::where('id','1000677')->get();      // get all customer
        
        $outstandings = [];
        foreach ($customers as $customer) {
            // dd($customer->id);
            $pending = Order::where('cust_id', $customer->id)
                    ->where('status', '1')
                    ->select(DB::raw('SUM(package_total) as total, package_user_id'))
                    ->groupBy('package_user_id')
                    ->get();

            $package_pending= Order::where('cust_id', $customer->id)
                    ->where('status', '2')
                    ->select(DB::raw('SUM(package_total - registration_fee) as total, package_user_id'))
                    ->groupBy('package_user_id')
                    ->get();


            $registration_pending  = Order::where('cust_id', $customer->id)
                    ->where('status', '3')
                    ->select(DB::raw('SUM(package_total - package_price) as total, package_user_id'))
                    ->groupBy('package_user_id')
                    ->get();

            if (!$pending->isEmpty()) {
                array_push($outstandings, $pending);      //push when data is exist
            }

            if (!$package_pending->isEmpty()){
                array_push($outstandings, $package_pending);      //push when data is exist
            }

            if (!$registration_pending->isEmpty()){
                array_push($outstandings, $registration_pending);      //push when data is exist
            }
                
        }

        //dd($outstandings->count());
        foreach ($outstandings as $outstanding) {

            foreach ($outstanding as $value) {

                $package_user = PackageUser::where('id', $value->package_user_id)->first();
                $package_user->outstanding = '0.00';
                $package_user->save();

                if (!$package_user) {
                    DB::rollback();
                }
            }
        }

        foreach ($outstandings as $outstanding) {
            // dd($outstanding->cust_id);
            foreach ($outstanding as $value) {
                
                $package_user = PackageUser::where('id', $value->package_user_id)->first();
                // dd($package_user->outstanding);
                $sum = $package_user->outstanding + $value->total;

                $package_user->outstanding = $sum;
                $package_user->save();

                if (!$package_user) {
                    DB::rollback();
                }
            }
        }

        DB::commit();
        echo 'success run';
    }
}

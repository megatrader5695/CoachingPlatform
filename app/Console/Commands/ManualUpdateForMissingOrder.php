<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Order;
use App\User;
use App\PackageUser;
use App\Package;
use Carbon\Carbon;

class ManualUpdateForMissingOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:missingOrder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Missing Order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | update table package_user
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();
        
        $getUsers1 = PackageUser::where('join_date','2019-08')
                    ->where('start_date','2019-08')
                    ->get();

        foreach ($getUsers1 as $packageUser) {

            $parent_id = User::where('id',$packageUser->user_id)->value('user_id');

            $memberName = User::where('id',$packageUser->user_id)->value('name');

            //check existing order

            if (!empty($parent_id)) {

                $orders = Order::where('member_name',$memberName)
                      ->where('cust_id',$parent_id)
                      ->where('package_user_id',$packageUser->id)
                      ->get();

                $addMonths = -1;

                if($orders->isEmpty()) {

                    $packageSubscription = Package::where('id',$packageUser->package_id)->value('subscription');

                    $packagePrice = Package::where('id',$packageUser->package_id)->value('price');

                    $packageDeposit = Package::where('id',$packageUser->package_id)->value('deposit');

                    $packageTotal = $packagePrice + $packageDeposit;

                    $addMonths = $addMonths + $packageSubscription;

                    $monthly_payment = new Carbon($packageUser->start_date);

                    $createOrder = new Order;

                    $createOrder->member_name = $memberName;
                    $createOrder->cust_id = $parent_id;
                    $createOrder->package_id = $packageUser->package_id;
                    $createOrder->outlet_id = $packageUser->outlet_id;
                    $createOrder->package_user_id = $packageUser->id;
                    $createOrder->package_price = $packagePrice;
                    $createOrder->package_depo = $packageDeposit;
                    $createOrder->registration_fee = 0;
                    $createOrder->package_total = $packageTotal;
                    $createOrder->monthly_payment = $monthly_payment;
                    // $createOrder->monthly_payment = $monthly_payment->addMonths($addMonths);
                    $createOrder->status = '1';
                    $createOrder->created_at = Carbon::now();
                    $createOrder->updated_at = Carbon::now();

                    $createOrder->save();

                }

            }

            
        }

        $getUsers2 = PackageUser::where('join_date','2019-10')
                    ->where('start_date','2019-10')
                    ->get();

        foreach ($getUsers2 as $packageUser) {

            $parent_id = User::where('id',$packageUser->user_id)->value('user_id');

            $memberName = User::where('id',$packageUser->user_id)->value('name');

            //check existing order

            if (!empty($parent_id)) {

                $orders = Order::where('member_name',$memberName)
                      ->where('cust_id',$parent_id)
                      ->where('package_user_id',$packageUser->id)
                      ->get();

                $addMonths = -1;

                if($orders->isEmpty()) {

                    $packageSubscription = Package::where('id',$packageUser->package_id)->value('subscription');

                    $packagePrice = Package::where('id',$packageUser->package_id)->value('price');

                    $packageDeposit = Package::where('id',$packageUser->package_id)->value('deposit');

                    $packageTotal = $packagePrice + $packageDeposit;

                    $addMonths = $addMonths + $packageSubscription;

                    $monthly_payment = new Carbon($packageUser->start_date);

                    $createOrder = new Order;

                    $createOrder->member_name = $memberName;
                    $createOrder->cust_id = $parent_id;
                    $createOrder->package_id = $packageUser->package_id;
                    $createOrder->outlet_id = $packageUser->outlet_id;
                    $createOrder->package_user_id = $packageUser->id;
                    $createOrder->package_price = $packagePrice;
                    $createOrder->package_depo = $packageDeposit;
                    $createOrder->registration_fee = 0;
                    $createOrder->package_total = $packageTotal;
                    $createOrder->monthly_payment = $monthly_payment;
                    // $createOrder->monthly_payment = $monthly_payment->addMonths($addMonths);
                    $createOrder->status = '1';
                    $createOrder->created_at = Carbon::now();
                    $createOrder->updated_at = Carbon::now();

                    $createOrder->save();

                }

            }

            
        }

        DB::commit();
    }
}

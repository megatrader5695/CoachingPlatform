<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Package;
use App\PackageUser;
use App\Attendance;
use App\Order;
use App\ClassroomUser;
use App\Classroom;
use App\User;
use Carbon\Carbon;
use DB;
use stdClass;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class GetClockInDeviceApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getClockInDevice:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get getClockInDevice:api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |------------------------------------------------------------------------------------
        | Auto terminate
        |------------------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $startOfDay = Carbon::now()->startOfDay();
        $endOfDay = Carbon::today()->endOfDay();

        // echo $startOfDay;

        $attendances = Attendance::whereBetween('clock_in_start',[$startOfDay,$endOfDay])->get();

        foreach ($attendances as $attendance) {

            if ($status != 200 || $status == null) {

                echo 'Device offline';

            } else {


            }
        }
        
        DB::commit();
    }
}

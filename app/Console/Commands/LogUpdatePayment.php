<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Package;
use App\OrderSubscription;
use App\Order;
use App\UserChangePackage;
use Carbon\Carbon;
use DB;

class LogUpdatePayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:updatepayment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | Create new order record
        | Update table Order Subscription
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();
        $currentDate = Carbon::now();
        // $upgrade_package = ['107','108','109'];
        $upgrade_package = [];
        $get_upgrade_package = Package::where('auto_upgrade','1')->get();
        

        foreach ($get_upgrade_package as $gup) {
            array_push($upgrade_package, $gup->id);
        }
        // dd($upgrade_package);


        //dd($currentDate);
        $package_users = DB::table('package_user')->get();
        foreach ($package_users as $package_user) {
            $package = Package::find($package_user->package_id);
            $user = User::find($package_user->user_id);

            // dd($package_user,$upgrade_package,!in_array($package->id, $upgrade_package));
            if ($user && !in_array($package->id, $upgrade_package)) {
                $insertOrder = [
                    'package_user_id'   => $package_user->id,
                    'package_price'     => $package->price,
                    'package_depo'      => '0',
                    'package_total'     => $package->price,
                    'registration_fee_status' => '1',
                    'status'            => '1',
                    'member_name'       => $user->name,
                    'cust_id'           => $user->user_id,
                    'package_id'        => $package->id,
                    'outlet_id'         => $user->outlet_id,
                ];

                // CHECK EXISTING DATA HAS PACKAGE_USER_ID IN TABLE ORDER OR NOT
                $orderSubscription = OrderSubscription::where('package_user_id', $package_user->id)->first();
                
                if ($currentDate->day == 25 && $package->type == 'Standard' && $package_user->status == 'active') { //
                    if (is_null($orderSubscription)) {

                        $order_month = Carbon::now()->addMonths(1)->startOfMonth()->format('Y-m-d');
                        $check_orders = Order::where([['package_user_id', $package_user->id],['monthly_payment', $order_month]])->first();
                        // dd($order_month);
                        if(!$check_orders){

                            $value = [
                                'monthly_payment' => $order_month
                            ];

                            $insertOrder = array_merge($insertOrder, $value);

                            $orders = Order::create($insertOrder);
                        }

                        $date = Carbon::now()->addMonths($package->subscription)->startOfMonth();
                        $month  = $date->month;
                        $year  = $date->year;
                        $latest_order = Order::where('package_user_id', $package_user->id)->get()->last();
                        // dd($latest_order);

                        $insertOrderSubscription = [
                            'order_id'     => $latest_order->id,
                            'package_user_id'   => $latest_order->package_user_id,
                            'month'             => $month,
                            'year'              => $year
                        ];

                        $orderSubscription = OrderSubscription::create($insertOrderSubscription);
                        if (!$orderSubscription) {
                            DB::rollback();
                        }
                        
                    }else{

                        if ($orderSubscription->month == $currentDate->month) {

                            $order_month = Carbon::create($orderSubscription->year, $orderSubscription->month, 1)->addMonths(1)->format('Y-m-d');
                            $check_orders = Order::where([['package_user_id', $package_user->id],['monthly_payment', $order_month]])->first();
                            // dd($order_month);

                            if(!$check_orders){

                                $value = [
                                    'monthly_payment' => $order_month
                                ];

                                $insertOrder = array_merge($insertOrder, $value);

                                $orders = Order::create($insertOrder);
                            }

                            $date = Carbon::create($orderSubscription->year, $orderSubscription->month, 1)->addMonths($package->subscription);
                            $month  = $date->month;
                            $year  = $date->year;
                            $latest_order = Order::where('package_user_id', $package_user->id)->get()->last();
                            //dd($latest_order);
                            $insertOrderSubscription = [
                                'order_id'     => $latest_order->id,
                                'package_user_id'   => $latest_order->package_user_id,
                                'month'             => $month,
                                'year'              => $year
                            ];

                            $orderSubscription = OrderSubscription::where('package_user_id', $latest_order->package_user_id)->update($insertOrderSubscription);
                            if (!$orderSubscription) {
                                DB::rollback();
                            }
                        }
                    }
                }
                
            }elseif ($user && in_array($package->id, $upgrade_package)) {
                // dd($package_user,$upgrade_package,in_array($package->id, $upgrade_package));
                $orderSubscription = OrderSubscription::where('package_user_id', $package_user->id)->first();
                
                if ($currentDate->day == 17 && $package->type == 'Standard' && $package_user->status == 'active') { //

                    $new_package = Package::where('id', $package->upgrade_to)->first();

                    $upgradedOrder = [
                        'package_user_id'   => $package_user->id,
                        'package_price'     => $new_package->price,
                        'package_depo'      => '0',
                        'package_total'     => $new_package->price,
                        'registration_fee_status' => '1',
                        'status'            => '1',
                        'member_name'       => $user->name,
                        'cust_id'           => $user->user_id,
                        'package_id'        => $new_package->id,
                        'outlet_id'         => $user->outlet_id,
                    ];
                    
                    if (is_null($orderSubscription)) {
                        
                        $order_month = Carbon::now()->addMonths(1)->startOfMonth()->format('Y-m-d');
                        $check_orders = Order::where([['package_user_id', $package_user->id],['monthly_payment', $order_month]])->first();
                        // dd($order_month);
                        if(!$check_orders){

                            $value = [
                                'monthly_payment' => $order_month
                            ];

                            $upgradedOrder = array_merge($upgradedOrder, $value);
                            
                            $orders = Order::create($upgradedOrder);
                        }

                        $date = Carbon::now()->addMonths($package->subscription)->startOfMonth();
                        $month  = $date->month;
                        $year  = $date->year;
                        $latest_order = Order::where('package_user_id', $package_user->id)->get()->last();
                        // dd($latest_order);

                        $insertOrderSubscription = [
                            'order_id'     => $latest_order->id,
                            'package_user_id'   => $latest_order->package_user_id,
                            'month'             => $month,
                            'year'              => $year
                        ];

                        $orderSubscription = OrderSubscription::create($insertOrderSubscription);
                        if (!$orderSubscription) {
                            DB::rollback();
                        }
                        
                    }else{

                        if ($orderSubscription->month == $currentDate->month) {

                            $order_month = Carbon::create($orderSubscription->year, $orderSubscription->month, 1)->addMonths(1)->format('Y-m-d');
                            $check_orders = Order::where([['package_user_id', $package_user->id],['monthly_payment', $order_month]])->first();
                            // dd($order_month);

                            if(!$check_orders){

                                $value = [
                                    'monthly_payment' => $order_month
                                ];

                                $upgradedOrder = array_merge($upgradedOrder, $value);
                                
                                $orders = Order::create($upgradedOrder);
                            }

                            $date = Carbon::create($orderSubscription->year, $orderSubscription->month, 1)->addMonths($package->subscription);
                            $month  = $date->month;
                            $year  = $date->year;
                            $latest_order = Order::where('package_user_id', $package_user->id)->get()->last();
                            //dd($latest_order);
                            $insertOrderSubscription = [
                                'order_id'     => $latest_order->id,
                                'package_user_id'   => $latest_order->package_user_id,
                                'month'             => $month,
                                'year'              => $year
                            ];
                            // dd('stop',$insertOrderSubscription);
                            $orderSubscription = OrderSubscription::where('package_user_id', $latest_order->package_user_id)->update($insertOrderSubscription);
                            if (!$orderSubscription) {
                                DB::rollback();
                            }
                        }
                    }
                }
            }
        }
        DB::commit();
        echo'job completed';

        /*
        |--------------------------------------------------------------------------
        | Remove Package_user and classroom_user for member subscribe private package
        |--------------------------------------------------------------------------
        */
        
        $classroom_users = DB::table('classroom_user')->get();
        $array = [];
        foreach ($classroom_users as $classroom_user) {
            $count = \App\Attendance::where([
                ['user_id', $classroom_user->user_id],
                ['package_id', $classroom_user->package_id],
                ['class_id', $classroom_user->classroom_id]
            ])
            ->count();

            if (isset($classroom_user->package_id)) {
                $package = \App\Package::find($classroom_user->package_id);

                if ($package->type === 'Private' && $count == $package->access) {

                    $user = \App\User::find($classroom_user->user_id);

                    $package_user =  $user->packages()->where('package_user.id', $classroom_user->package_user_id)->first();
                    $package_user->pivot->status = 'terminate';
                    $package_user->pivot->save();

                    DB::table('classroom_user')->where([
                        ['id', $classroom_user->id]
                    ])->update([
                        'status'        => 'inactive',
                    ]);
                }
            }
        }
    }
}

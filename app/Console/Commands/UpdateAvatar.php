<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use DB;

class UpdateAvatar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:avatar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update avatar from ZKTeco manually for existing user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |------------------------------------------------------------------------------------
        | Auto terminate
        |------------------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $users = User::all();

        foreach ($users as $user) {
            if($user->role_id == '7' || $user->role_id == '6'){
                $user->avatar = 'http://vgoaquatic.asuscomm.com:8098/upload/pers/user/cropface/'.$user->id.'/'.$user->id.'.jpg';
                $user->save();
            }
            
        }
        // dd($test);
        DB::commit();
        echo 'success run';
    }
}

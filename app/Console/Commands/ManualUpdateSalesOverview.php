<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PackageUser;
use App\Package;
use App\DashboardDetails;
use App\Order;
use App\Outlet;
use App\OrderSubscription;
use App\Inventory;
use App\Transaction;
use App\SalesOverview;
use App\Voucher;
use App\Category;
use Carbon\Carbon;
use DB;

class ManualUpdateSalesOverview extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:ManualUpdateSalesOverview {month}, {year}, {outlet}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update daily year to date sales overview';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //arguments passed by controller and set it as global variable
        $month = $this->argument('month');
        $year = $this->argument('year');
        $outletId = $this->argument('outlet');
        // dd($month,$year,$outletId);

        //===================== Code to Create item according to existance ====================// 

        DB::beginTransaction();

        $current = Carbon::now()->subDays(1);
        $year = Carbon::now()->subDays(1)->format('Y');
        $outlet = Outlet::where('id',$outletId)->first();
        // dd($outlets);
        // SalesOverview::query()->truncate();

        $packages = Package::whereHas('outlets', function($q) use ($outlet){
                                $q->where('outlet_id', '=', $outlet->id);
                            })->where([['status','active'],['category','SWIMMING']])->get();    

        foreach ($packages as $key => $package) {

            $existance = SalesOverview::where([['item',$package->name],['outlet_id',$outlet->id],['year',$year]])->get()->count();

            if($existance == '0'){

                for($i = 1; $i < 13; $i++) {

                    $start_month = Carbon::create()->month($i)->startOfMonth();

                    $insert = [
                        'outlet_id' => $outlet->id,
                        'category' => strtoupper($package->category.' Sales'),
                        'payment_type' => $package->payment_type,
                        'item' => $package->name,
                        'month' => strtoupper($start_month->format('F')),
                        'year' => $year,
                        'total_sales' => '0',
                        'created_at' => $current,
                        'updated_at' => $current
                    ];

                    $SalesOverviewData = SalesOverview::updateOrCreate($insert);
                    if (!$SalesOverviewData) {
                        DB::rollback();
                    }
                }
            }
        }

        $categories = Category::all();

        foreach ($categories as $key => $category) {
                  
            if($category->id == '8'){
                // $inventories_arr = ['FLEXI GYM 5','FLEXI GYM 8','FLEXI GYM UNLIMITED 1 MONTH','FLEXI GYM UNLIMITED 3 MONTHS','FLEXI GYM UNLIMITED 6 MONTHS','GYM Walk-In'];
                $inventories = Inventory::where('category', $category->id)->get();

                foreach ($inventories as $key => $product) {

                    $existance = SalesOverview::where([['item',$product->product_name],['outlet_id',$outlet->id],['year',$year]])->get()->count();

                    if($existance == '0' && $outlet->id == '24'){

                        for($i = 1; $i < 13; $i++) {

                            $start_month = Carbon::create()->month($i)->startOfMonth();

                            $insert = [
                                'outlet_id' => $outlet->id,
                                'category' => 'GYM SALES',
                                'payment_type' => '-',
                                'item' => $product->product_name,
                                'month' => strtoupper($start_month->format('F')),
                                'year' => $year,
                                'total_sales' => '0',
                                'created_at' => $current,
                                'updated_at' => $current
                            ];

                            $SalesOverviewData = SalesOverview::updateOrCreate($insert);
                            if (!$SalesOverviewData) {
                                DB::rollback();
                            }
                        }
                    } 
                }
            }else if($category->id == '10' || $category->id == '12'){
                if($outlet->id == '24'){
                    $sales_category = 'SCUBA SALES';
                    $payment_type = '-';
                }
            }else if($category->id == '9' || $category->id == '7' || $category->id == '14' || $category->id == '15'){
                // if($outlet->id == '24'){
                    $sales_category = 'SWIMMING SALES';
                    $payment_type = 'Exam & Miscellaneous Fee';
                // }
            }else{
                $sales_category = 'POINT-OF-SALES';
                $payment_type = '-';
            }

            if($category->id != '8'){

                $existance = SalesOverview::where([['item',$category->name],['outlet_id',$outlet->id],['year',$year]])->get()->count();

                if($existance == '0'){

                    for($i = 1; $i < 13; $i++) {

                        $start_month = Carbon::create()->month($i)->startOfMonth();

                        $insert = [
                            'outlet_id' => $outlet->id,
                            'category' => $sales_category,
                            'payment_type' => $payment_type,
                            'item' => $category->name,
                            'month' => strtoupper($start_month->format('F')),
                            'year' => $year,
                            'total_sales' => '0',
                            'created_at' => $current,
                            'updated_at' => $current
                        ];

                        $SalesOverviewData = SalesOverview::updateOrCreate($insert);
                        if (!$SalesOverviewData) {
                            DB::rollback();
                        }
                    }
                }
            }

            $discount_existance = SalesOverview::where([['item','voucher_total'],['outlet_id',$outlet->id],['year',$year]])->get()->count();

            if($discount_existance == '0'){

                for($i = 1; $i < 13; $i++) {

                    $start_month = Carbon::create()->month($i)->startOfMonth();

                    $insert = [
                        'outlet_id' => $outlet->id,
                        'category' => 'Total Discount',
                        'payment_type' => '-',
                        'item' => 'voucher_total',
                        'month' => strtoupper($start_month->format('F')),
                        'year' => $year,
                        'total_sales' => '0',
                        'created_at' => $current,
                        'updated_at' => $current
                    ];

                    $SalesOverviewData = SalesOverview::updateOrCreate($insert);
                    if (!$SalesOverviewData) {
                        DB::rollback();
                    }
                }
            }
        }
        DB::commit();

        //===================== Code to insert Data according to item on the table ====================// 

        DB::beginTransaction();
        
        $num_padded = sprintf("%02d", $month);
        $setdate = $year.'-'.$num_padded.'-01';
        // dd($setdate);
        $today = Carbon::parse($setdate)->startOfMonth();
        $get_month = strtoupper($today->format('F'));
        $get_year = $today->format('Y');
        
        $get_this_month = SalesOverview::where([['outlet_id',$outletId],['month', $get_month],['year', $get_year]])->get();
        
        foreach ($get_this_month as $key => $val) {
            $val->total_sales = '0';
            $val->save();
            // dd($val);
        }

        $start_date = $today;
        $end_date = Carbon::parse($today)->endOfMonth();
        

        $categories = Category::all();
        $categories_arr = [];
        
        foreach ($categories as $key => $cats) {
            $categories_arr[$cats->id] = $cats->name; 
        }
                
        $invoices = Transaction::with('order')->where([['outlet_id', $outletId],['status', 0]])
                ->whereBetween('created_at', array($start_date, $end_date))->select('invoice_no')->groupBy('invoice_no')->get();
            
        //loop by invoice number
        $total_voucher = 0;
        foreach ($invoices as $key => $invoice) {

            $voucher_value = DB::table('transactions')
            ->where('invoice_no',$invoice->invoice_no)
            ->pluck('voucher_value');

            if($voucher_value != null && $voucher_value != '0') {
                // dd($voucher_value[0]);
                $total_voucher += number_format($voucher_value[0], 2);
            }

            $products = DB::table('transactions')
            ->join('inventories', 'transactions.product_id', '=', 'inventories.id')
            ->select('inventories.product_name', 'inventories.price','transactions.quantity','transactions.product_id','transactions.status')
            ->where('transactions.invoice_no','=',$invoice->invoice_no)
            ->get();
            
            if($products == []) {
                $products = null;
            }

            $products_calculations = DB::table('transactions')
                    ->join('inventories', 'transactions.product_id', '=', 'inventories.id')
                    ->select('inventories.product_name','inventories.category', 'transactions.item_price','transactions.quantity','transactions.status','transactions.voucher_value','transactions.voucher_id')
                    ->where('transactions.status','=','0')
                    ->where('transactions.invoice_no','=',$invoice->invoice_no)
                    ->get();

            if($products_calculations == []) {
                $products_calculations = null;
            }
            $invoice_products = $products_calculations->count();

            $package_all = DB::table('transactions')
                        ->join('orders','transactions.order_id','=','orders.id')
                        ->join('packages','orders.package_id','=','packages.id')
                        ->select('packages.name','orders.package_price','orders.package_depo','transactions.order_id','transactions.status','orders.member_name','orders.registration_fee','orders.status as order_status','orders.registration_fee_status','transactions.only_reg_fee','transactions.package_id','transactions.voucher_value','transactions.voucher_id','transactions.deposit_use')
                        ->where('invoice_no','=',$invoice->invoice_no)
                        ->get();

            if($package_all == []) {
                $package_all = null;
            }

            $total_discount = 0;
            $total_prods = 0;
            $total_orders = 0;
            $total_orders_for_voucher = 0;
            $total_order_deposit = 0;
            $use_depo_invoice_total_new = 0;
            
            if ($package_all != null) {
                foreach($package_all as $package) {

                    $getpackage = Package::where('id',$package->package_id)->first();
                    $month = strtoupper($start_date->format('F'));
                    $total_sales = SalesOverview::where([['outlet_id', $outlet->id],['item', $getpackage->name],['month', $month],['year', $year]])->first();
                    $reg_sales = SalesOverview::where([['outlet_id', $outlet->id],['item', 'REGISTRATION FEE'],['month', $month],['year', $year]])->first();

                    if($package->voucher_id != null){
                        $voucher =Voucher::where('id', $package->voucher_id)->first();

                        if($voucher->discountType == 'percentage'){
                            $discounted = number_format(($package->item_price/100)*$voucher->discountValue,2);
                        }else{
                            $discounted = number_format($package->voucher_value,2);
                        }
                        // $discounted = number_format($package->voucher_value,2); 
                    }else{
                        $discounted = 0;
                    }

                    if($package->deposit_use != null){
                        $use_deposit = number_format($package->deposit_use,2); 
                    }else{
                        $use_deposit = 0;
                    }
                    
                    // Without registration_fee
                    if($package->registration_fee_status == 1 && $package->only_reg_fee == 0 && $package->order_status == 0) {
                        
                        if(!empty($total_sales)){
                            
                            $sum = $package->package_price + $package->package_depo;
                            $sum -= $discounted + $use_deposit;
                            $total_sales->total_sales += number_format($sum,2);
                            $total_sales->save();
                            
                        } 
                    }
                    // Without registration_fee

                    // Only registration_fee (Before)
                    elseif($package->registration_fee_status == 1 && $package->only_reg_fee == 1 && $package->order_status == 2) {
                        
                        if(!empty($reg_sales)){
                            
                            $sum = $package->registration_fee;
                            $sum -= $discounted + $use_deposit;
                            $reg_sales->total_sales += number_format($sum,2);
                            $reg_sales->save();
                            
                        } 
                    }
                    // Only registration_fee (Before)

                    // Only package
                    elseif($package->registration_fee_status == 0 && $package->order_status == 3) {
                        
                        if(!empty($total_sales)){
                            
                            $sum = $package->package_price + $package->package_depo;
                            $sum -= $discounted + $use_deposit;
                            $total_sales->total_sales += number_format($sum,2);
                            $total_sales->save();
                            
                        }
                    }
                    // Only package

                    // Only registration_fee (After)
                    elseif($package->registration_fee_status == 1 && $package->only_reg_fee == 1 && $package->order_status == 0) {
                        
                        if(!empty($reg_sales)){
                            
                            $sum = $package->registration_fee;
                            $sum -= $discounted + $use_deposit;
                            $reg_sales->total_sales += number_format($sum,2);
                            $reg_sales->save();
                            
                        }
                    }
                    // Only registration_fee (After)

                    // Full Pay
                    elseif($package->registration_fee_status == 1 && $package->only_reg_fee == 2 && $package->order_status == 0) {
                        
                        if(!empty($total_sales)){
                            
                            $sum = $package->package_price + $package->package_depo;
                            $sum -= $discounted + $use_deposit;
                            $total_sales->total_sales += number_format($sum,2);
                            $total_sales->save();
                            
                        }

                        if(!empty($reg_sales)){
                            
                            $sum = $package->registration_fee;
                            // $sum -= $discounted + $use_deposit;
                            $reg_sales->total_sales += number_format($sum,2);
                            $reg_sales->save();
                            
                        }
                    }
                    // Full Pay
                }
            }


            if ($products_calculations != null) {
                foreach($products_calculations as $product) {

                    if(array_key_exists($product->category, $categories_arr)) {

                        if($product->category == '8'){
                            $category_name = $product->product_name;
                        }else{
                            $category_name = $categories_arr[$product->category];
                        }

                        if($product->voucher_id != null){
                            
                            $voucher = Voucher::where('id', $product->voucher_id)->first();
                            
                            if($voucher->discountType == 'percentage'){

                                $total_item_price = $product->item_price * $product->quantity;
                                $discounted = number_format(($total_item_price/100)*$voucher->discountValue,2);

                            }else{
                                $discounted = number_format($product->voucher_value,2);
                            }

                             
                        }else{
                            $discounted = 0;
                        }

                        $month = strtoupper($start_date->format('F'));
                        $total_sales = SalesOverview::where([['outlet_id', $outlet->id],['item', $category_name],['month', $month],['year', $year]])->first();
                        
                        $sum = (number_format($product->item_price * $product->quantity,2));
                        $sum -= $discounted;

                        if(!empty($total_sales)){
                        $total_sales->total_sales += number_format($sum,2);
                        $total_sales->save();
                        }
                    }
                }
            }
        }
        $month = strtoupper($start_date->format('F'));
        $discount_value = SalesOverview::where([['item','voucher_total'],['outlet_id',$outlet->id],['month',$month],['year',$year]])->first();
        $discount_value->total_sales = $total_voucher;
        $discount_value->save(); 
                
        DB::commit();

        echo 'success run';
    }
}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PackageUser;
use App\Package;
use App\DashboardDetails;
use App\Order;
use App\Outlet;
use App\OrderSubscription;
use App\SalesOverview;
use App\Transaction;
use App\TransactionDetail;//new
use App\TransactionDiscount;//new
use App\TransactionInvoice;//new
use App\Category;
use App\Voucher;
use App\Inventory;
use Carbon\Carbon;
use DB;

class DashboardData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:DashboardData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update regularly for instant load time performance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        $current = Carbon::now();
        $current_month = $current->month;
        $start_year = Carbon::create()->startOfYear();
        $start_day = Carbon::create()->month($current_month)->startOfDay();
        $end_day = Carbon::create()->month($current_month)->endOfDay();
        $start_month = Carbon::create()->month($current_month)->startOfMonth();
        $end_month = Carbon::create()->month($current_month)->endOfMonth();
        $outlets = Outlet::all();
        $target = 0;

        foreach ($outlets as $key => $outlet) {
    
            if(!empty($current)){

                $curren_month = Carbon::parse($start_month)->format('M Y');
                $prev_month_check = Carbon::parse($start_month)->subMonths(1)->format('M');
                $prev_month = Carbon::parse($start_month)->subMonths(1)->format('M Y');
                $prev_annum_target = DashboardDetails::where([['outlet_id', $outlet->id],['month_of_year', $prev_month]])->first();
                $setted_target = DashboardDetails::where([['outlet_id', $outlet->id],['month_of_year', $curren_month]])->first();
                if($setted_target && $setted_target->annual_sales_target != '0'){
                    $target = $setted_target->annual_sales_target;
                }else{
                    
                    if($prev_annum_target){
                        $target = $prev_annum_target->annual_sales_target;
                        
                    }else{
                        $target = '0';
                    }
                }
                
                $daily_sum = TransactionInvoice::where([['outlet_id', $outlet->id],['status', '0']])
                ->whereBetween('created_at', array($start_day, $end_day))->sum('invoice_grand_total');
                
                $monthly_sum = TransactionInvoice::where([['outlet_id', $outlet->id],['status', '0']])->whereBetween('created_at', array($start_month, $end_month))->sum('invoice_grand_total');
                 
                $month_date_sale = number_format((float)$monthly_sum, 2, '.', '');

                $q = DashboardDetails::where([['outlet_id', $outlet->id],['month_of_year', $prev_month]])->first();


                if($q){
                    
                    if($prev_month_check == 'Dec'){
                      $last_sales = '0.00';  
                    }else{
                      $last_sales = $q->year_to_date_sales;
                    }
                    $new_year_to_date_sales = $last_sales + $month_date_sale;
                }

                $month_of_year = Carbon::parse($start_month)->format('M Y');
                $year_to_date_sales = number_format((float)$new_year_to_date_sales, 2, '.', '');
                $today_sales = number_format((float)$daily_sum, 2, '.', '');

                $packages = Package::where([['category','SWIMMING'],['status','active']])->select('id')->get();
                foreach ($packages as $package) {
                    $package_arr[] = $package->id;
                }

                $PackageUser = PackageUser::with('packages') 
                                ->where([['outstanding', '!=', '0.00'],['status','active'],['outlet_id',$outlet->id]])
                                ->whereIn('package_id',$package_arr)
                                ->get();

                $outstanding_sum = 0;
                foreach ($PackageUser as $key => $some) {
                    $outstanding_sum = $outstanding_sum + $some->outstanding;
                }

                $member_outstanding = $PackageUser->count();
                $outstanding_amount = number_format((float)$outstanding_sum, 2, '.', '');

                $freeze = PackageUser::with('packages', 'user')->where('outlet_id', $outlet->id)->where('status', 'freeze')->get();

                $terminate = PackageUser::with('packages', 'user')->where('outlet_id', $outlet->id)->where('status', 'terminate')->get();
                $active = PackageUser::whereHas('classroom', function($q){
                                        $q->where('status', '=', 'active');
                                    })->with('packages', 'user')->where([['outlet_id', $outlet->id],['status', 'active'],['class','SWIMMING']])->get();

                $active_swim_member = $active->count();
                $terminated_swim_member = $terminate->count();
                $freeze_swim_member = $freeze->count();
                $updated_at = carbon::now();
               
                $condition = [
                    'month_of_year'   => $month_of_year,
                    'outlet_id'=> $outlet->id
                ];

                $insert = [
                    'month_to_date_sales'            => $month_date_sale,
                    'year_to_date_sales'             => $year_to_date_sales,
                    'today_sales'                    => $today_sales,
                    'member_outstanding'             => $member_outstanding,
                    'outstanding_amount'             => $outstanding_amount,
                    'active_swim_member'             => $active_swim_member,
                    'terminated_swim_member'         => $terminated_swim_member,
                    'number_of_new_swimming_member'  => '0',
                    'annual_sales_target'            => $target,
                    'freeze_swim_member'             => $freeze_swim_member,
                    'updated_at'                     => $updated_at
                ];
                
                $DashboardData = DashboardDetails::updateOrCreate($condition, $insert);
                if (!$DashboardData) {
                    DB::rollback();
                }

            }
            
        }
        
        DB::commit();



        DB::beginTransaction();

        $current = Carbon::now();
        $current_month = $current->month;
        $start_year = Carbon::create()->startOfYear();
        $start_day = Carbon::create()->month($current_month)->startOfDay();
        $start_month = Carbon::create()->month($current_month)->startOfMonth();
        $end_month = Carbon::create()->month($current_month)->endOfMonth();
        $outlets = Outlet::all();
        $target = 0;

        foreach ($outlets as $key => $outlet) {

            // dd($outlet,$start_month->format('Y-m-d'));
            $UnpaidOrder = Order::where([['outlet_id', $outlet->id],['monthly_payment',$start_month->format('Y-m-d')],['status','1']])->get();
            // dd(count($UnpaidOrder));

            $packages = Package::where([['category','SWIMMING'],['status','active']])->select('id')->get();
            foreach ($packages as $package) {
                $package_arr[] = $package->id;
            }

            $package_user_id = [];
            $ids = [];

            foreach ($UnpaidOrder as $key => $Unpaid) {

                if(!empty($package_user_id) && array_key_exists($Unpaid->package_user_id, $package_user_id)){
                    // dd($package_user_id[$Unpaid->package_user_id]);

                    $package_user_id[$Unpaid->package_user_id]['amount'] = $package_user_id[$Unpaid->package_user_id]['amount'] + $Unpaid->package_total;

                    // dd($package_user_id[$Unpaid->package_user_id]);

                }else{

                    $ids[] = $Unpaid->package_user_id;
                    $package_user_id[$Unpaid->package_user_id] = [
                        'id' => $Unpaid->package_user_id,
                        'amount' => $Unpaid->package_total
                    ];
                }

            }

            // dd(count($ids));

            $PackageUser = PackageUser::with('packages') 
                            ->where([['status','active'],['outlet_id', $outlet->id]])
                            ->whereIn('id',$ids)
                            ->whereIn('package_id',$package_arr)
                            ->get();
            // dd($PackageUser);

            $total_out_amt = 0;
            $total_out_count = 0;

            foreach ($PackageUser as $key => $some) {
                // dd($package_user_id[$some->id]['amount']);    
                $total_out_amt += $package_user_id[$some->id]['amount'];
                $total_out_count += 1;
                
            }

            // dd($total_out_count,$total_out_amt);

            $month_of_year = Carbon::parse($start_month)->format('M Y');

            $condition = [
                'month_of_year'   => $month_of_year,
                'outlet_id'=> $outlet->id
            ];

            $insert = [
                'member_outstanding'             => $total_out_count,
                'outstanding_amount'             => $total_out_amt
            ];
            
            $DashboardData = DashboardDetails::updateOrCreate($condition, $insert);
            if (!$DashboardData) {
                DB::rollback();
            }
        }

        DB::commit();

        echo 'success run';
    }
}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OrderSubscription;
use App\Order;
use App\PackageUser;
use Carbon\Carbon;
use DB;

class UpdateOrderSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Order Subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        $ordersubscriptions = OrderSubscription::whereHas('order')
                    ->whereHas('package_user', function($q){
                                $q->where('status', '=', 'active');
                            })
                    ->get();

        // $empty_ordersubscriptions = OrderSubscription::doesnthave('package_user')->get();
        // foreach ($empty_ordersubscriptions as $empty_ordersubscription) {
        //     $empty_ordersubscription->package_user_id = '0';
        //     $empty_ordersubscription->save();
        // }
        //dd($ordersubscriptions);
        foreach ($ordersubscriptions as $key => $ordersubscription) {

            $order = Order::whereHas('package_user', function($q){
                                $q->where('status', '=', 'active');
                            })
                    ->whereHas('package', function($q){
                                $q->where('status', '=', 'active');
                            })
                    ->with('package')->where('package_user_id', $ordersubscription->package_user_id)->get()->last();
            //dd($order->package->subscription);
            
            $calculation = Carbon::parse($order->monthly_payment)->addMonths(($order->package->subscription-1))->startOfMonth()->format('Y-m-d');
            
            $date = Carbon::createFromFormat('Y-m-d', $calculation);
            //dd($date->month);

            $insertOrderSubscription = [
                'order_id'     => $order->id,
                'package_user_id'   => $order->package_user_id,
                'month'             => $date->month,
                'year'              => $date->year
            ];

            $orderSubscription = OrderSubscription::where('package_user_id', $ordersubscription->package_user_id)->update($insertOrderSubscription);
            if (!$orderSubscription) {
                DB::rollback();
            }            
        }
        DB::commit();

        echo 'success run';
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classroom;
use App\Attendance;
use App\Transfer;
use App\PackageUser;
use App\Replacement;
use Carbon\Carbon;
use App\Event;
use Carbon\CarbonPeriod;
use DB;

class ManualAttendanceStudent1 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tempattendance:students1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate missing student attendace';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | Check Events
        |--------------------------------------------------------------------------
        */
        //dd('hi');
        $events = Event::where(function ($q) {
                  $q->where('start', '<=', Carbon::now());
                  $q->where('end', '>=', Carbon::now());
                })
                ->value('calendar_id');

        if($events == '' || $events == 1 || $events == 4 || $events == 5) {

            /*
            |--------------------------------------------------------------------------
            | Add attendance for Swim Member
            |--------------------------------------------------------------------------
            */

            DB::beginTransaction();
            $current_date = Carbon::parse('2019-07-06');
            $today = $current_date->format('Y-m-d');
            $dayname = strtolower($current_date->format('l'));

            $classrooms = Classroom::with('users')->where('day', $dayname)->get();

            foreach ($classrooms as $classroom)
            {
                $clock_in_start = date("2019-07-06 H:i:s", strtotime("-15 minutes", strtotime($classroom->start)));
                $clock_in_end = date("2019-07-06 H:i:s", strtotime("-16 minutes", strtotime($classroom->end)));

                foreach ($classroom->users as $student) {
                    $transfersOut = Transfer::where('fromClassroom', $classroom->id)->where('member_id', $student->id)->first();

                    $attendance = new Attendance;
                    $attendance->user_id = $student->id;
                    $attendance->class_id = $classroom->id;
                    $attendance->package_id = $student->pivot->package_id;
                    $attendance->clockIn = Carbon::parse('2019-07-06');
                    $attendance->status = 'absent';
                    $attendance->replacement_id = is_null($transfersOut) ? null : $transfersOut->id;
                    $attendance->week = $current_date->weekOfMonth;
                    $attendance->clock_in_start = $clock_in_start;
                    $attendance->clock_in_end = $clock_in_end;
                    $attendance->outlet_id = $classroom->outlet_id;
                    $attendance->created_at     = $today;
                    $attendance->updated_at     = $today;
                    $attendance->save();
                    
                    if (!is_null($transfersOut)) {
                        $attendance = Attendance::where('replacement_id', $transfersOut->id)->whereDate('clockIn', $today)->forceDelete();
                    }

                    if (!$attendance) {
                        DB::rollback();
                    }
                }
            }

            $transfersIn = Transfer::where('toDate', $today)->get();
            foreach ($transfersIn as $value) {
                $classrooms = Classroom::findOrFail($value->fromClassroom);
                $clock_in_start = date("2019-07-06 H:i:s", strtotime("-15 minutes", strtotime($classrooms->start)));
                $clock_in_end = date("2019-07-06 H:i:s", strtotime("-16 minutes", strtotime($classrooms->end)));
                
                $classroom_user = $classrooms->users()->wherePivot('user_id', $value->member_id)->first();

                $attendance = new Attendance;
                $attendance->user_id = $value->member_id;
                $attendance->class_id = $value->toClassroom;
                $attendance->package_id = $classroom_user->pivot->package_id;
                $attendance->clockIn = Carbon::parse('2019-07-06');
                $attendance->status = 'absent';
                $attendance->replacement_id = $value->id;
                $attendance->week = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end = $clock_in_end;
                $attendance->outlet_id = $classrooms->outlet_id;
                $attendance->created_at     = $today;
                $attendance->updated_at     = $today;
                $attendance->save();

                if (!$attendance) {
                    DB::rollback();
                }
            }

            DB::commit();
            echo 'run sucess';
            /*
            |--------------------------------------------------------------------------
            | Add attendance for Swim Member (Class Replacement)
            |--------------------------------------------------------------------------
            */

            DB::beginTransaction();
            $replacements = Replacement::all();
            $current_date = Carbon::parse('2019-07-06');
            $today = $current_date->format('Y-m-d');

            foreach ($replacements as $key => $replacement) {
                if ($today == $replacement->beforeDate) {
                    // remove data attendances if replacement class is today.
                    $attendances = Attendance::where('class_id', $replacement->classroom_id)->whereDate('clock_in_start', $today)->forceDelete();
                }

                if ($today == $replacement->afterDate) {
                    $classroom = Classroom::find($replacement->classroom_id);
                    $clock_in_start = date("2019-07-06 H:i:s", strtotime("-15 minutes", strtotime($classroom->start)));
                    $clock_in_end = date("2019-07-06 H:i:s", strtotime("-16 minutes", strtotime($classroom->end)));
                    
                    // add data attendances if replacement class is today.
                    foreach ($classroom->users as $key => $user) {
                        $attendance = new Attendance;
                        $attendance->user_id = $user->id;
                        $attendance->class_id = $classroom->id;
                        $attendance->package_id = $user->pivot->package_id;
                        $attendance->clockIn = Carbon::parse('2019-07-06');
                        $attendance->status = 'absent';
                        $attendance->week = $current_date->weekOfMonth;
                        $attendance->clock_in_start = $clock_in_start;
                        $attendance->clock_in_end = $clock_in_end;
                        $attendance->outlet_id = $classroom->outlet_id;
                        $attendance->created_at     = $today;
                        $attendance->updated_at     = $today;
                        $attendance->save();

                        if (!$attendance) {
                            DB::rollback();
                        }
                    }
                }
            }

            DB::commit();
            echo 'run sucess';
            /*
            |--------------------------------------------------------------------------
            | Add attendance for Gym Member
            |--------------------------------------------------------------------------
            */

            DB::beginTransaction();
            $startOfMonth = Carbon::now()->startOfMonth();
            $endOfMonth = Carbon::now()->endOfMonth();
            $today = Carbon::today();
            $week = $today->weekOfMonth;
            $endOfDay = Carbon::today()->endOfDay();
            $package_users = PackageUser::with('packages')->gym()->get();

            foreach ($package_users as $key => $package_user) {
                $access = $package_user->packages->access;

                if($access == '-1'){    // unlimited access
                    $attendance = new Attendance;
                    $attendance->user_id        = $package_user->user_id;
                    $attendance->package_id     = $package_user->package_id;
                    $attendance->clockIn        = Carbon::parse('2019-07-06');
                    $attendance->status         = 'absent';
                    $attendance->week           = $week;
                    $attendance->clock_in_start = $today;
                    $attendance->clock_in_end   = $endOfDay;
                    $attendance->outlet_id      = $package_user->outlet_id;
                    $attendance->created_at     = $today;
                    $attendance->updated_at     = $today;
                    $attendance->created_at     = $today;
                    $attendance->updated_at     = $today;
                    $attendance->save();

                    if (!$attendance) {
                        DB::rollback();
                    }
                }
                else{
                    $countAttendance = Attendance::where([
                        ['user_id', $package_user->user_id],
                        ['package_id', $package_user->package_id],
                        ['outlet_id', $package_user->outlet_id],
                        ['status', 'present'],
                    ])
                    ->whereBetween('clockIn', [$startOfMonth, $endOfMonth])
                    ->count();

                    if ($countAttendance < $access) {  // limited access
                        $attendance = new Attendance;
                        $attendance->user_id        = $package_user->user_id;
                        $attendance->package_id     = $package_user->package_id;
                        $attendance->clockIn        = Carbon::parse('2019-07-06');
                        $attendance->status         = 'absent';
                        $attendance->week           = $week;
                        $attendance->clock_in_start = $today;
                        $attendance->clock_in_end   = $endOfDay;
                        $attendance->outlet_id      = $package_user->outlet_id;
                        $attendance->created_at     = $today;
                        $attendance->updated_at     = $today;
                        $attendance->save();

                        if (!$attendance) {
                            DB::rollback();
                        }
                    }
                }
            }

            DB::commit();
            echo 'run sucess';
        }

        /*
        |--------------------------------------------------------------------------
        | No swimm class
        |--------------------------------------------------------------------------
        */

        if ($events == 3) {

            /*
            |--------------------------------------------------------------------------
            | Add attendance for Gym Member
            |--------------------------------------------------------------------------
            */

            DB::beginTransaction();
            $startOfMonth = Carbon::now()->startOfMonth();
            $endOfMonth = Carbon::now()->endOfMonth();
            $today = Carbon::today();
            $week = $today->weekOfMonth;
            $endOfDay = Carbon::today()->endOfDay();
            $package_users = PackageUser::with('packages')->gym()->get();

            foreach ($package_users as $key => $package_user) {
                $access = $package_user->packages->access;

                if($access == '-1'){    // unlimited access
                    $attendance = new Attendance;
                    $attendance->user_id        = $package_user->user_id;
                    $attendance->package_id     = $package_user->package_id;
                    $attendance->clockIn        = Carbon::parse('2019-07-06');
                    $attendance->status         = 'absent';
                    $attendance->week           = $week;
                    $attendance->clock_in_start = $today;
                    $attendance->clock_in_end   = $endOfDay;
                    $attendance->outlet_id      = $package_user->outlet_id;
                    $attendance->created_at     = $today;
                    $attendance->updated_at     = $today;
                    $attendance->save();

                    if (!$attendance) {
                        DB::rollback();
                    }
                }
                else{
                    $countAttendance = Attendance::where([
                        ['user_id', $package_user->user_id],
                        ['package_id', $package_user->package_id],
                        ['outlet_id', $package_user->outlet_id],
                        ['status', 'present'],
                    ])
                    ->whereBetween('clockIn', [$startOfMonth, $endOfMonth])
                    ->count();

                    if ($countAttendance < $access) {  // limited access
                        $attendance = new Attendance;
                        $attendance->user_id        = $package_user->user_id;
                        $attendance->package_id     = $package_user->package_id;
                        $attendance->clockIn        = Carbon::parse('2019-07-06');
                        $attendance->status         = 'absent';
                        $attendance->week           = $week;
                        $attendance->clock_in_start = $today;
                        $attendance->clock_in_end   = $endOfDay;
                        $attendance->outlet_id      = $package_user->outlet_id;
                        $attendance->created_at     = $today;
                        $attendance->updated_at     = $today;
                        $attendance->save();

                        if (!$attendance) {
                            DB::rollback();
                        }
                    }
                }
            }

            DB::commit();
            echo 'run sucess';
        }
    }
}

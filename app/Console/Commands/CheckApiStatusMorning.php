<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classroom;
use App\ClassroomUser;
use App\Attendance;
use App\AttendanceDevice;
use App\AttendanceDeviceSync;
use App\Transfer;
use App\PackageUser;
use App\Replacement;
use App\User;
use App\Outlet;
use Carbon\Carbon;
use App\Event;
use Carbon\CarbonPeriod;
use DB;
use stdClass;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class CheckApiStatusMorning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkApi:statusMorning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check Api status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $oulets = Outlet::all();


        foreach($oulets as $outlet) {

            if($outlet->facial_recognitions == '1') {

                

                if ($status != 200 || $status == null) {

                    \Artisan::call('attendance:studentsOffline', ['outletId' => $outlet->id]);

                } else {

                    \Artisan::call('attendance:students', ['outletId' => $outlet->id]);

                }
            } else {
                \Artisan::call('attendance:studentsOffline', ['outletId' => $outlet->id]);
            }
        }
    }
}

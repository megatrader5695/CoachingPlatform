<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OrderSubscription;
use App\Order;
use App\Transaction;
use App\PackageUser;
use Carbon\Carbon;
use DB;

class ManualOrderCheckout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manual:checkout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'manual unpaid Order to transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        $orders = Order::doesntHave('transactions')->where('status','0')->get();
        //dd($orders->count());
        foreach ($orders as $key => $order) {
            
            $last_invoice = Transaction::select('invoice_no')->orderBy('invoice_no','desc')->first();

            $start = $last_invoice->invoice_no + 1;
            $count = 1;
            $digits = 10;

            for ($n = $start; $n < $start + $count; $n++) {
         
               $result = str_pad($n, $digits, "0", STR_PAD_LEFT);
         
            }

            $new_invoice_no = $result;

            $invoice = new Transaction;
            $invoice->cust_id = $order->cust_id;
            $invoice->outlet_id = $order->outlet_id;
            $invoice->cashier_id = '1001065';
            $invoice->order_id = $order->id;
            $invoice->product_id = '0';
            $invoice->quantity = '0';
            $invoice->amount_paid = $order->package_total;
            $invoice->deposit_use = '0';
            $invoice->invoice_total = $order->package_total;
            $invoice->sst_package = $order->package_total*0.06;
            $invoice->payment_method = 'Cash';
            $invoice->status = '0';
            $invoice->only_reg_fee = '0';
            $invoice->remarks = '';
            $invoice->invoice_no = $new_invoice_no;
            $invoice->created_at = date('Y-m-d H:i:s');
            $invoice->updated_at = date('Y-m-d H:i:s');
            $invoice->save();
            
        }
        
        
        DB::commit();

        echo 'success run';
    }
}

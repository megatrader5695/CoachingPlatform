<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Inventory;
use App\Outlet;
use App\Transfer;
use Carbon\Carbon;

class LogReplacement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:replacement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $firstday = (new Carbon('first day of this month') )->format('Y-m-d');
        $current =  Carbon::now()->format('Y-m-d');
        if ($firstday == $current) {
                Transfer::where('toDate', '<=', $current)->delete();
        }

        // change used to 0
        Transfer::where('toDate', '<', $current)->update(
            ['used' => '0']
        );        
        
        $outlet = Outlet::all();
        $product = Inventory::all();

        foreach ($outlet as $outlets) {
            foreach ($product as $products) {
                $check = DB::table('inventory_product')->where([
                        ['outlet_id', $outlets->id],
                        ['inventory_id', $products->id]
                        ])->first();
                if (empty( $check )) {
                    $inventory_product = DB::table('inventory_product')->insert([
                        [
                            'outlet_id' => $outlets->id,
                            'inventory_id' => $products->id,
                            'quantity' => 0,
                        ]
                    ]);
                }
            }
        }
    }
}

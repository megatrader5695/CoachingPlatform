<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OrderSubscription;
use App\Order;
use Carbon\Carbon;
use App\AttendanceDevice;
use App\AttendanceDeviceSync;
use App\Attendance;
use App\PackageUser;
use DB;

class UpdateAttendanceDevice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:attendanceDevice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update attendance device';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        
        $attendanceDevices = AttendanceDevice::all();

        foreach ($attendanceDevices as $attendanceDevice) {

            //check outstanding
            $packageUsers = PackageUser::where('id',$attendanceDevice->package_user_id)->first();

            if($packageUsers->outstanding > 0) {

                $payment_signal = '0';

            } else {

                $payment_signal = '1';

            }

            //check current time and date
            $currentTime = Carbon::now();

            if ($currentTime >= $attendanceDevice->clock_in_start && $currentTime <= $attendanceDevice->clock_in_end) {

                $access_signal = '1';

            } else {

                $access_signal = '0';
            }

            //update signal
            $attendances = AttendanceDevice::where('id',$attendanceDevice->id)
                          ->update([
                            'payment_signal' => $payment_signal,
                            'access_signal' => $access_signal,
                            'updated_at' => Carbon::now()
                          ]);

            $attendancesSync = AttendanceDeviceSync::where('id',$attendanceDevice->id)
                          ->update([
                            'payment_signal' => $payment_signal,
                            'access_signal' => $access_signal
                          ]);

            if (!$attendances) {
                DB::rollback();
            }

            if (!$attendancesSync) {
                DB::rollback();
            }
            
            if ($attendanceDevice->payment_signal == '1' && $attendanceDevice->access_signal == '1'){

                //update remarks
                $remarks = AttendanceDevice::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Success: Welcome!',
                            'updated_at' => Carbon::now()
                          ]);

                $remarksSync = AttendanceDeviceSync::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Success: Welcome!'
                          ]);

                if (!$remarks) {
                    DB::rollback();
                }

                if (!$remarksSync) {
                    DB::rollback();
                }

                $attendancesSyncs = AttendanceDeviceSync::all();

                foreach ($attendancesSyncs as $value) {

                  $attendanceDevice = AttendanceDevice::where('attendance_id',$value->attendance_id)
                                ->where('member_id',$value->member_id)
                                ->update([
                                  'clockIn' => $value->clock_in,
                                  'updated_at' => Carbon::now()
                                ]);
                }

                //update attendance
                if ($attendanceDevice->clock_in != null) {

                  $attendances = Attendance::where('id',$attendanceDevice->attendance_id)
                                ->where('user_id',$attendanceDevice->member_id)
                                ->update([
                                  'status' => 'present',
                                  'clockIn' => $attendanceDevice->clock_in,
                                  'updated_at' => Carbon::now()
                                ]);

                  if (!$attendances) {
                      DB::rollback();
                  }

                }

            } elseif ($attendanceDevice->payment_signal == '1' && $attendanceDevice->access_signal == '0') {

                //update remarks
                $remarks = AttendanceDevice::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Access Denied! Please proceed to counter or try again 15 minutes before your class start.',
                            'updated_at' => Carbon::now()
                          ]);

                $remarksSync = AttendanceDeviceSync::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Access Denied!'
                          ]);

                if (!$remarks) {
                    DB::rollback();
                }

                if (!$remarksSync) {
                    DB::rollback();
                }

            } elseif ($attendanceDevice->payment_signal == '0' && $attendanceDevice->access_signal == '1') {

                //update remarks
                $remarks = AttendanceDevice::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Account Overdue! Please proceed to counter.',
                            'updated_at' => Carbon::now()
                          ]);

                $remarksSync = AttendanceDeviceSync::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Account Overdue! Please proceed to counter.'
                          ]);

                if (!$remarks) {
                    DB::rollback();
                }

                if (!$remarksSync) {
                    DB::rollback();
                }
                
            } else {

                //update remarks
                $remarks = AttendanceDevice::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Access Denied! Please proceed to counter or try again 15 minutes before your class start.',
                            'updated_at' => Carbon::now()
                          ]);

                $remarksSync = AttendanceDeviceSync::where('id',$attendanceDevice->id)
                          ->update([
                            'remarks' => 'Access Denied!'
                          ]);

                if (!$remarks) {
                    DB::rollback();
                }

                if (!$remarksSync) {
                    DB::rollback();
                }
                
            }

        }

        DB::commit();
    }
}

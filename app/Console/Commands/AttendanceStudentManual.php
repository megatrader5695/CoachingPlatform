<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classroom;
use App\Attendance;
use App\AttendanceDevice;
use App\AttendanceDeviceSync;
use App\Transfer;
use App\PackageUser;
use App\Replacement;
use App\User;
use Carbon\Carbon;
use App\Event;
use Carbon\CarbonPeriod;
use DB;
use stdClass;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AttendanceStudentManual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance:studentsManual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate student attendace manually';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $knownStartDate = Carbon::create(2019, 10, 27 );
        Carbon::setTestNow($knownStartDate);   
        $startTodayTime = $knownStartDate->startOfDay();

        $knownEndDate = Carbon::create(2019, 10, 27 );
        Carbon::setTestNow($knownEndDate); 
        $endTodayTime = $knownEndDate->endOfDay();

        // echo Carbon::getTestNow();

        // echo Carbon::now();  

        // echo 'Date: '.$knownDate . ' ';
        // echo 'Start Time: '.$startTodayTime . ' ';
        // echo 'End Time: '.$endTodayTime . ' ';


        $getTotalAttendanceToday = Attendance::whereBetween('created_at',[$startTodayTime, $endTodayTime])
                                   ->count();
        // echo $getTotalAttendanceToday;

        // if($getTotalAttendanceToday == 0) {

            /*
            |--------------------------------------------------------------------------
            | Check Events
            |--------------------------------------------------------------------------
            */

            // dd(Carbon::now());

            $events = Event::where(function ($q) {
                      $q->where('start', '<=', Carbon::now());
                      $q->where('end', '>=', Carbon::now());
                    })
                    ->value('calendar_id');

            // dd($events);

            if($events == '' || $events == 1 || $events == 4 || $events == 5) {

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Swim Member
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                // $current_date = Carbon::now();
                $today = $startTodayTime->format('Y-m-d');
                $dayname = strtolower($startTodayTime->format('l'));

                $classrooms = Classroom::with('users')->where('day', $dayname)->get();

                foreach ($classrooms as $classroom)
                {
                    $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($today.$classroom->start)));
                    $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($today.$classroom->end)));

                    foreach ($classroom->users as $student) {
                        $transfersOut = Transfer::where('fromClassroom', $classroom->id)->where('member_id', $student->id)->where('fromDate', $today)->first();

                        $outstandings = PackageUser::where('id',$student->pivot->package_user_id)->value('outstanding');

                        if ($outstandings > 0) {

                            $outstanding = '0';
                            $remarks = 'Access Denied!';

                        } else {

                            $outstanding = '1';
                            $remarks = 'Access Denied!';

                        }

                        $attendance = new Attendance;
                        $attendance->user_id = $student->id;
                        $attendance->class_id = $classroom->id;
                        $attendance->package_id = $student->pivot->package_id;
                        $attendance->clockIn = $startTodayTime;
                        $attendance->status = 'absent';
                        $attendance->replacement_id = is_null($transfersOut) ? null : $transfersOut->id;
                        $attendance->week = $startTodayTime->weekOfMonth;
                        $attendance->clock_in_start = $clock_in_start;
                        $attendance->clock_in_end = $clock_in_end;
                        $attendance->outlet_id = $classroom->outlet_id;
                        $attendance->created_at = $startTodayTime;
                        $attendance->updated_at = $startTodayTime;
                        $attendance->save();

                        // $attendanceDevice = new AttendanceDevice;
                        // $attendanceDevice->attendance_id = $attendance->id;
                        // $attendanceDevice->member_id = $student->id;
                        // $attendanceDevice->package_user_id = $student->pivot->package_user_id;
                        // $attendanceDevice->payment_signal = $outstanding;
                        // $attendanceDevice->access_signal = '0';
                        // $attendanceDevice->clock_in_start = $clock_in_start;
                        // $attendanceDevice->clock_in_end = $clock_in_end;
                        // $attendanceDevice->clock_in = null;
                        // $attendanceDevice->remarks = $remarks;
                        // $attendanceDevice->created_at = Carbon::now();
                        // $attendanceDevice->updated_at = Carbon::now();
                        // $attendanceDevice->save();


                        // $attendanceDeviceSync = new AttendanceDeviceSync;
                        // $attendanceDeviceSync->attendance_id = $attendance->id;
                        // $attendanceDeviceSync->member_id = $student->id;
                        // $attendanceDeviceSync->payment_signal = $outstanding;
                        // $attendanceDeviceSync->access_signal = '0';
                        // $attendanceDeviceSync->clock_in = null;
                        // $attendanceDeviceSync->remarks = $remarks;
                        // $attendanceDeviceSync->save();
                        
                        
                        if (!is_null($transfersOut)) {

                            $attendanceDevices = Attendance::where('replacement_id', $transfersOut->id)->whereDate('clockIn', $today)->get();

                            // foreach ($attendanceDevices as $attandance) {

                            //     $attendanceDevice = AttendanceDevice::where('attandance_id',$attendance->id)
                            //                         ->forceDelete();

                            //     $attendanceDeviceSync = AttendanceDeviceSync::where('attandance_id',$attendance->id)
                            //                         ->forceDelete();

                            // }

                            $attendance = Attendance::where('replacement_id', $transfersOut->id)->whereDate('clockIn', $today)->forceDelete();
                        }

                        if (!$attendance) {
                            DB::rollback();
                        }

                        if ($outstandings > 0) {

                            $year = $startTodayTime->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        }

                        $accLevelIds = '1';

                        $details = new stdClass;
                        $details->pin = strval($student->id);
                        $details->name = $student->name;
                        if($student->gender == 'Male') {
                            $gender = 'M';
                        } else {
                            $gender = 'F';
                        }
                        $details->gender = $gender;
                        $details->accLevelIds = $accLevelIds;
                        $details->accStartTime = $clock_in_start;
                        $details->accEndTime = $clock_in_end;

                        $data = json_encode($details);

                        // if (!$attendanceDevice) {
                        //     DB::rollback();
                        // }

                        // if (!$attendanceDeviceSync) {
                        //     DB::rollback();
                        // }
                    }
                }

                $transfersIn = Transfer::where('toDate', $today)->get();
                foreach ($transfersIn as $value) {
                    $classrooms = Classroom::findOrFail($value->fromClassroom);
                    $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($today.$classrooms->start)));
                    $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($today.$classrooms->end)));
                    
                    $classroom_user = $classrooms->users()->wherePivot('user_id', $value->member_id)->first();

                    $outstandings = PackageUser::where('id',$classroom_user->pivot->package_user_id)->value('outstanding');

                    $userDetails = User::find($value->member_id);

                    if ($outstandings > 0) {

                        $outstanding = '0';
                        $remarks = 'Access Denied!';

                    } else {

                        $outstanding = '1';
                        $remarks = 'Access Denied!';

                    }

                    $attendance = new Attendance;
                    $attendance->user_id = $value->member_id;
                    $attendance->class_id = $value->toClassroom;
                    $attendance->package_id = $classroom_user->pivot->package_id;
                    $attendance->clockIn = $startTodayTime;
                    $attendance->status = 'absent';
                    $attendance->replacement_id = $value->id;
                    $attendance->week = $startTodayTime->weekOfMonth;
                    $attendance->clock_in_start = $clock_in_start;
                    $attendance->clock_in_end = $clock_in_end;
                    $attendance->outlet_id = $classrooms->outlet_id;
                    $attendance->save();

                    // $attendanceDevice = new AttendanceDevice;
                    // $attendanceDevice->attendance_id = $attendance->id;
                    // $attendanceDevice->member_id = $value->member_id;
                    // $attendanceDevice->package_user_id = $classroom_user->pivot->package_user_id;
                    // $attendanceDevice->payment_signal = $outstanding;
                    // $attendanceDevice->access_signal = '0';
                    // $attendanceDevice->clock_in_start = $clock_in_start;
                    // $attendanceDevice->clock_in_end = $clock_in_end;
                    // $attendanceDevice->clock_in = null;
                    // $attendanceDevice->remarks = $remarks;
                    // $attendanceDevice->created_at = Carbon::now();
                    // $attendanceDevice->updated_at = Carbon::now();
                    // $attendanceDevice->save();


                    // $attendanceDeviceSync = new AttendanceDeviceSync;
                    // $attendanceDeviceSync->attendance_id = $attendance->id;
                    // $attendanceDeviceSync->member_id = $value->member_id;
                    // $attendanceDeviceSync->payment_signal = $outstanding;
                    // $attendanceDeviceSync->access_signal = '0';
                    // $attendanceDeviceSync->clock_in = null;
                    // $attendanceDeviceSync->remarks = $remarks;
                    // $attendanceDeviceSync->save();
                    

                    if (!$attendance) {
                        DB::rollback();
                    }

                    if ($outstandings > 0) {

                        $year = $startTodayTime->format('Y-m-d');
                        $start_time = $year.' 03:00:00';
                        $end_time = $year.' 03:01:00';
                        $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                        $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                    }

                    $accLevelIds = '1';

                    $details = new stdClass;
                    $details->pin = strval($userDetails->id);
                    $details->name = $userDetails->name;
                    if($userDetails->gender == 'Male') {
                        $gender = 'M';
                    } else {
                        $gender = 'F';
                    }
                    $details->gender = $gender;
                    $details->accLevelIds = $accLevelIds;
                    $details->accStartTime = $clock_in_start;
                    $details->accEndTime = $clock_in_end;

                    $data = json_encode($details);

                    // if (!$attendanceDevice) {
                    //     DB::rollback();
                    // }

                    // if (!$attendanceDeviceSync) {
                    //     DB::rollback();
                    // }
                }

                DB::commit();

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Swim Member (Class Replacement)
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                $replacements = Replacement::all();
                // $current_date = Carbon::now();
                $today = $startTodayTime->format('Y-m-d');

                foreach ($replacements as $key => $replacement) {
                    if ($today == $replacement->beforeDate) {
                        // remove data attendances if replacement class is today.
                        $attendances = Attendance::where('class_id', $replacement->classroom_id)->whereDate('clock_in_start', $today)->forceDelete();
                    }

                    if ($today == $replacement->afterDate) {
                        $classroom = Classroom::find($replacement->classroom_id);
                        $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($today.$classroom->start)));
                        $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($today.$classroom->end)));
                        
                        // add data attendances if replacement class is today.
                        foreach ($classroom->users as $key => $user) {

                            $outstandings = PackageUser::where('id',$user->pivot->package_user_id)->value('outstanding');

                            $userDetails = User::find($user->id);

                            if ($outstandings > 0) {

                                $outstanding = '0';
                                $remarks = 'Access Denied!';

                            } else {

                                $outstanding = '1';
                                $remarks = 'Access Denied!';

                            }

                            $attendance = new Attendance;
                            $attendance->user_id = $user->id;
                            $attendance->class_id = $classroom->id;
                            $attendance->package_id = $user->pivot->package_id;
                            $attendance->clockIn = $startTodayTime;
                            $attendance->status = 'absent';
                            $attendance->replacement_id = $replacement->id;
                            $attendance->week = $startTodayTime->weekOfMonth;
                            $attendance->clock_in_start = $clock_in_start;
                            $attendance->clock_in_end = $clock_in_end;
                            $attendance->outlet_id = $classroom->outlet_id;
                            $attendance->save();


                            // $attendanceDevice = new AttendanceDevice;
                            // $attendanceDevice->attendance_id = $attendance->id;
                            // $attendanceDevice->member_id = $user->id;
                            // $attendanceDevice->package_user_id = $user->pivot->package_user_id;
                            // $attendanceDevice->payment_signal = $outstanding;
                            // $attendanceDevice->access_signal = '0';
                            // $attendanceDevice->clock_in_start = $clock_in_start;
                            // $attendanceDevice->clock_in_end = $clock_in_end;
                            // $attendanceDevice->clock_in = null;
                            // $attendanceDevice->remarks = $remarks;
                            // $attendanceDevice->created_at = Carbon::now();
                            // $attendanceDevice->updated_at = Carbon::now();
                            // $attendanceDevice->save();


                            // $attendanceDeviceSync = new AttendanceDeviceSync;
                            // $attendanceDeviceSync->attendance_id = $attendance->id;
                            // $attendanceDeviceSync->member_id = $user->id;
                            // $attendanceDeviceSync->payment_signal = $outstanding;
                            // $attendanceDeviceSync->access_signal = '0';
                            // $attendanceDeviceSync->clock_in = null;
                            // $attendanceDeviceSync->remarks = $remarks;
                            // $attendanceDeviceSync->save();
                            

                            if (!$attendance) {
                                DB::rollback();
                            }

                            if ($outstandings > 0) {

                                $year = $startTodayTime->format('Y-m-d');
                                $start_time = $year.' 03:00:00';
                                $end_time = $year.' 03:01:00';
                                $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                                $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                            }

                            $accLevelIds = '1';

                            $details = new stdClass;
                            $details->pin = strval($userDetails->id);
                            $details->name = $userDetails->name;
                            if($userDetails->gender == 'Male') {
                                $gender = 'M';
                            } else {
                                $gender = 'F';
                            }
                            $details->gender = $gender;
                            $details->accLevelIds = $accLevelIds;
                            $details->accStartTime = $clock_in_start;
                            $details->accEndTime = $clock_in_end;

                            $data = json_encode($details);
                        }
                    }
                }

                DB::commit();

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Gym Member
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                // $startOfMonth = Carbon::now()->startOfMonth();
                // $endOfMonth = Carbon::now()->endOfMonth();

                $knownStartDate = Carbon::create(2019, 10, 27 );
                Carbon::setTestNow($knownStartDate);   
                $startOfMonth = $knownStartDate->startOfMonth();

                $knownEndDate = Carbon::create(2019, 10, 27 );
                Carbon::setTestNow($knownEndDate); 
                $endOfMonth = $knownEndDate->endOfMonth();

                $today = $knownStartDate;
                $week = $today->weekOfMonth;
                $endOfDay = $knownEndDate->endOfDay();
                $package_users = PackageUser::with('packages')->gym()->get();

                foreach ($package_users as $key => $package_user) {
                    $access = $package_user->packages->access;

                    $outstandings = PackageUser::where('id',$package_user->id)->value('outstanding');

                    $userDetails = User::find($package_user->user_id);

                    if ($outstandings > 0) {

                        $outstanding = '0';
                        $remarks = 'Account Overdue! Please proceed to counter.';

                    } else {

                        $outstanding = '1';
                        $remarks = 'Success: Welcome!';

                    }

                    if($access == '-1'){    // unlimited access
                        $attendance = new Attendance;
                        $attendance->user_id        = $package_user->user_id;
                        $attendance->package_id     = $package_user->package_id;
                        $attendance->clockIn        = $knownEndDate->startOfDay();
                        $attendance->status         = 'absent';
                        $attendance->week           = $week;
                        $attendance->clock_in_start = $today;
                        $attendance->clock_in_end   = $endOfDay;
                        $attendance->outlet_id      = $package_user->outlet_id;
                        $attendance->save();


                        // $attendanceDevice = new AttendanceDevice;
                        // $attendanceDevice->attendance_id = $attendance->id;
                        // $attendanceDevice->member_id = $package_user->user_id;
                        // $attendanceDevice->package_user_id = $package_user->id;
                        // $attendanceDevice->payment_signal = $outstanding;
                        // $attendanceDevice->access_signal = '1';
                        // $attendanceDevice->clock_in_start = $today;
                        // $attendanceDevice->clock_in_end = $endOfDay;
                        // $attendanceDevice->clock_in = null;
                        // $attendanceDevice->remarks = $remarks;
                        // $attendanceDevice->created_at = Carbon::now();
                        // $attendanceDevice->updated_at = Carbon::now();
                        // $attendanceDevice->save();


                        // $attendanceDeviceSync = new AttendanceDeviceSync;
                        // $attendanceDeviceSync->attendance_id = $attendance->id;
                        // $attendanceDeviceSync->member_id = $package_user->user_id;
                        // $attendanceDeviceSync->payment_signal = $outstanding;
                        // $attendanceDeviceSync->access_signal = '1';
                        // $attendanceDeviceSync->clock_in = null;
                        // $attendanceDeviceSync->remarks = $remarks;
                        // $attendanceDeviceSync->save();
                        

                        if (!$attendance) {
                            DB::rollback();
                        }

                        // if (!$attendanceDevice) {
                        //     DB::rollback();
                        // }

                        // if (!$attendanceDeviceSync) {
                        //     DB::rollback();
                        // }
                    }
                    else{
                        $countAttendance = Attendance::where([
                            ['user_id', $package_user->user_id],
                            ['package_id', $package_user->package_id],
                            ['outlet_id', $package_user->outlet_id],
                            ['status', 'present'],
                        ])
                        ->whereBetween('clockIn', [$startOfMonth, $endOfMonth])
                        ->count();

                        if ($countAttendance < $access) {  // limited access
                            $attendance = new Attendance;
                            $attendance->user_id        = $package_user->user_id;
                            $attendance->package_id     = $package_user->package_id;
                            $attendance->clockIn        = $knownEndDate->startOfDay();
                            $attendance->status         = 'absent';
                            $attendance->week           = $week;
                            $attendance->clock_in_start = $today;
                            $attendance->clock_in_end   = $endOfDay;
                            $attendance->outlet_id      = $package_user->outlet_id;
                            $attendance->save();


                            // $attendanceDevice = new AttendanceDevice;
                            // $attendanceDevice->attendance_id = $attendance->id;
                            // $attendanceDevice->member_id = $package_user->user_id;
                            // $attendanceDevice->package_user_id = $package_user->id;
                            // $attendanceDevice->payment_signal = $outstanding;
                            // $attendanceDevice->access_signal = '1';
                            // $attendanceDevice->clock_in_start = $today;
                            // $attendanceDevice->clock_in_end = $endOfDay;
                            // $attendanceDevice->clock_in = null;
                            // $attendanceDevice->remarks = $remarks;
                            // $attendanceDevice->created_at = Carbon::now();
                            // $attendanceDevice->updated_at = Carbon::now();
                            // $attendanceDevice->save();


                            // $attendanceDeviceSync = new AttendanceDeviceSync;
                            // $attendanceDeviceSync->attendance_id = $attendance->id;
                            // $attendanceDeviceSync->member_id = $package_user->user_id;
                            // $attendanceDeviceSync->payment_signal = $outstanding;
                            // $attendanceDeviceSync->access_signal = '1';
                            // $attendanceDeviceSync->clock_in = null;
                            // $attendanceDeviceSync->remarks = $remarks;
                            // $attendanceDeviceSync->save();

                            if (!$attendance) {
                                DB::rollback();
                            }

                            // if (!$attendanceDevice) {
                            //     DB::rollback();
                            // }

                            // if (!$attendanceDeviceSync) {
                            //     DB::rollback();
                            // }
                        }
                    }
                    if ($outstandings > 0) {

                        $year = $knownEndDate->format('Y-m-d');
                        $start_time = $year.' 03:00:00';
                        $end_time = $year.' 03:01:00';
                        $today = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                        $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                    }

                    $accLevelIds = '1';

                    $details = new stdClass;
                    $details->pin = strval($userDetails->id);
                    $details->name = $userDetails->name;
                    if($userDetails->gender == 'Male') {
                        $gender = 'M';
                    } else {
                        $gender = 'F';
                    }
                    $details->gender = $gender;
                    $details->accLevelIds = $accLevelIds;
                    $details->accStartTime = $today;
                    $details->accEndTime = $endOfDay;

                    $data = json_encode($details);
                }

                DB::commit();

            }

            /*
            |--------------------------------------------------------------------------
            | No swimm class
            |--------------------------------------------------------------------------
            */

            if ($events == 3) {

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Gym Member
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                // $startOfMonth = Carbon::now()->startOfMonth();
                // $endOfMonth = Carbon::now()->endOfMonth();

                $knownStartDate = Carbon::create(2019, 10, 27 );
                Carbon::setTestNow($knownStartDate);   
                $startOfMonth = $knownStartDate->startOfMonth();

                $knownEndDate = Carbon::create(2019, 10, 27 );
                Carbon::setTestNow($knownEndDate); 
                $endOfMonth = $knownEndDate->endOfMonth();

                $today = $knownStartDate;
                $week = $today->weekOfMonth;
                $endOfDay = $knownStartDate->endOfDay();
                $package_users = PackageUser::with('packages')->gym()->get();

                foreach ($package_users as $key => $package_user) {
                    $access = $package_user->packages->access;

                    $outstandings = PackageUser::where('id',$package_user->id)->value('outstanding');

                    $userDetails = User::find($package_user->user_id);

                    if ($outstandings > 0) {

                        $outstanding = '0';
                        $remarks = 'Account Overdue! Please proceed to counter.';

                    } else {

                        $outstanding = '1';
                        $remarks = 'Success: Welcome!';

                    }

                    if($access == '-1'){    // unlimited access
                        $attendance = new Attendance;
                        $attendance->user_id        = $package_user->user_id;
                        $attendance->package_id     = $package_user->package_id;
                        $attendance->clockIn        = $knownStartDate->startOfDay();
                        $attendance->status         = 'absent';
                        $attendance->week           = $week;
                        $attendance->clock_in_start = $today;
                        $attendance->clock_in_end   = $endOfDay;
                        $attendance->outlet_id      = $package_user->outlet_id;
                        $attendance->save();


                        // $attendanceDevice = new AttendanceDevice;
                        // $attendanceDevice->attendance_id = $attendance->id;
                        // $attendanceDevice->member_id = $package_user->user_id;
                        // $attendanceDevice->package_user_id = $package_user->id;
                        // $attendanceDevice->payment_signal = $outstanding;
                        // $attendanceDevice->access_signal = '1';
                        // $attendanceDevice->clock_in_start = $today;
                        // $attendanceDevice->clock_in_end = $endOfDay;
                        // $attendanceDevice->clock_in = null;
                        // $attendanceDevice->remarks = $remarks;
                        // $attendanceDevice->created_at = Carbon::now();
                        // $attendanceDevice->updated_at = Carbon::now();
                        // $attendanceDevice->save();


                        // $attendanceDeviceSync = new AttendanceDeviceSync;
                        // $attendanceDeviceSync->attendance_id = $attendance->id;
                        // $attendanceDeviceSync->member_id = $package_user->user_id;
                        // $attendanceDeviceSync->payment_signal = $outstanding;
                        // $attendanceDeviceSync->access_signal = '1';
                        // $attendanceDeviceSync->clock_in = null;
                        // $attendanceDeviceSync->remarks = $remarks;
                        // $attendanceDeviceSync->save();

                        if (!$attendance) {
                            DB::rollback();
                        }

                        // if (!$attendanceDevice) {
                        //     DB::rollback();
                        // }

                        // if (!$attendanceDeviceSync) {
                        //     DB::rollback();
                        // }
                    }
                    else{
                        $countAttendance = Attendance::where([
                            ['user_id', $package_user->user_id],
                            ['package_id', $package_user->package_id],
                            ['outlet_id', $package_user->outlet_id],
                            ['status', 'present'],
                        ])
                        ->whereBetween('clockIn', [$startOfMonth, $endOfMonth])
                        ->count();

                        if ($countAttendance < $access) {  // limited access

                            $attendance = new Attendance;
                            $attendance->user_id        = $package_user->user_id;
                            $attendance->package_id     = $package_user->package_id;
                            $attendance->clockIn        = $knownStartDate->startOfDay();
                            $attendance->status         = 'absent';
                            $attendance->week           = $week;
                            $attendance->clock_in_start = $today;
                            $attendance->clock_in_end   = $endOfDay;
                            $attendance->outlet_id      = $package_user->outlet_id;
                            $attendance->save();


                            // $attendanceDevice = new AttendanceDevice;
                            // $attendanceDevice->attendance_id = $attendance->id;
                            // $attendanceDevice->member_id = $package_user->user_id;
                            // $attendanceDevice->package_user_id = $package_user->id;
                            // $attendanceDevice->payment_signal = $outstanding;
                            // $attendanceDevice->access_signal = '1';
                            // $attendanceDevice->clock_in_start = $today;
                            // $attendanceDevice->clock_in_end = $endOfDay;
                            // $attendanceDevice->clock_in = null;
                            // $attendanceDevice->remarks = $remarks;
                            // $attendanceDevice->created_at = Carbon::now();
                            // $attendanceDevice->updated_at = Carbon::now();
                            // $attendanceDevice->save();


                            // $attendanceDeviceSync = new AttendanceDeviceSync;
                            // $attendanceDeviceSync->attendance_id = $attendance->id;
                            // $attendanceDeviceSync->member_id = $package_user->user_id;
                            // $attendanceDeviceSync->payment_signal = $outstanding;
                            // $attendanceDeviceSync->access_signal = '1';
                            // $attendanceDeviceSync->clock_in = null;
                            // $attendanceDeviceSync->remarks = $remarks;
                            // $attendanceDeviceSync->save();

                            if (!$attendance) {
                                DB::rollback();
                            }

                            // if (!$attendanceDevice) {
                            //     DB::rollback();
                            // }

                            // if (!$attendanceDeviceSync) {
                            //     DB::rollback();
                            // }
                        }
                    }

                    if ($outstandings > 0) {

                        $year = $knownStartDate->format('Y-m-d');
                        $start_time = $year.' 03:00:00';
                        $end_time = $year.' 03:01:00';
                        $today = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                        $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                    }

                    $accLevelIds = '1';

                    $details = new stdClass;
                    $details->pin = strval($userDetails->id);
                    $details->name = $userDetails->name;
                    if($userDetails->gender == 'Male') {
                        $gender = 'M';
                    } else {
                        $gender = 'F';
                    }
                    $details->gender = $gender;
                    $details->accLevelIds = $accLevelIds;
                    $details->accStartTime = $today;
                    $details->accEndTime = $endOfDay;

                    $data = json_encode($details);
                }

                DB::commit();
            }

        // }
    }
}

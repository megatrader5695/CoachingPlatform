<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Order;
use App\User;
use App\PackageUser;
use App\Transaction;

class UpdateInvoiceTotal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update invoice';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | update table invoice
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $transactions = Transaction::all();

        foreach($transactions as $transaction) {

            $oldTotal = $transaction->invoice_total;

            if ($oldTotal > 0 ){

                $valueInString = strval(round($oldTotal,2));

                if (strpos($valueInString, ".") == 0) $valueInString = $valueInString.".00";

                $valueArray = explode(".", $valueInString);

                $substringValue = substr($valueArray[1], 1);
                 
                if ($substringValue == 3 || $substringValue == 4) {

                  $tempValue = str_replace(substr($valueArray[1], 1), 5, substr($valueArray[1], 1));

                  $tempValue = substr($valueArray[1],0,1).$tempValue;

                  $invoice_total_new = floatval($valueArray[0].".".$tempValue);

                } elseif($substringValue == 1 || $substringValue == 2) {

                  $newFloat = floatval($valueArray[0].".".substr($valueArray[1],0,1));

                  $invoice_total_new = ($newFloat+0.0);

                } elseif($substringValue == 6 || $substringValue == 7) {

                  $tempValue = str_replace(substr($valueArray[1], 1), 5, substr($valueArray[1], 1));

                  $tempValue = substr($valueArray[1],0,1).$tempValue;

                  $invoice_total_new = floatval($valueArray[0].".".$tempValue);

                } elseif($substringValue == 8 || $substringValue == 9) {

                  $newFloat = floatval($valueArray[0].".".substr($valueArray[1],0,1));

                  $invoice_total_new = ($newFloat+0.1);

                } else {

                  $invoice_total_new = round($oldTotal, 2);

                }
            } else {
                $invoice_total_new = 0;
            }

            $newUpdate = Transaction::where('id',$transaction->id)
                         ->update([
                            'invoice_total' => $invoice_total_new
                         ]);

            if (!$newUpdate) {
                    DB::rollback();
                }
        }

        DB::commit();
    }
}

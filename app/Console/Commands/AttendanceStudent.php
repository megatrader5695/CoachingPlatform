<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classroom;
use App\ClassroomUser;
use App\Attendance;
use App\AttendanceDevice;
use App\AttendanceDeviceSync;
use App\Transfer;
use App\PackageUser;
use App\Replacement;
use App\User;
use Carbon\Carbon;
use App\Event;
use Carbon\CarbonPeriod;
use DB;
use stdClass;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AttendanceStudent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance:students {outletId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate student attendace';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        /*
           |--------------------------------------------------------------------------
           | For API and general testing 
           |--------------------------------------------------------------------------
        */
        // dd('hi');
        // $details = new stdClass;
        // $details->pin = '1599';
        // $details->name = 'LOW CHENG YE';
        // $details->gender = 'M';
        // $details->accLevelIds = '1';
        // $details->accStartTime = date("Y-m-d H:i:s", strtotime('2020-07-2403:00:00'));
        // $details->accEndTime = date("Y-m-d H:i:s", strtotime('2020-07-2404:00:00'));

        // $data = json_encode($details);

        // $client = new Client([
        //     'headers' => [
        //         'Content-Type' => 'application/json'
        //     ]
        // ]); //GuzzleHttp\Client
        // $url = 'http://vgoaquatic.asuscomm.com:8098/api/person/add?access_token=62C26268C2';
        // // $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

        // try {
        //     $result = $client->post($url, [
        //         'body' => $data,
        //         'timeout' => 20
        //     ]);

        //     dd($result->getBody());

        // } 
        // catch (GuzzleHttp\Exception\ClientException $e){
        //     // echo 'hello';
        // }




        // dd('hi');
        $outletId = $this->argument('outletId');

        // echo $outletId;

        $startTodayTime = Carbon::now()->startOfDay();
        $endTodayTime = Carbon::now()->endOfDay();

        $getTotalAttendanceToday = Attendance::whereBetween('created_at',[$startTodayTime, $endTodayTime])
                                    ->where('outlet_id',$outletId)
                                    ->count();

        if($getTotalAttendanceToday == 0) {

            /*
            |--------------------------------------------------------------------------
            | Check Events
            |--------------------------------------------------------------------------
            */

            $events = Event::where(function ($q) {
                      $q->where('start', '<=', Carbon::now());
                      $q->where('end', '>=', Carbon::now());
                    })
                    ->value('calendar_id');

            if($events == '' || $events == 1 || $events == 4 || $events == 5) {

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Swim Member
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                $current_date = Carbon::now();
                $today = $current_date->format('Y-m-d');
                $dayname = strtolower($current_date->format('l'));

                $classrooms = Classroom::with('users')
                            ->where('day', $dayname)
                            ->where('outlet_id', $outletId)
                            ->active()
                            ->get();

                // $attendance_array = [];

                foreach ($classrooms as $classroom)
                {   
                    foreach ($classroom->users as $student) {

                        $checkClassroomUsers = ClassroomUser::with('packageUsers')
                                              ->where('user_id',$student->id)
                                              ->where('classroom_id',$classroom->id)
                                              ->where('status','active')
                                              ->get();

                        foreach ($checkClassroomUsers as $classroomUser) {

                            if (!is_null($classroomUser->packageUsers)) {

                                if($classroomUser->packageUsers->status == 'active') {

                                    $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classroom->start)));

                                    $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classroom->end)));

                                    $transfersOut = Transfer::where('fromClassroom', $classroom->id)->where('member_id', $student->id)->where('fromDate', $today)->first();

                                    $outstandings = $classroomUser->packageUsers->outstanding;

                                    if ($outstandings > 0) {

                                        $outstanding = '0';
                                        $remarks = 'Access Denied!';

                                        $year = Carbon::now()->subDays(1)->format('Y-m-d');
                                        $start_time = $year.' 03:00:00';
                                        $end_time = $year.' 03:01:00';
                                        $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                                        $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                                    } else {

                                        $outstanding = '1';
                                        $remarks = 'Access Denied!';

                                    }

                                    $attendance = new Attendance;
                                    $attendance->user_id = $student->id;
                                    $attendance->class_id = $classroom->id;
                                    $attendance->package_id = $student->pivot->package_id;
                                    $attendance->clockIn = Carbon::now();
                                    $attendance->status = 'absent';
                                    $attendance->replacement_id = is_null($transfersOut) ? null : $transfersOut->id;
                                    $attendance->week = $current_date->weekOfMonth;
                                    $attendance->clock_in_start = $clock_in_start;
                                    $attendance->clock_in_end = $clock_in_end;
                                    $attendance->outlet_id = $classroom->outlet_id;
                                    $attendance->created_at = Carbon::now();
                                    $attendance->updated_at = Carbon::now();
                                    $attendance->save();
                                    
                                    // array_push($attendance_array, $attendance);

                                    if (!is_null($transfersOut)) {

                                        $attendance = Attendance::where('replacement_id', $transfersOut->id)->whereDate('clockIn', $today)->forceDelete();
                                    }

                                    if (!$attendance) {
                                        DB::rollback();
                                    }

                                    $accLevelIds = '1';

                                    $details = new stdClass;
                                    $details->pin = strval($student->id);
                                    $details->name = $student->name;
                                    if($student->gender == 'Male') {
                                        $gender = 'M';
                                    } else {
                                        $gender = 'F';
                                    }
                                    $details->gender = $gender;
                                    $details->accLevelIds = $accLevelIds;
                                    $details->accStartTime = $clock_in_start;
                                    $details->accEndTime = $clock_in_end;
                                    // dd($details);
                                    

                                    $data = json_encode($details);

                                    // echo '1';
                                    // echo $data;


                                }

                            }

                        }

                    }

                }
                // dd('stop');
                // dd($attendance_array);

                $transfersIn = Transfer::where('toDate', $today)->get();

                foreach ($transfersIn as $value) {
                    // dd('hi');
                    $classrooms = Classroom::findOrFail($value->fromClassroom);


                    if ($classrooms->outlet_id == $outletId) {

                        $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classrooms->start)));

                        $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classrooms->end)));
                        
                        $classroom_user = $classrooms->users()->wherePivot('user_id', $value->member_id)->first();

                        $outstandings = PackageUser::where('id',$classroom_user->pivot->package_user_id)->value('outstanding');

                        $userDetails = User::find($value->member_id);

                        if ($outstandings > 0) {

                            $outstanding = '0';
                            $remarks = 'Access Denied!';

                            $year = Carbon::now()->subDays(1)->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        } else {

                            $outstanding = '1';
                            $remarks = 'Access Denied!';

                        }

                        $attendance = new Attendance;
                        $attendance->user_id = $value->member_id;
                        $attendance->class_id = $value->toClassroom;
                        $attendance->package_id = $classroom_user->pivot->package_id;
                        $attendance->clockIn = Carbon::now();
                        $attendance->status = 'absent';
                        $attendance->replacement_id = $value->id;
                        $attendance->week = $current_date->weekOfMonth;
                        $attendance->clock_in_start = $clock_in_start;
                        $attendance->clock_in_end = $clock_in_end;
                        $attendance->outlet_id = $classrooms->outlet_id;
                        $attendance->save();

                        // array_push($attendance_array, $attendance);

                        if (!$attendance) {
                            DB::rollback();
                        }

                        if ($outstandings > 0) {

                            $year = Carbon::now()->subDays(1)->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        }

                        $accLevelIds = '1';

                        $details = new stdClass;
                        $details->pin = strval($userDetails->id);
                        $details->name = $userDetails->name;
                        if($userDetails->gender == 'Male') {
                            $gender = 'M';
                        } else {
                            $gender = 'F';
                        }
                        $details->gender = $gender;
                        $details->accLevelIds = $accLevelIds;
                        $details->accStartTime = $clock_in_start;
                        $details->accEndTime = $clock_in_end;

                        $data = json_encode($details);

                    }

                    

                }

                DB::commit();

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Swim Member (Class Replacement)
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                
                $replacements = Replacement::all();
                $current_date = Carbon::now();
                $today = $current_date->format('Y-m-d');

                foreach ($replacements as $key => $replacement) {

                    if ($today == $replacement->beforeDate) {

                        // remove data attendances if replacement class is today.
                        $attendances = Attendance::where('class_id', $replacement->classroom_id)->whereDate('clock_in_start', $today)->forceDelete();

                    }

                    if ($today == $replacement->afterDate) {
                        
                        // add data attendances if replacement class is today.
                        foreach ($classroom->users as $key => $user) {

                            $classroom = Classroom::find($replacement->classroom_id);

                            if ($classroom->outlet_id == $outletId) {

                                $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classroom->start)));

                                $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classroom->end)));

                                $outstandings = PackageUser::where('id',$user->pivot->package_user_id)->value('outstanding');

                                $userDetails = User::find($user->id);

                                if ($outstandings > 0) {

                                    $outstanding = '0';
                                    $remarks = 'Access Denied!';

                                    $year = Carbon::now()->subDays(1)->format('Y-m-d');
                                    $start_time = $year.' 03:00:00';
                                    $end_time = $year.' 03:01:00';
                                    $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                                    $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                                } else {

                                    $outstanding = '1';
                                    $remarks = 'Access Denied!';

                                }

                                $attendance = new Attendance;
                                $attendance->user_id = $user->id;
                                $attendance->class_id = $classroom->id;
                                $attendance->package_id = $user->pivot->package_id;
                                $attendance->clockIn = Carbon::now();
                                $attendance->status = 'absent';
                                $attendance->replacement_id = $replacement->id;
                                $attendance->week = $current_date->weekOfMonth;
                                $attendance->clock_in_start = $clock_in_start;
                                $attendance->clock_in_end = $clock_in_end;
                                $attendance->outlet_id = $classroom->outlet_id;
                                $attendance->save();

                                if (!$attendance) {
                                    DB::rollback();
                                }

                                if ($outstandings > 0) {

                                    $year = Carbon::now()->subDays(1)->format('Y-m-d');
                                    $start_time = $year.' 03:00:00';
                                    $end_time = $year.' 03:01:00';
                                    $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                                    $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                                }

                                $accLevelIds = '1';

                                $details = new stdClass;
                                $details->pin = strval($userDetails->id);
                                $details->name = $userDetails->name;
                                if($userDetails->gender == 'Male') {
                                    $gender = 'M';
                                } else {
                                    $gender = 'F';
                                }
                                $details->gender = $gender;
                                $details->accLevelIds = $accLevelIds;
                                $details->accStartTime = $clock_in_start;
                                $details->accEndTime = $clock_in_end;

                                $data = json_encode($details);

                            }

                        }
                    }
                }

                DB::commit();

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Gym Member
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                $startOfMonth = Carbon::now()->startOfMonth();
                $endOfMonth = Carbon::now()->endOfMonth();
                $create_today = Carbon::today()->format('Y-m-d').' 00:00:00';
                $create_endOfDay = Carbon::today()->endOfDay()->format('Y-m-d').' 00:00:00';

                $today = Carbon::createFromFormat('Y-m-d H:i:s', $create_today)->toDateTimeString();
                $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $create_endOfDay)->toDateTimeString();

                $get_today = Carbon::today();
                $week = $get_today->weekOfMonth;
                
                $package_users = PackageUser::with('packages')->gym()->get();

                foreach ($package_users as $key => $package_user) {

                    if ($package_user->outlet_id == $outletId) {

                        $access = $package_user->packages->access;

                        $outstandings = PackageUser::where('id',$package_user->id)->value('outstanding');

                        $userDetails = User::find($package_user->user_id);

                        if ($outstandings > 0) {

                            $outstanding = '0';
                            $remarks = 'Account Overdue! Please proceed to counter.';

                            $year = Carbon::now()->subDays(1)->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $today = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        } else {

                            $outstanding = '1';
                            $remarks = 'Success: Welcome!';

                        }

                        if($access == '-1'){    // unlimited access
                            $attendance = new Attendance;
                            $attendance->user_id        = $package_user->user_id;
                            $attendance->package_id     = $package_user->package_id;
                            $attendance->clockIn        = Carbon::now();
                            $attendance->status         = 'absent';
                            $attendance->week           = $week;
                            $attendance->clock_in_start = $today;
                            $attendance->clock_in_end   = $endOfDay;
                            $attendance->outlet_id      = $package_user->outlet_id;
                            $attendance->save();

                            if (!$attendance) {
                                DB::rollback();
                            }

                        }
                        else{
                            $countAttendance = Attendance::where([
                                ['user_id', $package_user->user_id],
                                ['package_id', $package_user->package_id],
                                ['outlet_id', $package_user->outlet_id],
                                ['status', 'present'],
                            ])
                            ->whereBetween('clockIn', [$startOfMonth, $endOfMonth])
                            ->count();

                            if ($countAttendance < $access) {  // limited access
                                $attendance = new Attendance;
                                $attendance->user_id        = $package_user->user_id;
                                $attendance->package_id     = $package_user->package_id;
                                $attendance->clockIn        = Carbon::now();
                                $attendance->status         = 'absent';
                                $attendance->week           = $week;
                                $attendance->clock_in_start = $today;
                                $attendance->clock_in_end   = $endOfDay;
                                $attendance->outlet_id      = $package_user->outlet_id;
                                $attendance->save();
                                // array_push($attendance_array, $attendance);

                                if (!$attendance) {
                                    DB::rollback();
                                }

                            }

                        }

                        if ($outstandings > 0) {

                            $year = Carbon::now()->subDays(1)->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $today = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        }

                        $accLevelIds = '1';

                        $details = new stdClass;
                        $details->pin = strval($userDetails->id);
                        $details->name = $userDetails->name;
                        if($userDetails->gender == 'Male') {
                            $gender = 'M';
                        } else {
                            $gender = 'F';
                        }
                        $details->gender = $gender;
                        $details->accLevelIds = $accLevelIds;
                        $details->accStartTime = $today;
                        $details->accEndTime = $endOfDay;
                        // dd($details);
                        $data = json_encode($details);
                        // dd($details);
                        $client = new Client([
                            'headers' => [
                                'Content-Type' => 'application/json'
                            ]
                        ]); //GuzzleHttp\Client
                        // $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');
                        
                    }
                    
                }

                DB::commit();

            }

            /*
            |--------------------------------------------------------------------------
            | No swimm class
            |--------------------------------------------------------------------------
            */

            if ($events == 3) {

                /*
                |--------------------------------------------------------------------------
                | Add attendance for Gym Member
                |--------------------------------------------------------------------------
                */

                DB::beginTransaction();
                $startOfMonth = Carbon::now()->startOfMonth();
                $endOfMonth = Carbon::now()->endOfMonth();
                
                $create_today = Carbon::today()->format('Y-m-d').' 00:00:00';
                $create_endOfDay = Carbon::today()->endOfDay()->format('Y-m-d').' 00:00:00';

                $today = Carbon::createFromFormat('Y-m-d H:i:s', $create_today)->toDateTimeString();
                $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $create_endOfDay)->toDateTimeString();

                $get_today = Carbon::today();

                $week = $get_today->weekOfMonth;
                
                $package_users = PackageUser::with('packages')->gym()->get();

                foreach ($package_users as $key => $package_user) {

                    $today = Carbon::today();

                    $endOfDay = Carbon::today()->endOfDay();

                    if ($package_user->outlet_id == $outletId) {

                        $access = $package_user->packages->access;

                        $outstandings = PackageUser::where('id',$package_user->id)->value('outstanding');

                        $userDetails = User::find($package_user->user_id);

                        if ($outstandings > 0) {

                            $outstanding = '0';
                            $remarks = 'Account Overdue! Please proceed to counter.';

                            $year = Carbon::now()->subDays(1)->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $today = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        } else {

                            $outstanding = '1';
                            $remarks = 'Success: Welcome!';

                        }

                        if($access == '-1'){    // unlimited access
                            $attendance = new Attendance;
                            $attendance->user_id        = $package_user->user_id;
                            $attendance->package_id     = $package_user->package_id;
                            $attendance->clockIn        = Carbon::now();
                            $attendance->status         = 'absent';
                            $attendance->week           = $week;
                            $attendance->clock_in_start = $today;
                            $attendance->clock_in_end   = $endOfDay;
                            $attendance->outlet_id      = $package_user->outlet_id;
                            $attendance->save();

                            if (!$attendance) {
                                DB::rollback();
                            }

                        }

                        else{
                            $countAttendance = Attendance::where([
                                ['user_id', $package_user->user_id],
                                ['package_id', $package_user->package_id],
                                ['outlet_id', $package_user->outlet_id],
                                ['status', 'present'],
                            ])
                            ->whereBetween('clockIn', [$startOfMonth, $endOfMonth])
                            ->count();

                            if ($countAttendance < $access) {  // limited access

                                $attendance = new Attendance;
                                $attendance->user_id        = $package_user->user_id;
                                $attendance->package_id     = $package_user->package_id;
                                $attendance->clockIn        = Carbon::now();
                                $attendance->status         = 'absent';
                                $attendance->week           = $week;
                                $attendance->clock_in_start = $today;
                                $attendance->clock_in_end   = $endOfDay;
                                $attendance->outlet_id      = $package_user->outlet_id;
                                $attendance->save();

                                if (!$attendance) {
                                    DB::rollback();
                                }

                            }

                        }

                        if ($outstandings > 0) {

                            $year = Carbon::now()->subDays(1)->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $today = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $endOfDay = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        }

                        $accLevelIds = '1';

                        $details = new stdClass;
                        $details->pin = strval($userDetails->id);
                        $details->name = $userDetails->name;
                        if($userDetails->gender == 'Male') {
                            $gender = 'M';
                        } else {
                            $gender = 'F';
                        }
                        $details->gender = $gender;
                        $details->accLevelIds = $accLevelIds;
                        $details->accStartTime = $today;
                        $details->accEndTime = $endOfDay;

                        $data = json_encode($details);

                        
                        
                    }

                }

                DB::commit();
            }
            
        } 

        else {

            $getAttendanceTodays = Attendance::whereBetween('created_at',[$startTodayTime, $endTodayTime])
                                ->where('outlet_id',$outletId)
                                ->get();


            foreach ($getAttendanceTodays as $getAttendanceToday) {

                $member = User::find($getAttendanceToday->user_id);

                $accLevelIds = '1';

                $details = new stdClass;
                $details->pin = strval($getAttendanceToday->user_id);
                $details->name = $member->name;
                if($member->gender == 'Male') {
                    $gender = 'M';
                } else {
                    $gender = 'F';
                }
                $details->gender = $gender;
                $details->accLevelIds = $accLevelIds;
                $details->accStartTime = $getAttendanceToday->clock_in_start;
                $details->accEndTime = $getAttendanceToday->clock_in_end;

                $data = json_encode($details);

                

            }
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PackageUser;
use App\Order;
use App\OrderSubscription;
use Carbon\Carbon;
use DB;

class CreateOldOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:oldOlder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create missing order between start date till current now';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        $package_users = PackageUser::with('packages')->get();

        $fat = [];
        foreach ($package_users as $key => $pu) {
            if ($pu->status == 'active') {
                $orders = Order::where('package_user_id', $pu->id)->get();
                $expiry = new Carbon($pu->start_date);
                $current_date = Carbon::now();
                $diffInMonths = $expiry->diffInMonths($current_date, false) + 1;
                $countOrder = ceil($diffInMonths / $pu->packages->subscription);
                $index = $countOrder - $orders->count();

                $latestOrder = Order::where('package_user_id', $pu->id)->orderByDesc('monthly_payment')->first();
                $newExpiry = new Carbon($latestOrder->monthly_payment);
                for ($i=1; $i <= $index ; $i++) {

                    if ($latestOrder) {
                        $getSingleOrder = Order::where('package_user_id', $pu->id)->latest()->first();
                        $insertOrder = [
                            'package_user_id'   => $getSingleOrder->package_user_id,
                            'package_price'     => $getSingleOrder->package_price,
                            'package_depo'      => '0',
                            'package_total'     => $getSingleOrder->package_price,
                            'status'            => '1',
                            'member_name'       => $getSingleOrder->member_name,
                            'cust_id'           => $getSingleOrder->cust_id,
                            'package_id'        => $getSingleOrder->package_id,
                            'outlet_id'         => $getSingleOrder->outlet_id,
                            'monthly_payment'   => $newExpiry->addMonths($pu->packages->subscription)
                        ];

                        $orders = Order::create($insertOrder);
                        if (!$orders) {
                            DB::rollback();
                        }

                        $pu->expiry = $newExpiry;
                        $pu->save();
                        if (!$pu) {
                            DB::rollback();
                        }

                        $condition = [
                            'package_user_id'   => $pu->id
                        ];

                        $insert = [
                            'order_id'  => $pu->id,
                            'month'     => $newExpiry->month,
                            'year'      => $newExpiry->year,
                        ];

                        $orderSubscsription = OrderSubscription::updateOrCreate($condition, $insert);
                        if (!$orderSubscsription) {
                            DB::rollback();
                        }
                    }
                }
            }
        }
        DB::commit();

        echo 'success run';
    }
}

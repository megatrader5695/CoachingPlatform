<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Order;
use App\User;
use App\PackageUser;
use App\AttendanceDevice;
use App\AttendanceDeviceSync;

class TruncateAttendanceDevice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'truncate:attendanceDevice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate table attendance_device';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | update table package_user
        |--------------------------------------------------------------------------
        */
        DB::beginTransaction();
        
        $attendaceDevice = AttendanceDevice::truncate();

        $attendaceDeviceSync = AttendanceDeviceSync::truncate();

        if (!$attendaceDevice) {
            DB::rollback();
        }

        if (!$attendaceDeviceSync) {
            DB::rollback();
        }

        DB::commit();
    }
}

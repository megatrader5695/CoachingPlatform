<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\LeaveUsers;
use Carbon\Carbon;
use DB;

class UpdateLeaveUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leave:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |------------------------------------------------------------------------------------
        | Change status to 2 when admin still not approve / reject after selected start date
        |------------------------------------------------------------------------------------
        */
        DB::beginTransaction();
        $today = Carbon::now()->format('Y-m-d');

        $leaves = LeaveUsers::where('approve', '0')->whereDate('leave_user.start', '<=', $today)->get();

        foreach ($leaves as $key => $leave) {
            $leave->approve = 2;
            $leave->save();

            if (!$leave) {
                DB::rollback();
            }
        }
        DB::commit();
    }
}

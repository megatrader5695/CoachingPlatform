<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Package;
use App\PackageUser;
use App\Attendance;
use App\Order;
use App\ClassroomUser;
use App\Classroom;
use App\User;
use App\Outlet;
use Carbon\Carbon;
use DB;
use stdClass;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class AttendanceDeviceApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateAttendance:api';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update attendance:api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |------------------------------------------------------------------------------------
        | Auto terminate
        |------------------------------------------------------------------------------------
        */
        DB::beginTransaction();

        $startOfDay = Carbon::now()->startOfDay();
        $endOfDay = Carbon::today()->endOfDay();

        $outlets = Outlet::all();

        foreach($outlets as $outlet){

            if($outlet->facial_recognitions == '1') {

                $attendances = Attendance::whereBetween('clock_in_start',[$startOfDay,$endOfDay])
                            ->where('outlet_id',$outlet->id)
                            ->get();

                foreach ($attendances as $attendance) {

                    $packageUsers = PackageUser::where('user_id',$attendance->user_id)
                                    ->where('package_id',$attendance->package_id)
                                    ->where('status','active')
                                    ->first();

                    $userDetails = User::find($attendance->user_id);

                    $classrooms = Classroom::find($attendance->class_id);

                    if($packageUsers) {
                        if ($packageUsers->outstanding < 1) {

                            $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classrooms->start)));
                            $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classrooms->end)));
                            
                        } else {

                            $year = Carbon::now()->format('Y-m-d');
                            $start_time = $year.' 03:00:00';
                            $end_time = $year.' 03:01:00';
                            $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                            $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                        }

                        $accLevelIds = '1';

                        $details = new stdClass;
                        $details->pin = strval($userDetails->id);
                        $details->name = $userDetails->name;
                        if($userDetails->gender == 'Male') {
                            $gender = 'M';
                        } else {
                            $gender = 'F';
                        }
                        $details->gender = $gender;
                        $details->accLevelIds = $accLevelIds;
                        $details->accStartTime = $clock_in_start;
                        $details->accEndTime = $clock_in_end;

                        $data = json_encode($details);

                        

                        if ($status != 200 || $status == null) {

                            echo 'Device offline';

                        } else {

                            
                        }
                    }
                }
            }
        }
        
        DB::commit();

    }
}

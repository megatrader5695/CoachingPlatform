<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OrderSubscription;
use App\Order;
use App\Transaction;
use App\PackageUser;
use App\Inventory;
use Carbon\Carbon;
use DB;

class ManualUpdateTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manual:updateTransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'manual update transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        $date = carbon::parse('2020-02-01');
        $start_of_month = $date->startofMonth()->format('Y-m-d');
        $end_of_month = carbon::parse('2020-03-01')->endofMonth()->format('Y-m-d');

        $getTransactions = Transaction::where('status','0')->whereBetween('created_at',[$start_of_month,$end_of_month])->get();
        // dd($start_of_month, $end_of_month, $getTransactions->count());
        //dd($getTransactions->count());
        foreach ($getTransactions as $getTransaction) {

            //get data from order
            if ($getTransaction->order_id != 0) {

                $orders = Order::find($getTransaction->order_id);
                
                $updateTransaction = Transaction::find($getTransaction->id);

                if($orders){
                    $updateTransaction->package_id = $orders->package_id;
                    $updateTransaction->item_price = $orders->package_price;
                    $updateTransaction->package_depo = $orders->package_depo;
                    $updateTransaction->reg_fee = $orders->registration_fee;
                    $updateTransaction->save();
                
                }else{
                    $updateTransaction->package_id = 0;
                    $updateTransaction->item_price = 0;
                    $updateTransaction->package_depo = 0;
                    $updateTransaction->reg_fee = 0;
                    $updateTransaction->save(); 
                    // dd($something);
                }

            }

            //get data from product inventories
            if ($getTransaction->product_id != 0) {

                $products = Inventory::find($getTransaction->product_id);

                $updateTransaction = Transaction::find($getTransaction->id);

                // $updateTransaction->package_id = 0;
                $updateTransaction->package_id = 0;
                $updateTransaction->item_price = $products->price;
                $updateTransaction->package_depo = 0;
                $updateTransaction->reg_fee = 0;
                // $updateTransaction->reg_fee = $orders->registration_fee;

                $updateTransaction->save();

            }
            
        }
        
        DB::commit();

        echo 'success run';
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Temporary;
use App\Package;
use App\PackageUser;
use App\Order;
use App\OrderSubscription;
use App\UserChangePackage;
use App\ClassroomUser;
use Carbon\Carbon;
use DB;

class UpdatePackageUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:packageuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Package User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
        |--------------------------------------------------------------------------
        | update member package
        |--------------------------------------------------------------------------
        */

        DB::beginTransaction();
        $current_month = Carbon::now()->startOfMonth();
        $temporary = Temporary::where('start_date', $current_month)->get(); //$current_month->format('Y-m-d')

        foreach ($temporary as $key => $temp) {
            $date = new Carbon($temp->start_date);

            $packageuser = PackageUser::with('packages', 'user')->where('temporary_id', $temp->id)->first();

            $order = Order::where('package_user_id', $packageuser->id)->where('monthly_payment', $date->format('Y-m-d'))->first();
            $oldPackage = Package::find($temp->from_package_id);
            $newPackage = Package::find($temp->to_package_id);

            // $deposit = $newPackage->deposit > $oldPackage->deposit ? $newPackage->deposit - $oldPackage->deposit : 0;
            // $packageuser->user->parent;

            if ($newPackage->deposit > $oldPackage->deposit) {

              //current package < new package
              $newprice = $newPackage->price;
              $deposit = $newPackage->deposit - $oldPackage->deposit;

            } else {

              //current package > new package
              $newprice = $newPackage->price - ($oldPackage->deposit - $newPackage->deposit);
              $deposit = 0;

            }

            if ($order->status = '1') {
                $insertOrder = [
                    'member_name'       =>  $packageuser->user->name,
                    'cust_id'           =>  $packageuser->user->parent->id,
                    'package_id'        =>  $newPackage->id,
                    'outlet_id'         =>  $packageuser->user->outlet_id,
                    'package_user_id'   =>  $packageuser->id,
                    'package_price'     =>  $newprice,
                    'package_depo'      =>  $deposit,
                    'package_total'     =>  $newprice + $deposit,
                    'monthly_payment'   =>  $date->format('Y-m-d'),
                    'status'            =>  '1',
                    'is_new_member'     =>  '0'
                ];
                $orders = Order::create($insertOrder);

                if (!$orders) {
                    DB::rollback();
                }
                $order->delete();

                $date = new Carbon($orders->monthly_payment);
                $index = -1;
                $newDate = $date->addMonths($index + $newPackage->subscription);
                $month  = $newDate->month;
                $year  = $newDate->year;

                $insertOrderSubscription = [
                    'order_id'     => $orders->id,
                    'package_user_id'   => $orders->package_user_id,
                    'month'             => $month,
                    'year'              => $year
                ];

                $orderSubscription = OrderSubscription::where('package_user_id', $packageuser->id)->update($insertOrderSubscription);
                if (!$orderSubscription) {
                    DB::rollback();
                }

                $updatePackageUser = PackageUser::find($packageuser->id);
                $updatePackageUser->package_id  = $temp->to_package_id;
                $updatePackageUser->start_date  = $temp->start_date;
                $updatePackageUser->temporary_id  = $temp->id;
                $updatePackageUser->deposit     = $newPackage->deposit;
                // $updatePackageUser->deposit     = $newPackage->deposit > $oldPackage->deposit ? $newPackage->deposit : $oldPackage->deposit;
                $updatePackageUser->save();

                if (!$updatePackageUser) {
                    DB::rollback();
                }
            }
        }
        DB::commit();

        /*
        |--------------------------------------------------------------------------
        | update member package user status except terminate
        |--------------------------------------------------------------------------
        */

        DB::beginTransaction();

        $current_month = Carbon::now()->startOfMonth();
        $userChangeStatus = UserChangePackage::where('start_date', $current_month->format('Y-m-d'))->get();

        foreach ($userChangeStatus as $key => $value) {
            $packageuser = PackageUser::find($value->package_user_id);
            $date = new Carbon($value->start_date);
            if ($value->status == 'terminate' && $packageuser->status!='terminate') {
                $order = Order::where('package_user_id', $packageuser->id)->where('status', '1')->first();
                // if($packageuser->id == '636'){
                //     dd($packageuser->deposit);
                // }
                $insertOrder = [
                    'package_user_id'           => $packageuser->id,
                    'package_price'             => $packageuser->deposit,
                    'package_depo'              => '0',
                    'package_total'             => $packageuser->deposit,
                    'registration_fee_status'   => '1',
                    'status'                    => '1',
                    'member_name'               => $packageuser->user->name,
                    'cust_id'                   => $packageuser->user->user_id,
                    'package_id'                => $packageuser->packages->id,
                    'outlet_id'                 => $packageuser->outlet_id,
                    'monthly_payment'           => $date->format('Y-m-d')
                ];

                $orders = Order::create($insertOrder);
                if (!$orders) {
                    DB::rollback();
                }

                if($order){
                    $order->delete(); // delete latest unpaid bill
                }

                $newDate = new Carbon($value->start_date);
                $month  = $newDate->month;
                $year  = $newDate->year;

                $insertOrderSubscription = [
                    'order_id'     => $orders->id,
                    'package_user_id'   => $orders->package_user_id,
                    'month'             => $month,
                    'year'              => $year
                ];

                $orderSubscription = OrderSubscription::create($insertOrderSubscription);
                if (!$orderSubscription) {
                    DB::rollback();
                }

                // $packageuser->deposit = '0';
                $packageuser->save();
                if (!$packageuser) {
                    DB::rollback();
                }
            }

            if ($value->status == 'freeze') {
              $order = Order::where('package_user_id', $packageuser->id)->where('status', '1')->first();
               // if($packageuser->id == '636'){
               //      dd($packageuser->deposit);
               //  }
                $insertOrder = [
                    'package_user_id'   => $packageuser->id,
                    'package_price'     => '0',
                    'package_depo'      => '0',
                    'freeze_price'      => '0',
                    'package_total'     => '0',
                    'status'            => '1',
                    'member_name'       => $packageuser->user->name,
                    'cust_id'           => $packageuser->user->user_id,
                    'package_id'        => $packageuser->packages->id,
                    'outlet_id'         => $packageuser->outlet_id,
                    'monthly_payment'   => $date->format('Y-m-d')
                ];
                $orders = Order::create($insertOrder);
                if (!$orders) {
                    DB::rollback();
                }

                // $newDate = new Carbon($request->start);
                // $month  = $newDate->month;
                // $year  = $newDate->year;

                // $insertOrderSubscription = [
                //     'order_id'     => $orders->id,
                //     'package_user_id'   => $orders->package_user_id,
                //     'month'             => $month,
                //     'year'              => $year
                // ];

                // $orderSubscription = OrderSubscription::create($insertOrderSubscription);
                // if (!$orderSubscription) {
                //     DB::rollback();
                // }

                if ($packageuser->outstanding > 0) {

                  $newExpirys = $packageuser->outstanding - $orders->package_total;

                  if ($newExpirys < 0) {
                    $newExpirys = 0;
                  }

                  $newExpiry = $newExpirys + $value->freeze_price;

                  $packageuser->outstanding = $newExpiry;
                  // $getpackage->status = 'freeze';
                  $packageuser->save();

                } else {

                  $newExpiry = $value->freeze_price;

                  $packageuser->outstanding = $newExpiry;
                  // $getpackage->status = 'freeze';
                  $packageuser->save();

                }

                if (!$packageuser) {
                    DB::rollback();
                }

                $order->delete(); // delete latest unpaid bill
            }

            if ($value->status == 'terminate' || $value->status == 'stop') {
                $classroom_user = ClassroomUser::where('classroom_id', $value->class)->where('user_id', $packageuser->user->id)->first();
                if ($classroom_user) {
                    $classroom_user->delete();
                }
            }

            if($value->status != 'terminate'){
                $packageuser->status = $value->status;
            }
            
            $packageuser->save();
            if (!$packageuser) {
                DB::rollback();
            }
        }

        DB::commit();

        /*
        |--------------------------------------------------------------------------
        | update member package user terminate status
        |--------------------------------------------------------------------------
        */

        DB::beginTransaction();

        $last_month = Carbon::now()->subMonths(1)->startOfMonth()->format('Y-m-d');
        $userChangeStatus = UserChangePackage::where([['start_date', $last_month],['status','terminate']])->get();

        foreach ($userChangeStatus as $key => $val) {
            
            $packageuser = PackageUser::find($val->package_user_id);
            $packageuser->status = 'terminate';
            $packageuser->save();

            $classroom_user = ClassroomUser::where('package_user_id', $val->package_user_id)->get(); 
            foreach ($classroom_user as $key => $value) {
               $value->delete();
            }
        }
        
        DB::commit();
    }
}

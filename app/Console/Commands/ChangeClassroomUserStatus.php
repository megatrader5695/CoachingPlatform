<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ClassroomUser;
use App\PackageUser;
use App\Classroom;
use DB;
use Carbon\Carbon;

class ChangeClassroomUserStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:classroomUser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change member status for Classroom User when the status is waiting_next_month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();

        $classroomusers = ClassroomUser::whereIn('status',['waiting_next_month','waiting'])->get();

        $current_month_year = Carbon::now()->format('Y-m');

        foreach ($classroomusers as $classroomuser) {
            
            $check_start_date = PackageUser::where('id',$classroomuser->package_user_id)->value('start_date');

            $check_total_member_in_class = Classroom::where('id',$classroomuser->classroom_id)
                                            ->value('maximum');

            // echo 'id ='.$classroomuser->classroom_id.' ';

             // echo 'maximum ='.$check_total_member_in_class.' ';

            $check_current_total_member_in_class = ClassroomUser::where('classroom_id',$classroomuser->classroom_id)->where('status','active')->count();

            // $check_total_member_in_class = 1;

            // echo 'current ='.$check_total_member_in_class.' ';

            // echo $current_month_year;

            // echo $check_start_date;

            // echo $check_total_member_in_class;

            // echo $check_current_total_member_in_class;


            if ($check_start_date == $current_month_year) {

                // echo 'helo';

                if ($check_current_total_member_in_class < $check_total_member_in_class) {

                    // echo 'helo0';

                    $updateStatus = ClassroomUser::where('id', $classroomuser->id)
                                ->update([
                                    'status'  => 'active',
                                ]);

                    // $insertStatus = [
                    //     'status'     => 'active',
                    // ];

                } else {

                    $updateStatus = ClassroomUser::where('id', $classroomuser->id)
                                ->update([
                                    'status'  => 'waiting',
                                ]);

                    // $insertStatus = [
                    //     'status'     => 'waiting',
                    // ];

                }

                // $updateStatus = ClassroomUser::where('id', $classroomuser->id)
                //                 ->update($insertStatus);

                // if (!$updateStatus) {
                //     DB::rollback();
                // }
            }
        }

        DB::commit();

        /*
        |--------------------------------------------------------------------------
        | update classroom user replacement credit
        |--------------------------------------------------------------------------
        */

        DB::beginTransaction();

        $replacement_classroom_users = ClassroomUser::all();

        foreach ($replacement_classroom_users as $key => $replacement_user) {
            $replacement_user->replacement_credit = '1';
            $replacement_user->save();
        }
        
        DB::commit();
        echo'succes run';
    }
}

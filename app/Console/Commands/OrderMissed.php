<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PackageUser;
use App\Order;
use App\OrderSubscription;
use Carbon\Carbon;
use DB;

class OrderMissed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create single missing order accordingly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        $id_arr = ['1001058', '1001057', '1001056', '1001055', '1001002', '1001001', '1000810', '1000762', '1000673', '1000639', '1000635', '1000634', '1000558', '1000549', '1000541', '1000540', '1000538', '1000456', '1000435', '1000434', '1000433', '1000432', '1000431', '1000413', '1000412', '1000410', '1000313', '1000253', '1000252', '1891', '1847', '1727'];
        $package_users = PackageUser::with('packages')->whereIn('user_id',$id_arr)->get();
        
        $fat = [];
        foreach ($package_users as $key => $pu) {
            if ($pu->status == 'active'){
                $expiry = Carbon::parse()->subMonth(1)->startofMonth()->format('Y-m-d');
                $current_date = Carbon::now();
                //$parent = ;
                
                
                        $insertOrder = [
                            'package_user_id'   => $pu->id,
                            'package_price'     => $pu->packages->price,
                            'package_depo'      => '0',
                            'package_total'     => $pu->packages->price,
                            'status'            => '1',
                            'member_name'       => $pu->user->name,
                            'cust_id'           => $pu->user->parent->id,
                            'package_id'        => $pu->package_id,
                            'outlet_id'         => $pu->outlet_id,
                            'monthly_payment'   => $expiry
                        ];

                        $orders = Order::create($insertOrder);
                        if (!$orders) {
                            DB::rollback();
                        }

                        $pu->expiry = $expiry;
                        $pu->outstanding = $orders->package_total;
                        $pu->save();
                        if (!$pu) {
                            DB::rollback();
                        }

                        $condition = [
                            'package_user_id'   => $pu->id
                        ];

                        $calculation = Carbon::parse($orders->monthly_payment)->addMonths(($pu->packages->subscription-1))->startOfMonth()->format('Y-m-d');
                        $date = Carbon::createFromFormat('Y-m-d', $calculation);

                        //dd($date->year);
                        $insert = [
                            'order_id'  => $orders->id,
                            'month'     => $date->month,
                            'year'      => $date->year,
                        ];

                        $orderSubscsription = OrderSubscription::updateOrCreate($condition, $insert);
                        if (!$orderSubscsription) {
                            DB::rollback();
                        }
                    
                
            }
        }
        DB::commit();

        echo 'success run';
    }
}
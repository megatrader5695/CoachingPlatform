<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\LogUpdatePayment',
        'App\Console\Commands\LogUpdateFreeze',
        'App\Console\Commands\LogReplacement',
        'App\Console\Commands\AttendanceStudent',
        'App\Console\Commands\AttendanceStudentManual',
        'App\Console\Commands\ManualAttendanceStudent1',
        'App\Console\Commands\CalculateOutstanding',
        'App\Console\Commands\DeleteGymAbsent',
        'App\Console\Commands\UpdateLeaveUser',
        'App\Console\Commands\UpdatePackageUser',
        'App\Console\Commands\MissingOrderManuallyCreated',
        'App\Console\Commands\UpdatePackageExpiry',
        'App\Console\Commands\UpdateMonthlyPayment',
        'App\Console\Commands\UpdateOrderSubscription',
        'App\Console\Commands\ChangeClassroomUserStatus',
        'App\Console\Commands\ManualOrderCheckout',
        'App\Console\Commands\ManualUpdateForMissingOrder',
        'App\Console\Commands\OrderMissed',
        'App\Console\Commands\DashboardData',
        'App\Console\Commands\UpdateSalesOverview',
        'App\Console\Commands\UpdateInvoiceTotal',
        'App\Console\Commands\UpdateAttendanceDevice',
        'App\Console\Commands\TruncateAttendanceDevice',
        'App\Console\Commands\TerminatePackage',
        'App\Console\Commands\ActiveFreezePackage',
        'App\Console\Commands\AttendanceDeviceApi',
        'App\Console\Commands\GetClockInDeviceApi',
        'App\Console\Commands\ManualAddMemberToDevice',
        'App\Console\Commands\UpdateAvatar',
        'App\Console\Commands\GenerateAllOrder',
        'App\Console\Commands\CheckApiStatus',
        'App\Console\Commands\AttendanceStudentOffline',
        'App\Console\Commands\CheckApiStatusMorning',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
// ----------- In Use ----------------
        $schedule->command('log:updatepayment')
                ->daily();

        $schedule->command('terminate:package')
                ->dailyAt('00:20');

        $schedule->command('log:replacement')
                ->daily();

        $schedule->command('update:DashboardData')
                ->everyFifteenMinutes();

        $schedule->command('update:UpdateSalesOverview')
                ->dailyAt('01:00');

        // $schedule->command('checkApi:status')
        //         ->dailyAt('00:50');

        $schedule->command('attendance:students')
                ->dailyAt('09:50');

        $schedule->command('delete:gymAbsent')
                ->dailyAt('23:55');

        $schedule->command('calculate:outstanding')
                ->monthlyOn(1, '00:00');

        $schedule->command('update:packageuser')
                ->monthlyOn(1, '00:04');

        $schedule->command('update:classroomUser')
                ->monthlyOn(1, '00:02');

        $schedule->command('leave:user')
                ->daily();

        $schedule->command('activate:freeze')
                ->monthlyOn(1, '00:15');

        // $schedule->command('updateAttendance:api')
        //         ->everyThirtyMinutes()
        //         ->between('05:00', '23:50');
                // ->cron('*/1 5-23 * * *');

        // $schedule->command('checkApi:statusMorning')
        //         ->everyFifteenMinutes()
        //         ->between('05:00', '23:50');

        // $schedule->command('getClockInDevice:api')
        //         ->everyFiveMinutes()
        //         ->between('05:00', '23:50');
// ----------- In Use ----------------

// ----------- Not In Use ----------------
        // $schedule->command('update:attendanceDevice')
        //         ->everyThirtyMinutes()
        //         ->between('05:00', '23:50');

        // $schedule->command('truncate:attendanceDevice')
        //         ->dailyAt('00:10');

        // $schedule->command('attendance:students')
        //         ->dailyAt('00:50');
// ----------- Not In Use ----------------
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
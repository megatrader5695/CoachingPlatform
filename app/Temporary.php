<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temporary extends Model
{
    protected $table = 'temporary';

    protected $fillable = [
        'user_id', 'from_package_id', 'to_package_id', 'start_date', 'expiry'
    ];
}

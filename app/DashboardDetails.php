<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardDetails extends Model
{
    protected $fillable = [
        'outlet_id',
        'month_of_year',
        'month_to_date_sales',
        'year_to_date_sales',
        'today_sales',
        'member_outstanding',
        'outstanding_amount',
        'active_swim_member',
        'terminated_swim_member',
        'freeze_swim_member',
        'number_of_new_swimming_member',
        'annual_sales_target',
        'updated_at'
    ];

    protected $table = 'dashboard_data';
}

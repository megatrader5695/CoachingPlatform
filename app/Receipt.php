<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
     public function outlets(){
        return $this->belongsToMany('App\Outlet');
    }
}

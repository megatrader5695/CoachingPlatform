<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDiscount extends Model
{
    protected $fillable = ['transaction_id','voucher_id','discounted_value','deposit_use','discounted_sst','created_at','updated_at'];

    protected $table = 'transaction_discount';

    // public function product()
    // {
    //     return $this->belongsTo('App\Inventory','product_id');
    // }

    // public function orderss()
    // {
    //     return $this->belongsTo('App\Order','order_id');
    // }

    public function vouchers()
    {
        return $this->belongsTo('App\Voucher','voucher_id');
    }
}

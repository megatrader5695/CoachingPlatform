<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceDeviceSync extends Model
{
	protected $connection = 'mysql2';

	public $timestamps = false;
    
    protected $table = 'ingress';
}

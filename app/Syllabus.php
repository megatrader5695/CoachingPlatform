<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Syllabus extends Model
{
	protected $fillable = [
        
        'level',
        'name',
        'sequence'
    ];

    protected $table = 'syllabi';

    public function questions()
    {
        return $this->hasMany(SyllabusQuestion::class);
    }
}

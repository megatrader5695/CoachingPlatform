<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterDataPackageCategory extends Model
{
	use SoftDeletes;

	const OUTLET = 'Rating For Outlet';
    protected $table =  'master_datas_package_category';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function packages(){
        return $this->belongsToMany('App\Package');
    }

    public function outlets(){
        return $this->belongsToMany('App\Outlet');
    }
}

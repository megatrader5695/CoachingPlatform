<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierOrder extends Model
{
    public function inventory()
    {
    	return $this->belongsToMany('App\Inventory')->withPivot('quantity')->withTimestamps();
    }

    public function supplier()
    {
    	return $this->belongsTo('App\Supplier');
    }

    public function outlet()
    {
    	return $this->belongsTo('App\Outlet');
    }
}

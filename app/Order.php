<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $dates  = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'member_name','cust_id', 'package_id','outlet_id', 'package_user_id', 'package_price', 'package_depo', 'package_total', 'status', 'monthly_payment', 'registration_fee', 'registration_fee_status','freeze_price'
    ];

    public function package()
    {
    	return $this->belongsTo('App\Package', 'package_id');
    }

    public function package_user()
    {
        return $this->belongsTo('App\PackageUser', 'package_user_id');
    }

    public function scopePaid($query)
    {
        return $query->where('status', '0');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction', 'order_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserChangePackage extends Model
{
    protected $table = 'user_change_package';

    protected $dates  = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'package_user_id', 'start_date','end_date', 'status', 'class_id','freeze_price','changed_by'
    ];

    public function packageUsers()
    {
    	return $this->belongsTo('App\PackageUser', 'package_user_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timeslot extends Model
{
    public function packages(){
        return $this->belongsToMany('App\Package');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	protected $fillable = ['message', 'user_id', 'log_id', 'classroom_id', 'date', 'beforeTime', 'afterTime'];

    public function replied()
    {
        return $this->hasOne('App\Log', 'log_id');
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password','provider','provider_id', 'role_id', 'first_time_login', 'outlet_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'health' => 'array',
    ];

    public function emergency(){
        return $this->hasMany('App\People');
    }

    public function staff(){
        return $this->hasOne('App\Staff');
    }

    public function coach(){
        return $this->hasOne('App\Coach');
    }

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function designation(){
        return $this->belongsTo('App\Designation');
    }

    public function outlets(){
        return $this->belongsToMany('App\Outlet');
    }

    public function modules(){
        return $this->belongsToMany('App\Module');
    }

    public function leaves(){
        return $this->belongsToMany('App\Leave')->withPivot('id','start', 'end', 'reason', 'document', 'approve', 'reject_reason');
    }

    public function claims(){
        return $this->belongsToMany('App\Claim')->withPivot('id', 'amount', 'remarks', 'document', 'approve')->withTimestamps();
    }

    public function parent()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function children()
    {
        return $this->hasMany('App\User', 'user_id');
    }

    public function packages(){
        return $this->belongsToMany('App\Package')->withPivot('id','expiry','lastPayment', 'deposit', 'outstanding', 'swim', 'stroke', 'progress', 'badge', 'class', 'status', 'outlet_id', 'memberId', 'start_date', 'user_status_id', 'join_date');
    }

    public function classes(){
        return $this->belongsToMany('App\Classroom')->withPivot('remarks');
    }

    public function schedules(){
        return $this->hasMany('App\Schedule');
    }

    public function healths()
    {
        return $this->belongsToMany('App\Health');
    }

    public function questionnaires()
    {
        return $this->hasMany('App\UserQuestionnaire');
    }

    public function scopeNotPartTime($query)
    {
        $query->where('type', '!=', 'part time')->orWhereNull('type');
    }
}

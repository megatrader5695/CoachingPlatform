<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function packages(){
        return $this->hasMany('App\Package');
    }
}

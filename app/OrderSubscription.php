<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSubscription extends Model
{
    protected $table = 'orders_subscription';

    public function order()
    {
    	return $this->belongsTo('App\Order', 'order_id');
    }

    public function package_user()
    {
        return $this->belongsTo('App\PackageUser', 'package_user_id');
    }

    protected $dates  = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'order_id', 'package_user_id', 'month', 'year',
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Inventory;

class Transaction extends Model
{
	protected $table = 'transactions';

    protected $fillable = ['cust_id','outlet_id','cashier_id','order_id','product_id','voucher_id','quantity','amount_paid','deposit_use','voucher_value','invoice_total','sst_package','payment_method','status','only_reg_fee','package_id','item_price','package_depo','reg_fee','invoice_no','remarks','created_at','updated_at',];

    public function order()
    {
    	return $this->belongsTo('App\Order', 'order_id');
    }

    public function packages()
    {
        return $this->belongsTo('App\Package', 'package_id');
    }

    public function product()
    {
        return $this->belongsTo(Inventory::class,'product_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','cust_id');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Voucher','voucher_id');
    }
}

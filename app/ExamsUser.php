<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamsUser extends Model
{
    protected $table = 'exams_user';
}

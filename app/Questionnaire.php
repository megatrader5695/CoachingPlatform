<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    public function options(){
        return $this->hasMany('App\QuestionnaireOption');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiers extends Model
{
    public function designations(){
        return $this->belongsToMany('App\Designation');
    }
}

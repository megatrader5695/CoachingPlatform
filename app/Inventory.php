<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
class Inventory extends Model
{
	protected $table = 'inventories';

    public function outlets(){
        return $this->belongsToMany('App\Outlet');
    }

    public function categories(){
        return $this->belongsTo(Category::class, 'id');
    }
}

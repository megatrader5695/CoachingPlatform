<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceDevice extends Model
{
    
    protected $table = 'attendance_device';
}

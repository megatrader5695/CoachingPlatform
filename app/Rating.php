<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public function options()
    {
    	return $this->belongsToMany('App\QuestionnaireOption')->withTimestamps();
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}

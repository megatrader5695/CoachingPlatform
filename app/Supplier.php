<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
	use SoftDeletes;
    protected $table = 'suppliers';

    public function inventories()
    {
    	return $this->belongsToMany('App\Inventory')->withTimestamps()->withPivot('price');
    }
}

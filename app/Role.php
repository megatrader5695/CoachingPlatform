<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users(){
        return $this->hasMany('App\User');
    }

    public function claims(){
        return $this->belongsToMany('App\Claim')->withPivot('limit')->withTimestamps();
    }

    public function permissions(){
        return $this->belongsToMany('App\Permission');
    }
}

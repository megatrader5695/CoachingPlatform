<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $casts = [
        'role_id' => 'array'
    ];
}

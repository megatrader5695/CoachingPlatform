<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveUsers extends Model
{
    protected $table = 'leave_user';

    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function leave()
    {
    	return $this->belongsTo('App\Leave');
    }

    public function scopeNotDisapprove($query)
    {
        return $query->where('approve', '!=', 2);
    }
}

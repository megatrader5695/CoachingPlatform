<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
	use SoftDeletes;
	
    protected $table = 'feedbacks';

    public function users(){
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function scopeProcessing($query)
    {
    	return $query->where('status', 'processing');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'user_id', 'day', 'start', 'end', 'package_id', 'outlet_id'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function package(){
        return $this->belongsTo('App\Package');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyllabusQuestion extends Model
{
    public function options()
    {
        return $this->hasMany(QuestionOption::class);
    }
}

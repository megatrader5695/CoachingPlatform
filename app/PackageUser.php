<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageUser extends Model
{
    protected $fillable = [
        'status','outstanding'
    ];
    
    protected $casts = [
        'badge' => 'array',
    ];

    protected $table = 'package_user';

    public function scopeGym($query)
    {
        return $query->where('class', 'GYM');
    }

    public function packages(){
        return $this->belongsTo('App\Package', 'package_id');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function classroom(){
        return $this->hasOne('App\ClassroomUser','package_user_id','id');
    }

    public function order(){
        return $this->hasMany('App\Order','package_user_id','id');
    }
}

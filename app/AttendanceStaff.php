<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceStaff extends Model
{
    protected $fillable = [
        'staff_id',
        'checkIn',
        'checkOut',
        'clockStart',
        'clockEnd',
        'different_time',
        'working_hour',
        'type',
        'status'
    ];

    protected $table = 'attendances_staff';
}

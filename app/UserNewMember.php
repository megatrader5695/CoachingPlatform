<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNewMember extends Model
{
    protected $table = 'users_new_member';

    protected $dates  = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'user_id', 'status', 'category', 'package_category',
    ];
}

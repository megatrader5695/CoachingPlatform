<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
	protected $fillable = [
        'user_id', 'outlet_id', 'outlet_access', 'designation_id', 'department', 'salary', 'workingHour', 'contract', 'approve', 'signature', 'leave', 
    ];

	protected $casts = [
        'outlet_access' => 'array',
    ];

    protected $table = 'staffs';

    public function user(){
        return $this->hasOne('App\User');
    }

    public function departments(){
        return $this->belongsTo('App\Department', 'department');
    }

    public function designations(){
        return $this->belongsTo('App\Designation', 'designation_id');
    }

    public function staffs(){
        return $this->belongsTo('App\User', 'user_id');
    }
}

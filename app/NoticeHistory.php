<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoticeHistory extends Model
{
    use SoftDeletes;
    
    protected $table = 'notice_history';
    protected $dates = ['deleted_at'];

    protected $casts = [
        'role_id' => 'array',
        // 'broadcast' => 'array',
    ];
}

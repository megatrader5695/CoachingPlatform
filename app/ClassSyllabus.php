<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassSyllabus extends Model
{
    protected $casts = [
        'option_id' => 'array',
    ];

    protected $fillable = ['coach_id', 'class_id', 'question_id', 'option_id', 'status', 'syllabus_id', 'remark'];
}
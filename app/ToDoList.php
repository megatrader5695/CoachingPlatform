<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ToDoList extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $casts = [
        'coach_id' => 'array',
    ];

    public function coaches()
    {
    	return $this->belongsToMany('App\User')->withPivot('status');
    }

    public function pendings()
    {
    	return $this->belongsToMany('App\User')->wherePivot('status', 'pending');
    }
}

<?php

namespace App\Http\Controllers;

use App\Temporary;
use Illuminate\Http\Request;

class TemporaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Temporary  $temporary
     * @return \Illuminate\Http\Response
     */
    public function show(Temporary $temporary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Temporary  $temporary
     * @return \Illuminate\Http\Response
     */
    public function edit(Temporary $temporary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Temporary  $temporary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Temporary $temporary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Temporary  $temporary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Temporary $temporary)
    {
        //
    }
}

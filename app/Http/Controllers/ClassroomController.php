<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classroom;
use App\ClassroomUser;
use App\User;
use App\Syllabus;
use App\SyllabusQuestion;
use App\QuestionOption;
use App\ClassSyllabus;
use DB;
use Auth;
use stdClass;
use App\Replacement;
use App\Log;
use DateTime;
use App\Transfer;
use App\Attendance;
use App\PackageUser;
use Carbon\Carbon;
use Carbon\CarbonInterval;

use App\Http\Resources\ClassResources;
use App\Http\Resources\ClassCollection;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ClassroomController extends Controller
{
    private $classAccess = 'class';
    public function index($outlet)
    {  
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->classAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }
        
        $today = Carbon::today();
        
        $classroom = DB::table('classrooms')
                        ->where('outlet_id', $outlet)
                        ->where('status', '1')
                        ->select(DB::raw('day, count(*) as session'))
                        ->groupBy('day')
                        ->get();

        // Get total students by day
        $students = DB::table('classrooms')
                    ->join('classroom_user', 'classroom_user.classroom_id', '=', 'classrooms.id')
                    ->where('outlet_id', $outlet)
                    ->where('classroom_user.status','active')
                    ->select(DB::raw('count(classroom_user.user_id) as student, classrooms.day'))
                    ->groupBy('classrooms.day')
                    ->get();

        $studentsWaiting = DB::table('classrooms')
                    ->join('classroom_user', 'classroom_user.classroom_id', '=', 'classrooms.id')
                    ->where('outlet_id', $outlet)
                    ->where(function ($query) {
                        $query->where('classroom_user.status', '=', 'waiting')
                              ->orWhere('classroom_user.status', '=', 'waiting_next_month');
                    })
                    ->select(DB::raw('count(classroom_user.user_id) as student, classrooms.day'))
                    ->groupBy('classrooms.day')
                    ->count();

        $result = [];

        foreach ($classroom as $key => $c) {
            $day = new stdClass;
            $day->day = $c->day;
            
            $day->studentWaiting = $studentsWaiting = DB::table('classrooms')
                                ->join('classroom_user', 'classroom_user.classroom_id', '=', 'classrooms.id')
                                ->where('outlet_id', $outlet)
                                ->where(function ($query) {
                                    $query->where('classroom_user.status', '=', 'waiting')
                                          ->orWhere('classroom_user.status', '=', 'waiting_next_month');
                                })
                                ->where('classrooms.day',$c->day)
                                ->count();

            $tempCoachs = DB::table('classrooms')->where([
                                ['day', $c->day],
                                ['outlet_id', $outlet]
                            ])->orderBy('start','ASC')->get();
            $coach_ids = [];
            $coachs = [];
            
            $day->replacement = 0;
            foreach ($tempCoachs as $coach) {
                //dd($coach);
                $rep_student = Transfer::where('toClassroom', $coach->id)->where('toDate', '>=' , $today->format('Y-m-d'))->count();
                if(!in_array($coach->user_id, $coach_ids)){
                    $coach_ids[] = $coach->user_id;
                }

                $coachObj = new stdClass;
                $coachObj->id = $coach->user_id;
                $coachObj->start = $coach->start;
                $coachObj->end = $coach->end;
                $coachObj->name = User::find($coach->user_id)->name;
                $coachObj->avatar = User::find($coach->user_id)->avatar;
                $day->replacement = $day->replacement + $rep_student;
                array_push($coachs, $coachObj);
            }
            
            $day->coach = count($coach_ids);
            $day->coachs = $coachs;

            $day->session = $c->session;

            $day->index = $key;
            // $studentNumber = $students[0];

            $studentNumber = '0';
            foreach ($students as $key => $val) {
            
                if($val->day == $day->day){
                    //dd($val->day,$day->day);
                    $studentNumber = $val->student;
                }
            }  
            
            $day->students = $studentNumber;  
            
            


            array_push($result, $day);
        }

        // usort($result, function($firstItem, $secondItem) {
        //     $timeStamp1 = strtotime($firstItem['time']);
        //     $timeStamp2 = strtotime($secondItem['time']);
        //     return $timeStamp1 - $timeStamp2;
        // });

        // dd($result);
        return $result;

    }

    // public function showSessionByUser(Request $request){
    //     return DB::table('classrooms')
    //             ->where('user_id', '=', $request->user()->id)
    //             ->get();
    // }

    public function showSessionByUser($user_id)
    {
        $classroom_user =  DB::table('classrooms')
                ->join('classroom_user', 'classrooms.id', '=', 'classroom_user.classroom_id')
                ->where('classroom_user.user_id', '=', $user_id)
                ->get();
        
        return $classroom_user;
    }

    public function getSessionByUser($package_user_id)//for dashboard member_outstanding modal
    {
        //dd($package_user_id);
        $classroom_user =  DB::table('classrooms')
                ->join('classroom_user', 'classrooms.id', '=', 'classroom_user.classroom_id')
                ->where('classroom_user.package_user_id', '=', $package_user_id)
                ->get();
        
        return $classroom_user;
    }

    public function showByDay($outlet, $day, $status)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->classAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

        $class_raw = Classroom::all()->where('outlet_id', $outlet)->where('day', $day)->where('status', $status);

        $classes = [];
        
        foreach ($class_raw as $class) {
            array_push($classes, $class);
        }

        $sessions = [];
        // Loop through classes
        foreach ($classes as $class) {
            $object = new stdClass;
            $object->id = $class->id;
            $object->start = $class->start;
            $object->end = $class->end;
            $object->coach_id = $class->user_id;
            $object->status = $class->status;
            $object->coach = User::find($class->user_id)->name;
            $object->avatar = User::find($class->user_id)->avatar;
            $object->level = $class->level;
            $object->type = $class->type;
            $object->studentNumber = 0;
            $object->waiting = 0;

            //$object->students = Classroom::find($class->id)->users;
            $students = Classroom::find($class->id)->users;

            $classroomUsers_active = ClassroomUser::where('classroom_id',$class->id)->where('status','active')->count();
            $classroomUsers_waiting = ClassroomUser::where('classroom_id',$class->id)->where(function ($query) {$query->where('status', '=', 'waiting')
                          ->orWhere('status', '=', 'waiting_next_month');
                })->count();

            $object->studentNumber = $classroomUsers_active;
            $object->studentWaiting =$classroomUsers_waiting;
            
            $studentsArr = [];

            foreach ($students as $student ) {
                $studentObj = new stdClass;
                
                $studentObj->id = $student->id;
                $studentObj->name = $student->name;
                $studentObj->birthday = $student->birthday;
                $studentObj->parent = User::find($student->user_id);
                $studentObj->status = $student->status;
                //$studentObj->remarks =  $student->classes->where('id', $class->id)->where('start', $class->start);
                $studentObj->remarks =  $student->classes->first();
                // $object->studentNumber++;
                array_push($studentsArr, $studentObj);
            }

            //$studentsArr = $students;


            $object->students = $studentsArr;

            array_push($sessions, $object);
        }
        $array = json_decode(json_encode($sessions), true);
        usort($array, function($a, $b) {
            return strtotime($a['start']) - strtotime($b['start']);
        });

        $sessions = json_decode(json_encode($array));
        //return $classes;
        return $sessions;
    }


    public function getClassInformation(Classroom $class)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->classAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }
        
        return new ClassResources($class);
    }

    public function showStudentByClass(Classroom $class) {

        $students = $class->users;
        $studentsArr = [];

        foreach ($students as $student ) {
                $studentObj = new stdClass;
                $packageUser = PackageUser::find($student->pivot->package_user_id);
                
                $studentObj->id = $student->id;
                $studentObj->name = $student->name;
                $studentObj->avatar = $student->avatar;
                $studentObj->birthday = $student->birthday;
                $studentObj->parent = User::find($student->user_id)->name;
                $studentObj->overdue = $packageUser->outstanding;
                $studentObj->package_user_id = $packageUser->id;
                // $studentObj->overdue = DB::table('orders')->where([
                //     ['cust_id', $student->user_id],
                //     ['status', '1']
                // ])->orderByDesc('created_at')->first();
                $studentObj->remarks =  $student->classes->first();
                $studentObj->status = $student->pivot->status;
                
                array_push($studentsArr, $studentObj);
            }

        return $studentsArr;
    }

    public function showStudentTransferByClass(Classroom $class) {

        $transfers_raw = DB::table('transfers')
                        ->select(DB::raw('*'))
                        ->where('toClassroom', $class->id)
                        ->where('used', '1')
                        ->get();

        return $transfers_raw;
    }

    public function showStudentByCoach(Classroom $class, $memberId) {

        // return DB::table('classroom_user')->where([
        //             ['classroom_id', $class->id],
        //             ])
        //             ->get();

        return DB::table('classrooms')
                ->join('users', 'users.id', '=', 'classrooms.user_id')
                ->join('classroom_user', 'classrooms.id', '=', 'classroom_user.classroom_id')
                ->where([
                ['classrooms.id', $class->id],
                ['classroom_user.user_id', $memberId]
                ])
                ->get();
    }

    public function store(Request $request){
        $classroom = new Classroom;

        $classroom->start = $request->start;
        $classroom->end = $request->end;
        $classroom->day = lcfirst($request->day);
        $classroom->user_id = $request->user_id;
        $classroom->outlet_id = $request->outlet_id;
        $classroom->level = $request->level;
        $classroom->maximum = $request->maximum;
        $classroom->status = $request->status;
        $classroom->type = $request->type;

        $classroom->save();

        return $classroom;
        
    }

    public function update(Classroom $classroom, Request $request) {
        
        $classroom->start = $request->start;
        $classroom->end = $request->end;
        //$classroom->day = $request->day;
        if ($request->user_id) {
            $classroom->user_id = $request->user_id;
        }
        $classroom->level = $request->level;
        $classroom->maximum = $request->maximum;
        $classroom->status = $request->status;
        $classroom->type = ucfirst($request->type);

        $classroom->save();

        return $classroom;
    }

    public function addStudent(Classroom $class, Request $request){
        $user_id = $request->user_id;
        $remarks = $request->remarks;

        // Check for clash time
        $user = User::find($user_id);

        //check for duplicate

        

        $key_id = $user;

        $result = DB::table('classroom_user')->where([
            ['classroom_id', $class->id],
            ['user_id', $user_id]
        ])->count() > 0;

        // $num_rows = mysqli_num_rows($result);
        

        if ($result == null) {
                        $class->users()->attach([ $user_id => [
            'remarks' => $remarks
        ]]);

        return $class->users;
            
        }
        else{

            trigger_error('It exists.', E_USER_WARNING);


        }


    }

    public function removeStudent(Classroom $class, Request $request){
        $user_id = $request->user_id;
        
        $transfer = Transfer::where('member_id',$user_id)->delete();

        $class->users()->detach($user_id);

        return $class->users;
    }

    public function addReplacementClass(Request $request)
    {
        $current_date = Carbon::now();
        $today = $current_date->format('Y-m-d');
        
        $classroom = Classroom::find($request->classroom_id);
        $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classroom->start)));
        $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classroom->end)));

        $replacement = new Replacement;
        $replacement->beforeDate = date("Y-m-d", strtotime($request->beforeDate));
        $replacement->beforeStartTime = $classroom->start;
        $replacement->beforeEndTime = $classroom->end;
        $replacement->afterDate = date("Y-m-d", strtotime($request->afterDate));
        $replacement->afterStartTime = date("H:i:s", strtotime($request->afterStartTime));
        $replacement->afterEndTime = date("H:i:s", strtotime($request->afterEndTime));
        $replacement->classroom_id = $request->classroom_id;

        $replacement->save();

        if (date("Y-m-d", strtotime($request->afterDate)) == $today ) {
            foreach ($classroom->users as $key => $user) {
                $attendance = new Attendance;
                $attendance->user_id = $user->id;
                $attendance->class_id = $classroom->id;
                $attendance->package_id = $user->pivot->package_id;
                $attendance->clockIn = Carbon::now();
                $attendance->status = 'absent';
                $attendance->week = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end = $clock_in_end;
                $attendance->outlet_id = $classroom->outlet_id;
                $attendance->save();
            }
        }

        return $replacement;

    }

    public function getReplacementCredit($package_user_id)
    {
        // dd($package_user_id);

        $classroom_user = ClassroomUser::where('package_user_id',$package_user_id)->first();
        
        return $classroom_user;

    }    

    public function addReplacementCredit(Request $request)
    {
        // dd($request->remarks,$request->credits,$request->package_user_id,$request->member_id);

        $classroom_user = ClassroomUser::where('package_user_id',$request->package_user_id)->first();
        $classroom_user->remarks = $request->remarks;
        $classroom_user->replacement_credit = $request->credits;
        $classroom_user->save();

    }
    public function getAllReplacementClass($id) {
        return Replacement::where('classroom_id',$id)->get();
    }

    public function addLogRecord(Classroom $class, Request $request)
    {
        $condition = [
            'date'  => $request->date
        ];

        $insert = [
            'message'       => $request->message,
            'user_id'       => $request->user_id,
            'log_id'        => $request->log_id,
            'classroom_id'  => $class->id,
            'beforeTime'    => $request->beforeTime,
            'afterTime'     => $request->afterTime,
        ];
        $record = Log::updateOrCreate($condition, $insert);
    }


    public function updateLogRecord(Classroom $class, Request $request)
    {
        $condition = [
            'log_id'  => $request->log_id,
            'date'  => $request->date
        ];

        $update = [
            'message'       => $request->message,
            'user_id'       => $request->user_id,
            'classroom_id'  => $class->id,
            'beforeTime'    => $request->beforeTime,
            'afterTime'     => $request->afterTime,
        ];

        $record = Log::updateOrCreate($condition, $update);
        return response()->json(compact('record'));   
    }


    public function getLogRecord(Classroom $class) {
        $raw_logs =  DB::table('logs')->where([
            ['classroom_id', $class->id],
            ['log_id', null]
        ])->get();

        $recordsArr = [];

        foreach ($raw_logs as $log ) {
                $logObj = new stdClass;
                
                $logObj->id = $log->id;
                $logObj->message = $log->message;
                $logObj->date = $log->date;
                $logObj->beforeTime = $log->beforeTime;
                $logObj->afterTime = $log->afterTime;
                $logObj->author = User::find($log->user_id)->name;

                $tempLog = Log::find($log->id)->replied;

                $repliedObj = new stdClass;

                if ( $tempLog != null ) {
                    $repliedObj->id = $tempLog->id;
                    $repliedObj->message = $tempLog->message;
                    $repliedObj->author = User::find($tempLog->user_id)->name;
                } else {
                    $repliedObj = null;
                }

                $logObj->replied = $repliedObj;
                //$logObj->replied = $tempLog;
                
                array_push($recordsArr, $logObj);
            }

        return $recordsArr;
    }

    public function getLogRecordByDay(Classroom $class, Request $request)
    {
        $dateArr = [];

        $mDatePeriod = new \DatePeriod(
            Carbon::parse("First ".ucfirst(strtolower($request->day))." of ".$request->month." ".$request->year),
            CarbonInterval::week(),
            Carbon::parse("Last ".ucfirst(strtolower($request->day))." of ".$request->month." ".$request->year." 00:00:01")
        );

        foreach($mDatePeriod as $currentDay){

            $dateObj = new stdClass;
            $dateObj->date = $currentDay->format('Y-m-d');
            
            $tempLog = DB::table('logs')->where([
                                ['classroom_id', $class->id],
                                ['log_id', null],
                                ['date', $dateObj->date]
                            ])->get();

            foreach ($tempLog as $log ) {

                $fromPostObj = new stdClass;
                $fromPostObj->id = $log->id;
                $fromPostObj->message = $log->message;

                $dateObj->fromMessage = $fromPostObj;


                $dateObj->from = User::find($log->user_id)->name;
                if ( Log::find($log->id)->replied != null ){
                    $dateObj->replied = Log::find($log->id)->replied;
                    $dateObj->to = User::find(Log::find($log->id)->replied->user_id)->name;
                } else {
                    //$dateObj->replied = null;
                    
                    $dateObj->replied = new stdClass;
                    $dateObj->replied->id = null;
                }
            }

            array_push($dateArr, $dateObj);
            $currentDay->modify('next ' . $request->day);

        }

        return $dateArr;
        //return $monthNum;
    }

    public function getAllReplacementStudent(Classroom $class)
    {
        $current_date = Carbon::now();
        //$current_date = Carbon::parse('2019-10-30');
        
        //$transfers_raw = $class->transfers;

        $transfers_raw = DB::table('transfers')
                        ->where('used', '1')
                        ->where(function($q) use ($class){
                            $q->where('fromClassroom', $class->id)
                                ->orWhere('toClassroom', $class->id);
                        })
                        ->whereNull('deleted_at')
                        // ->where('toDate', $current_date)
                        // ->orWhere('fromDate', $current_date)
                        ->get();

        $transfers = [];
        //dd($transfers_raw);
        foreach ($transfers_raw as $transfer ) {

            $member = User::find($transfer->member_id);
            $attendance = Attendance::where('replacement_id',$transfer->id)->first();

            $transferObj = new stdClass;
            $transferObj->id = $transfer->id;
            $transferObj->memberId = $member->id;
            $transferObj->name = $member->name;
            $transferObj->avatar = $member->avatar;
            $transferObj->age = $member->birthday;
            $transferObj->fromClassroom = $transfer->fromClassroom;
            $transferObj->toClassroom = $transfer->toClassroom;
            $transferObj->fromClass = Classroom::with('coaches')->find($transfer->fromClassroom);
            $transferObj->toClass =  Classroom::with('coaches')->find($transfer->toClassroom);
            $transferObj->fromDate = $transfer->fromDate;
            $transferObj->toDate = $transfer->toDate;
            $transferObj->day = date('l', strtotime($transfer->fromDate));
            $transferObj->attendance = $attendance;

            array_push($transfers, $transferObj);
        }

        return $transfers;
    
    }

    public function getAllClass($member){

        //$package_user = PackageUser::findorfail($member);
        $outlet_id = User::where('id',$member)->pluck('outlet_id');

        $classExist =  Classroom::with('coaches')->where('status','1')
                ->where('outlet_id',$outlet_id)
                ->whereExists(function ($query) use ($member) {
                    $query->select(DB::raw(1))
                            ->from('classroom_user')
                            ->whereRaw('classroom_user.classroom_id = classrooms.id')
                            ->where('classroom_user.user_id', $member);
                })
                ->get();
        //dd($classExist);
        $classrooms = Classroom::with('coaches')->where('outlet_id',$outlet_id)->active()->get();
        // dd($classrooms);

        if($classrooms){
            foreach ($classrooms as $keys => $classroom) {
                foreach ($classExist as $key => $value) {
                    if ($classroom->day == $value->day && $classroom->id == $value->id) {
                        $classrooms->forget($keys);
                    }
                }
            }
        }

        $values = $classrooms->values();
        // dd($values);
        return $values;

    }

    public function getAllClassDetails($member,$day){

        //$package_user = PackageUser::findorfail($member);
        $outlet_id = User::where('id',$member)->pluck('outlet_id');

        $classExist =  Classroom::with('coaches')->where('status','1')
                ->where('outlet_id',$outlet_id)
                ->whereExists(function ($query) use ($member) {
                    $query->select(DB::raw(1))
                            ->from('classroom_user')
                            ->whereRaw('classroom_user.classroom_id = classrooms.id')
                            ->where('classroom_user.user_id', $member);
                })
                ->get();
        //dd($classExist);
        $classrooms = Classroom::with('coaches')->where('outlet_id',$outlet_id)->active()->get();
        // dd($classrooms);

        if($classrooms){
            foreach ($classrooms as $keys => $classroom) {
                foreach ($classExist as $key => $value) {
                    if ($classroom->day == $value->day && $classroom->id == $value->id) {
                        $classrooms->forget($keys);
                    }

                    if ($classroom->day != $day) {
                        $classrooms->forget($keys);
                    }
                }
            }
        }

        $values = $classrooms->values();
        // dd($values);
        return $values;

    }

    public function getAllSyllabus(){

        $syllabus = Syllabus::orderBy('sequence','asc')->get();


        $result = [];
        foreach ($syllabus as $key => $levels) {
            array_push($result, $levels->level);            
        }
        // dd($result);
        return $result;

    }

    public function countTransfer($date, $id){

        $memberId = DB::table("transfers")
                    ->where("member_id", $id)
                    ->select("fromDate")
                    ->get();

        $date = Carbon::parse($date);
        $date = $date->month;

        $count = 0;
        foreach ($memberId as $member ) {
            $test = Carbon::parse($member->fromDate);
            $test = $test->month;
            if ($date === $test) {
                $count +=1;
            }
        }

        return $count;
    }

    public function checkClassReplacement(Request $request)
    {
        $last_month = carbon::now()->subMonths(1);
        $next_month = carbon::now()->addMonths(1);
        $requested_date = carbon::parse($request->toDate);

        $last_month_replacement = Transfer::where('member_id',$request->member_id)
                                ->where('fromClassroom',$request->fromClassroom)
                                ->whereBetween('fromDate',[$last_month->copy()->startOfMonth(),$last_month->copy()->endOfMonth()])
                                ->get()->count();

        $next_month_replacement = Transfer::where('member_id',$request->member_id)
                                ->where('fromClassroom',$request->fromClassroom)
                                ->whereBetween('fromDate',[$next_month->copy()->startOfMonth(),$next_month->copy()->endOfMonth()])
                                ->get()->count();

        if($requested_date->month != $last_month || $requested_date->month != $next_month){

        }else{

        }
    }

    public function classReplacementStudent(Request $request)
    {
        // dd($request->package_user_id);
        $current_date = Carbon::now();
        $today = $current_date->format('Y-m-d');
        $requested_from = carbon::parse($request->fromDate);
        $member_id = $request->member_id;

        $check_credit_usage = Transfer::where([['member_id', $member_id],['fromClassroom', $request->fromClassroom]])->whereBetween('fromDate',[$requested_from->copy()->startOfMonth(), $requested_from->copy()->endOfMonth()])->whereNull('deleted_at')->first();

        $get_replacement_credit = ClassroomUser::where('package_user_id',$request->package_user_id)->first();
        $replacement_credit = $get_replacement_credit->replacement_credit;

        // dd($check_credit_usage,$get_replacement_credit->replacement_credit);
        if($check_credit_usage == '' || $replacement_credit  > 0){

            $transfer = new Transfer;
            $transfer->member_id = $member_id;
            $transfer->fromClassroom = $request->fromClassroom;
            $transfer->toClassroom = $request->toClassroom;
            $transfer->fromDate = $request->fromDate;
            $transfer->toDate = $request->toDate;
            $transfer->save();

            $classroom = Classroom::find($request->fromClassroom);
            $replacement_class = Classroom::find($request->toClassroom);
            $classroom_user = $classroom->users()->wherePivot('user_id', $member_id)->first();

            $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($replacement_class->start)));
            $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($replacement_class->end)));

            if ($today == $request->fromDate && $today == $request->toDate) {

                // dd($transfer->member_id);
                $attendance = Attendance::where([['user_id', $member_id],['class_id', $request->fromClassroom]])->whereBetween('clockIn', [$current_date->copy()->startOfDay(), $current_date->copy()->endOfDay()])->first();
                
                if($attendance){

                    // dd($classroom_user->pivot->package_id,$transfer->id);
                    
                    $attendance->user_id        = $member_id;
                    $attendance->class_id       = $classroom->id;
                    $attendance->package_id     = $classroom_user->pivot->package_id;
                    $attendance->clockIn        = Carbon::now();
                    $attendance->status         = 'absent';
                    $attendance->replacement_id = $transfer->id;
                    $attendance->week           = $current_date->weekOfMonth;
                    $attendance->clock_in_start = $clock_in_start;
                    $attendance->clock_in_end   = $clock_in_end;
                    $attendance->outlet_id      = $classroom->outlet_id;
                    $attendance->save();

                }else{

                    $attendance                 = new Attendance;
                    $attendance->user_id        = $member_id;
                    $attendance->class_id       = $classroom->id;
                    $attendance->package_id     = $classroom_user->pivot->package_id;
                    $attendance->clockIn        = Carbon::now();
                    $attendance->status         = 'absent';
                    $attendance->replacement_id = $transfer->id;
                    $attendance->week           = $current_date->weekOfMonth;
                    $attendance->clock_in_start = $clock_in_start;
                    $attendance->clock_in_end   = $clock_in_end;
                    $attendance->outlet_id      = $classroom->outlet_id;
                    $attendance->save();
                }

            }else if($today > $requested_from && $today == $request->toDate){

                $prior_attendance = Attendance::where([['user_id', $member_id],['class_id', $request->fromClassroom]])->whereBetween('clockIn', [$requested_from->copy()->startOfDay(), $requested_from->copy()->endOfDay()])->first();
                if($prior_attendance){
                    $prior_attendance->forceDelete();
                }
                

                $attendance                 = new Attendance;
                $attendance->user_id        = $member_id;
                $attendance->class_id       = $classroom->id;
                $attendance->package_id     = $classroom_user->pivot->package_id;
                $attendance->clockIn        = Carbon::now();
                $attendance->status         = 'absent';
                $attendance->replacement_id = $transfer->id;
                $attendance->week           = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end   = $clock_in_end;
                $attendance->outlet_id      = $classroom->outlet_id;
                $attendance->save();

            }else if($today != $request->fromDate && $today == $request->toDate){
                $attendance                 = new Attendance;
                $attendance->user_id        = $member_id;
                $attendance->class_id       = $classroom->id;
                $attendance->package_id     = $classroom_user->pivot->package_id;
                $attendance->clockIn        = Carbon::now();
                $attendance->status         = 'absent';
                $attendance->replacement_id = $transfer->id;
                $attendance->week           = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end   = $clock_in_end;
                $attendance->outlet_id      = $classroom->outlet_id;
                $attendance->save();
            }

            $get_replacement_credit->replacement_credit = $replacement_credit - 1;
            $get_replacement_credit->save();

            return $transfer;

        }else{
            //dd('error displayed');
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'This User Had Used The Replacement Credit For This Month',
            ), 401);
        }
    }

    public function updateWaitingToActive(Request $request){
        $classroom_id = $request->classroom_id;
        $user_id = $request->user_id;

        DB::table('classroom_user')
            ->where([
                ['classroom_id', '=', $classroom_id],
                ['user_id', '=', $user_id],
            ])
            ->update(
                [
                    'status' => 'active'
                ]
            );
    }

    public function deleteReplace($replacement_id, $id, $class){
        //dd($replacement_id, $id, $class);
        $replacement = Transfer::where([["id", $replacement_id],["member_id", $id],['fromClassroom', $class]])
                    ->forceDelete();
    }

    public function transferStudent($id, $class, $classId){

        // if member do transfer class, all replacement class will delete
        DB::table("transfers")
                    ->where("member_id", $id)
                    ->delete();

        $checkClassroom = DB::table('classroom_user')
                        ->where([
                        ['user_id', $id],
                        ['classroom_id', $class],
                        ])
                        ->first();

        if ($checkClassroom == '') {
            DB::table('classroom_user')
                ->where([
                ['user_id', $id],
                ['classroom_id', $classId],
                ])
                ->update(
                    [
                        'classroom_id' => $class
                    ]
                );
        }

        else{
            return response()->json([
                        'status' => 'error',
                        'message' => 'An error occurred!'
                    ], 500);
        }
    }

    public function searchStudent(Request $request)
    {
        $classroom_user = DB::table('classroom_user')
                        ->join('classrooms', 'classrooms.id', '=', 'classroom_user.classroom_id')
                        ->where('classroom_user.user_id', $request->student)
                        ->get();

        return response()->json([
            'user' => $classroom_user
        ]);
    }

    public function classQuestionnaire($level)
    {
        $syllabus = Syllabus::where('level', $level)->first();
        // $question = SyllabusQuestion::where('syllabus_id', $syllabus->id)->get();

        // $options = QuestionOption::where('syllabus_question_id', '2')->get();
        return response()->json(compact('syllabus'));
    }

    public function storeClassSyllabus($id, Request $request)
    {
        $questions = $request->answers;
        $remarks = $request->remarks;
        $userId = Auth::user()->id;
        $getClassSyllabus = ClassSyllabus::where([
                ['coach_id', $userId],
                ['class_id', $id],
                ['syllabus_id', $request->level],
            ])
            ->update([
                'status' => 'inactive'
            ]);

        foreach ($remarks as $key => $value) {
            $options = QuestionOption::where('id',$key)->where('title', 'remark')->first();

            if ($options) {
                $condition = [
                    'coach_id'  => $userId,
                    'class_id' => $id,
                    'question_id' => $options->syllabus_question_id,
                    'option_id' => $options->id,
                    'syllabus_id' => $request->level,
                ];
                $insert = [
                    'status'    => 'active',
                    'remark'    => $value,
                ];
                $createClassSyllabus = ClassSyllabus::updateOrCreate($condition, $insert);
            }
        }

        foreach ($questions as $question) {
            $split = explode("-", $question);
            $question_id = $split[0];
            $option_id = $split[1];

            $condition = [
                'coach_id'  => $userId,
                'class_id' => $id,
                'question_id' => (int)$question_id,
                'option_id' => (int)$option_id,
                'syllabus_id' => $request->level,
            ];
            $insert = [
                'status'    => 'active'
            ];
            $createClassSyllabus = ClassSyllabus::updateOrCreate($condition, $insert);
        }
    }

    public function showClassSyllabus($id)
    {
        $classSyllabus = ClassSyllabus::where('class_id', $id)->where('status', 'active')->get();
        return $classSyllabus;
    }

    public function classsummary($outlet, $day)
    {
        $package_type =  DB::table('master_datas_package_type')->get();

        $class_raw = Classroom::all()->where('outlet_id', $outlet)->where('day', $day)->where('status', '1');

        $levels = Syllabus::orderBy('sequence','asc')->get();

        $level_arr = [];

        foreach($levels as $syllabus){
            $arr = [
              'name' => $syllabus->level,
              'count'=> 0,
            ];
            array_push($level_arr,$arr);
        }
        // sort($get_levels);
        // dd($get_levels);

        $index = 4;
        $package_arr = [];
        // $level_arr = [
        //     [ 
        //         'name' => 'L1',
        //         'count'=> 0
        //     ],
        //     [ 
        //         'name' => 'L2',
        //         'count'=> 0
        //     ],
        //     [ 
        //         'name' => 'L3',
        //         'count'=> 0
        //     ],
        //     [ 
        //         'name' => 'L4',
        //         'count'=> 0
        //     ],
        //     [ 
        //         'name' => 'L5',
        //         'count'=> 0
        //     ],
        //     [ 
        //         'name' => 'L6',
        //         'count'=> 0
        //     ],
        //     [ 
        //         'name' => 'Adult',
        //         'count'=> 0
        //     ]
        // ];
        // dd($level_arr);
        foreach($package_type as $type){

            if($type->name == 'Standard'){

                $package_arr[0] = [
                    'name' =>$type->name,
                    'count' => 0
                ];
            }else if($type->name == 'Private'){

                $package_arr[1] = [
                    'name' =>$type->name,
                    'count' => 0
                ];
            }else if($type->name == 'PRE-COM'){

                $package_arr[2] = [
                    'name' =>$type->name,
                    'count' => 0
                ];
            }else if($type->name == 'Competitive'){

                $package_arr[3] = [
                    'name' =>$type->name,
                    'count' => 0
                ];
            }else{

                $package_arr[$index] = [

                    'name' =>$type->name,
                    'count' => 0
                ];

                $index++;
            }
        }
        ksort($package_arr);
        //dd($package_arr);
        foreach ($class_raw as $data) {
            
            foreach ($package_arr as $key => $type) {
                
                if($type['name'] == $data->type){
                    
                    $sum = $type['count'] +1;
                    $package_arr[$key]['count'] = $sum;
                    
                }
            }

            foreach ($level_arr as $key => $level) {
                
                if($level['name'] == $data->level){
                    $sum = $level['count'] +1;
                    $level_arr[$key]['count'] = $sum;
                }
            }

        }
 
        $result_arr = [];

        foreach ($level_arr as $key => $level) {
            $result = new stdClass;
            $result->category = 'level'; 
            $result->name = $level['name'];
            $result->count = '( '.$level['count'].' )';
            array_push($result_arr, $result);
        }

        foreach ($package_arr as $key => $type) {
            $result = new stdClass;
            $result->category = 'type'; 
            $result->name = $type['name'];
            $result->count = '( '.$type['count'].' )';
            array_push($result_arr, $result);
        }

        // dd($test);
        return $result_arr;
        // $classSyllabus = ClassSyllabus::where('class_id', $id)->where('status', 'active')->get();
        // return $classSyllabus;
    }

    public function createAttendance($outletId)
    {
        // dd($outletId);

        $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]); //GuzzleHttp\Client

        $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

        try {
            // $result = $client->get($url);In use
            // $status = $result->getStatusCode();In use
        } 
        catch (\GuzzleHttp\Exception\GuzzleException $e) {
            // This is will catch all connection timeouts
            // Handle accordinly
            // $status = $e->getResponse()->getStatusCode();
            // if ($e->hasResponse()) {
            //     $status = $e->getResponse()->getStatusCode();
            //     // var_dump($status->getStatusCode()); // HTTP status code
            //     // var_dump($status->getReasonPhrase()); // Message
            //     // var_dump((string) $status->getBody()); // Body
            //     // var_dump($status->getHeaders()); // Headers array
            //     // var_dump($status->hasHeader('Content-Type')); // Is the header presented?
            //     // var_dump($status->getHeader('Content-Type')[0]); // Concrete header value
            // } else {
            //     $status = null;
            // }
        }

        if ($status != 200 || $status == null) {

            // \Artisan::call('attendance:studentsOffline', ['outletId' => $outlet->id]);In use

        } else {

            // \Artisan::call('attendance:students', ['outletId' => $outlet->id]);In use

        }

        // \Artisan::call('attendance:studentsOffline', ['outletId' => $outletId]);
        $success = $outletId;
        return $success;
    }
}

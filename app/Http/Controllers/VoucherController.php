<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;

class VoucherController extends Controller
{
    public function index() {
    	return Voucher::all();
    }

    public function show(Voucher $voucher) {
    	return $voucher;
    }

    public function store(Request $request) {

    	$voucher = new Voucher;
    	
    	$voucher->code = $request->code;
        $voucher->description = $request->description;
        $voucher->type = $request->type;
    	$voucher->limits = $request->limits;
    	$voucher->discountType = $request->discountType;
        $voucher->discountValue = $request->discountValue;
    	$voucher->validFrom = $request->validFrom;
    	$voucher->validTo = $request->validTo;
		$voucher->limitOneCustomer = $request->limitOneCustomer;
		$voucher->is_unlimited = $request->is_unlimited;
		$voucher->status = $request->status;

    	$voucher->save();
    	return $voucher;
    }

    public function update(Voucher $voucher, Request $request) {

    	
    	$voucher->code = $request->code;
        $voucher->description = $request->description;
        $voucher->type = $request->type;
    	$voucher->limits = $request->limits;
    	$voucher->discountType = $request->discountType;
        $voucher->discountValue = $request->discountValue;
    	$voucher->validFrom = $request->validFrom;
    	$voucher->validTo = $request->validTo;
		$voucher->limitOneCustomer = $request->limitOneCustomer;
		$voucher->is_unlimited = $request->is_unlimited;
		$voucher->status = $request->status;

    	$voucher->save();
    	return $voucher;

    }

    public function delete(Voucher $voucher) {
    	$voucher->delete();
    	return 'Voucher has been deleted';
    }
}

<?php

namespace App\Http\Controllers;

use App\QuestionOption;
use App\SyllabusQuestion;
use Illuminate\Http\Request;

class QuestionOptionController extends Controller
{
    public function index(SyllabusQuestion $id)
    {
        $options = $id->options()->get();

        return response()->json(['data' => $options], 200);
    }

    public function store($id)
    {
        $option = new QuestionOption();
        $option->syllabus_question_id = $id;
        $option->title = request('title');
        $option->save();
    }

    public function destroy(QuestionOption $id)
    {
        $id->delete();
    }

    public function show(QuestionOption $id)
    {
        return response()->json(['data' => $id], 200);
    }

    public function update(QuestionOption $id)
    {
        $id->title = request('title');
        $id->save();
        return response()->json(['data' => $id], 200);
    }
}

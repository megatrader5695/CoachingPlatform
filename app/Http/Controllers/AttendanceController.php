<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendance;
use App\User;
use App\AttendanceStaff;
use App\Classroom;
use App\ClassroomUser;
use App\PackageUser;
use App\Schedule;
use App\Outlet;
use App\CurrentLogin;
use Carbon\Carbon;
use DB;
use Auth;
use Session;
use StdClass;

class AttendanceController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function student_package($memberId)
    {
        $packages = PackageUser::with('packages')->where('user_id', $memberId)->get();
        $student_package = [];
        $student_class = [];
        
        foreach ($packages as $key => $package) {
            $classrooms = ClassroomUser::with('classes')->where('package_user_id', $package->id)->get();
            
            foreach ($classrooms as $classroom) {
                $coach = User::where('id', $classroom->classes->user_id)->first();

                $obj = new stdClass;
                $obj->id = $classroom->classes->id;
                $obj->start = $classroom->classes->start;
                $obj->end = $classroom->classes->end;
                $obj->coach = $coach->name;
                array_push($student_class,$obj);
            }
            
            $package->classrooms = $student_class;

            $list = new stdClass;
            $list->id = $package->packages->id;
            $list->name = $package->packages->name;
            array_push($student_package,$list);
        }
        
        $result = [
            'package' => $student_package,
            'classes' => $student_class
        ];

        return $result;
    }

    public function store(Request $request)
    {
        //dd($request->attendance_Id,$request->classId,$request->userId);
        $classroom = $request->fromClassId != '' ? $request->fromClassId : $request->classId;

        $classroom_to = Classroom::findOrFail($request->classId);
        $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classroom_to->start)));
        $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classroom_to->end)));

        // get package id in table classroom_user
        $classroom_user = DB::table('classroom_user')->where([
            ['classroom_id', $classroom],
            ['user_id', $request->userId]
        ])
        ->first();

        $currentTime = Carbon::now('Asia/Kuala_Lumpur');
        $weekOfMonth = $currentTime->weekOfMonth;

        $replacement = $request->replacementId != '' ? $request->replacementId : null;
        $packageId = $classroom_user->package_id != '' ? $classroom_user->package_id : null;
        
        if($request->attendance_Id){

            $condition = [
                'id'=> $request->attendance_Id,
                'user_id' => $request->userId,
                'class_id' => $request->classId,
                'week' => $weekOfMonth,
                'package_id' => $packageId
            ];
        }else{

            $condition = [
                'user_id' => $request->userId,
                'class_id' => $request->classId,
                'week' => $weekOfMonth,
                'package_id' => $packageId
            ];
        }
        

        $setValue = [
            'clockIn' => $currentTime->toDateTimeString(),
            'status' => $request->status,
            'replacement_id' => $replacement,
            'clock_in_start' => $clock_in_start,
            'clock_in_end' => $clock_in_end,
            'outlet_id' => $classroom_to->outlet_id,
        ];

        $value = array_merge($condition, $setValue);
        $attendances = Attendance::updateOrCreate($condition, $value);
        return $attendances;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        if ($request->time == 'Invalid date') {
            $classroom = Classroom::findOrFail($request->class);
            $setTime = $classroom->start;
        }
        else{
            $setTime = $request->time;
        }

        $time = Carbon::createFromFormat('H:i:s', $setTime);

        $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $request->date. ' ' .$time->totimeString());

        $attendances = Attendance::findOrFail($id);
        $attendances->clockIn = $dateTime;
        $attendances->clock_in_start = $dateTime;
        $attendances->clock_in_end = $dateTime;
        $attendances->status = strtolower($request->status);
        $attendances->created_at = $dateTime;
        $attendances->updated_at = false;
        $attendances->updated_at = $dateTime;
        $attendances->save();

        return response()->json([
            'update' => $attendances
        ]);
    }

    public function destroy($id)
    {
        $attendances = Attendance::findOrFail($id);
        $attendances->delete();

        return $attendances;
    }

    public function get(Request $request){
        $userId = $request->user()->id;
        $statemet = 'select * from attendances where DATE(clockIn) = DATE(NOW()) AND user_id = ' . $userId;
        return DB::select(DB::raw($statemet));
    }

    public function getWeek($classId)
    {

        $now = Carbon::now();
        $attendances = Attendance::where('class_id', $classId)->whereMonth('clockIn', $now->month)->where('week', $now->weekOfMonth)->orderBy('id','desc')->get();
        //dd($now, $attendances);
        return $attendances;
    }

    public function getByWeek($week, $classId)
    {

        $now = Carbon::now();
        $attendances = Attendance::where('class_id', $classId)->whereMonth('clockIn', $now->month)->where('week', $week)->orderBy('id','desc')->get();
        //dd($now, $attendances);
        return $attendances;
    }

    public function getByMonth($month, $classId)
    {
        $now = Carbon::now();
        $month = $month+1;
        if($month < 9){
            $date = '01-0'.$month.'-'.$now->year;
        }else{
            $date = '01-'.$month.'-'.$now->year;
        }

        $start = Carbon::parse($date)->format('Y-m-d');
        $end = Carbon::parse($date)->endOfMonth()->format('Y-m-d');
        // dd($start,$end);
        $attendances = Attendance::with('user')->where('class_id', $classId)->whereBetween('clockIn',[$start, $end])->orderBy('id','asc')->get();

        $result = [];
        // $Month_Name = Carbon::parse($date)->format('F');

        // $title = 'Member Attendance For '.$Month_Name.' '.$now->year;
        // array_push($result, $title);
        // dd($title);

        foreach ($attendances as $attendance) {
            
            $record = new stdClass;
            $record->Name = $attendance->user->name;
            $record->ClockInTime = $attendance->clockIn;
            $record->Status = $attendance->status;
            $record->updated_at = Carbon::parse($attendance->updated_at)->format('Y-m-d');

            array_push($result, $record);

        }
        //dd($now, $attendances);
        return $result;
    }

    public function getAttendanceByStudents($id, $class, $package)
    {
        $attendances = Attendance::where([
            'user_id' => $id,
            // 'class_id' => $class,
            // 'package_id' => $package
        ])
        ->orderBY('created_at', 'desc')

        ->get();
        // dd($attendances->count());
        return $attendances;
    }

    public function ReplacemetAttachments($package_user_id)
    {
        $documents = DB::table('leave_documents')->where('package_user_id', $package_user_id)->get();
        // dd($documents);
        return $documents;
    }


    public function addStudentAttendance(Request $request)
    {
        if ($request->time == 'Invalid date') {
            $classroom = Classroom::findOrFail($request->classId);
            $setTime = $classroom->start;
        }
        else{
            $setTime = $request->time;
        }

        $setTime = new Carbon($setTime);

        $setTime = $setTime->format('H:i:s');

        $classroom_to = Classroom::findOrFail($request->classId);
        $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($classroom_to->start)));
        $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($classroom_to->end)));

        $time = Carbon::createFromFormat(' H:i:s', $setTime);

        $dateTime = Carbon::createFromFormat('Y-m-d H:i:s', $request->date. ' ' .$time->totimeString());
        //dd($dateTime->weekOfMonth);
        $condition = [
            'user_id' => $request->memberId,
            'class_id' => $request->classId,
            'package_id' => $request->packageId
        ];

        $check = Attendance::where($condition)->count();

        // $checkPackageType = Package::find($request->packageId);

        // $packageUser = PackageUser::where('user_id',$request->memberId)
        //                ->where('package_id',$request->packageId)
        //                ->first();

        // if ($checkPackageType->type == 'Private') {

        // }
        if ($check < 5) {

            $attendances = new Attendance;
            $attendances->user_id = $request->memberId;
            $attendances->class_id = $request->classId;
            $attendances->package_id = $request->packageId;
            $attendances->status = strtolower($request->status);
            $attendances->clockIn = $dateTime;
            $attendances->week = $dateTime->weekOfMonth;
            $attendances->clock_in_start = $clock_in_start;
            $attendances->clock_in_end = $clock_in_end;
            $attendances->outlet_id = $classroom_to->outlet_id;
            $attendances->created_at = $clock_in_start;
            $attendances->updated_at = false;
            $attendances->updated_at = $clock_in_start;
            $attendances->save();
        }
    }

    public function ClockInStaffAttendance(Request $request)
    {
        $today = Carbon::today()->format('l');  // get today name
        $user = Auth::user();             // get user information

        $schedule = Schedule::where('user_id', $user->id)->where('day', $today)->first();       //find user today schedule
        if (!$schedule) {
            return response()->json(array(
                'code'      =>  100,
                'message'   =>  'You do not have schedule time today.'
            ), 500);
        }

        // $date = Carbon::createFromFormat('d-m-Y', $request->date)->format('Y-m-d');
        // $time = date('H:i:s', strtotime($request->time));
        $current_latitude = $request->latitude;
        $current_longitude = $request->longitude;

        $outlet = Outlet::findOrFail($user->outlet_id);

        $earth_radius = 6371000;

        $dLat = deg2rad($current_latitude - $outlet->latitude );  
        $dLon = deg2rad($current_longitude - $outlet->longitude );

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($outlet->latitude)) * cos(deg2rad($current_latitude)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));
        $distance = $earth_radius * $c;

        if ($outlet->radius >= $distance) {
            $checkIn    = Carbon::now();
            $clockStart = Carbon::parse($schedule->start);
            $clockEnd   = Carbon::parse($schedule->end);

            $difftime = $clockStart->diffInMinutes($checkIn);

            if ($difftime < 0) {
                $status = 'early';
            }
            elseif ($difftime <= 60) {
                $status = 'ontime';
            }
            else{
                $status = 'late';
            }

            $different_time = date('H:i', mktime(0, $difftime));

            $attendances_staff = new AttendanceStaff;
            $attendances_staff->staff_id    = $user->id;
            $attendances_staff->checkIn     = $checkIn->format('Y-m-d H:i:s');
            $attendances_staff->clockStart  = $clockStart->format('Y-m-d H:i:s');
            $attendances_staff->clockEnd    = $clockEnd->format('Y-m-d H:i:s');
            $attendances_staff->different_time  = $different_time;
            $attendances_staff->type        = $user->type;
            $attendances_staff->status      = $status;
            $attendances_staff->save();

            return $attendances_staff;
        }
        else{
            return response()->json(array(
                'code'      =>  101,
                'message'   =>  'You are not in outlet radius'
            ), 500);
        }
    }

    public function ClockOutStaffAttendance(Request $request)
    {
        $user = Auth::user();
        $today = Carbon::today();

        $attendances_staff = AttendanceStaff::where('staff_id', $user->id)->whereDate('checkIn', $today->format('Y-m-d'))->first();

        $current_latitude = $request->latitude;
        $current_longitude = $request->longitude;

        $outlet = Outlet::findOrFail($user->outlet_id);

        $earth_radius = 6371000;

        $dLat = deg2rad($current_latitude - $outlet->latitude );  
        $dLon = deg2rad($current_longitude - $outlet->longitude );

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($outlet->latitude)) * cos(deg2rad($current_latitude)) * sin($dLon/2) * sin($dLon/2);  
        $c = 2 * asin(sqrt($a));
        $distance = $earth_radius * $c;
        
        if ($outlet->radius >= $distance) {
            $checkOut    = Carbon::now();
            $start = Carbon::parse($attendances_staff->checkIn);
            $working_hour = $checkOut->diffInHours($start);

            $attendances_staff->checkOut      = $checkOut->format('Y-m-d H:i:s');
            $attendances_staff->working_hour  = $working_hour;
            $attendances_staff->save();

            return $attendances_staff;
        }
        else{
            return response()->json(array(
                'code'      =>  101,
                'message'   =>  'You are not in outlet radius'
            ), 500);
        }
    }

    public function getStaffAttendance()
    {
        $user = Auth::user();
        $today = Carbon::today();

        $attendances_staff = AttendanceStaff::where('staff_id', $user->id)->whereDate('checkIn', $today->format('Y-m-d'))->first();
        return $attendances_staff;
    }

    public function dashboard($type)
    {
        $today = Carbon::today()->format('Y-m-d');
        $startOfWeek = Carbon::now()->startOfWeek();
        $endOfWeek = Carbon::now()->endOfWeek();
        $startOfMonth = Carbon::now()->startOfMonth();
        $endOfMonth = Carbon::now()->endOfMonth();

        $currentLogin = CurrentLogin::where('session_id', session()->getId())->first();

        if ($type == 'Week') {
            return Attendance::where('status', 'absent')->where('outlet_id', $currentLogin->outlet_id)->whereBetween('clockIn', [$startOfWeek, $endOfWeek])->count();
        }
        elseif ($type == 'Month') {
            return Attendance::where('status', 'absent')->where('outlet_id', $currentLogin->outlet_id)->whereBetween('clockIn', [$startOfMonth, $endOfMonth])->count();
        }
        else{
            return Attendance::where('status', 'absent')->where('outlet_id', $currentLogin->outlet_id)->whereDate('clockIn', $today)->count();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Designation;
use App\Tiers;
use DB;

class DesignationController extends Controller
{
    public function index() {
    	return $designation = Designation::all();
    }

    public function store(Request $request) {

        $tier1From  = (int)$request->tier1HoursFrom;
        $tier1To    = (int)$request->tier1HoursTo;
        $tier1Rate  = $request->tier1Rate;
        $tier2From  = (int)$request->tier2HoursFrom;
        $tier2To    = (int)$request->tier2HoursTo;
        $tier2Rate  = $request->tier2Rate;
        $tier3From  = (int)$request->tier3HoursFrom;
        $tier3To    = (int)$request->tier3HoursTo;
        $tier3Rate  = $request->tier3Rate;

        if  ($tier1To < 0 || $tier2To < 0 || $tier3To < 0) { 
            $returnData = array(
                'message' => 'Cannot set to negative number'
            );
            return response()->json($returnData, 500);
        }
        elseif ($tier2From < $tier2To && $tier3From < $tier3To) {

            $designation = new Designation;
            $designation->name = $request->name;
            $designation->save();
            $tiers = Tiers::all();

            foreach ($tiers as $tier) {
                $tier->designations()->attach($designation->id, [
                    'hours_from' => ${'tier'.$tier->id.'From'}, 
                    'hours_to' => ${'tier'.$tier->id.'To'},
                    'rate' => ${'tier'.$tier->id.'Rate'},
                ]);
            }

            return $designation;
        }
        else{
            $returnData = array(
                'message' =>  "'Hours To' must be greater than 'Hours From'"
            );
            return response()->json($returnData, 500);
        }
    }

    public function show($id){
        return  Designation::findOrFail($id);
    }

    public function getDesignation(Designation $config)
    {
        $config->tiers;
        return $config;
    }

    public function update(Designation $config, Request $request)
    {
        $config->name = $request->name;
        foreach ($request->tiers as $value) {
            ${'tier'.$value['id'].'From'}   = $value['pivot']['hours_from'];
            ${'tier'.$value['id'].'To'}     = $value['pivot']['hours_to'];
            ${'tier'.$value['id'].'Rate'}   = $value['pivot']['rate'];
        }

        if  ($tier1To < 0 || $tier2To < 0 || $tier3To < 0) { 
            $returnData = array(
                'message' => 'Cannot set to negative number'
            );
            return response()->json($returnData, 500);
        }
        elseif ($tier2From < $tier2To && $tier3From < $tier3To) {
            $tiers = Tiers::all();
            foreach ($tiers as $tier) {
                $tier->designations()->detach($config->id);
                $tier->designations()->attach($config->id, [

                    'hours_from'    => ${'tier'.$tier->id.'From'}, 
                    'hours_to'      => ${'tier'.$tier->id.'To'},
                    'rate'          => ${'tier'.$tier->id.'Rate'},
                ]);
            }
                    
            $config->save();
            return $config;
        }
        else{
            $returnData = array(
                'message' =>  "Hours To' must be greater than 'Hours From"
            );
            return response()->json($returnData, 500);
        }
    }
}

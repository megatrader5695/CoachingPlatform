<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Calendar;
use stdClass;

class EventController extends Controller
{
    public function index(Request $request){

        if ($request->event == 'all') {
            $events_raw = Event::whereYear('start', $request->year)
                ->where('outlet_id', $request->outlet_id)
                ->get();
        }
        else {
            $events_raw = Event::where('calendar_id', $request->event)
                ->whereYear('start', $request->year)
                ->where('outlet_id', $request->outlet_id)
                ->get();
        }
        
        $events = [];
        
        foreach ($events_raw as $event) {
            $eventObj = new stdClass;            
            $eventObj->id = $event->id;
            $eventObj->title = $event->title;
            $eventObj->start = $event->start;
            $eventObj->end = $event->end;
            $eventObj->remark = $event->remark;


            $category = Event::find($event->id)->calendar;
            $eventObj->category = $category->category;
            $eventObj->color = $category->color;

            array_push($events, $eventObj);
        }

        return $events;
    }

    public function store(Request $request){
        $event = new Event;

        $event->title = $request->title;
        $event->start = $request->start;
        $event->end = $request->end;
        $event->calendar_id = $request->calendar_id;
        $event->remark = $request->remark;
        $event->outlet_id = $request->outlet_id;

        $event->save();
        
        return $event;
    }

    public function show(Event $event){
        return $event;
    }

    public function update(Event $event, Request $request) {

        $event->title = $request->title;
        $event->start = $request->start;
        $event->end = $request->end;
        $event->calendar_id = $request->calendar_id;
        $event->remark = $request->remark;

        $event->save();

        return $event;
    }

    public function delete(Event $event) {
        $event->delete();
        return "Event has been deleted";
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon;
use App\MasterDataPackageCategory;
use App\Questionnaire;

class MasterDataController extends Controller
{
    // SET MAXIMUM STUDENT LIMIT FOR THE CLASS
    public function getStudentLimit(){
        return DB::table('master_datas')->where('id', 1)->get();
    }
    public function updateStudentLimit(Request $request){
        DB::table('master_datas')
            ->where('id', 1)
            ->update(
                [
                    'quantity' => $request->quantity
                ]
            );
    }

    // SET LIST OF PACKAGE TYPE

    public function listPackageType(){
        return DB::table('master_datas_package_type')->get();
    }

    public function insertPackageType(Request $request){
        DB::table('master_datas_package_type')->insert(
            [
                'name' => $request->name,
                'assits' => $request->assits,
                'syllabus' => $request->syllabus,
                'level' => $request->level
            ]
        );
    }

    public function updatePackageType(Request $request, $id){
        DB::table('master_datas_package_type')
            ->where('id', $id)
            ->update(
                [
                    'name' => $request->name,
                    'assits' => $request->assits,
                    'syllabus' => $request->syllabus,
                    'level' => $request->level
                ]
            );
    }

    public function deletePackageType($id) {
        DB::table('master_datas_package_type')
            ->where('id', $id)
            ->delete();
    }

    // SET LIST OF PACKAGE CATEGORY (YOGA, SWIM, GYM)
    
    public function listPackageCategory(){
        return MasterDataPackageCategory::where('type', 'package category')->get();
    }

    public function showPackageCategory($category){
        $package_category = MasterDataPackageCategory::where('name', $category)->first();
        return $package_category;
    }

    public function insertPackageCategory(Request $request){
        $insert = new MasterDataPackageCategory;
        $insert->name = ucwords($request->name);
        $insert->type = 'package category';
        $insert->save();
        
        return $insert;
    }

    public function updatePackageCategory(Request $request, MasterDataPackageCategory $id){
        $id->name = $request->name;
        $id->save();
    }
    
    public function deletePackageCategory(MasterDataPackageCategory $id) {
        $id->delete();
    }

    public function insertOtherCategory(Request $request)
    {
        $insert = new MasterDataPackageCategory;
        $insert->name = ucwords($request->name);
        
        
        if ($request->forRating == true) {
            $insert->shortname = 'rate';
            $insert->save();

            $questionnaire = new Questionnaire;
            $questionnaire->package_category_id = $insert->id;
            $questionnaire->question = 'remark';
            $questionnaire->save();

            $questionnaire = new Questionnaire;
            $questionnaire->package_category_id = $insert->id;
            $questionnaire->question = 'rate';
            $questionnaire->save();

        }
        $insert->save();
        
        return $insert;
    }

    public function getOtherCategory()
    {
        return MasterDataPackageCategory::whereNull('type')->get();
    }
}

<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Outlet;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use stdClass;
use PDF;
use Carbon\Carbon;

class InventoryController extends Controller
{
    private $inventoryAccess = 'inventory';
    public function index()
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->inventoryAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

        return DB::table('inventories')
                ->join('inventory_product', 'inventory_product.inventory_id', '=', 'inventories.id')
                ->where('inventory_product.outlet_id', Auth::user()->outlet_id)
                ->select('inventories.id', 'inventories.product_name', 'inventories.product_code', 'inventories.status', 'inventories.price', 'inventory_product.quantity')
                ->get();
    }

    public function create(Request $request)
    {
        $outlet = Outlet::all();

        $rules = [
            'product_code'  => 'required|unique:inventories,product_code',
            'product_name' => 'required',
            'price' => 'required',
            'category' => 'required'
        ];

        $this->validate($request, $rules);

        $inventory = new Inventory;
        $inventory->product_code = $request->product_code;
        $inventory->product_name = $request->product_name;
        $inventory->product_description = $request->product_description;
        $inventory->price = $request->price;
        $inventory->category = $request->category;
        $inventory->status = $request->status;
        $inventory->save();

        foreach ($outlet as $outlets) {
            // $tax = $outlets->gstpercentage / 100;                        //divide percentage by 100
            // $price = ($request->price * $tax) + $request->price;            //calculate price including percentage

            $inventory_product = DB::table('inventory_product')->insert([
                [
                    'outlet_id' => $outlets->id,
                    'inventory_id' => $inventory->id,
                    // 'price' => $price,
                    'quantity' => 0,
                ]
            ]);
        }
        return $inventory;
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Inventory $inventory)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->inventoryAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }
        
        $product = DB::table('inventories')
                ->join('inventory_product', 'inventory_product.inventory_id', '=', 'inventories.id')
                ->where([
                    ['inventory_product.outlet_id', Auth::user()->outlet_id ],
                    ['inventories.id', $inventory->id]
                ])
                ->select('inventories.id', 'inventories.product_name', 'inventories.product_code', 'inventories.product_description', 'inventories.status', 'inventories.price', 'inventory_product.quantity', 'inventories.category')
                ->first();

        return response()->json($product);
    }

    public function edit(Inventory $inventory)
    {
        //
    }

    public function update(Request $request, Inventory $inventory)
    {   
        $inventory->product_code = $request->product_code;
        $inventory->product_name = $request->product_name;
        $inventory->product_description = $request->product_description;
        $inventory->category = $request->category;
        $inventory->price = $request->price;
        $inventory->status = $request->status;

        $inventory->save();
        return $inventory;
    }

    public function destroy(Inventory $inventory)
    {
        $inventory->delete();
        return 'Inventory has been deleted';    
    }

    public function createReceive(Request $request, Outlet $outlet)
    {
        // create new inventory history
        $inventory_history = DB::table('inventory_history')->insert([
                [
                    'user_id' => $request->user_id,
                    'outlet_id' => $outlet->id,
                    'name' => $request->name,
                    'receive_date' => $request->date,
                    'total_quantity' => $request->total_quantity,
                    'total_price' => $request->total_price,
                ]
            ]);

        // get latest history id
        $inventory_history = DB::table('inventory_history')->where('receive_date', $request->date)->orderBy('created_at', 'desc')->first();

        $product = $request->productslist;
        foreach ($product as $products) {
            $inv_product = DB::table('inventory_product')->where([
                    ['inventory_id',$products['item_id'] ],
                    ['outlet_id', $outlet->id]
                ])->first();

            DB::table('inventory_product')
                ->where([
                    ['outlet_id', $outlet->id],
                    ['inventory_id', $products['item_id']]
                ])
                ->update(
                    [
                        'quantity' => $inv_product->quantity + $products['quantity']
                    ]
                );

            // create new inventory item list
            $outlet->inventorys()->attach($products['item_id'],
                    [
                        'history_id' => $inventory_history->id,
                        'quantity' => $products['quantity'],
                        'price_quantity' => $products['price_quantity'],
                        'remark' => $request->remark,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                );  
        }
    }

    public function createReturn(Request $request, Outlet $outlet)
    {
        // create new inventory history
        $inventory_history = DB::table('inventory_history')->insert([
                [
                    'user_id' => $request->user_id,
                    'outlet_id' =>$outlet->id,
                    'name' => $request->name,
                    'return_date' => $request->date,
                    'total_quantity' => $request->total_quantity,
                    'total_price' => $request->total_price,
                ]
            ]);

        // get latest history id
        $inventory_history = DB::table('inventory_history')->where('return_date', $request->date)->orderBy('created_at', 'desc')->first();

        $product = $request->productslist;
        foreach ($product as $products) {
            $inv_product = DB::table('inventory_product')->where([
                    ['inventory_id', $products['item_id'] ],
                    ['outlet_id', $outlet->id]
                ])->first();

            DB::table('inventory_product')
                ->where([
                    ['outlet_id', $outlet->id],
                    ['inventory_id', $products['item_id']]
                ])
                ->update(
                    [
                        'quantity' => $inv_product->quantity - $products['quantity']
                    ]
                );

            // create new inventory item list
            $outlet->inventorys()->attach($products['item_id'],
                    [
                        'history_id' => $inventory_history->id,
                        'quantity' => $products['quantity'],
                        'price_quantity' => $products['price_quantity'],
                        'remark' => $request->remark
                    ]
                );  
        }
    }

    public function showReceiveOutlet($outlet)
    {
        return DB::table('inventory_history')
                ->where('outlet_id', $outlet)
                ->orderBy('receive_date', 'desc')
                ->get();
    }

    public function showHistoryOutlet($id)
    {
        return DB::table('inventory_outlet')
                ->join('inventory_history', 'inventory_history.id', '=', 'inventory_outlet.history_id')
                ->join('inventories', 'inventories.id', '=', 'inventory_outlet.inventory_id')
                ->where('inventory_outlet.history_id', $id)
                ->select('inventories.product_name', 'inventories.price', 'inventory_outlet.quantity', 'inventory_outlet.price_quantity', 'inventory_outlet.remark', 'inventory_history.total_price', 'inventory_history.total_quantity', 'inventory_history.receive_date', 'inventory_history.return_date', 'inventory_history.name')
                ->get();

        // SELECT * FROM `inventory_outlet`
        // JOIN inventory_history ON inventory_history.id = inventory_outlet.history_id
        // JOIN inventories ON inventories.id = inventory_outlet.inventory_id
        // WHERE inventory_outlet.history_id = 1
    }

    public function searchProduct()
    {
        $inventory;

        if ($_GET['q']) {
            $params = $_GET['q'];

            $inventory = DB::table('inventories')
                    ->where('product_name', 'like', '%'.$params.'%')
                    ->orWhere('product_code', 'like', '%'.$params.'%')
                    ->get();
        }
        else {
            $inventory = Inventory::all();
        }


        $object = new stdClass;
        $object->items = $inventory;

        return (array) $object;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ToDoList;
use App\User;
use App\CurrentLogin;
use StdClass;
use Session;
use Auth;

class ToDoListController extends Controller
{
    public function index()
    {
        return ToDolist::with('coaches')->get();
    }

    public function indexDashboard()
    {
        $currentLogin = CurrentLogin::where('user_id', auth()->user()->id)->where('session_id', session()->getId())->first();
        return ToDolist::with('pendings')->where('outlet_id', $currentLogin->outlet_id)->where('status', 'pending')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $outlet_id = auth()->user()->outlet_id;

        $todolist = new ToDolist;
        $todolist->name = ucwords($request->name);
        $todolist->type = strtolower($request->type);
        $todolist->status = 'pending';
        $todolist->outlet_id = $outlet_id;
        $todolist->save();

        $coaches = $request->coach_id;
        foreach ($coaches as $key => $coach) {
            $coach = User::findOrFail($coach['id']);

            $todolist->coaches()->attach($coach->id,
                [
                    'status' => 'pending',
                ]
            );
        }
        return $todolist;
    }

    public function show(ToDolist $todolist)
    {
        $todolist->coaches;
        return $todolist;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, ToDolist $todolist)
    {
        $todolist->name = ucwords($request->name);
        $todolist->type = strtolower($request->type);
        $todolist->save();

        $setCoach = [];
        $coaches = $request->coaches;
        foreach ($coaches as $key => $coach) {
            $coach = User::findOrFail($coach['id']);
            array_push($setCoach, $coach->id);
        }
        $todolist->coaches()->sync($setCoach);
        
        return $todolist;
    }

    public function destroy(ToDolist $todolist)
    {
        $todolist->delete();
    }
}

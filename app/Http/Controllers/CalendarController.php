<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;

class CalendarController extends Controller
{
    public function index() {
    	return Calendar::all();
    }

    public function show(Calendar $calendar) {
    	return $calendar;
    }

    public function store(Request $request) {

    	$calendar = new Calendar;
    	
    	$calendar->category = $request->category;
    	$calendar->color = $request->color;
    	$calendar->save();

    	return $calendar;
    }

    public function update(Calendar $calendar, Request $request) {

    	
    	
    	$calendar->category = $request->category;
    	$calendar->color = $request->color;

    	$calendar->save();
    	return $calendar;

    }

    public function delete(Calendar $calendar) {
    	$calendar->delete();
    	return 'Calendar Category has been deleted';
    }
}

<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Leave;
use App\LeaveUsers;
use App\User;
use App\LeaveUserDetail;
use Auth;
use StdClass;
use Carbon\Carbon;

class LeaveController extends Controller
{
    private $leaveAccess = 'leave';

    public function index()
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->leaveAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }
        
        $leaves = Leave::all();
        return $leaves;
    }

    public function store(Request $request)
    {
        $leave = new Leave;

        $leave->name = ucwords($request->name);
        // $leave->minimumDay = $request->minimumDay;
        // $leave->continuous = $request->continuous;
        $leave->save();

		return $leave;
    }

    public function show(Leave $leave)
    {
        return $leave;
    }


    // public function leaveHistory(){
    //     $leaves = User::with('leaves')->get();

    //     return $leaves->pluck('leaves')->unique();
    // }

    public function update(Request $request, Leave $leave)
    {
        $leave->name = $request->name;

        $leave->save();
        return $leave;
    }

    public function destroy(Leave $leave)
    {
        $users = $leave->users;
        foreach ($users as $user) {
            $leave->users()->detach($user->id);
        }

        $leave->delete();

        return;
    }

    //$user->leaves()->attach([1 => ['start'=>'2018-02-16', 'end'=>'2018-02-16']])
    public function assignLeave(User $user, Request $request){
        $leave = $request->leave_id;
        $start = $request->start;
        $end = $request->end;
        $reason = $request->reason;
        $session = $request->session;
        //$document = $request->file('document')->store('leave');

        $firstApproval = 0;
        $secondApproval = 0;
        $thirdApproval = 0;

        $role = $user->role;

        if ($role->role == 'operation' || $role->role == 'admin staff') {
            $firstApproval = 1;
        }
        else{
            $secondApproval = 1;
        }
    
        $user->leaves()->attach([ $leave => [
            'start' => $start,
            'end' => $end,
            'reason' => $reason,
            'session' => $session,
            'firstApproval' => $firstApproval,
            'secondApproval' => $secondApproval,
            'thirdApproval' => $thirdApproval
        ] ]);
        
        $bridge = DB::table('leave_user')
                    ->where([
                        ['start', '=', $start],
                        ['end', '=', $end],
                        ['user_id', '=', $user->id],
                        ['leave_id', '=', $leave]
                    ])->get();


        return $bridge;
    }


    // Upload supported dcument
    public function uploadLeaveDoc(Request $request, $leave_user_id){

        $photo_url = $request->file('attachments')->store('leave');
        
         $leave = DB::table('leave_user')
                ->where('leave_user.id', $leave_user_id)
                ->update(['document' => $photo_url]);

        return;

    }

    // Upload supported document for students
    public function uploadStudentLeaveDoc(Request $request, $leave_user_id){

        $photo_url = $request->file('attachments')->store('student_leave');
        $url = $_SERVER['HTTP_HOST'].'/storage/'.$photo_url;

        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
        $end_url = $protocol.'://'.$url;
        // dd($protocol.'://'.$url);
        DB::table('leave_documents')->insert([
            'package_user_id' => $leave_user_id, 
            'document' => $end_url,
            'created_at' => carbon::now(),
            'updated_at' => carbon::now(),
        ]);

        return;
    }

    // $user->leaves()->sync([2 => ['reason' => 'demam']]);
    public function updateLeaveUser(User $user, Leave $leave, Request $request){
        $reason = $request->reason;

        $user->leaves()->sync([$leave->id => ['reason' => $reason]]);

        return $user;
    }


    public function getLeaveApplication(){
        $leave = DB::table('leave_user')
                ->join('users', 'users.id', '=', 'leave_user.user_id')
                ->join('leaves', 'leaves.id', '=', 'leave_user.leave_id')
                ->select('leave_user.start', 'leave_user.end', 'users.name', 'users.email', 'leaves.name as leave', 'leave_user.reason as reason')
                ->where('leave_user.id', $id)
                ->get();

        return $leave;
    }

    public function indexLeaveApplication(){
        
        $leave = DB::table('leave_user')
                ->join('users', 'users.id', '=', 'leave_user.user_id')
                ->join('leaves', 'leaves.id', '=', 'leave_user.leave_id')
                ->select('leave_user.start', 'leave_user.end', 'users.name', 'users.email', 'leaves.name as leave', 'leave_user.reason as reason')
                ->get();

        return $leave;
    }

    public function showSingleApplication($id)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->leaveAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

        // $leave = DB::table('leave_user')
        //         ->join('users', 'users.id', '=', 'leave_user.user_id')
        //         ->join('leaves', 'leaves.id', '=', 'leave_user.leave_id')
        //         ->select('leave_user.start', 'leave_user.end', 'leave_user.document', 'users.name', 'users.email', 'leaves.name as leave', 'leave_user.reason as reason', 'leave_user.approve')
        //         ->where('leave_user.id', $id)
        //         ->get();

        $leave = LeaveUsers::with('user', 'leave')->where('id', $id)->first();

        return $leave;
    }
    public function getSingleApplication(){

        $leave = DB::table('leave_user')
                ->join('users', 'users.id', '=', 'leave_user.user_id')
                ->join('leaves', 'leaves.id', '=', 'leave_user.leave_id')
                ->select('leave_user.start', 'leave_user.end', 'users.name', 'users.email', 'leaves.name as leave', 'leave_user.reason as reason')
                // ->where('leave_user.id', $id)
                ->get();

        return $leave;
    }

    public function approveLeave($id, Request $request)
    {
        //dd($id,$request->approve);
        $user = auth()->user()->role;
       //dd($user->role);
        if ($request->approve == '2') {
            $leaveUser = LeaveUsers::find($id);
            
            if($user->role == 'human resource'){

                $leaveUser->thirdApproval = '2';
                $leaveUser->approve = 2;
                $leaveUser->reject_reason = $request->reject_reason;

            }elseif($user->role == 'branch manager'){

                $leaveUser->secondApproval = '2';
                $leaveUser->approve = 5;
                $leaveUser->reject_reason = $request->reject_reason;
            }
            $leaveUser->save();

            return $leaveUser;

        }else{
            // Initialize

            $leaveUser = LeaveUsers::find($id);
            
            if($user->role == 'human resource'){
                $start = carbon::parse($request->start);
                $end = carbon::parse($request->end);
                $difference = date_diff($start,$end);
                $format = '%a';
                $taking_days = $difference->format($format);
                
                $get_balance = $this->leaveBalance(User::find($leaveUser->user_id));

                foreach ($get_balance as $balance) {

                    if($request->leave['name'] == $balance->name && $taking_days <= $balance->balance ){

                        $leaveUser->thirdApproval = 1;
                        $leaveUser->approve = 1;
                        $leaveUser->reject_reason = '';

                    }else{

                        return response()->json(array(
                            'code'      =>  401,
                            'message'   =>  'Unable to Approve Due to Application Exceed(s) User Leave Balance .'
                        ), 401);
                    }
                }

            }elseif($user->role == 'branch manager'){
                $leaveUser->secondApproval = 1;
                $leaveUser->approve = 4;
                $leaveUser->reject_reason = '';
            }
            $leaveUser->save();
            //dd($leaveUser);
            return $leaveUser;
           
        }

    }

    public function listStaffs()
    {
        $outlet_id = Auth::user()->outlet_id;

        $notStaffRole = [1,2,5,6,7];
        $staffs = User::whereHas('staff', function ($query) {
            $query->where('approve', 1);
        })->with('role')->where('users.outlet_id', $outlet_id)->notPartTime()->whereNotIn('role_id', $notStaffRole)->get();    //->where('users.type', '!=', 'part time')

        return $staffs;
    }

    public function leaveBalance(User $user)
    {
        $currentYear = Carbon::now()->format('Y');
        $leaves = $user->leaves()->whereYear('leave_user.start', $currentYear)->whereYear('leave_user.end', $currentYear)->wherePivot('user_id', '=', $user->id)->wherePivot('approve', '!=', '2')->get();

        $leaveUsers = LeaveUserDetail::with('leave')->where('user_id', $user->id)->get();

        $userBalances = [];
        foreach ($leaveUsers as $key => $leaveUser) {
            $name = str_replace(' ', '', $leaveUser->leave->name);
            ${$name} = $leaveUser->days + $leaveUser->carryfoward;

            $temp = new StdClass;
            $temp->id = $leaveUser->leave->id;
            $temp->name = $leaveUser->leave->name;
            $temp->balance = ${$name};
            array_push($userBalances, $temp);
        }
        
        foreach ($leaves as $key => $leave) {
            $name = str_replace(' ', '', $leave->name);
            $datetime1 = date_create($leave->pivot->start);
            $datetime2 = date_create($leave->pivot->end);
            $differenceFormat = '%a';

            $interval = date_diff($datetime1, $datetime2);
            
            $differentDate = $interval->format($differenceFormat);
            
            
            foreach ($userBalances as $key => $userBalance) {
                if ($userBalance->name == $leave->name) {
                    $calcBalance = $userBalance->balance - ($differentDate + 1);
                    $userBalance->balance = $calcBalance;
                    
                }
            }
        }

        return $userBalances;
    }

    public function leaveApproved($month, $year)
    {
        $auth = auth()->user()->role;

        if($auth->role == 'branch manager'){

            $dateObj   = Carbon::createFromFormat('!m', $month + 1);
            $monthName = $dateObj->format('F');

            $first = Carbon::parse('first day of '.$monthName. ' '.$year)->format('Y-m-d');
            $last = Carbon::parse('last day of '.$monthName. ' '.$year)->format('Y-m-d');

            $leaveUser = LeaveUsers::with('user', 'leave')->where('firstApproval', 1)->whereIn('approve',['1','2','4','5'])->where(function ($q) use ($last, $first){
                $q->whereBetween('start', [$first, $last])->orWhereBetween('end', [$first, $last]);
            })
            ->get();
        //dd($leaveUser);
        }else if($auth->role == 'human resource'){

            $dateObj   = Carbon::createFromFormat('!m', $month + 1);
            $monthName = $dateObj->format('F');

            $first = Carbon::parse('first day of '.$monthName. ' '.$year)->format('Y-m-d');
            $last = Carbon::parse('last day of '.$monthName. ' '.$year)->format('Y-m-d');

            $leaveUser = LeaveUsers::with('user', 'leave')->where('secondApproval', 1)->whereIn('approve', ['1','2'])->where(function ($q) use ($last, $first){
                $q->whereBetween('start', [$first, $last])->orWhereBetween('end', [$first, $last]);
            })
            ->get();
        }

        

        // $permission = ['super admin', 'it', 'finance', 'human resource', 'branch manager'];
        $permission = ['human resource', 'branch manager'];
        
        if (in_array($auth->role, $permission)) {   
            return $data = [
                    'permission' => true,
                    'leaveUser' => $leaveUser
                ];
        }
        else{
            return $data = [
                    'permission' => false,
                    'leaveUser' => $leaveUser
                ];
        }
    }
}

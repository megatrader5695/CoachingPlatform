<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Badge;
use App\User;
use App\People;
use App\Package;
use App\Transaction;
use App\ExamsUser;
use App\Exams;
use App\Order;
use App\Outlet;
use App\OrderSubscription;
use App\UserChangePackage;
use App\UserQuestionnaire;
use App\Classroom;
use App\ClassroomUser;
use App\ClassSyllabus;
use App\Syllabus;
use App\PackageUser;
use App\UserNewMember;
use App\Temporary;
use App\Http\Resources\MemberResource;
use Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use stdClass;
use DB;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class MemberController extends Controller
{
    private $value = 'member';

    public function index($status, $outlet)
    {
      $check = app('App\Http\Controllers\CheckPermissionController')->index($this->value);
      if ($check == 'false') {
          return Response::json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
      }

      $users = User::with('parent')->where([['outlet_id', $outlet],['status', $status]])->whereIn('role_id',['6'])->orderBy('name','asc')->paginate('10');
      
      $students = [];
      foreach ($users as $key => $user) {
        
        $member = new stdClass;
        $member->index_number = $users->firstItem() + $key;
        $member->id = $user->id;
        $member->avatar = $user->avatar;
        $member->name = strtoupper($user->name);

        $package_user = PackageUser::where([['user_id', $user->id],['status','active']])->get()->count();

        if($package_user > 0){
          //for latest classroom and instructor 
          $latest_class = PackageUser::with('classroom')->where([['user_id', $user->id],['status','active']])->orderBy('id','desc')->first();
          if(!$latest_class->classroom){

            $member->class_level = '-';
            $member->instructor = '-';

          }else{
            $classroom_detail = Classroom::with('coaches')->where('id',$latest_class->classroom->classroom_id)->first();

            $member->class_level = $classroom_detail->level;
            $member->instructor = strtoupper($classroom_detail->coaches->name);
            //dd($classroom_detail->badge);
            
            if($latest_class->badge != null){
              $badge = Badge::find(max($latest_class->badge));
              $member->padi_level = $badge->name;
            }else{
              $member->padi_level = '-';
            }
          }

        }else{
          $member->class_level = '-';
          $member->instructor = '-';
          $member->padi_level = '-';
        }

        //member parent details
        if(!$user->parent){
          $member->parent_name = '-';
          $member->email = '-';
          $member->contact = '-';
        }else{
          $member->parent_name = strtoupper($user->parent->name);
          $member->email = $user->parent->email;
          $member->contact = $user->parent->contact;
        }
        
        $students[]=$member;
      }

      // $users = DB::table('users as users1')
      //         ->leftJoin('users as users2','users1.id','=','users2.user_id')
      //         ->leftJoin('package_user','users2.id','=','package_user.user_id')
      //         ->select(DB::raw('COUNT(package_user.user_id) as package, users2.id,users2.name as child,users1.name as parent,users1.contact as parent_contact,users2.status,users2.avatar'))
      //         ->where('users2.outlet_id',$outlet)
      //         ->where('users2.status',$status)
      //         ->where( function ( $query )
      //           {
      //               $query->orWhere( 'users2.role_id','5')
      //                   ->orWhere( 'users2.role_id','6' )
      //                   ->orWhere( 'users2.role_id','7' );
      //           })
      //         ->groupBy('users2.id','child','parent','parent_contact','users2.status','users2.avatar')
      //         ->get();
      
      

      $response = [
           'pagination' => [
               'total' => $users->total(),
               'per_page' => $users->perPage(),
               'current_page' => $users->currentPage(),
               'last_page' => $users->lastPage(),
               'from' => $users->firstItem(),
               'to' => $users->lastItem()
           ],
           'data' => $students
       ];

      return $response;

    }

    public function searchStudent(Request $request)
    {
        // $classroom_user = DB::table('classroom_user')
        //                 ->join('classrooms', 'classrooms.id', '=', 'classroom_user.classroom_id')
        //                 ->join('users','classroom_user.user_id','=','users.id')
        //                 ->where('classroom_user.user_id', $request->student)
        //                 ->get();

        $classroom_user = DB::table('users')
                        ->where('id', $request->student)
                        ->get();

        return response()->json([
            'user' => $classroom_user
        ]);
    }

    public function showAllClass($packageUserId)
    {
        $classes = ClassroomUser::with('classes')
                   ->where('package_user_id',$packageUserId)
                   ->get();

        return response()->json([
            'user' => $classes
        ]);
    }

    public function csvData($status, $outlet)
    {
      $check = app('App\Http\Controllers\CheckPermissionController')->index($this->value);
      if ($check == 'false') {
          return Response::json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
      }

      $users = User::with('parent')->where([['outlet_id', $outlet],['status', $status]])->whereIn('role_id',['6'])->orderBy('name','asc')->get();
      
      $students = [];
      foreach ($users as $key => $user) {
        
        $member = new stdClass;
        $member->index_number = $key+1;
        $member->id = $user->id;
        $member->avatar = $user->avatar;
        $member->name = strtoupper($user->name);

        $package_user = PackageUser::where([['user_id', $user->id]])->get()->count();

        if($package_user > 0){
          //for latest classroom and instructor 
          $latest_class = PackageUser::with('classroom')->where('user_id', $user->id)->orderBy('id','desc')->first();
          if(!$latest_class->classroom){
            $member->status = 'No Package';
            $member->class_level = '-';
            $member->instructor = '-';
            $member->padi_level = '-';

          }else{
            $classroom_detail = Classroom::with('coaches')->where('id',$latest_class->classroom->classroom_id)->first();

            $member->status = $latest_class->status;

            if(!$classroom_detail){
              //dd($latest_class->classroom->classroom_id);
              $member->class_level = 'UNKNOWN';
              $member->instructor = 'UNKNOWN';
            }else{
              $member->class_level = $classroom_detail->level;
              $member->instructor = strtoupper($classroom_detail->coaches->name);
            }

            //dd($classroom_detail->badge);
            
            if($latest_class->badge != null){
              $badge = Badge::find(max($latest_class->badge));
              $member->padi_level = $badge->name;
            }else{
              $member->padi_level = '-';
            }
          }

        }else{
          $member->status = 'No Package';
          $member->class_level = '-';
          $member->instructor = '-';
          $member->padi_level = '-';
        }

        //member parent details
        if(!$user->parent){
          $member->parent_name = '-';
          $member->email = '-';
          $member->contact = '-';
        }else{
          $member->parent_name = strtoupper($user->parent->name);
          
          if(empty($user->parent->email)){
            $member->email = '-';
          }else{
            $member->email = $user->parent->email;
          }

          if(empty($user->parent->contact)){
            $member->contact = '-';
          }else{
            $member->contact = $user->parent->contact;
          }
        }
        
        $students[]=$member;
      }

      return $students;

    }

    public function show(User $user)
    {
      $check = app('App\Http\Controllers\CheckPermissionController')->index($this->value);
      if ($check == 'false') {
          return Response::json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
      }
      
      if ($user->role_id == 5 || $user->role_id == 6 || $user->role_id == 7) {
        return new MemberResource($user);
      } else {
        return "This user is not a member";
      }
    }

    public function disableAutoUpgrade(Request $request)
    {
      $package_user = PackageUser::where('id', $request->package_user_id)->first();
      $package_user->auto_upgrade_status = $request->auto_upgrade;
      $package_user->auto_upgrade_reason = $request->reason;
      $package_user->save();

      return $package_user;
    }

    public function store(Request $request){

      $user = new User;

      $user->name = $request->input('name');
      $user->birthday = $request->input('birthday');
      $user->email = $request->input('email');
      $user->password = bcrypt($request->input('password'));
      $user->contact = $request->input('contact');
      $user->health = $request->input('healths');
      $user->join_date = $request->input('join_date');
      $user->gender = $request->input('gender');
      $user->emergency_name1 = $request->input('emergency_name1');
      $user->emergency_relationship1 = $request->input('emergency_relationship1');
      $user->emergency_contact1 = $request->input('emergency_contact1');
      $user->emergency_name2 = $request->input('emergency_name2');
      $user->emergency_relationship2 = $request->input('emergency_relationship2');
      $user->emergency_contact2 = $request->input('emergency_contact2');


      $user->icNumber = $request->input('icNumber');
      $user->street1 = $request->input('street1');
      $user->street2 = $request->input('street2');
      $user->city = $request->input('city');
      $user->postcode = $request->input('postcode');
      $user->state = $request->input('state');
      $user->country = $request->input('country');

      $user->user_id = $request->user_id;
      $user->remarks = $request->input('remark');
      $user->status = $request->input('status');
      $user->role_id = 6;
      $user->outlet_id = Auth::user()->outlet_id;
      $user->save();

      // Assign health to the user
      $healths = $request->input('healths');

      foreach ($healths as $health) {
        $user->healths()->attach($health);
      }

      $year = Carbon::now()->format('Y-m-d');
      $start_time = $year.' 03:00:00';
      $end_time = $year.' 03:01:00';
      $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
      $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();
      
  
      $accLevelIds = '1';
      $Deptcode = '1';

      $details = new stdClass;
      $details->pin = strval($user->id);
      $details->name = $user->name;
      if($user->gender == 'Male') {
          $gender = 'M';
      } else {
          $gender = 'F';
      }
      $details->deptCode = $Deptcode;
      $details->gender = $gender;
      $details->accLevelIds = $accLevelIds;
      $details->accStartTime = $clock_in_start;
      $details->accEndTime = $clock_in_end;


      // $details1 = new stdClass;
      // $details1->pin = '0000000';
      // $details1->name = 'mega june system';
      // // if($user->gender == 'Male') {
      // //     $gender = 'M';
      // // } else {
      // //     $gender = 'F';
      // // }
      // // $details->deptCode = $Deptcode;
      // $details1->gender = 'M';
      // $details1->accLevelIds = '1';
      // $details1->accStartTime = '2020-06-1408:56:00';
      // $details1->accEndTime = '2020-06-1408:56:00';
      // dd($details1,$details);
      $data = json_encode($details);
      
        

      // Assign emergency contact number 
      // $people1 = new People;
      // $people1->name = $request->emergency_name;
      // $people1->relationship = $request->emergency_relationship;
      // $people1->contact = $request->emergency_contact;
      // $people1->user_id = $user->id;
      // $people1->save();

      // if ($request->emergency_name2 && $request->emergency_relationship2 && $request->emergency_contact2) {
      //   $people2 = new People;
      //   $people2->name = $request->emergency_name2;
      //   $people2->relationship = $request->emergency_relationship2;
      //   $people2->contact = $request->emergency_contact2;
      //   $people2->user_id = $user->id;
      //   $people2->save();
      // }

      return $user;
    }

    public function update(Request $request, User $user, $parent){
      
      $old_parent = $user->user_id;


      $user->name = $request->input('name');
      $user->birthday = $request->input('birthday');
      // $user->email = $request->input('email');
      $user->contact = $request->input('contact');
      $user->gender = $request->input('gender');
      $user->street1 = $request->input('street1');
      $user->street2 = $request->input('street2');
      $user->city = $request->input('city');
      $user->postcode = $request->input('postcode');
      $user->state = $request->input('state');
      $user->country = $request->input('country');
      $user->remarks = $request->input('remark');
      $user->health = $request->input('healths');
      $user->join_date = $request->input('join_date');
      $user->status = $request->input('status');
      $user->user_id = $parent;
      $user->emergency_name1 = $request->input('emergency_name1');
      $user->emergency_relationship1 = $request->input('emergency_relationship1');
      $user->emergency_contact1 = $request->input('emergency_contact1');
      $user->emergency_name2 = $request->input('emergency_name2');
      $user->emergency_relationship2 = $request->input('emergency_relationship2');
      $user->emergency_contact2 = $request->input('emergency_contact2');
      $user->updated_at = Carbon::now();

      if($old_parent != $user->user_id){

        $orders = Order::where('member_name',$user->name)->get();

        foreach ($orders as $order) {
          $order->cust_id = $parent;
          $order->save();
          // dd($order);
          $paid_order = Transaction::where('order_id',$order->id)->first();

          if($paid_order){
            $paid_order->cust_id = $parent;
            $paid_order->save();
          }
        }
      }


      // $userDetails = User::find($user->id);

      // if ($userDetails->user_id != $request->input('user_id')) {

      //   $listOrders = Order::where('member_name',$userDetails->name)
      //                 ->where('cust_id',$userDetails->user_id)
      //                 ->get();

      //   foreach ($listOrders as $listOrders) {
      //     //change transaction details
      //     // $transactions = Transaction::where('order_id',$listOrders->id)
      //     //                 ->update([
      //     //                   'cust_id' => $request->input('user_id'),
      //     //                   'updated_at' => Carbon::now()
      //     //                 ]);
      //   }

      //   //change order details
      //   // $orders = Order::where('member_name',$userDetails->name)
      //   //           ->where('cust_id',$userDetails->user_id)
      //   //           ->update([
      //   //             'cust_id' => $request->input('user_id'),
      //   //             'updated_at' => Carbon::now()

      //   //           ]);
      // }

      $user->save();

      $year = Carbon::now()->format('Y-m-d');
      $start_time = $year.' 03:00:00';
      $end_time = $year.' 03:01:00';
      $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
      $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

      // $accLevelIds = '402880276ce125e4016ce12701cf0377';
      $accLevelIds = '1';
      $Deptcode = '1';

      $details = new stdClass;
      $details->pin = strval($user->id);
      $details->name = $user->name;
      if($user->gender == 'Male') {
          $gender = 'M';
      } else {
          $gender = 'F';
      }
      $details->gender = $gender;
      $details->accLevelIds = $accLevelIds;
      $details->deptCode = $Deptcode;
      // $details->accStartTime = $clock_in_start;
      // $details->accEndTime = $clock_in_end;

      $data = json_encode($details);


      return response()->json(compact('user'));
    }

    public function parent($outlet)
    {
        $users;
        
        if ($_GET['q']) {
          $params = $_GET['q'];

          $users = DB::table('users')
                ->where([
                  ['name', 'like', '%' . $params . '%'],
                  ['role_id', 'like', [5],],
                  ['outlet_id', $outlet]
                ])
                ->get();
        } else {
          $users = User::all();
        }


        $object = new stdClass;
        $object->items = $users;

        return (array) $object;
    }

    public function students($outlet){
        $users;

        if ($_GET['q']) {
          $params = $_GET['q'];

          $users = DB::table('users')
                ->where([
                  ['outlet_id', 'like', $outlet],
                  ['name', 'like', '%' . $params . '%'],
                  ['role_id', 'like', [6,7]]
                ])
                ->get();
        } else {
          $users = User::all();
        }

        
        $object = new stdClass;
        $object->items = $users;


        return (array) $object;
    }

    public function studentsClass($outlet)
    {
        $users;

        if ($_GET['q']) {
          $params = $_GET['q'];

          $users = DB::table('users')->where([
                  ['outlet_id', 'like', $outlet],
                  ['name', 'like', '%' . $params . '%'],
                  ['role_id', 'like', [6,7]]
                ])
                // ->whereExists(function ($query) {
                //     $query->select(DB::raw(1))
                //           ->from('classroom_user')
                //           ->whereRaw('classroom_user.user_id = users.id');
                // })
                ->get();

        } else {
          $users = User::all();
        }

        $object = new stdClass;
        $object->items = $users;
        // $object->items->url = $url;

        return (array) $object;
    }

    public function coaches(){
        $users;

        if ($_GET['q']) {
          $params = $_GET['q'];

          $users = DB::table('users')
                ->where([
                  ['name', 'like', '%' . $params . '%'],
                  ['role_id', 'like', 4]
                ])
                ->get();
        } else {
          $users = User::all();
        }


        $object = new stdClass;
        $object->items = $users;

        return (array) $object;
    }

    public function updateBadge(PackageUser $id, Request $request)
    {
      $id->badge = $request->input('level');
      $id->save();
      
      // return "202";
      return $id;
    }

    public function assignEmergency(Request $request, $id){
      
      $people = new People;
      $people->name = $request->name;
      $people->relationship = $request->relationship;
      $people->contact = $request->contact;
      $people->user_id = $id;

      $people->save();
    }

    public function mergeMember($fromMemberId, $toMemberId){
      //Update Package Ownership
      DB::table('package_user')
      ->where('user_id', $fromMemberId)
      ->update(
          [
              'user_id' => $toMemberId
          ]
      );

      // Update Transaction Ownership
      DB::table('transactions')
      ->where('member_id', $fromMemberId)
      ->update(
          [
              'member_id' => $toMemberId
          ]
      );

      // Update old member status to STOP
      DB::table('users')
            ->where('id', $fromMemberId)
            ->update(
                [
                    'status' => 'stop'
                ]
            );



    }

    public function packageMember($id, $classId)
    {
      // get user_id and package_id in table package_user
      $package_user = DB::table('package_user')->where('id',$id)->select('user_id', 'package_id', 'created_at')->first();

      // get parent id in table users
      $user = DB::table('users')->where('id',$package_user->user_id)->select('user_id')->first();

      //delete data in table orders
      DB::table('orders')->where([
          ['cust_id', $user->user_id],
          ['package_id', $package_user->package_id],
          ['status', '1'],
          ['created_at', $package_user->created_at]
          ])->delete();

      // delete data in package_user
      DB::table('package_user')->where('id',$id)->delete();

      if ($classId != '') {
          //delete data in classroom_user
          DB::table('classroom_user')->where([
              ['classroom_id', $classId],
              ['user_id', $package_user->user_id]
              ])->delete();
      }

      // return response()->json([
      //     "success" => "2",
      //     "text" => $classroom
      // ]);
    }

    public function editDate($id, $date, $userid)
    {
      $package = Package::find($userid);
      $expiry = (new Carbon($date))->addMonths($package->subscription);

      DB::table('package_user')
      ->where('id', $id)
      ->update(
          [
              'start_date' => $date,
              'expiry' => $expiry
          ]
      );

      return response()->json([
          "success" => "2",
          "sdate" => $date,
          "edate" => $expiry
      ]);
    }

    public function checkReplacement($classId, $id)
    {
        $now = Carbon::now();
        $checkReplacement = DB::table('transfers')
            ->where([
              ['member_id', $id],
              ['fromClassroom', $classId]
            ])
            ->whereMonth('fromDate', $now->month)
            ->get();

        return $checkReplacement;
    }

    public function viewMemberPackage($id)
    {
        $package_user = DB::table('package_user')->where('id', $id)->first();
        return response()->json($package_user);
    }

    public function getPackageUserId($user_id)
    {
        // $package_user = DB::table('package_user')
        //                 ->where('package_id', $id)
        //                 ->where('user_id', $user_id)
        //                 ->get();
        // return response()->json($package_user);


        
      $package_user = PackageUser::with('packages','user','classroom')
                        ->where('user_id', $user_id)
                        ->get();
      //dd($package_user);
      foreach ($package_user as $pu) {
        $order = Order::where('package_user_id', $pu->id)->orderBy('id', 'DESC')->first();
        $pu->order = $order;
        $pu->outlet = Outlet::find($pu->outlet_id);
        if(!empty($pu->classroom)){
          $classroom_detail = Classroom::with('coaches')->where('id', $pu->classroom->classroom_id)->first();
          $pu->classDetail = $classroom_detail;
        }else{
          $pu->classDetail = null;
        }
        
      }
        // dd($package_user);
        return $package_user;
    }

    public function updateRemarks(Request $request, $packageUser)
    {
      // dd($request->Remarks);
      $package_user = PackageUser::where('id', $packageUser)->first();
      $package_user->remarks = $request->Remarks;
      $package_user->save();
    }

    public function checkPrePayment($packageUser, $date)
    {
      // dd($request->Remarks);
      $existingOrder = Order::where([['package_user_id', $packageUser],['monthly_payment', $date],['registration_fee', '0'],['status', '0']])->get()->last();

      if ($existingOrder) {
        return $existingOrder;
      }else{
        return 'not_paid';
      }
    }

    public function updatePrePayment(Request $request, $packageUser, $date)
    {
      // dd($packageUser, $date, $request->id);
      $existingOrder = Order::findOrfail($request->id);
      // dd($existingOrder);

      if ($existingOrder) {
        
        $existingOrder->monthly_payment = $date;
        $existingOrder->created_at = Carbon::now();
        $existingOrder->updated_at = Carbon::now();
        $existingOrder->save();

      }else{

        return 'order_not_found';
      }
    }

    public function getPackageUserIds($user_id)
    {
        // $package_user = DB::table('package_user')
        //                 ->where('package_id', $id)
        //                 ->where('user_id', $user_id)
        //                 ->get();
        // return response()->json($package_user);


      $package_user = PackageUser::find($user_id);

      // foreach ($package_user as $pu) {
      //   $order = Order::where('package_user_id', $pu->id)->orderBy('id', 'DESC')->first();
      //   $pu->order = $order;
      //   $pu->outlet = Outlet::find($pu->outlet_id);
      //   if(!empty($pu->classroom)){
      //     $classroom_detail = Classroom::with('coaches')->where('id', $pu->classroom->classroom_id)->first();
      //     $pu->classDetail = $classroom_detail;
      //   }else{
      //     $pu->classDetail = null;
      //   }
        
      // }
        // dd($package_user);
        return $package_user;
    }

    public function showOrder($id)
    {
      // dd($id);
        $package_user = Order::where('package_user_id', $id)
                        ->paid()
                        ->orderByDesc('monthly_payment')
                        ->first();
        return response()->json($package_user);
    }

    public function viewOrderMember($id)
    {
        $order = Order::with('package')->where('package_user_id', $id)->paid()->orderByDesc('monthly_payment')->first();
        return $order;
    }

    public function updateMemberPackage(Request $request, PackageUser $package_user)
    {   
      // dd('hi');
        DB::beginTransaction();
        $package_user->packages;

        $current_month = Carbon::now()->startOfMonth();

        $getDate = Carbon::now()->format('d'); //get current date

        $getNextMonth = Carbon::now()->addMonths(1)->format('m'); //get next month

        $date = new Carbon($request->start);
        $startMonth = $date->month;
        $startYear = $date->year;

        $order = Order::where('package_user_id', $package_user->id)->where('monthly_payment', $date->format('Y-m-d'))->where('status', '1')->get()->last();

        $latest_order = Order::where('package_user_id', $package_user->id)->where('status', '1')->get()->last();

        $order_paid_reg = Order::where('package_user_id', $package_user->id)->where('monthly_payment', $date->format('Y-m-d'))->where('registration_fee','!=', '0')->first();

        $orders_count = Order::where('package_user_id', $package_user->id)->get();
        // dd($current_month,$date,$order,$order_paid_reg,$orders_count->count());
        // dd($orders_count->count(),$orders_count[0],$orders_count[1]);
        // dd($order);
        //selected month is prior to current month and the order's has been paid on selected month
        if($current_month > $date && $order == null && $order_paid_reg == null){

          $temporary = 'denied';
          return $temporary;
        }

        $conditions = [
            'user_id' => $package_user->user_id
        ];
        $value = [
          'from_package_id' => $package_user->packages->id,
          'to_package_id'   => $request->selected,
          'start_date'      => $date->format('Y-m-d'),
          'expiry'          => $package_user->expiry,
        ];

        $temporary = Temporary::updateOrCreate($conditions, $value);

        if (!$temporary) {
            DB::rollback();
        }

        

        //dd($orders_count->count());
        //for new member change package 
        //condition: first order is unpaid and registration fee is 0 or one
        if($orders_count->count() == '2' && $orders_count[0]->status == '1' && $orders_count[1]->status == '1'){
            
          $newPackage = Package::find($temporary->to_package_id);
          $newprice = $newPackage->price;
          $deposit = $newPackage->deposit;

            if ($order) {
              $order->package_id = $newPackage->id;
              $order->package_price = $newprice;
              $order->package_depo = $deposit;
              
              $order->package_total = $newprice + $deposit + $order->registration_fee;
              $order->monthly_payment = $date->format('Y-m-d');

              $order->save();

              $orders_count[0]->package_id = $newPackage->id;
              $orders_count[0]->save();

              $date = new Carbon($order->monthly_payment);
              $index = -1;
              $newDate = $date->addMonths($index + $newPackage->subscription);
              $month  = $newDate->month;
              $year  = $newDate->year;

              $condition = [
                  'order_id'          => $order->id,
                  'package_user_id'   => $order->package_user_id,
              ];

              $insertOrderSubscription = [
                  'order_id'          => $order->id,
                  'package_user_id'   => $order->package_user_id,
                  'month'             => $month,
                  'year'              => $year
              ];
              // dd($insertOrderSubscription);
              $orderSubscription = OrderSubscription::updateOrCreate($condition,$insertOrderSubscription);
              
              if (!$orderSubscription) {
                  DB::rollback();
              }

              $updatePackageUser = PackageUser::with('user')->where('id',$package_user->id)->first();
              $updatePackageUser->package_id  = $temporary->to_package_id;
              // $updatePackageUser->start_date  = $temporary->start_date;
              $updatePackageUser->temporary_id  = $temporary->id;
              $updatePackageUser->deposit     = $newPackage->deposit;
              $updatePackageUser->outstanding = $order->package_total + $orders_count[0]->registration_fee;
              // $updatePackageUser->deposit     = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit : $package_user->packages->deposit;
              $updatePackageUser->save();

              if (!$updatePackageUser) {
                  DB::rollback();
              }
            }
        }else if($orders_count->count() == '2' && $orders_count[0]->status == '0' && $orders_count[1]->status == '1'){
          
          $newPackage = Package::find($temporary->to_package_id);
          $newprice = $newPackage->price;
          $deposit = $newPackage->deposit;
          // dd($order_paid_reg);
            if ($order) {
              // dd('hi');
              $order->package_id = $newPackage->id;
              $order->package_price = $newprice;
              $order->package_depo = $deposit;
              
              $order->package_total = $newprice + $deposit + $order->registration_fee;
              $order->monthly_payment = $date->format('Y-m-d');

              $order->save();

              $orders_count[0]->package_id = $newPackage->id;
              $orders_count[0]->save();

              $date = new Carbon($order->monthly_payment);
              $index = -1;
              $newDate = $date->addMonths($index + $newPackage->subscription);
              $month  = $newDate->month;
              $year  = $newDate->year;

              // dd($order->id);
              $condition = [
                  'order_id'          => $order->id,
                  'package_user_id'   => $order->package_user_id,
              ];

              $insertOrderSubscription = [
                  'order_id'          => $order->id,
                  'package_user_id'   => $order->package_user_id,
                  'month'             => $month,
                  'year'              => $year
              ];

              $orderSubscription = OrderSubscription::updateOrCreate($condition,$insertOrderSubscription);
              
              if (!$orderSubscription) {
                  DB::rollback();
              }

              $updatePackageUser = PackageUser::with('user')->where('id',$package_user->id)->first();
              // dd($updatePackageUser);
              $updatePackageUser->package_id  = $temporary->to_package_id;
              // $updatePackageUser->start_date  = $temporary->start_date;
              $updatePackageUser->temporary_id  = $temporary->id;
              $updatePackageUser->deposit     = $newPackage->deposit;
              $updatePackageUser->outstanding = $order_paid_reg->package_total - $order_paid_reg->registration_fee;
              // $updatePackageUser->deposit     = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit : $package_user->packages->deposit;
              $updatePackageUser->save();
              // dd($updatePackageUser);
              if (!$updatePackageUser) {
                  DB::rollback();
              }
            }
          
        }else if($package_user->packages->subscription > '1' && $latest_order->status == '1'){

          $unpaid_orders = Order::where([['package_user_id', $package_user->id],['id', '!=', $latest_order->id],['registration_fee', '0'],['status','1']])->get();

          foreach ($unpaid_orders as $unpaid) {
            $unpaid->delete();
          }
          
          $newPackage = Package::find($temporary->to_package_id);
          // $deposit = 0;
          // $deposit = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit - $package_user->packages->deposit : 0;

          if ($newPackage->deposit > $package_user->packages->deposit) {

            //current package < new package
            $newprice = $newPackage->price;
            $deposit = $newPackage->deposit - $package_user->packages->deposit;

          } else {

            //current package > new package
            $newprice = $newPackage->price - ($package_user->packages->deposit - $newPackage->deposit);
            $deposit = 0;

          }

          $latest_order->package_id = $newPackage->id;
          $latest_order->package_price = $newprice;
          $latest_order->package_depo = $deposit;
          $latest_order->package_total = $newprice + $deposit;
          $latest_order->save();

          $current_month;
          $unpaid_month = carbon::parse($latest_order->monthly_payment);
          $diff = $current_month->diff($unpaid_month);
          $index = round(($diff->format('%y') * 12 + $diff->format('%m'))/$newPackage->subscription);
          $orders = '';
          //dd($diff, $newPackage->subscription, $index);
          for($i=0; $i<$index; $i++){
            //$date_test = [$i];
            $check_paid_order = Order::where([['status','0'],['monthly_payment', $unpaid_month->addMonth($newPackage->subscription)->format('Y-m-d')]])->first();
            
            if(!$check_paid_order){
              
              $insertOrder = [
                  'member_name'             =>  $latest_order->member_name,
                  'cust_id'                 =>  $latest_order->cust_id,
                  'package_id'              =>  $newPackage->id,
                  'outlet_id'               =>  $latest_order->outlet_id,
                  'package_user_id'         =>  $package_user->id,
                  'package_price'           =>  $newPackage->price,
                  'package_depo'            =>  '0',
                  'package_total'           =>  $newPackage->price,
                  'monthly_payment'         =>  $unpaid_month->addMonth($newPackage->subscription)->format('Y-m-d'),
                  'registration_fee_status' =>  '1',
                  'status'                  =>  '1',
                  'is_new_member'           =>  '0'
              ];

              $orders = Order::create($insertOrder);
              if (!$orders) {
                  DB::rollback();
              }
            } 
          }//dd($date_test);
          if($orders == ''){
            $date = new Carbon($latest_order->monthly_payment);
            $order_id = $latest_order->id;
            
          }else{
            $date = new Carbon($orders->monthly_payment);
            $order_id = $orders->id;
          }
          
          $index = -1;
          $newDate = $date->addMonths($index + $newPackage->subscription);
          $month  = $newDate->month;
          $year  = $newDate->year;

          $condition = [
              'package_user_id'   => $latest_order->package_user_id,
          ];

          $insertOrderSubscription = [
              'order_id'     => $order_id,
              'package_user_id'   => $package_user->id,
              'month'             => $month,
              'year'              => $year
          ];

          $orderSubscription = OrderSubscription::updateOrCreate($condition,$insertOrderSubscription);

          if (!$orderSubscription) {
              DB::rollback();
          }

          $updatePackageUser = PackageUser::with('user')->where('id',$package_user->id)->first();
          $updatePackageUser->package_id  = $temporary->to_package_id;
          // $updatePackageUser->start_date  = $temporary->start_date;
          $updatePackageUser->temporary_id  = $temporary->id;
          $updatePackageUser->deposit     = $newPackage->deposit;
          // $updatePackageUser->deposit     = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit : $package_user->packages->deposit;
          $updatePackageUser->save();

          if (!$updatePackageUser) {
              DB::rollback();
          }

        }else if($package_user->packages->subscription == '1' && $latest_order->status == '1'){

          $unpaid_orders = Order::where([['package_user_id', $package_user->id],['id', '!=', $latest_order->id],['status','1']])->get();

          foreach ($unpaid_orders as $unpaid) {
            $unpaid->delete();
          }

          $newPackage = Package::find($temporary->to_package_id);
          // $deposit = 0;
          // $deposit = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit - $package_user->packages->deposit : 0;

          if ($newPackage->deposit > $package_user->packages->deposit) {

            //current package < new package
            $newprice = $newPackage->price;
            $deposit = $newPackage->deposit - $package_user->packages->deposit;

          } else {

            //current package > new package
            $newprice = $newPackage->price - ($package_user->packages->deposit - $newPackage->deposit);
            $deposit = 0;

          }
           
          $insertOrder = [
                  'member_name'       =>  $package_user->user->name,
                  'cust_id'           =>  $package_user->user->user_id,
                  'package_id'        =>  $newPackage->id,
                  'outlet_id'         =>  $package_user->outlet_id,
                  'package_user_id'   =>  $package_user->id,
                  'package_price'     =>  $newprice,
                  'package_depo'      =>  $deposit,
                  'registration_fee'  =>  '0',
                  'registration_fee_status'  =>  '1',
                  'package_total'     =>  $newprice + $deposit,
                  'monthly_payment'   =>  $date->format('Y-m-d'),
                  'status'            =>  '1',
                  'is_new_member'     =>  '0'
              ];

          $orders = Order::create($insertOrder);
          if (!$orders) {
              DB::rollback();
          }

          if($latest_order){
            $latest_order->delete();
          }
          

          $date = new Carbon($orders->monthly_payment);
          $index = -1;
          $newDate = $date->addMonths($index + $newPackage->subscription);
          $month  = $newDate->month;
          $year  = $newDate->year;

          $condition = [
              'package_user_id'   => $latest_order->package_user_id,
          ];

          $insertOrderSubscription = [
              'order_id'     => $orders->id,
              'package_user_id'   => $orders->package_user_id,
              'month'             => $month,
              'year'              => $year
          ];

          $orderSubscription = OrderSubscription::updateOrCreate($condition,$insertOrderSubscription);
          
          if (!$orderSubscription) {
              DB::rollback();
          }

          $updatePackageUser = PackageUser::where('id',$package_user->id)->with('user')->first();
          $updatePackageUser->package_id  = $temporary->to_package_id;
          // $updatePackageUser->start_date  = $temporary->start_date;
          $updatePackageUser->temporary_id  = $temporary->id;
          $updatePackageUser->deposit     = $newPackage->deposit;
          // $updatePackageUser->deposit     = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit : $package_user->packages->deposit;
          $updatePackageUser->save();

          if (!$updatePackageUser) {
              DB::rollback();
          }

        }else{
      //for change package that start date is next month and now it more than 25th
          if ($getDate >= 25 && $startMonth == $getNextMonth) {

            $newPackage = Package::find($temporary->to_package_id);

            $checkNewMember = UserNewMember::where('user_id',$package_user->user_id)
                              ->first();

            if ($newPackage->deposit > $package_user->packages->deposit) {

              //current package < new package
              $newprice = $newPackage->price;
              $deposit = $newPackage->deposit - $package_user->packages->deposit;

            } else {

              //current package > new package
              $newprice = $newPackage->price - ($package_user->packages->deposit - $newPackage->deposit);
              $deposit = 0;

            }

            if ($order) {
              $order->package_id = $newPackage->id;
              $order->package_price = $newprice;
              $order->package_depo = $deposit;

              if($checkNewMember->status == 'new') {

                $regFee = $newPackage->registration_fee;
                $order->registration_fee = $regFee;

              } elseif ($checkNewMember->status == 'old') {

                $regFee = 0;
                $order->registration_fee = $regFee;

              }
              
              $order->package_total = $newprice + $deposit + $regFee;
              $order->monthly_payment = $date->format('Y-m-d');

              $order->save();

              $date = new Carbon($order->monthly_payment);
              $index = -1;
              $newDate = $date->addMonths($index + $newPackage->subscription);
              $month  = $newDate->month;
              $year  = $newDate->year;

              $insertOrderSubscription = [
                  'order_id'          => $order->id,
                  'package_user_id'   => $order->package_user_id,
                  'month'             => $month,
                  'year'              => $year
              ];

              $orderSubscription = OrderSubscription::where('package_user_id', $package_user->id)->update($insertOrderSubscription);
              if (!$orderSubscription) {
                  DB::rollback();
              }

            } else {

              if($checkNewMember->status == 'new') {
                $regFee = $newPackage->registration_fee;
              } elseif($checkNewMember->status == 'old') {
                $regFee = 0;
              }

              $insertOrder = [
                  'member_name'       =>  $package_user->user->name,
                  'cust_id'           =>  $package_user->user->user_id,
                  'package_id'        =>  $newPackage->id,
                  'outlet_id'         =>  $package_user->outlet_id,
                  'package_user_id'   =>  $package_user->id,
                  'package_price'     =>  $newprice,
                  'package_depo'      =>  $deposit,
                  'registration_fee'  =>  $regFee,
                  'package_total'     =>  $newprice + $deposit + $regFee,
                  'monthly_payment'   =>  $date->format('Y-m-d'),
                  'status'            =>  '1',
                  'is_new_member'     =>  '0'
              ];

              $orders = Order::create($insertOrder);
              if (!$orders) {
                  DB::rollback();
              }

              $date = new Carbon($orders->monthly_payment);
              $index = -1;
              $newDate = $date->addMonths($index + $newPackage->subscription);
              $month  = $newDate->month;
              $year  = $newDate->year;

              $insertOrderSubscription = [
                  'order_id'          => $orders->id,
                  'package_user_id'   => $orders->package_user_id,
                  'month'             => $month,
                  'year'              => $year
              ];

              $orderSubscription = OrderSubscription::where('package_user_id', $package_user->id)->update($insertOrderSubscription);
              if (!$orderSubscription) {
                  DB::rollback();
              }

            }

            $updatePackageUser = PackageUser::where('id',$package_user->id)->first();;
            $updatePackageUser->package_id  = $temporary->to_package_id;
            // $updatePackageUser->start_date  = $temporary->start_date;
            $updatePackageUser->temporary_id  = $temporary->id;
            $updatePackageUser->deposit     = $newPackage->deposit;
            // $updatePackageUser->deposit     = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit : $package_user->packages->deposit;
            $updatePackageUser->save();

            if (!$updatePackageUser) {
                DB::rollback();
            }

          }

          if ($current_month->format('Y-m-d') == $date->format('Y-m-d')) {
              $newPackage = Package::find($temporary->to_package_id);
              // $deposit = 0;
              // $deposit = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit - $package_user->packages->deposit : 0;

              if ($newPackage->deposit > $package_user->packages->deposit) {

                //current package < new package
                $newprice = $newPackage->price;
                $deposit = $newPackage->deposit - $package_user->packages->deposit;

              } else {

                //current package > new package
                $newprice = $newPackage->price - ($package_user->packages->deposit - $newPackage->deposit);
                $deposit = 0;

              }

              $insertOrder = [
                  'member_name'       =>  $order->member_name,
                  'cust_id'           =>  $order->cust_id,
                  'package_id'        =>  $newPackage->id,
                  'outlet_id'         =>  $order->outlet_id,
                  'package_user_id'   =>  $package_user->id,
                  'package_price'     =>  $newprice,
                  'package_depo'      =>  $deposit,
                  'package_total'     =>  $newprice + $deposit,
                  'monthly_payment'   =>  $date->format('Y-m-d'),
                  'status'            =>  '1',
                  'is_new_member'     =>  '0'
              ];

              $orders = Order::create($insertOrder);
              if (!$orders) {
                  DB::rollback();
              }
              $order->delete();

              $date = new Carbon($orders->monthly_payment);
              $index = -1;
              $newDate = $date->addMonths($index + $newPackage->subscription);
              $month  = $newDate->month;
              $year  = $newDate->year;

              $insertOrderSubscription = [
                  'order_id'     => $orders->id,
                  'package_user_id'   => $orders->package_user_id,
                  'month'             => $month,
                  'year'              => $year
              ];

              $orderSubscription = OrderSubscription::where('package_user_id', $package_user->id)->update($insertOrderSubscription);
              if (!$orderSubscription) {
                  DB::rollback();
              }

              $updatePackageUser = PackageUser::with('user')->where('id',$package_user->id)->first();
              $updatePackageUser->package_id  = $temporary->to_package_id;
              // $updatePackageUser->start_date  = $temporary->start_date;
              $updatePackageUser->temporary_id  = $temporary->id;
              $updatePackageUser->deposit     = $newPackage->deposit;
              // $updatePackageUser->deposit     = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit : $package_user->packages->deposit;
              $updatePackageUser->save();

              if (!$updatePackageUser) {
                  DB::rollback();
              }
          }
          else{

              $newPackage = Package::find($temporary->to_package_id);
              // $deposit = 0;
              // $deposit = $newPackage->deposit > $package_user->packages->deposit ? $newPackage->deposit - $package_user->packages->deposit : 0;

              if ($newPackage->deposit > $package_user->packages->deposit) {

                //current package < new package
                $newprice = $newPackage->price;
                $deposit = $newPackage->deposit - $package_user->packages->deposit;

              } else {

                //current package > new package
                $newprice = $newPackage->price - ($package_user->packages->deposit - $newPackage->deposit);
                $deposit = 0;

              }

              $updatePackageUser = PackageUser::with('user')->where('id',$package_user->id)->first();
              $updatePackageUser->package_id  = $temporary->to_package_id;
              // $updatePackageUser->start_date  = $temporary->start_date;
              $updatePackageUser->temporary_id  = $temporary->id;
              $updatePackageUser->deposit     = $newPackage->deposit;
              $updatePackageUser->save();

              if (!$updatePackageUser) {
                  DB::rollback();
              }
          }
      }

      DB::commit();
      return $temporary;
    }

    public function updateMemberPackageMonth(Request $request, PackageUser $package_user)
    {
        DB::beginTransaction();

        $package_user->packages;

        $date = new Carbon($request->start);

        $original_join_date = new Carbon($package_user->start_date);

        $diff_in_months = $date->diffInMonths($original_join_date);

        if ($diff_in_months == 0) {

          $diff_in_days = $date->diffInDays($original_join_date);

          if($diff_in_days > 27) {

            $diff_in_months = 1;

          }

        }

        //dd($diff_in_months, $original_join_date, $date);

        $startMonth = $date->month;

        $startYear = $date->year;

        $subscription = $package_user->packages->subscription;
        //dd('package_id', $package_user->packages->id);
        $orders = Order::where([['package_user_id', $package_user->id],['package_id', $package_user->packages->id]])->get();

        $monthly_payment_arr = [];
        $expiry_arr = [];
        foreach ($orders as $key => $order) {
        
          if ($order->monthly_payment != null && $key == '0') {

            $monthly_payment = new Carbon($date);
            $new_monthly_payment = $monthly_payment->format('Y-m-d');

            $monthly_payment_arr[$key] = $new_monthly_payment;
            if($order->status == '1' && empty($expiry_arr)){
              $expiry_arr[0] = $new_monthly_payment;
            }
            $update_month = Order::where('id',$order->id)
                          ->update([
                            'monthly_payment' => $new_monthly_payment,
                            'updated_at' => Carbon::now()
                          ]);
          }else if ($order->monthly_payment != null){
            
            $index = $key-1;
            
            $monthly_payment = Carbon::parse($monthly_payment_arr[$index]);
            
            $new_monthly_payment = $monthly_payment->addMonths($subscription)->format('Y-m-d');
            //dd('add',$new_monthly_payment);
            $monthly_payment_arr[$key] = $new_monthly_payment;
            if($order->status == '1' && empty($expiry_arr)){
              $expiry_arr[0] = $new_monthly_payment;
            }
            $update_month = Order::where('id',$order->id)
                          ->update([
                            'monthly_payment' => $new_monthly_payment,
                            'updated_at' => Carbon::now()
                          ]);
          }
          
        }//dd($expiry_arr[0]);

        if(!empty($expiry_arr)){
          $new_expiry = $expiry_arr[0];
        }else{
          $new_expiry = Carbon::parse(end($monthly_payment_arr))->addMonths($subscription)->format('Y-m-d');
        }

        $update_package_user = PackageUser::where('id',$package_user->id)
                               ->update([
                                'join_date' => $request->start,
                                'start_date' => $request->start,
                                'expiry' => $new_expiry,
                                'updated_at' =>Carbon::now()
                               ]);

        $orderSubscription = OrderSubscription::where('package_user_id', $package_user->id)->first();
            
        $next_order_month = Carbon::parse(end($monthly_payment_arr))->addMonths($subscription - 1);

        $updateOrderSubs = OrderSubscription::where('id', $orderSubscription->id)
                             ->update([
                              'month' => $next_order_month->month,
                              'year' => $next_order_month->year,
                              'updated_at' => Carbon::now()
                             ]);
        

        DB::commit();

        return $orders;
    }


    public function updateMemberPackageStatus(Request $request, $id, $class, $memberid)
    {
        DB::beginTransaction();
        $getpackage = PackageUser::find($id);
        $status = $request->status;

        $package = Package::find($getpackage->package_id);
        $current_month = Carbon::now()->startOfMonth();
        $dateStart = new Carbon($request->start);
        $dateStart = $dateStart->startOfMonth();

        if ($status == 'freeze') {
          $dateEnd = new Carbon($request->end);
          $dateEnd = $dateEnd->endOfMonth();
          $freeze_price = $request->freeze_price;
        }

        $conditions = [
          'package_user_id' =>  $id
        ];

        if ($status == 'freeze') {
          $insertChangePackage = [
            'status'          => $status,
            'start_date'      => $dateStart->format('Y-m-d'),
            'end_date'        => $dateEnd->format('Y-m-d'),
            'class_id'        => $class,
            'changed_by'      => Auth::user()->id,
            // 'freeze_price'    => $freeze_price,
          ];
        } elseif ($status == 'terminate') {
          $insertChangePackage = [
            'status'      => $status,
            'start_date'  => $dateStart->format('Y-m-d'),
            'class_id'    => $class,
            'changed_by'  => Auth::user()->id,
          ];
        } else {
          $insertChangePackage = [
            'status'      => $status,
            'start_date'  => $dateStart->format('Y-m-d'),
            'end_date'    => $dateStart->format('Y-m-d'),
            'class_id'    => $class,
            'changed_by'  => Auth::user()->id,
          ];
          
        }
        

        $userChangePackage = UserChangePackage::updateOrCreate($conditions, $insertChangePackage);
        if (!$userChangePackage) {
            DB::rollback();
        }

        if ($status == 'active') {

          $order = Order::where('package_user_id', $getpackage->id)->where('status', '1')->first();

          $order->monthly_payment = $dateStart->format('Y-m-d');

          $order->save();

          $addMonths = -1;
          $addMonths = $addMonths + $getpackage->packages->subscription;

          $newDate = $dateStart->format('Y-m-d');
          $date = $newDate->addMonths($addMonths);
          $month  = $date->month;
          $year  = $date->year;

          $conditionsOrderSubscription = [
              'package_user_id'   => $order->package_user_id,
          ];

          $insertOrderSubscription = [
              'order_id'          => $order->id,
              'month'             => $month,
              'year'              => $year
          ];

          $orderSubscription = OrderSubscription::updateOrCreate($conditionsOrderSubscription,$insertOrderSubscription);

          if (!$orderSubscription) {
              DB::rollback();
          }

          if ($order) {
            $newOutstandingss = ($getpackage->outstanding - $order->package_total);
          } else {
            $newOutstandingss = $getpackage->outstanding;
          }

          if ($newOutstandingss < 0) {
            $newOutstandingss = 0;
          }

          $newOutstandings = $newOutstandingss + $package->price;

          $getpackage->outstanding = $newOutstandings;
          $getpackage->expiry = $dateStart->format('Y-m-d');

          if($dateStart->format('Y-m-d') == $current_month->format('Y-m-d')) {

            $getpackage->status = $request->status;

          }
          
          $getpackage->save();

          if (!$getpackage) {
              DB::rollback();
          }

        }

        if ($dateStart->format('Y-m-d') < $current_month->format('Y-m-d')) {

          if ($status == 'terminate') {
                $order = Order::where('package_user_id', $getpackage->id)->where('status', '1')->first();

                $insertOrder = [
                    'package_user_id'   => $getpackage->id,
                    'package_price'     => $getpackage->deposit,
                    'package_depo'      => '0',
                    'package_total'     => $getpackage->deposit,
                    'status'            => '1',
                    'member_name'       => $order->member_name,
                    'cust_id'           => $order->cust_id,
                    'package_id'        => $getpackage->packages->id,
                    'outlet_id'         => $order->outlet_id,
                    'monthly_payment'   => $dateStart->format('Y-m-d')
                ];
                $orders = Order::create($insertOrder);
                if (!$orders) {
                    DB::rollback();
                }

                $order->delete(); // delete latest unpaid bill

                $newDate = new Carbon($request->start);
                $month  = $newDate->month;
                $year  = $newDate->year;

                $insertOrderSubscription = [
                    'order_id'     => $orders->id,
                    'package_user_id'   => $orders->package_user_id,
                    'month'             => $month,
                    'year'              => $year
                ];

                $orderSubscription = OrderSubscription::create($insertOrderSubscription);
                if (!$orderSubscription) {
                    DB::rollback();
                }

                $getpackage->status = $request->status;
                $getpackage->save();
                if (!$getpackage) {
                    DB::rollback();
                }

            }

            if ($status == 'terminate' || $status == 'stop') {
                $classroom_user = ClassroomUser::where('classroom_id', $class)->where('user_id', $getpackage->user->id)->first();
                $classroom_user->delete();  
            }

          if ($status == 'freeze') {

              $order = Order::with('package')->where('package_user_id', $getpackage->id)->where('status', '1')->first();
              
              $newExpirys = new Carbon($request->end);
              $newExpiry = $newExpirys->addMonths(1);
              $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');

                $insertOrder = [
                    'package_user_id'   => $getpackage->id,
                    'package_price'     => $package->price,
                    'package_depo'      => '0',
                    'package_total'     => $package->price,
                    'status'            => '1',
                    'member_name'       => $getpackage->user->name,
                    'cust_id'           => $getpackage->user->user_id,
                    'package_id'        => $getpackage->packages->id,
                    'outlet_id'         => $getpackage->outlet_id,
                    'monthly_payment'   => $newExpiry
                ];

                $orders = Order::create($insertOrder);
                if (!$orders) {
                    DB::rollback();
                }

                $addMonths = -1;
                $addMonths = $addMonths + $getpackage->packages->subscription;

                $newDates = new Carbon($request->end);
                $newDate = $newDates->addMonths(1)->format('Y-m-d');
                $newDates = new Carbon($newDate);
                $date = $newDates->addMonths($addMonths);
                $month  = $date->month;
                $year  = $date->year;

                $conditionsOrderSubscription = [
                    'package_user_id'   => $orders->package_user_id,
                ];

                $insertOrderSubscription = [
                    'order_id'          => $orders->id,
                    'month'             => $month,
                    'year'              => $year
                ];

                $orderSubscription = OrderSubscription::updateOrCreate($conditionsOrderSubscription,$insertOrderSubscription);

                if (!$orderSubscription) {
                    DB::rollback();
                }

                if ($getpackage->outstanding > 0) {

                  if ($order) {

                    $newOutstandings = ($getpackage->outstanding - $order->package_total) + $package->price;

                  } else {

                    $newOutstandings = $package->price;

                  }

                  if ($newOutstandings < 0) {
                    $newOutstandings = 0;
                  }

                  $newOutstanding = $newOutstandings;

                  $getpackage->outstanding = $newOutstanding;
                  $newExpirys = new Carbon($request->end);
                  $newExpiry = $newExpirys->addMonths(1);
                  $newExpiry = new Carbon($newExpiry);
                  $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
                  $getpackage->expiry = $newExpiry;
                  $getpackage->status = $request->status;
                  $getpackage->save();

                } else {

                  $newOutstanding = $package->price;

                  $getpackage->outstanding = $newOutstanding;
                  $newExpirys = new Carbon($request->end);
                  $newExpiry = $newExpirys->addMonths(1);
                  $newExpiry = new Carbon($newExpiry);
                  $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
                  $getpackage->expiry = $newExpiry;
                  $getpackage->status = $request->status;
                  $getpackage->save();

                }

                if (!$getpackage) {
                    DB::rollback();
                }

                if ($order) {

                  $order->delete(); // delete latest unpaid bill

                }
            }
        }

        if ($dateStart->format('Y-m-d') > $current_month->format('Y-m-d')) {

          if ($status == 'freeze') {

              $order = Order::with('package')->where('package_user_id', $getpackage->id)->where('status', '1')->first();
              
              $newExpirys = new Carbon($request->end);
              $newExpiry = $newExpirys->addMonths(1);
              $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');

                $insertOrder = [
                    'package_user_id'   => $getpackage->id,
                    'package_price'     => $package->price,
                    'package_depo'      => '0',
                    'package_total'     => $package->price,
                    'status'            => '1',
                    'member_name'       => $getpackage->user->name,
                    'cust_id'           => $getpackage->user->user_id,
                    'package_id'        => $getpackage->packages->id,
                    'outlet_id'         => $getpackage->outlet_id,
                    'monthly_payment'   => $newExpiry
                ];

                $orders = Order::create($insertOrder);
                if (!$orders) {
                    DB::rollback();
                }

                $addMonths = -1;
                $addMonths = $addMonths + $getpackage->packages->subscription;

                $newDates = new Carbon($request->end);
                $newDate = $newDates->addMonths(1)->format('Y-m-d');
                $newDates = new Carbon($newDate);
                $date = $newDates->addMonths($addMonths);
                $month  = $date->month;
                $year  = $date->year;

                $conditionsOrderSubscription = [
                    'package_user_id'   => $orders->package_user_id,
                ];

                $insertOrderSubscription = [
                    'order_id'          => $orders->id,
                    'month'             => $month,
                    'year'              => $year
                ];

                $orderSubscription = OrderSubscription::updateOrCreate($conditionsOrderSubscription,$insertOrderSubscription);

                if (!$orderSubscription) {
                    DB::rollback();
                }

                if ($getpackage->outstanding > 0) {

                  if ($order) {

                    $newOutstandings = ($getpackage->outstanding - $order->package_total) + $package->price;

                  } else {

                    $newOutstandings = $package->price;

                  }

                  if ($newOutstandings < 0) {
                    $newOutstandings = 0;
                  }

                  $newOutstanding = $newOutstandings;

                  $getpackage->outstanding = $newOutstanding;
                  $newExpirys = new Carbon($request->end);
                  $newExpiry = $newExpirys->addMonths(1);
                  $newExpiry = new Carbon($newExpiry);
                  $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
                  $getpackage->expiry = $newExpiry;
                  if($dateEnd->format('Y-m-d') == $current_month->format('Y-m-d')) {
                    $getpackage->status = $request->status;
                  }
                  $getpackage->save();

                } else {

                  $newOutstanding = $package->price;

                  $getpackage->outstanding = $newOutstanding;
                  $newExpirys = new Carbon($request->end);
                  $newExpiry = $newExpirys->addMonths(1);
                  $newExpiry = new Carbon($newExpiry);
                  $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
                  $getpackage->expiry = $newExpiry;
                  if($dateEnd->format('Y-m-d') == $current_month->format('Y-m-d')) {
                    $getpackage->status = $request->status;
                  }
                  $getpackage->save();

                }

                if (!$getpackage) {
                    DB::rollback();
                }

                if ($order) {

                  $order->delete(); // delete latest unpaid bill

                }
            }
        }

        if ($dateStart->format('Y-m-d') == $current_month->format('Y-m-d')) {

            if ($status == 'terminate') {
                $order = Order::where('package_user_id', $getpackage->id)->where('status', '1')->first();

                $insertOrder = [
                    'package_user_id'   => $getpackage->id,
                    'package_price'     => $getpackage->deposit,
                    'package_depo'      => '0',
                    'package_total'     => $getpackage->deposit,
                    'status'            => '1',
                    'member_name'       => $order->member_name,
                    'cust_id'           => $order->cust_id,
                    'package_id'        => $getpackage->packages->id,
                    'outlet_id'         => $order->outlet_id,
                    'monthly_payment'   => $dateStart->format('Y-m-d')
                ];
                $orders = Order::create($insertOrder);
                if (!$orders) {
                    DB::rollback();
                }

                $order->delete(); // delete latest unpaid bill

                $newDate = new Carbon($request->start);
                $month  = $newDate->month;
                $year  = $newDate->year;

                $insertOrderSubscription = [
                    'order_id'     => $orders->id,
                    'package_user_id'   => $orders->package_user_id,
                    'month'             => $month,
                    'year'              => $year
                ];

                $orderSubscription = OrderSubscription::create($insertOrderSubscription);
                if (!$orderSubscription) {
                    DB::rollback();
                }

                // $getpackage->deposit = '0';
                $getpackage->save();
                if (!$getpackage) {
                    DB::rollback();
                }
            }

            if ($status == 'freeze') {

              $order = Order::with('package')->where('package_user_id', $getpackage->id)->where('status', '1')->first();

              $newExpirys = new Carbon($request->end);
              $newExpiry = $newExpirys->addMonths(1);
              $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');

              $insertOrder = [
                  'package_user_id'   => $getpackage->id,
                  'package_price'     => $package->price,
                  'package_depo'      => '0',
                  'package_total'     => $package->price,
                  'status'            => '1',
                  'member_name'       => $getpackage->user->name,
                  'cust_id'           => $getpackage->user->user_id,
                  'package_id'        => $getpackage->packages->id,
                  'outlet_id'         => $getpackage->outlet_id,
                  'monthly_payment'   => $newExpiry
              ];

              $orders = Order::create($insertOrder);
              if (!$orders) {
                  DB::rollback();
              }

              $addMonths = -1;
              $addMonths = $addMonths + $getpackage->packages->subscription;

              $newDates = new Carbon($request->end);
              $newDate = $newDates->addMonths(1)->format('Y-m-d');
              $newDates = new Carbon($newDate);
              $date = $newDates->addMonths($addMonths);
              $month  = $date->month;
              $year  = $date->year;

              $conditionsOrderSubscription = [
                  'package_user_id'   => $orders->package_user_id,
              ];

              $insertOrderSubscription = [
                  'order_id'          => $orders->id,
                  'month'             => $month,
                  'year'              => $year
              ];

              $orderSubscription = OrderSubscription::updateOrCreate($conditionsOrderSubscription,$insertOrderSubscription);

              if (!$orderSubscription) {
                  DB::rollback();
              }

              if ($getpackage->outstanding > 0) {

                if ($order) {

                  $newOutstandings = ($getpackage->outstanding - $order->package_total) + $package->price;

                } else {

                  $newOutstandings = $package->price;

                }

                if ($newOutstandings < 0) {
                  $newOutstandings = 0;
                }

                $newOutstanding = $newOutstandings;

                $getpackage->outstanding = $newOutstanding;
                $newExpirys = new Carbon($request->end);
                $newExpiry = $newExpirys->addMonths(1);
                $newExpiry = new Carbon($newExpiry);
                $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
                $getpackage->expiry = $newExpiry;
                $getpackage->status = $request->status;
                $getpackage->save();

              } else {

                $newOutstanding = $package->price;

                $getpackage->outstanding = $newOutstanding;
                $newExpirys = new Carbon($request->end);
                $newExpiry = $newExpirys->addMonths(1);
                $newExpiry = new Carbon($newExpiry);
                $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
                $getpackage->expiry = $newExpiry;
                $getpackage->status = $request->status;
                $getpackage->save();

              }

              if (!$getpackage) {
                  DB::rollback();
              }

              if ($order) {

                $order->delete(); // delete latest unpaid bill

              }

            }
            

            if ($status == 'terminate' || $status == 'stop') {
                $classroom_user = ClassroomUser::where('classroom_id', $class)->where('user_id', $getpackage->user->id)->first();
                $classroom_user->delete();  
            }

            $getpackage->status = $request->status;
            $getpackage->save();
            if (!$getpackage) {
                DB::rollback();
            }
        }
        DB::commit();
        return response()->json(compact('getpackage', 'status', 'package', 'userChangePackage'));
    }

    public function getNewPackage($id, $package)
    {
        $current_month = Carbon::now()->startOfMonth();
        $getpackage = DB::table('temporary')
          ->where([
              ['user_id', $id],
              ['from_package_id', $package],
              ['start_date', '>=', $current_month->format('Y-m-d')]
            ])
          ->get();

        $newpackage = [];
        foreach ($getpackage as $getpackages) {
          $package = Package::findOrfail($getpackages->to_package_id);

          $temp = new stdClass;
          $temp->start_date = $getpackages->start_date;
          $temp->id = $getpackages->id;
          $temp->name = $package->name;

          array_push($newpackage, $temp);
        }

        return response()->json(compact('newpackage'));
    }

    public function deleteNewPackage($id)
    {
        DB::table('package_user')->where('temporary_id', $id)->update([
            'temporary_id'  => null
        ]);

        $getpackage = DB::table('temporary')
          ->where([
              ['id', $id]
            ])
          ->delete();

        return response()->json(compact('getpackage'));
    }

    public function addClass(Request $request, $id)
    {
        $package_user = PackageUser::find($request->packuserId);
        $package_user->status = 'active';
        $package_user->save();
        
        $insert = [
            'classroom_id' => $request->class,
            'user_id' => $id,
            'package_id' => $package_user->package_id,
            'package_user_id' => $package_user->id,
            'status' => 'active'
        ];
        $classroom_user = ClassroomUser::create($insert);

        return response()->json(compact('classroom_user'));
    }

    public function createOrder(Request $request)
    {
      
      $date = Carbon::parse($request->ordermonth);

      $user = User::findOrfail($request->user);

      $packages_user = $user->packages()->where('package_user.id', $request->package_user)->first();
      $pu = PackageUser::with('packages')->where('id', $request->package_user)->first();
      
      $orders = Order::where('package_user_id', $packages_user->pivot->id)->get();
      // dd($orders);
      if($pu->deposit_status == 'paid'){
        
        $deposit = '0';
        $price = $pu->packages->price;
        $total = $pu->packages->price + $deposit;

      }else{
        
        $deposit = $pu->packages->deposit;
        $price = $pu->packages->price;
        $total = $pu->packages->price + $deposit;
      }

      $date_arr = [];
      foreach ($orders as $key => $order) {
        $date_arr[] = $order->monthly_payment;
      }
      
      if (!in_array($date->format('Y-m-d'), $date_arr)) {
        
        $insert = [
            "member_name"       => $user->name,
            "cust_id"           => $user->user_id,
            "package_id"        => $packages_user->id,
            "outlet_id"         => $user->outlet_id,
            "package_user_id"   => $packages_user->pivot->id,
            "package_price"     => $request->packageprice,
            "registration_fee_status" => '1',
            "package_depo"      => $request->advancefee,
            "package_total"     => $request->packageprice + $request->advancefee,
            "status"            => '1',
            "monthly_payment"   => $date->startOfMonth()->format('Y-m-d'),
            "created_at"        => Carbon::now(),
            "updated_at"        => Carbon::now(),
        ];
        // dd($insert);
        $orders = Order::create($insert);

        //dd($orders);
        $calculation = Carbon::parse($orders->monthly_payment)->addMonths(($orders->package->subscription-1))->startOfMonth()->format('Y-m-d');
      
        $date = Carbon::createFromFormat('Y-m-d', $calculation);

        $condition = [
            'package_user_id' => $packages_user->pivot->id
        ];

        $add = [
            'order_id'  => $orders->id,
            'month'     => $date->month,
            'year'      => $date->year,
        ];

        $orderSubscsription = OrderSubscription::updateOrCreate($condition, $add);
        if (!$orderSubscsription) {
            DB::rollback();
        }

        return response()->json([
            "order" => $packages_user
        ]);

      }else{
        
        return response()->json(array(
            'code'      =>  500,
            'message'   =>  'The Bill already created'
        ), 500);
      }
          
    }

    public function userChangePackage($id)
    {
        $userChangePackage = UserChangePackage::with('packageUsers')->where('package_user_id',$id)->get()->last();
        //dd($userChangePackage);
        if($userChangePackage == null){
          $userChangePackage = [
            'package_user_id' => $id,
            'status' =>  'empty'
          ];
        }
        return $userChangePackage;

    }

    public function update_freeze(Request $request, $id)
    {
        //dd($request->start_date, $request->end_date);
        $userChangePackage = UserChangePackage::where('package_user_id',$id)->get()->last();
        $userChangePackage->start_date = carbon::parse($request->start_date)->startofMonth()->format('Y-m-d');
        $userChangePackage->end_date = carbon::parse($request->end_date)->endofMonth()->format('Y-m-d');
        $userChangePackage->save();

        return $userChangePackage;
    }

    public function userQuestionnaire($package_user)
    {
        $package_user = PackageUser::find($package_user);

        $questions = Exams::where('package_id', $package_user->package_id)->get();

        $result_array = [];

        foreach ($questions as $question) {

          $result = ExamsUser::where([['package_user_id',$package_user->id],['package_id', $package_user->package_id],['question_id', $question->id]])->first();

          if($result){

            $result_array[] = [
              'id' => $question->id,
              'question' => $question->question,
              'result' => strval($result->result)
            ];

          }else{

            $result_array[] = [
              'id' => $question->id,
              'question' => $question->question,
              'result' => '0'
            ];

          }

        }

        return $result_array;
    }

    public function usersubmitQuestionnaire(Request $request, $package_user)
    {
      // dd($request[0]['question']);
      // dd($request);
      $get_package_user = $package_user;
      $total_exams = 0;
      $total_passed = 0;

      foreach ($request->all() as $key => $value) {
        
        $question_id = $value['question'];
        $result_id = intval($value['answer']);

        if($question_id && $result_id == 1){

          $total_exams++;
          $total_passed++;

        }else{
          $total_exams++;
        }

        $package_user = PackageUser::find($get_package_user);
        // dd($package_user->id);
        // $questions = Exams::where('package_id', $package_user->package_id)->first();

        $existing_results = ExamsUser::where([['package_user_id',$package_user->id],['package_id', $package_user->package_id],['question_id', $question_id]])->first();

        if($existing_results){

          $existing_results->result = $result_id;
          $existing_results->save();

        }else{

          $new_results = new ExamsUser;
          $new_results->question_id = $question_id;
          $new_results->package_id = $package_user->package_id;
          $new_results->package_user_id = $package_user->id;
          $new_results->result = $result_id;
          $new_results->created_at = carbon::now();
          $new_results->updated_at = carbon::now();
          $new_results->save();

        }

      }

      if($total_exams == $total_passed){

        $package_user->auto_upgrade_status = '1';
        $package_user->save();

      }else{

        $package_user->auto_upgrade_status = '0';
        $package_user->save();
      }

      

      return $package_user;
    }

    public function userProgress($classId)
    {
        $classroom = Classroom::find($classId);
        $syllabus = Syllabus::where('level', $classroom->level)->first();
        $question = \App\SyllabusQuestion::where('syllabus_id', $syllabus->id)->get();
        $class_syllabus = ClassSyllabus::where([
          ['class_id', $classId],
          ['status', 'active'],
          ['coach_id', $classroom->coaches->id],
        ])->get();

        return response()->json([
          'class_syllabus'  => $class_syllabus,
          'syllabus'        => $syllabus,
          'count'           => count($question)
      ]);
    }

    public function updatePaymentLog(Request $request, $id){
      DB::beginTransaction();
        // dd($request->package_id);
        //dd($id, $depo, $outstanding, $expiry, $order_sub_month, $order_sub_year);
        if($request->month == '0'){//because javascript will send it as zero if decembaer is selected.
          $order_sub_month = '12';
          $order_sub_year = $request->year - 1;
        }else{
          $order_sub_month = $request->month;
          $order_sub_year = $request->year;
        }
        //dd($order_sub_year);
        $package_user = [ 
          'deposit' => $request->deposit,
          'expiry' => $request->expiry,
          'outstanding' => $request->outstanding
        ];

        $order_sub = [
          'month' => $order_sub_month,
          'year' => $order_sub_year
        ];

        $updatePackageUser = PackageUser::where('id', $id)->update($package_user);
        $updateOrderSubscription = OrderSubscription::where('package_user_id', $id)->update($order_sub);
        if(!$updatePackageUser || !$updateOrderSubscription){
            DB::rollback();
        }

        $package_user = PackageUser::find($id);
        $status = $request->status;

        $package = Package::find($package_user->package_id);
        $new_package = Package::find($request->package_id);
        $current_month = Carbon::now()->startOfMonth();
        $dateStart = Carbon::parse($request->start)->startOfMonth();

        if($package->id != $request->package_id){

          $today_date = carbon::now()->format('d');
          $unpaid_order = Order::where([['package_user_id',$package_user->id],['monthly_payment',$current_month],['status','1']])->get()->last();
          // dd($unpaid_order->id);
          
          if($unpaid_order){

            $new_exp = carbon::parse($unpaid_order->monthly_payment)->startofMonth();
            $new_order_month = carbon::parse($unpaid_order->monthly_payment)->addMonths($new_package->subscription)->subMonths(1)->startofMonth();
            $new_subscription = OrderSubscription::where([['package_user_id', $package_user->id],['order_id',$unpaid_order->id]])->get()->last();

            if($package->deposit > $new_package->deposit){

              if($package_user->deposit_status == 'unpaid'){

                $new_package_price = $new_package->price;
                $new_deposit = $new_package->deposit;
                $deposit_status = $package_user->deposit_status;

              }else if($package_user->deposit_status == 'paid'){

                $deduct_deposit = $package->deposit - $new_package->deposit;
                $new_package_price = $new_package->price - $deduct_deposit;
                $new_deposit = '0';
                $deposit_status = $package_user->deposit_status;

              }

              $unpaid_order->package_id = $new_package->id;
              $unpaid_order->package_price = $new_package_price;
              $unpaid_order->package_depo = $new_deposit;
              $unpaid_order->package_total = $new_deposit+$new_package_price+$unpaid_order->registration_fee;
              $unpaid_order->save();

              $package_user->package_id = $new_package->id;
              $package_user->deposit = $new_package->deposit;
              $package_user->deposit_status = $deposit_status;
              if($unpaid_order->registration_fee_status == 0){
                $package_user->outstanding = $new_package_price+$new_deposit+100;
              }else if($unpaid_order->registration_fee_status == 1){
                $package_user->outstanding = $new_package_price;
              }
              $package_user->expiry = $new_exp;
              $package_user->save();

              if($new_package->subscription == 1 && $today_date >= 25 ){

                $new_order = [ 
                  'member_name' => $unpaid_order->member_name,
                  'cust_id' => $unpaid_order->cust_id,
                  'package_id' => $unpaid_order->package_id,
                  'outlet_id' => $unpaid_order->outlet_id,
                  'package_user_id' => $unpaid_order->package_user_id,
                  'package_price' => $unpaid_order->package_price,
                  'package_depo' => '0',
                  'registration_fee' => '0',
                  'registration_fee_status' => $unpaid_order->registration_fee_status,
                  'freeze_price' => $unpaid_order->freeze_price,
                  'package_total' => $unpaid_order->package_price+$unpaid_order->package_depo,
                  'monthly_payment' => carbon::parse($unpaid_order->monthly_payment)->addMonths(1)->startofMonth()->format('Y-m-d'),
                  'status' => '1',
                  'created_at' => carbon::now(),
                  'updated_at' => carbon::now()
                ];

                $createNewOrder = Order::create($new_order);

                if($createNewOrder){
                  $generate_order = Carbon::createFromFormat('Y-m-d', $createNewOrder->monthly_payment);

                  $new_subscription->order_id = $createNewOrder->id;
                  $new_subscription->month = $generate_order->month;
                  $new_subscription->year = $generate_order->year;
                  $new_subscription->save();
                }

              }else{

                $generate_order = $new_order_month;

                $new_subscription->order_id = $unpaid_order->id;
                $new_subscription->month = $new_order_month->month;
                $new_subscription->year = $new_order_month->year;
                $new_subscription->save();

              }

            }else if($package->deposit < $new_package->deposit){

              if($package_user->deposit_status == 'unpaid'){

                $new_deposit = $new_package->deposit;
                $deposit_status = $package_user->deposit_status; 

              }else if($package_user->deposit_status == 'paid'){

                $new_deposit = $new_package->deposit - $package->deposit;
                $deposit_status = $package_user->deposit_status; 

              }

              $unpaid_order->package_id = $new_package->id;
              $unpaid_order->package_price = $new_package->price;
              $unpaid_order->package_depo = $new_deposit;
              $unpaid_order->package_total = $new_deposit+$new_package->price+$unpaid_order->registration_fee;
              $unpaid_order->save();

              $package_user->package_id = $new_package->id;
              $package_user->deposit = $new_package->deposit;
              $package_user->deposit_status = $deposit_status;
              if($unpaid_order->registration_fee_status == 0){
                $package_user->outstanding = $new_package->price+$new_deposit+100;
              }else if($unpaid_order->registration_fee_status == 1){
                $package_user->outstanding = $new_package->price+$new_deposit;
              }
              $package_user->expiry = $new_exp;
              $package_user->save();

              if($new_package->subscription == 1 && $today_date >= 25 ){

                $new_order = [ 
                  'member_name' => $unpaid_order->member_name,
                  'cust_id' => $unpaid_order->cust_id,
                  'package_id' => $unpaid_order->package_id,
                  'outlet_id' => $unpaid_order->outlet_id,
                  'package_user_id' => $unpaid_order->package_user_id,
                  'package_price' => $unpaid_order->package_price,
                  'package_depo' => '0',
                  'registration_fee' => '0',
                  'registration_fee_status' => $unpaid_order->registration_fee_status,
                  'freeze_price' => $unpaid_order->freeze_price,
                  'package_total' => $unpaid_order->package_price+$unpaid_order->package_depo,
                  'monthly_payment' => carbon::parse($unpaid_order->monthly_payment)->addMonths(1)->startofMonth()->format('Y-m-d'),
                  'status' => '1',
                  'created_at' => carbon::now(),
                  'updated_at' => carbon::now()
                ];

                $createNewOrder = Order::create($new_order);

                if($createNewOrder){
                  $generate_order = Carbon::createFromFormat('Y-m-d', $createNewOrder->monthly_payment);

                  $new_subscription->order_id = $createNewOrder->id;
                  $new_subscription->month = $generate_order->month;
                  $new_subscription->year = $generate_order->year;
                  $new_subscription->save();
                }

              }else{

                $generate_order = $new_order_month;

                $new_subscription->order_id = $unpaid_order->id;
                $new_subscription->month = $new_order_month->month;
                $new_subscription->year = $new_order_month->year;
                $new_subscription->save();

              }

            }
          }else if(!$unpaid_order){

            $package_user->package_id = $new_package->id;
            $package_user->deposit = $new_package->deposit;
            $package_user->deposit_status = 'paid';
            $package_user->save();
          }
          
        }

        if ($status == 'freeze') {
          $freeze_price = '-';
        }

        $conditions = [
          'package_user_id' =>  $id
        ];

        if ($status == 'freeze') {
          $insertChangePackage = [
            'status'          => $status,
            'start_date'      => Carbon::parse($request->start)->startOfMonth()->format('Y-m-d'),
            'end_date'        => Carbon::parse($request->start)->endOfMonth()->format('Y-m-d'),
            'class_id'        => $request->classroom_id,
            'changed_by'      => Auth::user()->id,
            // 'freeze_price'    => $freeze_price,
          ];
        } elseif ($status == 'terminate') {
          $insertChangePackage = [
            'status'      => $status,
            'start_date'  => $dateStart->format('Y-m-d'),
            'class_id'    => $request->classroom_id,
            'changed_by'  => Auth::user()->id,
          ];
        } else {
          $insertChangePackage = [
            'status'      => $status,
            'start_date'  => $dateStart->format('Y-m-d'),
            'class_id'    => $request->classroom_id,
            'changed_by'  => Auth::user()->id,
          ];
        }

        $userChangePackage = UserChangePackage::updateOrCreate($conditions, $insertChangePackage);
        if (!$userChangePackage) {
            DB::rollback();
        }

        if ($request->status == 'freeze' && $dateStart->format('Y-m-d') <= $current_month->format('Y-m-d')) {

          if ($package_user->outstanding > 0) {

              $order = Order::where('package_user_id', $package_user->id)->where('status', '1')->first();

              if ($order) {

                $newOutstandings = ($package_user->outstanding - $order->package_total) + $package->price;

              } else {

                $newOutstandings = $package->price;

              }

              if ($newOutstandings < 0) {
                $newOutstandings = 0;
              }

              $newOutstanding = $newOutstandings;

              $package_user->outstanding = $newOutstanding;
              $newExpirys = new Carbon($request->end);
              $newExpiry = $newExpirys->addMonths(1);
              $newExpiry = new Carbon($newExpiry);
              $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
              $package_user->expiry = $newExpiry;
              $package_user->status = $request->status;
              $package_user->save();

            } else {

              $newOutstanding = $package->price;

              $package_user->outstanding = $newOutstanding;
              $newExpirys = new Carbon($request->end);
              $newExpiry = $newExpirys->addMonths(1);
              $newExpiry = new Carbon($newExpiry);
              $newExpiry = $newExpiry->startOfMonth()->format('Y-m-d');
              $package_user->expiry = $newExpiry;
              $package_user->status = $request->status;
              $package_user->save();

            }

            if (!$package_user) {
                DB::rollback();
            }
        
        }elseif($request->status == 'terminate' && $dateStart->format('Y-m-d') <= $current_month->format('Y-m-d')){

          $package_user->status = $request->status;
          $package_user->save();

          $classroom_user = ClassroomUser::where('classroom_id', $request->classroom_id)->where('package_user_id', $id)->first();

          if ($classroom_user) {
            $classroom_user->delete(); 
          }

          if (!$package_user) {
              DB::rollback();
          }
        
        }elseif($request->status == 'active'){
          //dd($request->status);
          if($dateStart->format('Y-m-d') == $current_month->format('Y-m-d')) {

            $package_user->status = $request->status;

          }
          
          $package_user->save();

          if (!$package_user) {
              DB::rollback();
          }

        }elseif($request->status == 'stop'){

          $classroom_user = ClassroomUser::where('classroom_id', $request->classroom_id)->where('package_user_id', $id)->first();
          
          if ($classroom_user) {
            $classroom_user->delete(); 
          }

        }

        DB::commit();
    }

    public function getPackageUser(PackageUser $id)
    {
        return $id;
    }

    public function LatestMemberOrder($id){
        $order = Order::where('package_user_id', $id)->get()->last();
        return $order;
    }

    public function MemberPackageUser($id){
        $packageuser = PackageUser::with('packages','classroom')->where('id', $id)->get()->last();
        return $packageuser;
    }

    public function MemberOrderSub($id){
        $OrderSubscription = OrderSubscription::where('package_user_id', $id)->get()->last();
        return $OrderSubscription;
    }

    public function hardTerminate($id){
      
      $classroom_user = ClassroomUser::where('package_user_id', $id)->first();
      $orderSubscription = OrderSubscription::where('package_user_id', $id)->first();
      $package_user = PackageUser::find($id);

      $conditions = [
          'package_user_id' =>  $id
        ];

      $insertChangePackage = [
            'status'     => 'terminate',
            'start_date' => carbon::parse()->format('Y-m-d'),
            'class_id'   => $classroom_user->classroom_id,
            'changed_by' => Auth::user()->id,
          ];

      $deleted_log = UserChangePackage::updateOrCreate($conditions, $insertChangePackage);

      
      $classroom_user->delete(); // remove user from classroom

      $order = Order::where('package_user_id', $id)->where('status', '1')->get()->last();
      if($order){
        $order->delete(); // delete latest unpaid bill
      }
      

     
      $orderSubscription->delete(); // delete subscription
      
      
      $package_user->status = 'terminate';
      $package_user->save(); //Package User change status to terminate 
      
      $data = [$classroom_user, $order, $orderSubscription, $package_user];
      

      return $data;
          
    }
}

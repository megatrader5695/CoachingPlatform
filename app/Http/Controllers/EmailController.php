<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;

class EmailController extends Controller
{
    public function index() {
    	return Email::all();
    }

    public function show(Email $email) {
    	return $email;
    }

    public function store(Request $request) {
    	$email = new Email;
    	$email->title = $request->title;
        $email->content = $request->content;
    	$email->subject = $request->subject;

    	$email->save();
    	return $email;
    }
    
    public function update(Email $email, Request $request) {

    	$email->title = $request->title;
    	$email->content = $request->content;
    	$email->sent = $request->sent;
    	$email->save();

    	return $email;

    }

    public function delete(Email $email) {
    	$email->delete();
    	return "Email has been deleted";
    }

    public function getVariable()
    {
        return [
            '{user_name}' => 'User Name',
            '{package_name}' => 'Package Name',
            '{package_price}' => 'Package Price',
            '{class}' => 'Class',
            '{amount}' => 'Amount',
            '{date}' => 'Date',
            '{outstanding}' => 'User Outstanding',
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Schedule;
use App\Staff;
use App\CurrentLogin;
use Carbon\Carbon;
use DB;
use Session;

class ScheduleController extends Controller
{
    public function index()
    {
        $today = Carbon::now()->format('l');
        $session = CurrentLogin::where('user_id',auth()->user()->id)->where('session_id', session()->getId())->first();
        $outlet_id = $session->outlet_id;

        $notStaffRole = [1,2,5,6,7];

        $offDuty = User::whereHas('staff', function ($q) {
                        $q->where('approve', 1);
                    })->with('role')->where('users.outlet_id', $outlet_id)->notPartTime()->whereNotIn('role_id', $notStaffRole)->whereNotExists(function ($q) use ($today) {
                        $q->select(DB::raw(1))
                                ->from('schedules')
                                ->whereRaw('schedules.user_id = users.id')
                                ->where('schedules.day', $today);
                    })->get();

        $onDuty = User::whereHas('staff', function ($q) {
                        $q->where('approve', 1);
                    })->with('role')->where('users.outlet_id', $outlet_id)->notPartTime()->whereNotIn('role_id', $notStaffRole)->whereExists(function ($q) use ($today) {
                        $q->select(DB::raw(1))
                                ->from('schedules')
                                ->whereRaw('schedules.user_id = users.id')
                                ->where('schedules.day', $today);
                    })->get();


        $data = [
            'onDuty' => $onDuty,
            'offDuty' => $offDuty,
        ];
        
        return $data;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

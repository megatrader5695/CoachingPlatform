<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Health;

class HealthController extends Controller
{
    
    public function index() {
    	return Health::all();
    }

    public function show(Health $health) {
    	return $health;
    }

    public function store(Request $request) {
    	$health = new Health;
    	$health->name = $request->name;

    	$health->save();
    	return $health;
    }
    
    public function update(Health $health, Request $request) {

    	$health->name = $request->name;
    	$health->save();

    	return $health;

    }

    public function delete(Health $health) {
    	$health->delete();
    	return "Health has been deleted";
    }
}

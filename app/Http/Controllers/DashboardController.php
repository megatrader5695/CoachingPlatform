<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Claim;
use App\Leave;
use App\Transfer;
use App\SalesOverview;
use App\PackageUser;
use App\DashboardDetails;
use App\UserChangePackage;
use App\Package;
use App\ClassroomUser;
use App\Classroom;
use App\Attendance;
use App\Transaction;
use App\LeaveUsers;
use App\ClaimUsers;
use App\Feedback;
use App\CurrentLogin;
use Session;
use DB;
use Carbon\Carbon;
use StdClass;
use Auth;

class DashboardController extends Controller
{
    private $leaveAccess = 'leave';
    private $claimAccess = 'claim';

    public function swim_attendance(Request $request)
    {
        // dd($request->attendanceId,$request->classId,$request->userId,$request->replacementId,$request->fromClassId,$request->attendanceWeek,$request->status);

        
        if($request->replacementId != null && $request->fromClassId != null){
            $classroom = $request->fromClassId;
        }else{
            $classroom = $request->classId;
        }
        // dd($classroom);
        $week = intval($request->week);
        $selected_date = $request->date;
        $classroom_to = Classroom::findOrFail($classroom);
        $clock_in_start = Carbon::parse($selected_date.' '.$classroom_to->start)->subMinutes(15)->format('Y-m-d H:i:s');
        $clock_in_end = Carbon::parse($selected_date.' '.$classroom_to->end)->subMinutes(16)->format('Y-m-d H:i:s');
        // $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($selected_date.' '.$classroom_to->start)));
        // $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($selected_date.' '.$classroom_to->end)));
        // dd($clock_in_start,$clock_in_end,$week,$classroom_to);
        // dd($request->attendanceId);
        // get package id in table classroom_user
        // dd($week);

        $classroom_user = DB::table('classroom_user')->where([
            ['classroom_id', $classroom],
            ['user_id', $request->userId]
        ])
        ->first();
        // dd($classroom_user);

        $currentTime = Carbon::now('Asia/Kuala_Lumpur');
        // $weekOfMonth = $currentTime->weekOfMonth;

        $packageId = $classroom_user->package_id != '' ? $classroom_user->package_id : null;

        $get_existing_attendance = Attendance::where([['user_id', $request->userId],['class_id', $classroom],['week', $week]])->orWhere('week', 0)->orWhere('package_id', null)->get();
        // dd($get_existing_attendance,$request->attendanceId);
        
       
        if($get_existing_attendance->count() > 0){

            $latest_existing_att = $get_existing_attendance->last()->id;

            foreach ($get_existing_attendance as $ea) {
                
                if($ea->id != $latest_existing_att){
                    $ea->forceDelete();
                }
            }
        }  

        if($request->attendanceId){

            // $convert_week = intval($request->attendanceWeek);
            // dd($convert_week);
            $check_attendances = Attendance::where('id', $request->attendanceId)->first();
            // dd($check_attendances);
            if($check_attendances){

                $check_attendances->package_id = $packageId;
                $check_attendances->clockIn = $currentTime->toDateTimeString();
                $check_attendances->clock_in_start = $clock_in_start;
                $check_attendances->clock_in_end = $clock_in_end;
                $check_attendances->status = $request->status;
                $check_attendances->outlet_id = $classroom_to->outlet_id;
                $check_attendances->created_at = $clock_in_start;
                $check_attendances->updated_at = $currentTime->toDateTimeString();
                $check_attendances->save();

                $attendances = $check_attendances;
            
            }else{

                $attendances = new Attendance;
                $attendances->user_id = $request->userId;
                $attendances->class_id = $classroom;
                $attendances->week = $week;
                $attendances->package_id = $request->packageId;
                $attendances->clockIn = $currentTime->toDateTimeString();
                $attendances->status = $request->status;
                $attendances->clock_in_start = $clock_in_start;
                $attendances->clock_in_end = $clock_in_end;
                $attendances->outlet_id = $classroom_to->outlet_id;
                $attendances->save();

            }

            return $attendances;

        }else if($request->attendanceId == null){
            // dd('hi');

            // $convert_week = intval($request->attendanceWeek);

            $check_attendances = Attendance::where([['user_id', $request->userId],['class_id', $classroom],['week', $week],['package_id', $packageId]])->first();

            // dd($check_attendances);
            if($check_attendances){

                $check_attendances->package_id = $packageId;
                $check_attendances->clockIn = $currentTime->toDateTimeString();
                $check_attendances->clock_in_start = $clock_in_start;
                $check_attendances->clock_in_end = $clock_in_end;
                $check_attendances->status = $request->status;
                $check_attendances->outlet_id = $classroom_to->outlet_id;
                $check_attendances->created_at = $clock_in_start;
                $check_attendances->updated_at = $currentTime->toDateTimeString();
                $check_attendances->save();

                $attendances = $check_attendances;
            
            }else{

                $attendances = new Attendance;
                $attendances->user_id = $request->userId;
                $attendances->class_id = $classroom;
                $attendances->week = $week;
                $attendances->package_id = $packageId;
                $attendances->clockIn = $currentTime->toDateTimeString();
                $attendances->status = $request->status;
                $attendances->clock_in_start = $clock_in_start;
                $attendances->clock_in_end = $clock_in_end;
                $attendances->outlet_id = $classroom_to->outlet_id;
                $attendances->save();

            }

            return $attendances;

        }else if($request->replacementId){
            
            $condition = [
                'replacement_id' => $request->replacementId,
                'user_id' => $request->userId,
                'class_id' => $request->fromClassId,
                'week' => intval($request->week),
                'package_id' => $packageId
            ];

            $setValue = [
                'clockIn' => $currentTime->toDateTimeString(),
                'status' => $request->status,
                'clock_in_start' => $clock_in_start,
                'clock_in_end' => $clock_in_end,
                'outlet_id' => $classroom_to->outlet_id,
            ];

            $value = array_merge($condition, $setValue);
            $attendances = Attendance::updateOrCreate($condition, $value);
            return $attendances;

            }
        
    }

    public function leaveHistory(Request $request)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->leaveAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

        $user = $request->user();

        $role = $user->role;

        if ($role->role == 'human resource') {

            $secondApproval = 1;
            $thirdApproval = 0;
            $leaves = LeaveUsers::with('user', 'leave')
                        ->where([
                            ['secondApproval', '=', $secondApproval],
                            ['thirdApproval', '=', $thirdApproval],
                        ])
                        ->get();

        }
        else if($role->role == "branch manager"){
            $firstApproval = 1;
            $secondApproval = 0;
            $thirdApproval = 0;
            $leaves = LeaveUsers::with('user', 'leave')
                        ->where([
                            ['secondApproval', '=', $secondApproval],
                            ['firstApproval', '=', $firstApproval],
                        ])
                        ->get();

        }
        else{
            return 'null';
        }
       
        // dd($leaves);
        return $leaves;
    }

    public function claimHistory(Request $request)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->leaveAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

        $user = $request->user();

        $role = $user->role;

        if ($role->role == 'finance') {

            $secondApproval = 1;
            $thirdApproval = 0;
            $claims = ClaimUsers::with('user', 'claim')
                        ->where([
                            ['secondApproval', '=', $secondApproval],
                            ['thirdApproval', '=', $thirdApproval],
                        ])
                        ->get();

        }
        else if($role->role == "branch manager"){
            $firstApproval = 1;
            $secondApproval = 0;
            $thirdApproval = 0;
            $claims = ClaimUsers::with('user', 'claim')
                        ->where([
                            ['secondApproval', '=', $secondApproval],
                            ['firstApproval', '=', $firstApproval],
                        ])
                        ->get();

        }
        else{
            return 'null';
        }
       
        // dd($claims);
        return $claims;
    }

    public function leaveHistoryUser($month, $year, $status)
    {

        $user = auth()->user();

        $leave_data = [];
        $type = [];
        foreach ($user->leaves as $leave) {

            if($status == '2'){

                $type = ['2','5'];
            
            }elseif($status == '0'){

                $type = ['0'];
            
            }elseif($status == '1'){

                $type = ['1','4'];
            
            }else{

                $type = ['0','1','2','4','5'];
         
            }

            $leave_year = carbon::parse($leave->pivot->start)->format('Y');
            $leave_month = carbon::parse($leave->pivot->start)->format('n');
            $get_month = $month + 1;//Because javascript month starts from zero; 
            //dd($month, $year, $status);
            if($leave_year == $year && $leave_month == $get_month && in_array($leave->pivot->approve, $type)){

                array_push($leave_data, $leave);
                
            }

        }

        //$data = json_encode($leave_data);
        $data = $leave_data;
            
        
        //dd($data);
        return $data;
    }

    public function dashboardData($month, $year)
    {
        $create_date = $year.'-'.($month+1).'-01';
        $month_of_year = Carbon::parse($create_date)->format('M Y');
        // dd($month_of_year);
        $outlet_id = Auth::user()->outlet_id;
        $data = DashboardDetails::where([['outlet_id', $outlet_id],['month_of_year', $month_of_year]])->first();

        return $data;
    }

    public function manualSalesUpdate($month, $year, $outlet)
    {
        // dd($month, $year, $outlet);
        \Artisan::call('update:ManualUpdateSalesOverview', ['month' => $month, 'year' => $year, 'outlet' => $outlet]);
    }

    public function claimHistoryUser($month, $year, $status)
    {

        $user = auth()->user();

        $claim_data = [];
        $type = [];
        foreach ($user->claims as $claim) {

            if($status == '2'){

                $type = ['2','5'];
            
            }elseif($status == '0'){

                $type = ['0'];
            
            }elseif($status == '1'){

                $type = ['1','4'];
            
            }else{

                $type = ['0','1','2','4','5'];
         
            }

            $claim_year = carbon::parse($claim->pivot->start)->format('Y');
            $claim_month = carbon::parse($claim->pivot->start)->format('n');
            $get_month = $month + 1;//Because javascript month starts from zero; 
            //dd($month, $year, $status);
            if($claim_year == $year && $claim_month == $get_month && in_array($claim->pivot->approve, $type)){

                array_push($claim_data, $claim);
                
            }

        }

        //$data = json_encode($leave_data);
        $data = $claim_data;
            
        
        //dd($data);
        return $data;
    }

    public function replacement()
    {
        $startOfMonth = Carbon::now()->startOfMonth();
        $endOfMonth = Carbon::now()->endOfMonth();
        return Transfer::with('member', 'class')
            ->whereBetween('toDate', [$startOfMonth, $endOfMonth])
            ->get();
    }

    // public function freeze()
    // {
    //     $sessionId = Session::getId();

    //     $condition = [
    //         'user_id' => Auth::user()->id
    //     ];

    //     $insert = [
    //         'outlet_id' => Auth::user()->outlet_id,
    //         'session_id' => $sessionId
    //     ];
    //     $session = CurrentLogin::updateOrCreate($condition,$insert);

    //     $currentLogin = CurrentLogin::where('session_id', Session::getId())->first();
    //     $user_status = new StdClass;
    //     $user_status->freeze = PackageUser::with('packages', 'user')->where('outlet_id', $currentLogin->outlet_id)->where('status', 'freeze')->get();
    //     $user_status->terminate = PackageUser::with('packages', 'user')->where('outlet_id', $currentLogin->outlet_id)->where('status', 'terminate')->get();
    //     $user_status->active = PackageUser::whereHas('classroom', function($q){
    //                             $q->where('status', '=', 'active');
    //                         })->with('packages', 'user')->where([['outlet_id', $currentLogin->outlet_id],['status', 'active'],['class','SWIMMING']])->get();

    //     $response = [
    //        'data' => [
    //            'freeze' => $user_status->freeze,
    //            'terminated' => $user_status->terminate,
    //            'active' => $user_status->active
    //        ]
    //    ];

    //   return $response;
    // }

    public function attendance()
    {
        $sessionId = Session::getId();

        $condition = [
            'user_id' => Auth::user()->id
        ];

        $insert = [
            'outlet_id' => Auth::user()->outlet_id,
            'session_id' => $sessionId
        ];
        $session = CurrentLogin::updateOrCreate($condition,$insert);

        $startOfMonth = Carbon::now()->startOfMonth();
        $endOfMonth = Carbon::now()->endOfMonth();
        $currentLogin = CurrentLogin::where('session_id', Session::getId())->first();

        return Attendance::selectRaw('COUNT(user_id), user_id, class_id')->where('outlet_id', $currentLogin->outlet_id)->whereBetween('clockIn', [$startOfMonth, $endOfMonth])->groupBy('user_id', 'class_id')->get(); //->havingRaw('COUNT(user_id) > 1')
    }

    public function sales()
    {
        $today = Carbon::now();
        $transactions = Transaction::whereDate('created_at', $today->format('Y-m-d'))->select('invoice_total', 'invoice_no')->distinct()->get();

        $total = 0;
        foreach ($transactions as $key => $value) {
            $total = $total + $value->invoice_total;
        }

        return $total;
    }

    public function salesOverview($outlet, $year)
    {
        $final_data = [];
        // $categories = ['SWIMMING SALES', 'GYM SALES', 'SCUBA SALES', 'POINT-OF-SALES', 'TOTAL PER MONTH', 'TARGET PER MONTH', 'SALES TO DATE', 'TARGET PER ANNUM', 'PROGRESS (%)', 'PROGRESS TARGET TO HIT (%)'];

        $categories = ['SWIMMING SALES', 'GYM SALES', 'SCUBA SALES', 'POINT-OF-SALES', 'TOTAL PER MONTH', 'TARGET PER MONTH'];
        $months = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
        $payment_type = ['Exam & Miscellaneous Fee','Monthly-Collection','Private-Collection','Quarterly-Collection','Other-Collection'];
        $special_item = ['PADI SWIM SCHOOL EXAM','REGISTRATION FEE','FREEZE FEE','TRANSFER FEE'];

        $total_by_category = [];

        foreach ($categories as $key => $category) {

            if($category == 'TOTAL PER MONTH'){

                foreach ($months as $key => $month) {
                    
                    $monthly_sum = [];
                    foreach ($total_by_category as $turn => $val) {
                       
                       if($val['month'] == $month){
                            $monthly_sum[] = $val['amount'];
                        } 
                    }
                    // if($month == 'DECEMBER'){
                    //    dd(array_sum($monthly_sum)); 
                    // }
                    $total_amount = array_sum($monthly_sum);

                    $final_data[] = [
                        'category'     => $category,
                        'payment_type' => '-',
                        'item'         => '-',
                        'month'        => $month,
                        'total_sales'  => round($total_amount)
                        // 'total_sales'  => 
                    ];
                    
                }

            }else if($category == 'TARGET PER MONTH'){

                foreach ($months as $key => $val) {
                    
                    $month_of_target = carbon::parse()->month($key+1)->startOfMonth()->format('M Y');
                    // dd($month_of_target);
                    $get_target = DashboardDetails::where('month_of_year', $month_of_target)->first();
                    if($get_target){
                        $monthly_targ = ($get_target->annual_sales_target/12);
                    }else{
                        $monthly_targ = '0.00';
                    }
                    

                    $final_data[] = [
                        'category'     => $category,
                        'payment_type' => '-',
                        'item'         => '-',
                        'month'        => $val,
                        // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                        'total_sales'  => $monthly_targ
                    ];
                }

            }else if($category == 'SALES TO DATE'){

                foreach ($months as $key => $val) {
                    
                    $now = carbon::parse()->startOfMonth()->format('F');
                    if($month == $now){
                        $sales_to_date = $now->format('Y-m-d');
                    }else{
                        $sales_to_date = carbon::parse()->month($key+1)->endOfMonth()->format('Y-m-d');
                    }

                    $final_data[] = [
                        'category'     => $category,
                        'payment_type' => '-',
                        'item'         => '-',
                        'month'        => $val,
                        // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                        'total_sales'  => $sales_to_date
                    ];

                }
                
            }else if($category == 'TARGET PER ANNUM'){

                foreach ($months as $key => $val) {
                    
                    $month_of_target = carbon::parse()->month($key+1)->startOfMonth()->format('M Y');
                    $get_target = DashboardDetails::where('month_of_year', $month_of_target)->first();
                    $yearly_targ = $get_target->annual_sales_target;

                    $final_data[] = [
                        'category'     => $category,
                        'payment_type' => '-',
                        'item'         => '-',
                        'month'        => $val,
                        // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                        'total_sales'  => $yearly_targ
                    ];
                }
                
            }else if($category == 'PROGRESS (%)'){

                foreach ($months as $key => $val) {

                    $monthly_sales_amount = 0;
                    foreach ($final_data as $turn => $get_data) {
                        if($get_data['category'] == 'TOTAL PER MONTH' && $get_data['month'] == $val){
                            $monthly_sales_amount = $get_data['total_sales'];
                        }
                    }
                    // if($val == 'DECEMBER'){
                    //     dd('%'.$monthly_sales_amount);
                    // }
                   
                    $month_of_target = carbon::parse()->month($key+1)->startOfMonth()->format('M Y');
                    $get_target = DashboardDetails::where('month_of_year', $month_of_target)->first();
                    $monthly_targ = ($get_target->annual_sales_target/12);
                    $percentage = round($monthly_sales_amount/$monthly_targ*100);

                    $final_data[] = [
                        'category'     => $category,
                        'payment_type' => '-',
                        'item'         => '-',
                        'month'        => $val,
                        // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                        'total_sales'  => $percentage
                    ];
                }
                
            }else if($category == 'PROGRESS TARGET TO HIT (%)'){

                foreach ($months as $key => $val) {
                    
                    $monthly_sales_amount = 0;
                    foreach ($final_data as $turn => $get_data) {
                        if($get_data['category'] == 'TOTAL PER MONTH' && $get_data['month'] == $val){
                            $monthly_sales_amount = $get_data['total_sales'];
                        }
                    }
                    // if($val == 'DECEMBER'){
                    //     dd('%'.$monthly_sales_amount);
                    // }
                   
                    $month_of_target = carbon::parse()->month($key+1)->startOfMonth()->format('M Y');
                    $get_target = DashboardDetails::where('month_of_year', $month_of_target)->first();
                    $monthly_targ = ($get_target->annual_sales_target/12);
                    $balance_sales_targ = $monthly_targ - $monthly_sales_amount; 
                    $percentage = round($balance_sales_targ/$monthly_targ*100);

                    $final_data[] = [
                        'category'     => $category,
                        'payment_type' => '-',
                        'item'         => '-',
                        'month'        => $val,
                        // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                        'total_sales'  => $percentage
                    ];
                }
                
            }else{

                foreach ($months as $key => $month) {
                    $data = SalesOverview::where([['outlet_id', $outlet],['category', $category],['month', $month],['year',$year]])->get();

                    
                    $totalsales_amount = [];
                    // to calculate total_sales of the category
                    foreach ($data as $key => $val) {
                        $totalsales_amount[] = $val->total_sales;
                    }

                    $total_by_category[] = [
                        'month' => $month,
                        'category' => $category,
                        'amount' => array_sum($totalsales_amount),
                    ];
                    
                    // to insert data total_sales of the category
                    
                    if($category == 'SWIMMING SALES'){

                        foreach ($payment_type as $key => $payment) {
                            // dd($payment);
                            $swimming_data = SalesOverview::where([['outlet_id', $outlet],['category', 'SWIMMING SALES'],['month', $month],['year',$year],['payment_type', $payment]])->get(); 
                            
                            if($payment == 'Exam & Miscellaneous Fee'){

                                foreach ($special_item as $key => $item) {
                                
                                    $item_data = SalesOverview::where([['outlet_id', $outlet],['category', 'SWIMMING SALES'],['month', $month],['year',$year],['payment_type', $payment],['item', $item]])->first();
                                    // dd($outlet,$month,$year,$payment,$item);
                                    if($item_data){
                                        $final_data[] = [
                                            'category'     => $category,
                                            'payment_type' => $payment,
                                            'item'         => $item,
                                            'month'        => $month,
                                            // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                                            'total_sales'  => $item_data->total_sales
                                        ];
                                    }
                                }
                            }else{

                                foreach ($swimming_data as $key => $swim) {
                                    
                                    $final_data[] = [
                                        'category'     => $category,
                                        'payment_type' => $payment,
                                        'item'         => $swim->item,
                                        'month'        => $month,
                                        // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                                        'total_sales'  => $swim->total_sales
                                    ];   
                                }
                            }
                        }
                    }else{

                        $other_data =  SalesOverview::where([['outlet_id', $outlet],['category',$category],['month', $month],['year',$year]])->get(); 

                        foreach ($other_data as $key => $value) {
                            
                            $final_data[] = [
                                'category'     => $category,
                                'payment_type' => $value->payment_type,
                                'item'         => $value->item,
                                'month'        => $month,
                                // 'total_sales'  => number_format((float)$val->total_sales, 2, '.', '')
                                'total_sales'  => $value->total_sales
                            ];
                        }
                    }
                }
            }
        }
        //dd($final_data);
        return $final_data;
    }

    public function salesTarget(Request $request, $amount)
    {
        $month_of_year = Carbon::parse()->format('M Y');
        $outlet_id = Auth::user()->outlet_id;
        $data = DashboardDetails::where([['outlet_id', $outlet_id],['month_of_year', $month_of_year]])->first();
        $data->annual_sales_target = $amount;
        $data->save();

        return $data;
    }

    public function feedback()
    {
        $sessionId = Session::getId();

        $condition = [
            'user_id' => Auth::user()->id
        ];

        $insert = [
            'outlet_id' => Auth::user()->outlet_id,
            'session_id' => $sessionId
        ];
        $session = CurrentLogin::updateOrCreate($condition,$insert);

        $currentLogin = CurrentLogin::where('session_id', Session::getId())->first();

        return Feedback::processing()->where('outlet_id', $currentLogin->outlet_id)->get();
    }

    public function totalsales($date)
    {
        // dd($date);
        $month_of_year = Carbon::parse($date)->startOfYear();
        $outlet_id = Auth::user()->outlet_id;
        
        $months = 13;
        $totalsales = [];

        for($i = 1; $i < $months; $i++){
           
            $month = $month_of_year->month($i)->format('M Y');
            $sales = DashboardDetails::where([['outlet_id', $outlet_id],['month_of_year', $month]])->first();
            if($sales){
                $sum = $sales->month_to_date_sales;
            }else{
                $sum = '0.00';
            }
            

            $temp = new StdClass;
            $temp->date = $month;
            $temp->total = number_format((float)$sum, 2, '.', '');

            array_push($totalsales, $temp);
            
        }

        return $totalsales;
    }

    public function memberOutsanding($outlet)
    {
        //dd($outlet);
        $packages = Package::where([['category','SWIMMING'],['status','active']])->select('id')->get();
        foreach ($packages as $package) {
            $package_arr[] = $package->id;
        }

        $PackageUser = PackageUser::whereHas('classroom', function($q){
                                $q->where('status', '=', 'active');
                            })
                        ->with('packages') 
                        ->where([['outstanding', '!=', '0.00'],['status','active'],['outlet_id', $outlet]])
                        ->whereIn('package_id',$package_arr)
                        ->get();

        foreach ($PackageUser as $key => $some) {

            $classroom = ClassroomUser::with('classes')->where('package_user_id',$some->id)->where('status','active')->first();

            $some->classes = $classroom;
            $some->customer = User::where('id',$some->user->user_id)->first();
            $some->member = User::where('id',$some->user->id)->first();
            
            
        }

        return $PackageUser;
    }


    public function checkUnpaidOrder($outlet,$date)
    {
        // dd($outlet,$date);
        $UnpaidOrder = Order::where([['outlet_id', $outlet],['monthly_payment',$date],['status','1']])->get();
        // dd(count($UnpaidOrder));
        if(count($UnpaidOrder) == '0'){
            // dd('hi');
            $message = 'no_data';
            return $message;
        }

        $packages = Package::where([['category','SWIMMING'],['status','active']])->select('id')->get();
        foreach ($packages as $package) {
            $package_arr[] = $package->id;
        }

        $package_user_id = [];

        foreach ($UnpaidOrder as $key => $Unpaid) {

            if(!empty($package_user_id) && array_key_exists($Unpaid->package_user_id, $package_user_id)){
                // dd($package_user_id[$Unpaid->package_user_id]);

                $package_user_id[$Unpaid->package_user_id]['amount'] = $package_user_id[$Unpaid->package_user_id]['amount'] + $Unpaid->package_total;

                // dd($package_user_id[$Unpaid->package_user_id]);

            }else{

                $ids[] = $Unpaid->package_user_id;
                $package_user_id[$Unpaid->package_user_id] = [
                    'id' => $Unpaid->package_user_id,
                    'amount' => $Unpaid->package_total
                ];
            }

        }

        // dd(count($ids));

        $PackageUser = PackageUser::with('packages') 
                        ->where([['status','active'],['outlet_id', $outlet]])
                        ->whereIn('id',$ids)
                        ->whereIn('package_id',$package_arr)
                        ->get();

        foreach ($PackageUser as $key => $some) {

            $some->outstanding = $package_user_id[$some->id]['amount'];
            $classroom = ClassroomUser::with('classes')->where('package_user_id',$some->id)->where('status','active')->first();

            if($classroom){
                $some->classes = $classroom;
            }else{

                $classroom = new StdClass;
                $classroom->classes = new StdClass;
                $classroom->classes->day = null;
                $classroom->classes->start = null;

                $some->classes = $classroom;
                // dd($some);
            }
            // $some->classes = $classroom;
            $some->customer = User::where('id',$some->user->user_id)->first();
            $some->member = User::where('id',$some->user->id)->first();
            
            
        }
        // dd($PackageUser);
        return $PackageUser;
    }

    public function currentlogin(Request $request, User $user)
    {
        $selected_outlet = $request->value;
        $sessionId = Session::getId();
        $condition = [
            'user_id' => $user->id,
            'session_id' => $sessionId
        ];

        $insert = [
            'outlet_id' => $selected_outlet,
        ];
        $session = CurrentLogin::updateOrCreate($condition,$insert);
        return $user;
    }

    public function getcurrentlogin()
    {
        $sessionId = Session::getId();

        $condition = [
            'user_id' => Auth::user()->id
        ];

        $insert = [
            'outlet_id' => Auth::user()->outlet_id,
            'session_id' => $sessionId
        ];
        $session = CurrentLogin::updateOrCreate($condition,$insert);
        
        $userId = auth()->user();

        $session = CurrentLogin::with('user')->where('user_id', $userId->id)->where('session_id', Session::getId())->first();

        return $session;
    }

    public function monthly_sales()
    {
        $outlet_id = Auth::user()->outlet_id;
        $current_month = carbon::now();
        $start_month = Carbon::create()->month($current_month->month)->startOfMonth();
        $end_month = Carbon::create()->month($current_month->month)->endOfMonth();
        $monthly_sales = Transaction::with('user')->where('outlet_id', $outlet_id)->whereBetween('created_at', array($start_month, $end_month))->get();

        $result_arr = [];
        foreach ($monthly_sales as $sales) {
           
           $result = new StdClass;

            if($sales->user){
                $result->name = $sales->user->name;
            }else{
                $result->name = 'User Not Exist';
            }
           
           $result->invoice_no = $sales->invoice_no;
           $result->status = $sales->status;
           $result->invoice_total = $sales->invoice_total;
           $result->sst_package = $sales->sst_package;
           $result->payment_method = $sales->payment_method;
           $result->created_at = carbon::parse($sales->created_at)->format('Y-m-d');
           array_push($result_arr, $result);
        }
        
        return $result_arr;
    }

    public function invoice($invoice_no)
    {

        $outlet_id = Auth::user()->outlet_id;
        $invoice = Transaction::with('user','order','product')->where([['outlet_id', $outlet_id],['invoice_no', $invoice_no]])->get();

        foreach ($invoice as $key => $sales) {
            
            if($sales->order){
                $package_name = Package::where('id',$sales->order->package_id)->first();
                $sales->order->package_name = $package_name->name;
                $sales->item_category = false;

            }else{

                $sales->item_category = true;
            }
        }
        
        return $invoice;
    }

    public function NewSwimMember($date){

        $outlet_id = Auth::user()->outlet_id;
        $start_date = Carbon::parse($date)->startOfMOnth()->format('Y-m');

        $data = PackageUser::with('packages', 'user')->where([['outlet_id', $outlet_id],['start_date', $start_date]])->get();
        foreach ($data as $val) {
            $val->parent = $val->user->parent;
            $val->start_date = Carbon::parse($val->start_date)->startOfMOnth()->format('Y-m-d');
        }

        return $data;
    }

    public function statusChart($year)
    {
        $create_date = $year.'-01-01';
        $outlet_id = Auth::user()->outlet_id;
        $status = [];

        for ($x = 0; $x < 12; $x++) {
            
            $start_date = Carbon::parse($create_date)->addMonths($x)->format('Y-m');
            $members = PackageUser::where([['outlet_id', $outlet_id],['start_date',$start_date]])->get();

            $st_date_termianted = Carbon::parse($create_date)->subMonths(1)->addMonths($x)->startOfMOnth()->format('Y-m-d');
            $st_date_freeze = Carbon::parse($create_date)->addMonths($x)->startOfMOnth()->format('Y-m-d');
            //dd($st_date_termianted);
            $terminate = UserChangePackage::whereHas('packageUsers', function ($query) {
                        $query->where('outlet_id', '=', Auth::user()->outlet_id);
                        })->where([['start_date', $st_date_termianted],['status','terminate']])->get();

            $freeze = UserChangePackage::whereHas('packageUsers', function ($query) {
                        $query->where('outlet_id', '=', Auth::user()->outlet_id);
                        })->where([['start_date', $st_date_freeze],['status','freeze']])->get();
            if($x==0){
                $test_arr[] = $terminate;
            }
            
            //$terminated_members = PackageUser::where([['outlet_id', $outlet_id],['start_date',$start_date]])->get();
            
            //$count_terminate = 0;
            $count_member = $members->count();
            $count_freeze = $freeze->count();
            $count_terminate = $terminate->count();
            //dd($count_member);
            // foreach ($members as $member) {
            //     //dd($x);
            //     if($member->status == 'terminate'){

            //         $count_terminate++;

            //     }
            // }

            $status[] = [
                'month'     => intval($x),
                'freezed'    => intval($count_freeze),
                'terminated'=> intval($count_terminate),
                'members'   => intval($count_member)
            ];
        } //dd($test_arr);


        return $status;
        
    }

    public function terminated($date){
        
        $outlet_id = Auth::user()->outlet_id;
        $st_date = Carbon::parse($date)->subMonths(1)->startOfMOnth()->format('Y-m-d');

        $user_change_package = UserChangePackage::whereHas('packageUsers', function ($query) {
                        $query->where('outlet_id', '=', Auth::user()->outlet_id);
                        })->where([['start_date', $st_date],['status','terminate']])->get();

        
        foreach ($user_change_package as $val) {

            $data = PackageUser::with('packages', 'user')->where('id', $val->package_user_id)->first();
            $val->start_date = Carbon::parse($val->start_date)->addMonths(1)->format('Y-m-d');
            $val->package_user = $data;
            $val->parent = $data->user->parent;
            $val->terminated_by = user::find($val->changed_by);
        }

        return $user_change_package;
    }

    public function freezed($date){
        
        $outlet_id = Auth::user()->outlet_id;
        $st_date = Carbon::parse($date)->startOfMOnth()->format('Y-m-d');

        $user_change_package = UserChangePackage::whereHas('packageUsers', function ($query) {
                        $query->where('outlet_id', '=', Auth::user()->outlet_id);
                        })->where([['start_date', $st_date],['status','freeze']])->get();

        
        foreach ($user_change_package as $val) {

            $data = PackageUser::with('packages', 'user')->where('id', $val->package_user_id)->first();
            $val->package_user = $data;
            $val->parent = $data->user->parent;
            $val->terminated_by = user::find($val->changed_by);
        }

        return $user_change_package;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feedback;
use Auth;

class FeedbackController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;

        return Feedback::with('users')->where('user_id', $user_id)->get();
    }

    public function indexDashboard()
    {
        $outlet_id = Auth::user()->outlet_id;
        return Feedback::with('users')->where('outlet_id', $outlet_id)->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $user_id = Auth::user()->id;

        $feedback = new Feedback;
        $feedback->user_id = $user_id;
        $feedback->outlet_id = $request->outlet_id;
        $feedback->type = $request->type;
        $feedback->title = $request->title;
        $feedback->message = $request->message;
        $feedback->status = 'processing';
        $feedback->save();

        return $feedback;
    }

    public function show(Feedback $feedback)
    {
        return $feedback;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, Feedback $feedback)
    {
        $feedback->status = $request->status;
        $feedback->save();

        return $feedback;
    }

    public function destroy(Feedback $feedback)
    {
        $feedback->delete();
    }
}

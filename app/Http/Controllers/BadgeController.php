<?php

namespace App\Http\Controllers;

use App\Badge;
use Illuminate\Http\Request;

class BadgeController extends Controller
{
    public function index()
    {
        return Badge::orderBy('id')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $badge = new Badge;
        $badge->name = $request->name;
        $badge->level = $request->level;
        $badge->status = 'active';

        $badge->save();
        return $badge;
    }

    public function show(Badge $badge)
    {
        return $badge;
    }

    public function edit(Badge $badge)
    {
        //
    }

    public function update(Request $request, Badge $badge)
    {
        $badge->name = $request->name;
        $badge->status = $request->status;
        $badge->level = $request->level;

        $badge->save();
        return $badge;
    }

    public function delete(Badge $badge)
    {
        $badge->delete();
        return 'Badge has been deleted';
    }
}

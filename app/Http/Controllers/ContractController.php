<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contract;

class ContractController extends Controller
{
    public function index() {
    	return Contract::all();
    }

    public function indexFilter($types) {
        return Contract::where('type', $types)->latest('created_at')->first();
    }

    public function show(Contract $contract) {
    	return $contract;
    }

    public function store(Request $request) {
    	$contract = new Contract;
        $contract->title = $request->title;
    	$contract->type = $request->type;
    	$contract->content = $request->content;

    	$contract->save();
    	return $contract;
    }
    
    public function update(Contract $contract, Request $request) {

    	$contract->title = $request->title;
        $contract->type = $request->type;
    	$contract->content = $request->content;
    	$contract->save();

    	return $contract;

    }

    public function delete(Contract $contract) {
    	$contract->delete();
    	return "Contract has been deleted";
    }
}

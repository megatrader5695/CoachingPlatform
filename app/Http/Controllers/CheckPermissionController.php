<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class CheckPermissionController extends Controller
{
    public function index($name)
    {
        $permissions = Auth::user()->role->permissions;
        $count = 0;
        foreach ($permissions as $value) {
            if($value->name == $name)
              $count ++;
        }
        $boolean = $count > 0 ? true : false;
        return json_encode($boolean);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;

class RolePermissionController extends Controller
{
    public function index()
    {
        return Role::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Role $rolespermission)
    {
        $permission = Permission::all();
        $roleaccess = $rolespermission->permissions()->get();

        return response()->json(compact('permission', 'rolespermission', 'roleaccess'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, Role $rolespermission)
    {
        // $name = $request->name;
        $access = $request->access;

        $rolespermission->permissions()->sync($access);

        return response()->json(compact('access'));
    }

    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Syllabus;
use App\SyllabusQuestion;
use Illuminate\Http\Request;

class SyllabusController extends Controller
{
    public function index()
    {
        $syllabi = Syllabus::orderby('sequence','asc')->get();

        return $syllabi;
    }

    public function show(Syllabus $id)
    {
        return response()->json(['data' => $id], 200);
    }

   	public function update(Syllabus $id, Request $request)
   	{
      $id->level = $request->level;
   		$id->name = $request->name;
      $id->sequence = $request->sequence;
   		$id->save();

   		return $id;
   	}

    public function createSyllabus( Request $request)
    {
        $syllabus = new Syllabus;
        $syllabus->name = $request->name;
        $syllabus->level = $request->level;
        $syllabus->sequence = $request->sequence;
        $syllabus->save();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Staff;
use App\Department;
use App\Role;
use App\Schedule;
use App\Mail\NewStaffRegistration;
use App\Http\Resources\StaffResource;
use DB;
use Mail;
use stdClass;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class StaffController extends Controller
{
	private $value = 'staff';
	private $exceptRole = [1, 2, 4, 5, 6, 7];

	public function registerStaff(Request $request)
	{
		$getDepartment = Department::find($request->input('department'));
		$roles = Role::where('role', $getDepartment->name)->first();

		$user = new User;
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->contact = $request->input('contact');
		$user->role_id = $roles->id;
		$user->icNumber = $request->input('icNumber');
		$user->outlet_id = Auth::user()->outlet_id;
		$user->status = 'active';
		$user->save();

		$outlet_access = $request->selectedOutlet;
		// array_push($outlet_access,strval(Auth::user()->outlet_id));

		$userId = $user->id;
		$staff = new Staff;
		$staff->user_id = $userId;
		$staff->outlet_id = Auth::user()->outlet_id;
		$staff->status = $request->status;
		$staff->outlet_access = $outlet_access;
		$staff->designation_id = $request->input('designation');
		$staff->approve = 0;

		//Through Super Admin Dashboard
		if ($request->input('obo') == 'admin') {
			$staff->approve = 1;
		}

    	$workingHours = $request->input('schedules');

		$totaltime = 0;
		foreach ($workingHours as $hour) {
			if ($hour['disabled'] == false) {
				$schedule = new Schedule;
				$schedule->user_id = $user->id;
				$schedule->day = $hour['day'];
				$schedule->start = $hour['start'];
				$schedule->end = $hour['end'];
				$schedule->save();

				//calculate working hours
				$start = Carbon::parse($schedule->start);
		        $end = Carbon::parse($schedule->end);
		        $hours = $end->diffInHours($start);
		        $totaltime = $totaltime + $hours;
			}
		}

		$staff->department = $request->input('department');
		$staff->salary = $request->input('salary');
		$staff->workingHour = $totaltime;
		$staff->contract = $request->input('contract');
		$staff->signature = $request->input('signature');
		$staff->leave = $request->input('leave');
		$staff->save();

		return $user;
		

	}

	public function registerProfile(Request $request){
		$rules = [
			'name'  => 'required',
			'email' => 'required|email|',
			'gender' => 'required',
			'password' => 'required',
			'contact' => 'required',
			'icNumber' => 'required',
			'designation' => 'required',
			'status' => 'required',
		];

		$this->validate($request, $rules);

		$user = new User;
		
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->gender = $request->input('gender');
		$user->password = bcrypt($request->input('password'));
		$user->contact = $request->input('contact');
		$user->icNumber = $request->input('icNumber');
		$user->status = $request->input('status');
		$user->role_id = 2;
		$user->outlet_id = $request->input('outlet');

		$user->save();

		$outlet_access = [$request->outlet];
		// $outlets = $request->input('outlets');
		// foreach ($outlets as $outlet) {
  //           $user->outlets()->attach($outlet);
		// }
		//dd($request->outlet);

		$staff = new Staff;
		$staff->user_id = $user->id;
		$staff->status = 'Confirmed';
		$staff->designation_id = $request->input('designation');
		$staff->outlet_id = $request->input('outlet');
		$staff->outlet_access = $outlet_access;
		$staff->approve = 1;
		$staff->save();

		return response()->json(compact('user'));
	}

	public function approveStaff(User $staff,Request $request)
	{
		$getDepartment = Department::find($request->department);
		$roles = Role::where('role', $getDepartment->name)->first();

		// Update Staff status
		$staff->staff->approve = 1;
		$staff->staff->department = $request->department;
		$staff->staff->salary = $request->salary;
		$staff->staff->workingHour = $request->workingHour;
		$staff->role_id = $roles->id;
		$staff->status = $request->status;

		// Assign Module to the Staff;
		// if ($request->module_id) {
		// 	$moduleId = $request->module_id;
		// 	$staff->modules()->attach($moduleId);
		// }

		// For Coach
		if ($request->type) {
			$staff->type = $request->type;
		};

		$staff->staff->save();
		$staff->save();
		
		return $staff;
	}

	public function getAllStaffAdmin($status, $filter_status)
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->value);
	    if ($check == 'false') {
	        return response()->json(array(
	            'code'      =>  401,
	            'message'   =>  'You cannot access.'
	        ), 401);
	    }

		// It can be 'approve' , 'disapprove' 
		if ($status) {
			
			$users_raw = User::whereNotIn('role_id', $this->exceptRole)
				->where([['outlet_id',Auth::user()->outlet_id],['status', $filter_status]])
				->whereExists(function ($query) use ($status) {
                    $query->select(DB::raw(1))
                          ->from('staffs')
                          ->whereRaw('staffs.user_id = users.id')
                          ->where('staffs.approve', '1');
                })
				->get();
			
		}else{

			$users_raw = User::all()->whereNotIn('role_id', $this->exceptRole)->where([['outlet_id',Auth::user()->outlet_id],['status', $filter_status]]);
		}

		$users = [];
		
		foreach ($users_raw as $user) {
			array_push($users, $user);
		}

		return $users;
	}

	public function getStaff(User $staff)
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->value);
	    if ($check == 'false') {
	        return response()->json(array(
	            'code'      =>  401,
	            'message'   =>  'You cannot access.'
	        ), 401);
	    }

		if (in_array($staff->role_id, $this->exceptRole)) {	//if role_id same with exceptRole, it not staff admin
			return "This user not a Staff Admin";
		}else{
			return new StaffResource($staff);
		}
	}

	public function updateStaff(User $staff, Request $request)
	{
		if($request->status == 'Terminated'){
			$staff->status = 'inactive';
		}else{
			$staff->status = 'active';
		}

		$schedule = Schedule::where('user_id', $staff->id)->delete();

		$getDepartment = Department::find($request->input('department'));
		$roles = Role::where('role', $getDepartment->name)->first();

		$staff->name = $request->input('name');
		$staff->contact = $request->input('contact');
		$staff->icNumber = $request->input('icNumber');
		$staff->email = $request->input('email');
		$staff->role_id = $roles->id;

		$designation_id = $request->input('designation');
		$approve = $request->input('approve');
		$department = $request->input('department');
		$salary = $request->input('salary');
		// $workingHour = $request->input('workingHour');
		$workingHours = $request->input('schedules');
		$contract = $request->input('contract');
		$outlet_access = $request->input('outlet_access');

		$totaltime = 0;
		foreach ($workingHours as $hour) {
			if ($hour['disabled'] == false) {
				$schedule = new Schedule;
				$schedule->user_id = $staff->id;
				$schedule->day = $hour['day'];
				$schedule->start = $hour['start'];
				$schedule->end = $hour['end'];
				$schedule->save();

				//calculate working hours
				$start = Carbon::parse($schedule->start);
		        $end = Carbon::parse($schedule->end);
		        $hours = $end->diffInHours($start);
		        $totaltime = $totaltime + $hours;
			}
		}
		
		// array_push($outlet_access,strval(Auth::user()->outlet_id));

		$user = Staff::where('user_id', $staff->id)->first();
		$user->status = $request->status;
		$user->designation_id = $designation_id;
		$user->approve = $approve;
		$user->department = $department;
		$user->salary = $salary;
		$user->workingHour = $totaltime;
		$user->outlet_access = $outlet_access;
		$user->contract = $contract['id'];
		$user->save();

		$staff->save();

		return $staff;
	}

	public function deleteStaff(User $staff){
		if ($staff->role_id == 3) {
			$staff->delete();
			return;
		}else{
			return "This user not a Staff";
		}
	}

	public function registerSignupStaff(Request $request)
	{
		$user = new User;
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->contact = $request->input('contact');
		$user->role_id = 3;
		$user->outlet_id = 24;
		$user->first_time_login = 1;
		$user->save();

		$staff = new Staff;
		$staff->user_id = $user->id;
        $staff->outlet_id = $user->outlet_id;
        $staff->approve = 0;
        $staff->salary = 0.00;
        $staff->save();

        $objDemo = new stdClass();
        $objDemo->name = $user->name;
        $objDemo->email = $user->email;
        $objDemo->contact = $user->contact;
        $objDemo->receiver = 'Admin';
 
        Mail::to("admin@vgo.com")->send(new NewStaffRegistration($objDemo));

        return $user;
	}
}

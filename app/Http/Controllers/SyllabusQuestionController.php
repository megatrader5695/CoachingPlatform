<?php

namespace App\Http\Controllers;

use App\Syllabus;
use App\SyllabusQuestion;
use App\QuestionOption;
use Illuminate\Http\Request;
use StdClass;

class SyllabusQuestionController extends Controller
{
    public function index(Syllabus $id)
    {
        $questions = $id->questions()->orderBy('sequence')->get();

        $setQues = [];
        foreach ($questions as $question) {
            $temp = new StdClass;
            $temp->id = $question->id;
            $temp->sequence = $question->sequence;
            $temp->title = $question->title;
            $temp->syllabus_id = $question->syllabus_id;
            $temp->options = [];

          array_push($setQues, $temp);
        }

        return response()->json(['data' => $setQues], 200);
    }

    public function store($id)
    {
        $this->validate(request(), [
            'title' => 'required|string'
        ]);

        $checkOrder = SyllabusQuestion::where('syllabus_id', $id)->orderByDesc('sequence')->first();
        $value = empty($checkOrder) ? null : $checkOrder->sequence;

        $question = new SyllabusQuestion;
        $question->syllabus_id = $id;
        $question->title = request('title');
        $question->sequence = $value != null ? $value + 1 : '1';
        $question->save();

        $option = new QuestionOption;
        $option->syllabus_question_id = $question->id;
        $option->title = 'remark';
        $option->save();
    }

    public function show(SyllabusQuestion $id)
    {
        $questions = $id->options()->orderByDesc('created_at')->get();

        return response()->json(['data' => $questions], 200);
    }

    public function destroy(SyllabusQuestion $id)
    {
        $id->delete();
    }

    public function showQuestion(SyllabusQuestion $id)
    {
        return response()->json(['data' => $id], 200);
    }

    public function updateQuestion($id)
    {
        $this->validate(request(), [
            'title' => 'required|string',
            // 'sequence' => 'required|integer|unique:syllabus_questions,sequence,'.$id
        ]);

        $questions = SyllabusQuestion::find($id);

        $swapSequence = SyllabusQuestion::where('sequence', request('sequence'))->where('syllabus_id', $questions->syllabus_id)->first();
        if (!empty($swapSequence)) {
            $swapSequence->sequence = $questions->sequence;
            $swapSequence->save();
        }

        $questions->title = request('title');
        $questions->sequence = request('sequence');
        $questions->save();
        return response()->json(['data' => $questions], 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    public function store(Request $request)
    {
        $question = new Question;

        $question->name = $request->name;
        $question->collection = $request->collection;


        $question->save();
		return $question;
    }
}

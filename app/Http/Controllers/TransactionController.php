<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Package;
use App\User;
use App\Outlet;
use App\Classroom;
use App\Order;
use App\OrderSubscription;
use App\UserNewMember;
use App\Attendance;
use App\Http\Resources\TransactionResource;
use DB;
use Carbon\Carbon;
use DateTime;
use stdClass;
use Illuminate\Support\Facades\Auth;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class TransactionController extends Controller
{
    public function index() {
        return Transaction::all();
    }

    public function store(Request $request, User $member)
    {
        DB::beginTransaction();
        // if(Auth::user()->outlet_id){
        //     return $this->package_for_dpc();
        // }

        // if($member->outlet_id == '25'){
        //     return $this->package_for_dpc($request, $member);
        // }

        $start_date = new Carbon($request->start_date);
        $current_date = Carbon::now();

        $monthYear = $current_date->format('Y-m');

        // Save Transaction
        $package = Package::find($request->package_id);
        $total = $package->price + $package->deposit;

        // Assign the package to ther user
        $diffInMonths = $start_date->diffInMonths($current_date) + 1;
        $countOrder = ceil($diffInMonths / $package->subscription);
        $setAddMonth = $countOrder * $package->subscription;
        $expiry = (new Carbon($request->start_date)); //->addMonths($setAddMonth);
        $index = intval($countOrder);

        // $expiry = new DateTime($package->subscription);
        $date = $expiry->format("Y-m-d");

        // check first time category member
        $checkNewMember = UserNewMember::where('package_category', $package->category)->where('user_id', $member->id)->first();
        $register_fee = 0;
        if (!$checkNewMember) {
            $register_fee += $package->registration_fee;
            // $total = $total + $package->registration_fee;
        }

        // calculate outstanding
        $outstanding = 0;
        for ($i=0; $i < $index ; $i++) {
            $outstanding = $outstanding + ( ($i==0) ? $total : $package->price );
        }

        $condition = [
            'user_id'   =>  $member->id,
            'package_category'  => $package->category,
        ];

        $insert = [
            'status'    => 'new',
        ];
        $userNewMember = UserNewMember::firstOrCreate($condition, $insert);
        if (!$userNewMember) {
            DB::rollback();
        }

        $member->packages()->attach($request->package_id,
            [
                'outlet_id' => $member->outlet_id,
                'deposit' => $package->deposit,
                'class' => $package->category,
                'swim' => 'white',
                'progress' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'expiry' => ($package->type == 'Standard' ) ? $date : $expiry->addMonths($setAddMonth)->format("Y-m-d"),
                'lastPayment' => new DateTime(),
                // 'memberId' => Outlet::find(7)->prefix . $member->id,
                'status' => 'active',
                'start_date' => $request->start_date,
                'join_date' => $request->start_date,
                'deposit_status' => 'unpaid',
                'outstanding'   => $outstanding,
            ]
        );

        $package_user = $member->packages->last();

        // store user questionnaire
        $questionnaire = $request->questionnaire;
        foreach ($questionnaire as $question) {
            if ($question[0] != null || $question[1] != null) {
                $insertQues = $member->questionnaires()->create([
                    'question_id'   => $question[0],
                    'option_id'     => $question[1],
                    'package_user_id'     => $package_user->pivot->id,
                ]);

                if (!$insertQues) {
                    DB::rollback();
                }
            }
        }

        $orders = DB::select("SELECT name,user_id from users where `id` = ".$member->id);
        $member_name = $orders[0]->name;
        $cust_id = $orders[0]->user_id;
        $addMonths = -1;
        
        if (!$checkNewMember) {
            
            for ($i=0; $i < $index ; $i++) {
                $addMonths = $addMonths + $package->subscription;
                $monthly_payment = new Carbon($request->start_date);

                $insertOrder = [
                    'member_name'       => $member_name,
                    'cust_id'           => $cust_id,
                    'package_id'        => $request->package_id,
                    "outlet_id"         => $member->outlet_id,
                    'package_user_id'   => $package_user->pivot->id,
                    'package_price'     => '0',
                    'package_depo'      => '0',
                    'package_total'     => $register_fee,
                    'monthly_payment'   => $monthly_payment,
                    // 'monthly_payment'   => $monthly_payment->addMonths($addMonths),
                    'registration_fee'  => $register_fee,
                    'status'            => '1',
                    "created_at"        => now(),
                    "updated_at"        => now()

                ];
                $orders = Order::create($insertOrder);
                if (!$orders) {
                    DB::rollback();
                }
            }
        }
        

        for ($i=0; $i < $index ; $i++) {
            $addMonths = $addMonths + $package->subscription;
            $monthly_payment = new Carbon($request->start_date);

            $insertOrder = [
                'member_name'       => $member_name,
                'cust_id'           => $cust_id,
                'package_id'        => $request->package_id,
                "outlet_id"         => $member->outlet_id,
                'package_user_id'   => $package_user->pivot->id,
                'package_price'     => $package->price,
                'package_depo'      => ($i==0) ? $package->deposit : 0,
                'package_total'     => ($i==0) ? $total : $package->price,
                'monthly_payment'   => $monthly_payment,
                // 'monthly_payment'   => $monthly_payment->addMonths($addMonths),
                'registration_fee'  => '0',
                'status'            => '1',
                "created_at"        => now(),
                "updated_at"        => now()

            ];
            $orders = Order::create($insertOrder);
            if (!$orders) {
                DB::rollback();
            }
        }

        // Assign the user into his class

        $user_id = $request->user_id;
        $remarks = $request->remarks;

        if ($request->start_date <= Carbon::now()->format('Y-m')) {

            // Check for clash time
            if ($package->category == 'SWIMMING') {
                $user = User::find($user_id);
                $class = Classroom::find($request->classroom_id);
                $class->users()->attach([ $member->id => [
                    'remarks' => $remarks,
                    'status' => $request->status,
                    'package_id' => $package->id,
                    'package_user_id' => $package_user->pivot->id,
                    "created_at"        => now(),
                    "updated_at"        => now()
                ]]);

                $classroom_user = $class->users->last();
                $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($class->start)));
                $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($class->end)));

                $dayName = strtolower($current_date->format('l'));

                if ($dayName == $class->day) {
                    $attendance                 = new Attendance;
                    $attendance->user_id        = $member->id;
                    $attendance->class_id       = $class->id;
                    $attendance->package_id     = $package->id;
                    $attendance->clockIn        = Carbon::now();
                    $attendance->status         = 'absent';
                    $attendance->replacement_id = null;
                    $attendance->week           = $current_date->weekOfMonth;
                    $attendance->clock_in_start = $clock_in_start;
                    $attendance->clock_in_end   = $clock_in_end;
                    $attendance->outlet_id      = $class->outlet_id;
                    $attendance->save();

                    if (!$attendance) {
                        DB::rollback();
                    }
                }
            }
            else {
                $clock_in_start = Carbon::today();
                $clock_in_end = Carbon::today()->endOfDay();

                $attendance                 = new Attendance;
                $attendance->user_id        = $member->id;
                $attendance->package_id     = $package->id;
                $attendance->clockIn        = Carbon::now();
                $attendance->status         = 'absent';
                $attendance->replacement_id = null;
                $attendance->week           = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end   = $clock_in_end;
                $attendance->outlet_id      = $member->outlet_id;
                $attendance->save();

                if (!$attendance) {
                    DB::rollback();
                }
            }

            $accLevelIds = '402880276ce125e4016ce12701cf0377';

            $details = new stdClass;
            $details->pin = $member->id;
            $details->name = $member->name;
            if($member->gender == 'Male') {
                $gender = 'M';
            } else {
                $gender = 'F';
            }
            $details->gender = $gender;
            $details->accLevelIds = $accLevelIds;
            $details->accStartTime = $clock_in_start;
            $details->accEndTime = $clock_in_end;

            $data = json_encode($details);

            // dd($data);

            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]); //GuzzleHttp\Client

            $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

            try {
                $result = $client->get($url);
                $status = $result->getStatusCode();
            } 
            catch (\GuzzleHttp\Exception\GuzzleException $e) {
                // This is will catch all connection timeouts
                // Handle accordinly
                // $status = $e->getResponse()->getStatusCode();
                if ($e->hasResponse()) {
                    $status = $e->getResponse()->getStatusCode();
                    // var_dump($status->getStatusCode()); // HTTP status code
                    // var_dump($status->getReasonPhrase()); // Message
                    // var_dump((string) $status->getBody()); // Body
                    // var_dump($status->getHeaders()); // Headers array
                    // var_dump($status->hasHeader('Content-Type')); // Is the header presented?
                    // var_dump($status->getHeader('Content-Type')[0]); // Concrete header value
                } else {
                    $status = null;
                }
            }

            if ($status != 200 || $status == null) {

                echo 'Device offline';

            } else {

                $clients = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ]
                ]); //GuzzleHttp\Client
                $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

                try {
                    $result = $clients->post($url, [
                        'body' => $data,
                        'timeout' => 20
                    ]);
                } 
                catch (GuzzleHttp\Exception\ClientException $e){
                    // echo 'hello';
                }

            }


        } else {
            // Check for clash time
            if ($package->category == 'SWIMMING') {
                $user = User::find($user_id);
                $class = Classroom::find($request->classroom_id);
                $class->users()->attach([ $member->id => [
                    'remarks' => $remarks,
                    'status' => 'waiting_next_month',
                    'package_id' => $package->id,
                    'package_user_id' => $package_user->pivot->id,
                    "created_at"        => now(),
                    "updated_at"        => now()
                ]]);

                $classroom_user = $class->users->last();

                $year = Carbon::now()->format('Y-m-d');
                $start_time = $year.' 03:00:00';
                $end_time = $year.' 03:01:00';
                $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                // $dayName = strtolower($current_date->format('l'));

                // if ($dayName == $class->day) {
                //     $attendance                 = new Attendance;
                //     $attendance->user_id        = $member->id;
                //     $attendance->class_id       = $class->id;
                //     $attendance->package_id     = $package->id;
                //     $attendance->clockIn        = Carbon::now();
                //     $attendance->status         = 'absent';
                //     $attendance->replacement_id = null;
                //     $attendance->week           = $current_date->weekOfMonth;
                //     $attendance->clock_in_start = $clock_in_start;
                //     $attendance->clock_in_end   = $clock_in_end;
                //     $attendance->outlet_id      = $class->outlet_id;
                //     $attendance->save();

                //     if (!$attendance) {
                //         DB::rollback();
                //     }
                // }
            }
            else{
                $clock_in_start = Carbon::today();
                $clock_in_end = Carbon::today()->endOfDay();

                $attendance                 = new Attendance;
                $attendance->user_id        = $member->id;
                $attendance->package_id     = $package->id;
                $attendance->clockIn        = Carbon::now();
                $attendance->status         = 'absent';
                $attendance->replacement_id = null;
                $attendance->week           = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end   = $clock_in_end;
                $attendance->outlet_id      = $member->outlet_id;
                $attendance->save();

                if (!$attendance) {
                    DB::rollback();
                }
            }

            $accLevelIds = '402880276ce125e4016ce12701cf0377';

            $details = new stdClass;
            $details->pin = $member->id;
            $details->name = $member->name;
            if($member->gender == 'Male') {
                $gender = 'M';
            } else {
                $gender = 'F';
            }
            $details->gender = $gender;
            $details->accLevelIds = $accLevelIds;
            $details->accStartTime = $clock_in_start;
            $details->accEndTime = $clock_in_end;

            $data = json_encode($details);

            $client = new Client([
                'headers' => [
                    'Content-Type' => 'application/json'
                ]
            ]); //GuzzleHttp\Client

            $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

            try {
                $result = $client->get($url);
                $status = $result->getStatusCode();
            } 
            catch (\GuzzleHttp\Exception\GuzzleException $e) {
                // This is will catch all connection timeouts
                // Handle accordinly
                // $status = $e->getResponse()->getStatusCode();
                if ($e->hasResponse()) {
                    $status = $e->getResponse()->getStatusCode();
                    // var_dump($status->getStatusCode()); // HTTP status code
                    // var_dump($status->getReasonPhrase()); // Message
                    // var_dump((string) $status->getBody()); // Body
                    // var_dump($status->getHeaders()); // Headers array
                    // var_dump($status->hasHeader('Content-Type')); // Is the header presented?
                    // var_dump($status->getHeader('Content-Type')[0]); // Concrete header value
                } else {
                    $status = null;
                }
            }

            if ($status != 200 || $status == null) {

                echo 'Device offline';

            } else {

                $clients = new Client([
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ]
                ]); //GuzzleHttp\Client
                $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');
                // $result = $client->post($url, [
                //     'body' => $data
                // ]);
                try {
                    $result = $clients->post($url, [
                        'body' => $data,
                        'timeout' => 20
                    ]);
                } 
                catch (GuzzleHttp\Exception\ClientException $e){
                    // echo 'hello';
                }

            }

        }

        // add new record in tbl subscription for track expired bill
        $addMonths = -1;
        $addMonths = $addMonths + $package->subscription;
        $monthly_payment = new Carbon($request->start_date);
        $dateMonthArray = explode('-', $request->start_date);
        $setYear = $dateMonthArray[0];
        $setMonth = $dateMonthArray[1];

        // $date = Carbon::createFromFormat('Y-m-d', $orders->monthly_payment)->addMonths($package->subscription);
        $date = $monthly_payment;
        $month  = $date->month;
        $year  = $date->year;
        $insertOrderSubscription = [
                    'order_id'     => $orders->id,
                    'package_user_id'   => $orders->package_user_id,
                    'month'             => $monthly_payment->addMonths($addMonths)->month,
                    'year'              => $year
                ];

        if ($package->type === 'Standard') {
            $orderSubscription = OrderSubscription::create($insertOrderSubscription);
            if (!$orderSubscription) {
                DB::rollback();
            }
        }

        DB::commit();
    }

    public function package_for_dpc(Request $request, User $member)
    {
        DB::beginTransaction();

        $start_date = new Carbon($request->start_date);
        $current_date = Carbon::now();

        $monthYear = $current_date->format('Y-m');

        // Save Transaction
        $package = Package::find($request->package_id);
        $total = $package->price + $package->deposit;

        // Assign the package to ther user
        $diffInMonths = $start_date->diffInMonths($current_date) + 1;
        $countOrder = ceil($diffInMonths / $package->subscription);
        $setAddMonth = $countOrder * $package->subscription;
        $expiry = (new Carbon($request->start_date)); //->addMonths($setAddMonth);
        $index = intval($countOrder);

        // $expiry = new DateTime($package->subscription);
        $date = $expiry->format("Y-m-d");

        // check first time category member
        $checkNewMember = UserNewMember::where('package_category', $package->category)->where('user_id', $member->id)->first();
        $register_fee = 0;
        if (!$checkNewMember) {
            $register_fee = $register_fee + $package->registration_fee;
            $total = $total + $package->registration_fee;
        }

        // calculate outstanding
        $outstanding = 0;
        for ($i=0; $i < $index ; $i++) {
            $outstanding = $outstanding + ( ($i==0) ? $total : $package->price );
        }

        $condition = [
            'user_id'   =>  $member->id,
            'package_category'  => $package->category,
        ];

        $insert = [
            'status'    => 'new',
        ];
        $userNewMember = UserNewMember::firstOrCreate($condition, $insert);
        if (!$userNewMember) {
            DB::rollback();
        }

        $member->packages()->attach($request->package_id,
            [
                'outlet_id' => $member->outlet_id,
                'deposit' => $package->deposit,
                'class' => $package->category,
                'swim' => 'white',
                'progress' => 0,
                'created_at' => new DateTime(),
                'updated_at' => new DateTime(),
                'expiry' => ($package->type == 'Standard' ) ? $date : $expiry->addMonths($setAddMonth)->format("Y-m-d"),
                'lastPayment' => new DateTime(),
                // 'memberId' => Outlet::find(7)->prefix . $member->id,
                'status' => 'active',
                'start_date' => $request->start_date,
                'join_date' => $request->start_date,
                'deposit_status' => 'unpaid',
                'outstanding'   => $outstanding,
            ]
        );

        $package_user = $member->packages->last();

        // store user questionnaire
        $questionnaire = $request->questionnaire;
        foreach ($questionnaire as $question) {
            if ($question[0] != null || $question[1] != null) {
                $insertQues = $member->questionnaires()->create([
                    'question_id'   => $question[0],
                    'option_id'     => $question[1],
                    'package_user_id'     => $package_user->pivot->id,
                ]);

                if (!$insertQues) {
                    DB::rollback();
                }
            }
        }

        $orders = DB::select("SELECT name,user_id from users where `id` = ".$member->id);
        $member_name = $orders[0]->name;
        $cust_id = $orders[0]->user_id;

        $addMonths = -1;
        for ($i=0; $i < $index ; $i++) {
            $addMonths = $addMonths + $package->subscription;
            $monthly_payment = new Carbon($request->start_date);

            $insertOrder = [
                'member_name'       => $member_name,
                'cust_id'           => $cust_id,
                'package_id'        => $request->package_id,
                "outlet_id"         => $member->outlet_id,
                'package_user_id'   => $package_user->pivot->id,
                'package_price'     => $package->price,
                'package_depo'      => ($i==0) ? $package->deposit : 0,
                'package_total'     => ($i==0) ? $total : $package->price,
                'monthly_payment'   => $monthly_payment,
                // 'monthly_payment'   => $monthly_payment->addMonths($addMonths),
                'registration_fee'  => ($i==0) ? $register_fee : 0,
                'status'            => '1',
                "created_at"        => now(),
                "updated_at"        => now()

            ];
            $orders = Order::create($insertOrder);
            if (!$orders) {
                DB::rollback();
            }
        }

        // Assign the user into his class

        $user_id = $request->user_id;
        $remarks = $request->remarks;

        if ($request->start_date <= Carbon::now()->format('Y-m')) {

            // Check for clash time
            if ($package->category == 'SWIMMING') {
                $user = User::find($user_id);
                $class = Classroom::find($request->classroom_id);
                $class->users()->attach([ $member->id => [
                    'remarks' => $remarks,
                    'status' => $request->status,
                    'package_id' => $package->id,
                    'package_user_id' => $package_user->pivot->id,
                    "created_at"        => now(),
                    "updated_at"        => now()
                ]]);

                $classroom_user = $class->users->last();
                $clock_in_start = date("Y-m-d H:i:s", strtotime("-15 minutes", strtotime($class->start)));
                $clock_in_end = date("Y-m-d H:i:s", strtotime("-16 minutes", strtotime($class->end)));

                $dayName = strtolower($current_date->format('l'));

                if ($dayName == $class->day) {
                    $attendance                 = new Attendance;
                    $attendance->user_id        = $member->id;
                    $attendance->class_id       = $class->id;
                    $attendance->package_id     = $package->id;
                    $attendance->clockIn        = Carbon::now();
                    $attendance->status         = 'absent';
                    $attendance->replacement_id = null;
                    $attendance->week           = $current_date->weekOfMonth;
                    $attendance->clock_in_start = $clock_in_start;
                    $attendance->clock_in_end   = $clock_in_end;
                    $attendance->outlet_id      = $class->outlet_id;
                    $attendance->save();

                    if (!$attendance) {
                        DB::rollback();
                    }
                }
            }
            else {
                $clock_in_start = Carbon::today();
                $clock_in_end = Carbon::today()->endOfDay();

                $attendance                 = new Attendance;
                $attendance->user_id        = $member->id;
                $attendance->package_id     = $package->id;
                $attendance->clockIn        = Carbon::now();
                $attendance->status         = 'absent';
                $attendance->replacement_id = null;
                $attendance->week           = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end   = $clock_in_end;
                $attendance->outlet_id      = $member->outlet_id;
                $attendance->save();

                if (!$attendance) {
                    DB::rollback();
                }
            }

            // $accLevelIds = '402880276ce125e4016ce12701cf0377';

            // $details = new stdClass;
            // $details->pin = $member->id;
            // $details->name = $member->name;
            // if($member->gender == 'Male') {
            //     $gender = 'M';
            // } else {
            //     $gender = 'F';
            // }
            // $details->gender = $gender;
            // $details->accLevelIds = $accLevelIds;
            // $details->accStartTime = $clock_in_start;
            // $details->accEndTime = $clock_in_end;

            // $data = json_encode($details);

            // // dd($data);

            // $client = new Client([
            //     'headers' => [
            //         'Content-Type' => 'application/json'
            //     ]
            // ]); //GuzzleHttp\Client

            // $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

            // try {
            //     $result = $client->get($url);
            //     $status = $result->getStatusCode();
            // } 
            // catch (\GuzzleHttp\Exception\GuzzleException $e) {
            //     // This is will catch all connection timeouts
            //     // Handle accordinly
            //     // $status = $e->getResponse()->getStatusCode();
            //     if ($e->hasResponse()) {
            //         $status = $e->getResponse()->getStatusCode();
            //         // var_dump($status->getStatusCode()); // HTTP status code
            //         // var_dump($status->getReasonPhrase()); // Message
            //         // var_dump((string) $status->getBody()); // Body
            //         // var_dump($status->getHeaders()); // Headers array
            //         // var_dump($status->hasHeader('Content-Type')); // Is the header presented?
            //         // var_dump($status->getHeader('Content-Type')[0]); // Concrete header value
            //     } else {
            //         $status = null;
            //     }
            // }

            // if ($status != 200 || $status == null) {

            //     echo 'Device offline';

            // } else {

            //     $clients = new Client([
            //         'headers' => [
            //             'Content-Type' => 'application/json'
            //         ]
            //     ]); //GuzzleHttp\Client
            //     $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

            //     try {
            //         $result = $clients->post($url, [
            //             'body' => $data,
            //             'timeout' => 20
            //         ]);
            //     } 
            //     catch (GuzzleHttp\Exception\ClientException $e){
            //         // echo 'hello';
            //     }

            // }


        } else {
            // Check for clash time
            if ($package->category == 'SWIMMING') {
                $user = User::find($user_id);
                $class = Classroom::find($request->classroom_id);
                $class->users()->attach([ $member->id => [
                    'remarks' => $remarks,
                    'status' => 'waiting_next_month',
                    'package_id' => $package->id,
                    'package_user_id' => $package_user->pivot->id,
                    "created_at"        => now(),
                    "updated_at"        => now()
                ]]);

                $classroom_user = $class->users->last();

                $year = Carbon::now()->format('Y-m-d');
                $start_time = $year.' 03:00:00';
                $end_time = $year.' 03:01:00';
                $clock_in_start = Carbon::createFromFormat('Y-m-d H:i:s', $start_time)->toDateTimeString();
                $clock_in_end = Carbon::createFromFormat('Y-m-d H:i:s', $end_time)->toDateTimeString();

                // $dayName = strtolower($current_date->format('l'));

                // if ($dayName == $class->day) {
                //     $attendance                 = new Attendance;
                //     $attendance->user_id        = $member->id;
                //     $attendance->class_id       = $class->id;
                //     $attendance->package_id     = $package->id;
                //     $attendance->clockIn        = Carbon::now();
                //     $attendance->status         = 'absent';
                //     $attendance->replacement_id = null;
                //     $attendance->week           = $current_date->weekOfMonth;
                //     $attendance->clock_in_start = $clock_in_start;
                //     $attendance->clock_in_end   = $clock_in_end;
                //     $attendance->outlet_id      = $class->outlet_id;
                //     $attendance->save();

                //     if (!$attendance) {
                //         DB::rollback();
                //     }
                // }
            }
            else{
                $clock_in_start = Carbon::today();
                $clock_in_end = Carbon::today()->endOfDay();

                $attendance                 = new Attendance;
                $attendance->user_id        = $member->id;
                $attendance->package_id     = $package->id;
                $attendance->clockIn        = Carbon::now();
                $attendance->status         = 'absent';
                $attendance->replacement_id = null;
                $attendance->week           = $current_date->weekOfMonth;
                $attendance->clock_in_start = $clock_in_start;
                $attendance->clock_in_end   = $clock_in_end;
                $attendance->outlet_id      = $member->outlet_id;
                $attendance->save();

                if (!$attendance) {
                    DB::rollback();
                }
            }

            // $accLevelIds = '402880276ce125e4016ce12701cf0377';

            // $details = new stdClass;
            // $details->pin = $member->id;
            // $details->name = $member->name;
            // if($member->gender == 'Male') {
            //     $gender = 'M';
            // } else {
            //     $gender = 'F';
            // }
            // $details->gender = $gender;
            // $details->accLevelIds = $accLevelIds;
            // $details->accStartTime = $clock_in_start;
            // $details->accEndTime = $clock_in_end;

            // $data = json_encode($details);

            // $client = new Client([
            //     'headers' => [
            //         'Content-Type' => 'application/json'
            //     ]
            // ]); //GuzzleHttp\Client

            // $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

            // try {
            //     $result = $client->get($url);
            //     $status = $result->getStatusCode();
            // } 
            // catch (\GuzzleHttp\Exception\GuzzleException $e) {
            //     // This is will catch all connection timeouts
            //     // Handle accordinly
            //     // $status = $e->getResponse()->getStatusCode();
            //     if ($e->hasResponse()) {
            //         $status = $e->getResponse()->getStatusCode();
            //         // var_dump($status->getStatusCode()); // HTTP status code
            //         // var_dump($status->getReasonPhrase()); // Message
            //         // var_dump((string) $status->getBody()); // Body
            //         // var_dump($status->getHeaders()); // Headers array
            //         // var_dump($status->hasHeader('Content-Type')); // Is the header presented?
            //         // var_dump($status->getHeader('Content-Type')[0]); // Concrete header value
            //     } else {
            //         $status = null;
            //     }
            // }

            // if ($status != 200 || $status == null) {

            //     echo 'Device offline';

            // } else {

            //     $clients = new Client([
            //         'headers' => [
            //             'Content-Type' => 'application/json'
            //         ]
            //     ]); //GuzzleHttp\Client
            //     $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');
            //     // $result = $client->post($url, [
            //     //     'body' => $data
            //     // ]);
            //     try {
            //         $result = $clients->post($url, [
            //             'body' => $data,
            //             'timeout' => 20
            //         ]);
            //     } 
            //     catch (GuzzleHttp\Exception\ClientException $e){
            //         // echo 'hello';
            //     }

            // }

        }

        // add new record in tbl subscription for track expired bill
        $addMonths = -1;
        $addMonths = $addMonths + $package->subscription;
        $monthly_payment = new Carbon($request->start_date);
        $dateMonthArray = explode('-', $request->start_date);
        $setYear = $dateMonthArray[0];
        $setMonth = $dateMonthArray[1];

        // $date = Carbon::createFromFormat('Y-m-d', $orders->monthly_payment)->addMonths($package->subscription);
        $date = $monthly_payment;
        $month  = $date->month;
        $year  = $date->year;
        $insertOrderSubscription = [
                    'order_id'     => $orders->id,
                    'package_user_id'   => $orders->package_user_id,
                    'month'             => $monthly_payment->addMonths($addMonths)->month,
                    'year'              => $year
                ];

        if ($package->type === 'Standard') {
            $orderSubscription = OrderSubscription::create($insertOrderSubscription);
            if (!$orderSubscription) {
                DB::rollback();
            }
        }

        DB::commit();
    }

    public function view(Transaction $transaction){
        return new TransactionResource($transaction);
    }
}

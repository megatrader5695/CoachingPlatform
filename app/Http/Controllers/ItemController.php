<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index()
    {
        return Item::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'item_name'  => 'required',
            'item_code'  => 'required',
            'price' => 'required',
            'description' => 'required',
        ];

        $this->validate($request, $rules);

        $item = new Item;
        $item->item_name = $request->item_name;
        $item->item_code = $request->item_code;
        $item->price = $request->price;
        $item->description = $request->description;
        $item->save();

        return $item;
    }

    public function show(Item $item)
    {
        return $item;
    }

    public function edit(Item $item)
    {
        //
    }

    public function update(Request $request, Item $item)
    {
        $rules = [
            'item_name'  => 'required',
            'item_code'  => 'required',
            'price' => 'required',
            'description' => 'required',
        ];

        $this->validate($request, $rules);

        $item->item_name = $request->item_name;
        $item->item_code = $request->item_code;
        $item->price = $request->price;
        $item->description = $request->description;
        $item->save();

        return $item;
    }

    public function destroy(Item $item)
    {
        $item->delete();
    }
}

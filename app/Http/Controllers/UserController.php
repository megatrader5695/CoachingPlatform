<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use App\Staff;
use App\Outlet;
use App\PackageUser;
use App\classroomUser;
use App\Attendance;
use App\Order;
use App\OrderSubscription;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Http\Resources\StaffResource;
use App\Http\Resources\MemberResource;
use Illuminate\Support\Facades\Storage;
use App\Schedule;
use App\Transaction;
use App\TransactionDetail;//new
use App\TransactionDiscount;//new
use App\TransactionInvoice;//new
use DB;
use stdClass;
use Carbon\Carbon;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class UserController extends Controller
{	
	private $profile = 'profile';
	private $superadminAccess = 'superadmin';
	private $customerAccess = 'customer';

	public function show(Request $request)
	{
		$user = $request->user();
		$user->outlet = Outlet::find($user->outlet_id);
		return $user;
	}

	public function getOutlet(Request $request)
	{
		return $request->user();
	}

	public function getPackageUser($package_user)
	{
		//dd($package_user);
		$package_user_details = PackageUser::with('packages')->where('id',$package_user)->first();
		//dd($package_user_details);
		return $package_user_details;
	}

	public function role(Request $request)
	{
		return auth()->user()->role;
	}

	public function updateProfile(Request $request)
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->profile);
	    if ($check == 'false') {
	        return response()->json(array(
	            'code'      =>  401,
	            'message'   =>  'You cannot access.'
	        ), 401);
	    }
	    
		$rules = [
			'name'  => 'required',
			'email' => 'required|email|',
			'icNumber' => 'required',
			'contact' => 'required',
		];

		$this->validate($request, $rules);

		$user = $request->user();
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->icNumber = $request->input('icNumber');
		$user->contact = $request->input('contact');
		$user->save();

		return response()->json(compact('user'));
	}

	public function updatePassword(Request $request)
	{
		$rules = [
			'new_password'         => 'required',
			'confirm_new_password' => 'required|same:new_password'
		];

		$this->validate($request, $rules);

		$user = $request->user();
		$user->password = bcrypt($request->input('new_password'));
		$user->saveOrFail();

		return response()->json(compact('user'));
	}

	public function changePassword(Request $request, $id)
	{
		$rules = [
			'new'         	=> 'required|min:8',
			'confirm' 		=> 'required|same:new'
		];

		$this->validate($request, $rules);

		$user = User::find($id);
		$user->password = bcrypt($request->new);
		$user->saveOrFail();

		return response()->json(compact('user'));
	}
	
	public function getAllSuperAdmin()
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->superadminAccess);
	    if ($check == 'false') {
	        return response()->json(array(
	            'code'      =>  401,
	            'message'   =>  'You cannot access.'
	        ), 401);
	    }
		
        // $users_raw = User::all()->where('role_id',2);
        $users_raw = DB::table('staffs')
        	->join('designations', 'designations.id', '=', 'staffs.designation_id')
            ->join('users', 'users.id', '=', 'staffs.user_id')
            // ->select('users.name', 'users.email', 'users.contact', 'designations.name')
            ->where([
                ['role_id', '=' , 2]
            ])
            ->get();

		$users = [];
		
		foreach ($users_raw as $user) {
			array_push($users, $user);
		}

		return $users;

		
	}

	public function getSuperAdmin(User $superadmin)
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->superadminAccess);
	    if ($check == 'false') {
	        return response()->json(array(
	            'code'      =>  401,
	            'message'   =>  'You cannot access.'
	        ), 401);
	    }
		return (new UserResource($superadmin)) ;
	}

	public function updateSuperAdmin(Request $request, User $superadmin)
	{
		// dd($request->input('password'));
		$rules = [
			'name'  => 'required',
			'email' => 'required|email|',
			'contact' => 'required',
			'designation' => 'required',
			'status' => 'required',
			'gender' => 'required',
			'icNumber' => 'required',
			'outlets' => 'required'
		];

		$this->validate($request, $rules);

		// $user = $request->user();
		$superadmin->name = $request->input('name');
		$superadmin->email = $request->input('email');
		if($request->input('password')){
			$superadmin->password = bcrypt($request->input('password'));
		}
		$superadmin->contact = $request->input('contact');
		$superadmin->gender = $request->input('gender');
		$superadmin->icNumber = $request->input('icNumber');
		$superadmin->outlet_id = $request->input('outlets');
		$superadmin->status = $request->input('status');
		$superadmin->save();

		$designation_id = $request->input('designation');
		$outlet_id = $request->input('outlets');
		DB::table('staffs')
            ->where('user_id', $superadmin->id)
            ->update([
            	'designation_id' => $designation_id,
            	'outlet_id' => $outlet_id
            ]);

		return response()->json(compact('superadmin'));
	}

	public function deleteSuperAdmin(User $superadmin){
		$superadmin->delete();
		return;
	}


	// Customer
	public function getAllCustomer($outlet) 
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->customerAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

		$users_raw = User::all()->where('role_id',5)->where('outlet_id', $outlet);

		$users = [];
		
		foreach ($users_raw as $user) {
			array_push($users, $user);
		}

		return $users;
	}
	public function registerCustomer(Request $request){
		$rules = [
			'name'  => 'required',
			'email' => 'required|email|',
			'password' => 'required',
			'contact' => 'required',
		];

		$this->validate($request, $rules);

		$user = new User;
		
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->contact = $request->input('contact');
		$user->birthday = $request->input('birthday');
		//$user->approve = 0;
		$user->role_id = 5;


      	$user->gender = $request->input('gender');
		$user->icNumber = $request->input('icNumber');
		$user->passportNumber = $request->input('passportNumber');  
      	$user->street1 = $request->input('street1');
      	$user->street2 = $request->input('street2');
      	$user->city = $request->input('city');
      	$user->postcode = $request->input('postcode');
      	$user->state = $request->input('state');
      	$user->country = $request->input('country');
      	$user->outlet_id = Auth::user()->outlet_id;


		//Thorugh Super Admin Dashboard
		// if ($request->input('obo') == 'admin') {
		// 	$user->approve = 1;
		// }

		$user->save();
		return $user;
	}

	public function getCustomer(User $customer)
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->customerAccess);
	    if ($check == 'false') {
	        return response()->json(array(
	            'code'      =>  401,
	            'message'   =>  'You cannot access.'
	        ), 401);
	    }

		if ($customer->role_id == 5) {
			return new MemberResource($customer);
		}else{
			return "This user not a Customer";
		}
	}

	public function updateCustomer(User $customer, Request $request){
		if ($customer->role_id !== 5) {
			return 'This user is not a customer';
		}
		$customer->name = $request->input('name');
		$customer->contact = $request->input('contact');
		$customer->birthday = $request->input('birthday');
		$customer->icNumber = $request->input('icNumber');
		$customer->gender = $request->input('gender');
		$customer->email = $request->input('email');
		$customer->passportNumber = $request->input('passportNumber');
		$customer->street1 = $request->input('street1');
		$customer->street2 = $request->input('street2');
		$customer->city = $request->input('city');
		$customer->postcode = $request->input('postcode');
		$customer->state = $request->input('state');
		$customer->country = $request->input('country');
		//$customer->approve = $request->input('approve');

		$customer->save();

		$accLevelIds = '402880276ce125e4016ce12701cf0377';

	    $details = new stdClass;
	    $details->pin = $customer->id;
	    $details->name = $customer->name;
	    if($customer->gender == 'Male') {
	        $gender = 'M';
	    } else {
	        $gender = 'F';
	    }
	    $details->gender = $gender;
	    $details->accLevelIds = $accLevelIds;
	    // $details->accStartTime = $clock_in_start;
	    // $details->accEndTime = $clock_in_end;

	    $data = json_encode($details);

	    $client = new Client([
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]); //GuzzleHttp\Client

        $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

        try {
            // $result = $client->get($url);// In use
            // $status = $result->getStatusCode();
        } 
        catch (\GuzzleHttp\Exception\GuzzleException $e) {
            // This is will catch all connection timeouts
            // Handle accordinly
            // $status = $e->getResponse()->getStatusCode();
            if ($e->hasResponse()) {
                $status = $e->getResponse()->getStatusCode();
                // var_dump($status->getStatusCode()); // HTTP status code
                // var_dump($status->getReasonPhrase()); // Message
                // var_dump((string) $status->getBody()); // Body
                // var_dump($status->getHeaders()); // Headers array
                // var_dump($status->hasHeader('Content-Type')); // Is the header presented?
                // var_dump($status->getHeader('Content-Type')[0]); // Concrete header value
            } else {
                $status = null;
            }
        }

        if ($status != 200 || $status == null) {

            echo 'Device offline';

        } else {

            $clients = new Client([
		    	'headers' => [
		          'Content-Type' => 'application/json'
		        ]
		      ]); //GuzzleHttp\Client
		    $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');
		      // $result = $client->post($url, [
		      //   'body' => $data
		      // ]);
		    try {
				// // In use
		        // $result = $clients->post($url, [
		        //     'body' => $data,
		        //     'timeout' => 30.14
		        // ]);
		    } 
		    catch (GuzzleHttp\Exception\ClientException $e){
		          // echo 'hello';
		    }

        } 

		return $customer;
	}

	public function deleteCustomer(User $customer){
		if ($customer->role_id == 5) {
			
			$members = User::where('user_id',$customer->id)->get();
			
			foreach ($members as $key => $child) {
				
				 	// get user_id and package_id in table package_user
			     	$PackageUser = PackageUser::where('user_id',$child->id)->get();

			     	$client = new Client([
			            'headers' => [
			                'Content-Type' => 'application/json'
			            ]
			        ]); //GuzzleHttp\Client

			        $url = config('zkbio.default_zkbio').'person/add?access_token='.config('zkbio.default_zkbio_token');

			        try {
						// In use
			            // $result = $client->get($url);
			            // $status = $result->getStatusCode();
			        } 
			        catch (\GuzzleHttp\Exception\GuzzleException $e) {
			            // This is will catch all connection timeouts
			            // Handle accordinly
			            // $status = $e->getResponse()->getStatusCode();
			            if ($e->hasResponse()) {
			                $status = $e->getResponse()->getStatusCode();
			                // var_dump($status->getStatusCode()); // HTTP status code
			                // var_dump($status->getReasonPhrase()); // Message
			                // var_dump((string) $status->getBody()); // Body
			                // var_dump($status->getHeaders()); // Headers array
			                // var_dump($status->hasHeader('Content-Type')); // Is the header presented?
			                // var_dump($status->getHeader('Content-Type')[0]); // Concrete header value
			            } else {
			                $status = null;
			            }
			        }

			        if ($status != 200 || $status == null) {

			            echo 'Device offline';

			        } else {

			            $clients = new Client([
	                    	'headers' => [
	                        	'Content-Type' => 'application/json'
	                    	]
	                	]); //GuzzleHttp\Client
	                	$url = config('zkbio.default_zkbio').'person/delete/'.$child->id.'?access_token='.config('zkbio.default_zkbio_token');
	                	// $result = $client->delete($url);

	                	try {
			                $result = $clients->delete($url);
			            } 
			            catch (GuzzleHttp\Exception\ClientException $e){
			                // echo 'hello';
			            }

			        } 

			      // get parent id in table users
			      
			      foreach($PackageUser as $val){

			      	$orders = Order::where([
			          ['cust_id', $child->user_id],
			          ['package_id', $val->package_id],
			          ['package_user_id', $val->id],
			          ['status', '1']
			          ])->first();
			      	if ($orders) {
			      		$orders->delete();
			      	}

			      	$ordersubscription = OrderSubscription::where('package_user_id',$val->id)->first();
			      	if ($ordersubscription) {
			      		$ordersubscription->delete();
			      	}
			      	
			      	$class = ClassroomUser::where('package_user_id', $val->id)->first();

			      	if ($class) {
			      		$class->delete();
			      	}
			      	
			      	$attendances = Attendance::where('user_id',$child->id)->get();
			      	if($attendances){
						foreach ($attendances as $key => $attendance) {
							$today = Carbon::now()->format('Y-m-d');
							$attendance_date = carbon::parse($attendance->clockIn)->format('Y-m-d');
							
							if($today == $attendance_date){
								$attendance->delete();
							}
						}
					}

			      	$val->delete();
			      }

				$child->delete();
			}
			
			$customer->delete();

			return;
		}else{
			return "This user not a Customer";
		}
	}

	// Swim School Mmeber
	public function getAllSwimMember() {

		$users_raw = User::all()->where('role_id',6);

		$users = [];
		
		foreach ($users_raw as $user) {
			array_push($users, $user);
		}

		return $users;
	}

	public function registerSwimMember(Request $request){
		$rules = [
			'name'  => 'required',
			'email' => 'required|email|',
			'password' => 'required',
			'contact' => 'required',
			'birthday' => 'required',
			'health' => 'required',
			'class' => 'required'
		];

		$this->validate($request, $rules);

		$user = new User;
		
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->contact = $request->input('contact');
		$user->birthday = $request->input('birthday');
		$user->approve = 1;
		$user->role_id = 6;
		$user->health = $request->input('health');
		$user->class = $request->input('class');
		$user->user_id = $request->input('user_id');
		$user->status = 'active';
		$user->progress = 0;
		

		$user->save();
		return $user;
	}
	public function getSwimMember(User $swim){
		if ($swim->role_id == 6) {
			return $swim;
		}else{
			return "This user not a Customer";
		}
	}
	public function updateSwimMember(User $swim, Request $request){
		if ($swim->role_id !== 6) {
			return 'This user is not a swim Member';
		}
		$swim->name = $request->input('name');
		$swim->contact = $request->input('contact');
		$swim->birthday = $request->input('birthday');
		$swim->health = $request->input('health');
		$swim->class = $request->input('class');
		$swim->swim = $request->input('swim');
		$swim->stroke = $request->input('stroke');
		$swim->progress = $request->input('progress');
		$swim->badge = $request->input('badge');
		$swim->deposit = $request->input('deposit');

		$swim->user_id = $request->input('user_id');
		$swim->status = $request->input('status');

		$swim->save();

		return $swim;
	}

	public function deleteSwimMember(User $swim){
		if ($swim->role_id == 6) {
			$swim->delete();
			return;
		}else{
			return "This user not a swim member";
		}
	}
	
	// Gym Member

	public function getAllGymMember() {

		$users_raw = User::all()->where('role_id',7);

		$users = [];
		
		foreach ($users_raw as $user) {
			array_push($users, $user);
		}

		return $users;
	}

	public function registerGymMember(Request $request){
		$rules = [
			'name'  => 'required',
			'email' => 'required|email|',
			'password' => 'required',
			'contact' => 'required',
			'birthday' => 'required',
			'class' => 'required'
		];

		$this->validate($request, $rules);

		$user = new User;
		
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->contact = $request->input('contact');
		$user->birthday = $request->input('birthday');
		$user->class = $request->input('class');
		$user->status = 'active';

		$user->approve = 1;
		$user->role_id = 7;
		$user->user_id = $request->input('user_id');
	
		$user->save();
		return $user;
	}

	public function getGymMember(User $gym){
		if ($gym->role_id == 7) {
			return $gym;
		}else{
			return "This user not a Gym Member";
		}
	}

	public function updateGymMember(User $gym, Request $request){
		if ($gym->role_id !== 7) {
			return 'This user is not a Gym Member';
		}
		$gym->name = $request->input('name');
		$gym->contact = $request->input('contact');
		$gym->birthday = $request->input('birthday');
		$gym->deposit = $request->input('deposit');

		$gym->user_id = $request->input('user_id');
		$gym->status = $request->input('status');

		$gym->save();

		return $gym;
	}

	public function deletegymMember(User $gym){
		if ($gym->role_id == 7) {
			$gym->delete();
			return;
		}else{
			return "This user not a gym member";
		}
	}

	public function getCustomerPayment($id){

		// $payments = Transaction::with('order')
		// 	->whereExists(function ($query) {
  //               $query->select(DB::raw(1))
  //                     ->from('orders')
  //                     ->whereRaw('orders.id = transactions.order_id');
  //           })
		// 	->where('cust_id', $id)
		// 	->where('status', 0)
		// 	->orderByDesc('created_at')
		// 	->get();
		$cust_invoice = TransactionInvoice::where('cust_id', $id)
                ->where('status','0')
                ->orderBy('created_at','Desc')
                ->get();



        $cust_inv_arr = [];

        foreach ($cust_invoice as $cust_inv) {
        	array_push($cust_inv_arr, $cust_inv->invoice_no);
        }

		// dd($cust_inv_arr);
		$payments = TransactionDetail::with('invoice','orderlinks','productlinks','voucher')
                ->whereIn('invoice_no', $cust_inv_arr)
                ->orderBy('created_at','Desc')
                ->get();
        // dd($id,$payments->count());

        $user_name = User::where('id',$id)->pluck('name');

        $invoices = [];

        foreach($payments as $payment) {

        	$temp = new StdClass;
        	if($payment->order_id != 0) {
        		if ($payment->orderlinks != null) {
	        		$temp->name = $payment->orderlinks->member_name;
	        		$temp->package_name = $payment->orderlinks->package->name;
					$temp->package_price = $payment->orderlinks->package_price;
					$temp->quantity = 1;
					$temp->package_depo = $payment->orderlinks->package_depo;
					$temp->registration = $payment->orderlinks->registration_fee;
					$temp->payment = $payment->orderlinks->monthly_payment;
					$temp->invoice_no = $payment->invoice_no;

					if($payment->voucher){

						if($payment->voucher->voucher_id > 0){

							$temp->voucher_description = $payment->voucher->vouchers->description;
							if($payment->voucher->vouchers->discountType == 'value'){
								$temp->voucher_value = round($payment->voucher->vouchers->discountValue,2);
							}else if($payment->voucher->vouchers->discountType == 'percentage'){
								$temp->voucher_value = round(($payment->orderlinks->package_total/100)*$payment->voucher->vouchers->discountValue,2);
							}

						}else{

							$temp->voucher_description = 'Used Advance Fee';
							$temp->voucher_value = $payment->voucher->deposit_use;
						}
						
					}else{
						$temp->voucher_description = '-';
						$temp->voucher_value = '-';
					}
					
					$temp->package_total = $payment->orderlinks->package_total;
					if($temp->voucher_value != '-'){
						$calculate = round(($payment->orderlinks->package_total - $temp->voucher_value) / 0.05) * 0.05;
						$temp->grand_total = round($calculate,2);
					}else{
						$temp->grand_total = round($payment->orderlinks->package_total,2);
					}
					$temp->method = $payment->invoice->payment_method;
					$temp->status = $payment->invoice->status;
					$temp->payment_method = $payment->invoice->payment_method;
					$temp->remarks = $payment->invoice->remarks;
					$temp->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $payment->invoice->created_at)->format('Y-m-d H:i:s');
					// if ($payment->order->package->subscription != 1 && !is_null($payment->order->monthly_payment)) {
					// 	$temp->sub = $payment->order->package->subscription - 1;
					// 	$temp->date = new Carbon($payment->order->monthly_payment);
					// 	$temp->monthly_payment = $date->subMonths($sub)->format('Y-m-d');
					// }
	        	}
        	}
        	
			if ($payment->product_id != 0) {
				// if ($payment->payment != null){
					$temp->name = $user_name;
					$temp->package_name = $payment->productlinks->product_name;
					$temp->package_price = $payment->productlinks->price;
					$temp->registration = 0;
					$temp->package_depo = 0;
					$temp->quantity = $payment->invoice->quantity;
					$temp->payment = '';
					$temp->invoice_no = $payment->invoice_no;
					if($payment->voucher){

						$temp->voucher_description = $payment->voucher->vouchers->description;
						if($payment->voucher->vouchers->discountType == 'value'){
							$temp->voucher_value = round($payment->voucher->vouchers->discountValue,2);
						}else if($payment->voucher->vouchers->discountType == 'percentage'){
							$temp->voucher_value = round(($payment->productlinks->price/100)*$payment->voucher->vouchers->discountValue,2);
						}
					}else{
						$temp->voucher_description = '-';
						$temp->voucher_value = '-';
					}
					
					$temp->package_total = $payment->productlinks->price;
					if($temp->voucher_value != '-'){
						$calculate = round(($payment->productlinks->price - $temp->voucher_value) / 0.05) * 0.05;
						$temp->grand_total = number_format($calculate,2);
					}else{
						$temp->grand_total = round($payment->productlinks->price,2);
					}
					$temp->method = $payment->invoice->payment_method;
					$temp->status = $payment->invoice->status;
					$temp->payment_method = $payment->invoice->payment_method;
					$temp->remarks = $payment->invoice->remarks;
					$temp->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $payment->invoice->created_at)->format('Y-m-d H:i:s');
				// }
			}
			if ($payment->order_id != 0) {
				array_push($invoices, $temp);
			}
			if ($payment->product_id != 0){
				array_push($invoices, $temp);
			}
			
        }

		// foreach ($payments as $key => $payment) {
		// 	$payment->order->package;	// get package information
		// 	if ($payment->order->package->subscription != 1 && !is_null($payment->order->monthly_payment)) {
		// 		$sub = $payment->order->package->subscription - 1;
		// 		$date = new Carbon($payment->order->monthly_payment);
		// 		$payment->order->monthly_payment = $date->subMonths($sub)->format('Y-m-d');
		// 	}
		// }
		return $invoices;
		// return $payments->unique('invoice_no')->values()->all();
	}
	
}
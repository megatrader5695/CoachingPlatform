<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Banner;
use App\Badge;
use App\Outlet;
use Illuminate\Support\Facades\Storage;


class UploadController extends Controller
{
    
    public function ic(Request $request, User $user){
        // $url = url('/');
        if ($request->file('attachments') != null) {
            $ic = $request->file('attachments')->store('ic');
            // $avatar = $request->file('avatars')->store('avatar');
            $user->ic = $ic;
            // $user->avatar = $url. '/storage/' .$avatar;
            $user->save();
            return $user;
        }
    }

    public function email($email, Request $request){
        // $url = url('/');
        //dd($email);
        if ($email != null) {
            $existing_email = User::where('email', $email)->first();
            
            if ($existing_email != null) {
                
                return response()->json(array(
                    'code'      =>  401,
                    'message'   =>  'email exist.'
                ), 401);
            }

        
        }
        return $existing_email;
    }

    public function customer_email($email,$user, Request $request){
        // $url = url('/');
        //dd($email);
        if ($email != null) {
            $existing_email = User::where([['email', $email],['id','!=',$user]])->first();
            
            if ($existing_email != null) {
                
                return response()->json(array(
                    'code'      =>  401,
                    'message'   =>  'email exist.'
                ), 401);
            }

        
        }
        return $existing_email;
    }

    public function avatar(Request $request, User $user){
        $url = url('/');

        if ($request->file('avatars') != null) {
            $avatar = $request->file('avatars')->store('avatar');
            $user->avatar = $url. '/storage/' .$avatar;
            $user->save();
            return $user;
        }

        if ($request->file('attachments') != null) {
            $avatar = $request->file('attachments')->store('avatar');
            $user->avatar = $url. '/storage/' .$avatar;
            $user->save();
            return $user;
        }

    }

    public function banner(Request $request, Banner $banner){
        if ($request->file('attachments') != null) {
            $banner_img = $request->file('attachments')->store('banner');
            $banner->image = $banner_img;
            $banner->save();
            return $banner;
        }
    }

    public function badge(Request $request, Badge $badge){
        if ($request->file('attachments') != null) {
            $badge_img = $request->file('attachments')->store('badge');
            $badge->image = $badge_img;
            $badge->save();
            return $badge;
        }
    }

    public function logo(Request $request, Outlet $outlet){
        if ($request->file('attachments') != null) {
            $logo = $request->file('attachments')->store('logo');
            $outlet->outletlogo = $logo;
            $outlet->save();
            return $outlet;
        }
    }

    public function tnc(Request $request, User $user)
    {
        $data_uri = $request->data;
        $encoded_image = explode(",", $data_uri)[1];
        $decoded_image = base64_decode($encoded_image);
        Storage::put('signature/'.$user->name.'_signature.png', $decoded_image);

        $user->signature = 'signature/'.$user->name.'_signature.png';
        $user->first_time_login = 0;
        $user->save();

        return $user;
    }

}

<?php

namespace App\Http\Controllers;

use App\Department;
use App\Role;
use App\Claim;
use Illuminate\Http\Request;
use DB;

class DepartmentController extends Controller
{
    public function index()
    {
        return Department::all();
    }

    public function indexActive()
    {
        return Department::where('status', 'active')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'name'  => 'required|unique:departments',
            'status' => 'required'
        ];

        $this->validate($request, $rules);

        DB::beginTransaction();

        $roles = new Role;
        $roles->role = strtolower($request->name);
        $roles->save();

        if (!$roles) {
            DB::rollback();
        }

        $department = new Department;
        $department->name = $request->name;
        $department->status = $request->status;
        $department->save();

        if (!$department) {
            DB::rollback();
        }

        $claims = Claim::all();
        if ($claims) {
            foreach ($claims as $key => $claim) {
                $claim->roles()->attach($roles->id, [
                    'limit' => 0.00
                ]);
                if (!$claims) {
                    DB::rollback();
                }
            }
        }
        DB::commit();
        return $department;
    }

    public function show(Department $department)
    {
        return $department;
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, Department $department)
    {
        $roleName = $department->name;
        $roles = Role::where('role', $roleName)->first();
        $roles->role = strtolower($request->name);
        $roles->save();

        $department->name = $request->name;
        $department->status = $request->status;
        $department->save();
        return $department;
    }

    public function destroy($id)
    {
        //
    }
}

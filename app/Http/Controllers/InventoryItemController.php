<?php

namespace App\Http\Controllers;

use App\Inventory_item;
use App\Inventory;
use DB;
use Illuminate\Http\Request;

use stdClass;

class InventoryItemController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(inventory_item $inventory_item)
    {
        //
    }

    public function edit(inventory_item $inventory_item)
    {
        //
    }

    public function update(Request $request, inventory_item $inventory_item)
    {
        //
    }

    public function destroy(inventory_item $inventory_item)
    {
        //
    }

    public function receiveInv(Inventory $inventory)
    { 
        //
    }

    public function productName()
    {
        //
    }

}

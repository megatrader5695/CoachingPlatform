<?php

namespace App\Http\Controllers;

use App\CoachReplacement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\CoachReplacementEmail;
// use Mail;
use DB;
use StdClass;
use Carbon\Carbon;

class CoachReplacementController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //dd($request);
        $date = date("Y-m-d", strtotime($request->date));

        DB::beginTransaction();
        $replacement = new CoachReplacement;
        $replacement->coach_id = $request->coach_id['id'];
        $replacement->outlet_id = $request->coach_id['outlet_id'];
        $replacement->date = $date;
        $replacement->class_id = $request->classId;
        $replacement->status = 'approve';
        $replacement->save();

        if (!$replacement) {
            DB::rollback();
        }
        
        $data = new StdClass();
        $data->coach_name = $replacement->coach->name;
        $data->day = $replacement->class->day;
        $data->start = $replacement->class->start;
        $data->end = $replacement->class->end;
        $data->date = Carbon::parse($replacement->date)->format('d-M-Y');

        // Mail::to($replacement->coach->email)->send(new CoachReplacementEmail($data));

        // if (Mail::failures()) {
        //     DB::rollback();
        // }

        DB::commit();
        return $replacement;
    }

    public function show($class, $month, $year)// add where between
    {
        $selected_month = $month+1;
        $months = sprintf('%02d', $selected_month);
        $create_date = $year.'-'.$months.'-01';
        
        $first_of_month = carbon::parse($create_date)->startOfMonth()->format('Y-m-d');
        $last_of_month = carbon::parse($create_date)->endOfMonth()->format('Y-m-d');

        $replacement = CoachReplacement::with('coach')->where('class_id', $class)->whereBetween('date',[$first_of_month,$last_of_month])->orderBy('date','desc')->get();
        
        return $replacement;
    }

    public function edit(CoachReplacement $coachReplacement)
    {
        //
    }

    public function update(Request $request, CoachReplacement $coachReplacement)
    {
        //
    }

    public function destroy(CoachReplacement $coachReplacement)
    {
        //
    }
}

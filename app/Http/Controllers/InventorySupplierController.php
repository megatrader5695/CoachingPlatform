<?php

namespace App\Http\Controllers;

use App\InventorySupplier;
use Illuminate\Http\Request;

class InventorySupplierController extends Controller
{
    public function index($supplier)
    {
        return InventorySupplier::with('inventory')->where('supplier_id', $supplier)->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(InventorySupplier $inventorySupplier)
    {
        return $inventorySupplier;
    }

    public function edit(InventorySupplier $inventorySupplier)
    {
        //
    }

    public function update(Request $request, InventorySupplier $inventorySupplier)
    {
        $inventorySupplier->price = $request->price;
        $inventorySupplier->save();

        return $inventorySupplier;
    }

    public function destroy(InventorySupplier $inventorySupplier)
    {
        //
    }
}

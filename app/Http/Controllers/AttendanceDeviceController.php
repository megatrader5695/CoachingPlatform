<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AttendanceDevice;
use App\AttendanceDeviceSync;
use Carbon\Carbon;

class AttendanceDeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $startOfDay = Carbon::today();
        $endOfDay = Carbon::today()->endOfDay();

        $attendances = AttendanceDevice::whereBetween('created_at',[$startOfDay,$endOfDay])
                       ->select(
                        'member_id',
                        'attendance_id',
                        'payment_signal',
                        'access_signal',
                        'clock_in',
                        'remarks'
                        )
                      ->get();

        return response()->json($attendances);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

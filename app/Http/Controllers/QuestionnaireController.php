<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;
use App\QuestionnaireOption;
use StdClass;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questionnaire = new Questionnaire;
        $questionnaire->package_category_id = $request->package_category_id;
        $questionnaire->question = $request->question;
        $questionnaire->status   = 'active';
        $questionnaire->save();
        return response()->json($questionnaire);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        return $questionnaire;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire)
    {
        $questionnaire->status = 'inactive';
        $questionnaire->save();
        return $questionnaire;
    }

    public function showAll($questionnaire)
    {
        $questionnaire = Questionnaire::where('package_category_id', $questionnaire)->where('status', 'active')->get();

        $setQues = [];
        foreach ($questionnaire as $question) {
            $temp = new StdClass;
            $temp->id = $question->id;
            $temp->question = $question->question;
            $temp->package_category_id = $question->package_category_id;
            $temp->options = [];

          array_push($setQues, $temp);
        }

        return $setQues;
    }

    public function storeOption(Request $request)
    {
        $options = new QuestionnaireOption([
            'options'    => $request->options,
        ]);
        $questionnaire = Questionnaire::find($request->package_category_id);

        $options = $questionnaire->options()->save($options);
        return $options;
    }

    public function showOptions(Questionnaire $questionnaire)
    {
        $options = $questionnaire->options()->where('status', 'active')->get();
        return response()->json(['data' => $options], 200);
    }

    public function showSingleOption($options)
    {
        $options = QuestionnaireOption::find($options);
        return $options;
    }

    public function updateOption(QuestionnaireOption $options, Request $request)
    {
        $options->options = $request->options;
        $options->save();

        return $options;
    }

    public function deleteOption(QuestionnaireOption $options)
    {
        $options->status = 'inactive';
        $options->save();

        return $options;
    }
}

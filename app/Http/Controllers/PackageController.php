<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Package;
use App\Outlet;
use App\Question;
use App\Schedule;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\PackageResource; 
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{   
    private $packageAccess = 'package';
    public function index()
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->packageAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }
        //dd('hi');
        $packages = Package::whereHas('outlets', function($q){$q->where('id', Auth::user()->outlet_id);})->where('status', 'active')->get();
        return $packages;
    }

    public function getallpackage()
    {
        $outlet = Outlet::with('packages')->where('id', Auth::user()->outlet_id)->get();
        $package_arr = [];

        foreach ($outlet as $store) {
            
            foreach ($store->packages as $package) {

                if($package->category == 'SWIMMING'){
                    array_push($package_arr, $package->id);
                }
            }
        }
        
        $packages = Package::where('status', 'active')->whereIn('id', $package_arr)->get();
        return $packages;
    }

    public function getPackages()
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->packageAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }
        
        $packages = Package::with('outlets')->where('status', 'active')->get();
        return $packages;
    }

    public function inactivePackage()
    {
        $packages = Package::where('status', 'inactive')->get();
        return $packages;
    }

    public function store(Request $request)
    {
        $package = new Package;

        $package->name = $request->name;
        $package->description = $request->description;
        // $package->swimCategory = $request->swimCategory;
        $package->subscription = $request->subscription;
        $package->access = $request->access;
        $package->validityFrom = $request->validityFrom;
        $package->validityTo = $request->validityTo;
        $package->price = $request->price;
        $package->deposit = $request->deposit;
        $package->registration_fee = $request->registration_fee;
        $package->status = $request->status;
        $package->gateAccess = $request->gateAccess;
        $package->minAge = $request->minAge;
        $package->maxAge = $request->maxAge;


        $package->category = $request->category;
        $package->type = $request->type;
        $package->payment_type = $request->payment_type;
        $package->auto_upgrade = 0;
        $package->upgrade_to = 0;
        
        $package->save();


        // Assign Outlets
        $currentPackage = Package::find($package->id);


        if ($request->input('outlets')) {
            $outlets = $request->input('outlets');
            foreach ($outlets as $outlet) {
                $currentPackage->outlets()->attach($outlet);
            }
        }

        if ($request->input('question_id')) {
            $questionId = $request->input('question_id');
            $question = Question::find($questionId);
            $question->packages()->save($currentPackage);
        }


        //Assign Working Hour
        $workingHours = $request->input('schedules');
        $packageId = $package->id;
        
        foreach ($workingHours as $hour) {
            $schedule = new Schedule;
            $schedule->package_id = $packageId;
            $schedule->day = $hour['day'];
            $schedule->start = $hour['start'];
            $schedule->end = $hour['end'];
            $schedule->save();
        }
        

        return $currentPackage;
        
    }

    public function show(Package $package)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->packageAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }
        
        return new PackageResource($package);
    }

    public function update(Request $request, Package $package)
    {
        
        
        $package->name = $request->name;
        $package->description = $request->description;
        // $package->swimCategory = $request->swimCategory;
        $package->subscription = $request->subscription;
        $package->access = $request->access;
        $package->validityFrom = $request->validityFrom;
        $package->validityTo = $request->validityTo;
        $package->price = $request->price;
        $package->registration_fee = $request->registration_fee;
        $package->deposit = $request->deposit;
        $package->status = $request->status;

        $package->gateAccess = $request->gateAccess;
        $package->minAge = $request->minAge;
        $package->maxAge = $request->maxAge;

        $package->category = $request->category;
        $package->type = $request->type;
        $package->payment_type = $request->payment_type;


        // $package->photo = $request->photo;

        // Remove all outlet
        $package->outlets()->detach();
        $outlets = $request->input('outlets');
        foreach ($outlets as $outlet) {
            $package->outlets()->attach($outlet['id']);
        }

        // Remove all schedules
        $workingHours = $request->input('schedules');
        DB::table('schedules')->where('package_id', '=', $package->id)->delete();


        $workingHours = $request->input('schedules');
        $packageId = $package->id;
        
        foreach ($workingHours as $hour) {
            $schedule = new Schedule;
            $schedule->package_id = $packageId;
            $schedule->day = $hour['day'];
            $schedule->start = $hour['start'];
            $schedule->end = $hour['end'];
            $schedule->save();
        }

        if($request->autoupgrade != 0){

            $package->auto_upgrade = 1;
            $package->upgrade_to = $request->autoupgrade;

        }else{

            $package->auto_upgrade = 0;
            $package->upgrade_to = 0;
        }

        $package->save();
        return $package;
    }

    public function destroy(Package $package)
    {

        // // Clean Outlet
        // $outlets = $package->outlets;
        // foreach ($outlets as $outlet) {
        //     $package->outlets()->detach($outlet->id);
        // }

        // // Payment
        // $payments = $package->payments;
        // foreach ($payments as $payment) {
        //     $package->payments()->detach($payment->id);
        // }

        // // Timeslot
        // $timeslots = $package->timeslots;
        // foreach ($timeslots as $timeslot) {
        //     $package->timeslots()->detach($timeslot->id);
        // }
        
        // $package->delete();


        // return;
    }

    public function uploadPackageImage(Request $request, Package $package){

        // dd($request->file('attachments'));
        if($request->file('attachments') != null){
            $photo_url = $request->file('attachments')->store('package');
            $package->photo = $photo_url;
            $package->save();
        }
    }

}

<?php

namespace App\Http\Controllers;

use App\LeaveUserDetail;
use App\User;
use Illuminate\Http\Request;

class LeaveUserDetailController extends Controller
{
    public function index($user)
    {
        return LeaveUserDetail::where('user_id', $user)->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request, User $user)
    {
        $input = $request->value;

        foreach ($input as $key => $val) {
            if ($val != null) {
                $condition = [
                    'user_id' => $user->id,
                    'leave_id' => $val['leave_id'],
                ];

                $insert = [
                    'days' => $val['days'],
                    'carryfoward' => $val['carryfoward']
                ];

                $leaveUser = LeaveUserDetail::updateOrCreate($condition, $insert);
            }
        }
    }

    public function show(LeaveUserDetail $leaveUserDetail)
    {
        //
    }

    public function edit(LeaveUserDetail $leaveUserDetail)
    {
        //
    }

    public function update(Request $request, LeaveUserDetail $leaveUserDetail)
    {
        //
    }

    public function destroy(LeaveUserDetail $leaveUserDetail)
    {
        //
    }
}

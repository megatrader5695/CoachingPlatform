<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Timeslot;

class TimeslotController extends Controller
{
    public function index(){
        return Timeslot::all();
    }
    public function store(Request $request)
    {
        $timeslot = new Timeslot;

        $timeslot->start = $request->start;
        $timeslot->end = $request->end;
        $timeslot->day = $request->day;


        $timeslot->save();
		return $timeslot;
    }
}

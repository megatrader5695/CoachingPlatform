<?php

namespace App\Http\Controllers\Outlets;

use App\Http\Controllers\Controller;
use App\Outlet;
use App\User;
use Illuminate\Http\Request;

use App\Http\Resources\OutletResource as OutletResource;

class OutletsController extends Controller
{
    public function index()
    {
        $outlets = Outlet::all();
        return $outlets;
    }


    public function store(Request $request)
    {
        $outlet = new Outlet;

        $outlet->name = $request->name;
        $outlet->company_name = $request->company_name;
        $outlet->street1 = $request->street1;
        $outlet->street2 = $request->street2;
        $outlet->city = $request->city;
        $outlet->postcode = $request->postcode;
        $outlet->state = $request->state;
        $outlet->country = $request->country;
        $outlet->telephone = $request->telephone;
        $outlet->latitude = $request->latitude;
        $outlet->longitude = $request->longitude;
        $outlet->radius = $request->radius;
        $outlet->gstNumber = $request->gstNumber;
        $outlet->gstpercentage = $request->gstpercentage;
        $outlet->currency = strtoupper($request->currency);
        $outlet->tax_label = strtoupper($request->taxLabel);
        $outlet->receipt_message = $request->receiptmessage;
        $outlet->prefix = $request->prefix;
        $outlet->payment = $request->payment;
        $outlet->facial_recognitions = $request->facial_recognitions;
        $outlet->receipt_type = $request->receipt_type;
        $outlet->receipt_size = $request->receipt_size;

        $outlet->save();

		return $outlet;
    }

    public function show(Outlet $outlet)
    {        
        return new OutletResource($outlet);
    }

    public function edit(Outlet $outlet)
    {
        //
    }

    public function update(Request $request, Outlet $outlet)
    {
        $outlet->name = $request->name;
        $outlet->company_name = $request->company_name;
        $outlet->radius = $request->radius;
        $outlet->street1 = $request->street1;
        $outlet->street2 = $request->street2;
        $outlet->city = $request->city;
        $outlet->postcode = $request->postcode;
        $outlet->state = $request->state;
        $outlet->country = $request->country;
        $outlet->telephone = $request->telephone;
        $outlet->latitude = $request->latitude;
        $outlet->longitude = $request->longitude;
        $outlet->gstNumber = $request->gstNumber;
        $outlet->gstpercentage = $request->gstpercentage;
        $outlet->currency = strtoupper($request->currency);
        $outlet->receipt_message = $request->receiptmessage;
        $outlet->prefix = $request->prefix;
        $outlet->tax_label = strtoupper($request->taxlabel);
        $outlet->payment = $request->payment;
        $outlet->facial_recognitions = $request->facial_recognitions;
        $outlet->receipt_type = $request->receipt_type;
        $outlet->receipt_size = $request->receipt_size;

        $outlet->save();
        return $outlet;
    }

    public function destroy(Outlet $outlet)
    {
        //Clean the outlet_user table
        $users = $outlet->users;
        foreach ($users as $user) {
            $outlet->users()->detach($user->id);
        }

        //Clean the outlet_package table
        $packages = $outlet->packages;
        foreach ($packages as $package) {
            $outlet->packages()->detach($package->id);
        }

        $outlet->delete();
        return;
    }

    public function assignOutlet(User $superadmin, Request $request){
        $outlet_id = $request->outlet_id;
        $status = $request->add;
        if ($status == true) {
            $superadmin->outlets()->attach($outlet_id);
        }else{
            $superadmin->outlets()->detach($outlet_id);
        }

        return $superadmin;
    }

        public function uploadOutletLogo(Request $request, Outlet $outlet){

        $photo_url = $request->file('attachments')->store('outlet');
        $outlet->outletlogo = $photo_url;

        $outlet->save();

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Schedule;
use App\Staff;
use App\Coach;
use App\Designation;
use App\Classroom;
use App\CoachReplacement;
use DB;
use stdClass;
use App\Http\Resources\StaffResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class CoachController extends Controller
{
	private $coachAccess = 'coach';

	public function registerCoach(Request $request)
	{
		$rules = [
			'name'  => 'required',

			'email' => 'required|email|',
			'password' => 'required',
			'contact' => 'required'
		];

		$this->validate($request, $rules);

		$user = new User;
		
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->password = bcrypt($request->input('password'));
		$user->contact = $request->input('contact');
		$user->role_id = 4;
      	$user->gender = $request->input('gender');
      	$user->birthday = $request->input('birthday');
      	$user->icNumber = $request->input('icNumber');
      	$user->street1 = $request->input('street1');
      	$user->street2 = $request->input('street2');
      	$user->city = $request->input('city');
      	$user->postcode = $request->input('postcode');
      	$user->state = $request->input('state');
      	$user->country = $request->input('country');
		$user->status = $request->input('status');
		$user->type = $request->input('type');
		$user->outlet_id = Auth::user()->outlet_id;

		$user->save();

		$outlet_access = $request->selectedOutlet;
		
		$staff = new Staff;
		$staff->user_id = $user->id;
		$staff->designation_id = $request->input('designation');
		$staff->outlet_access = $outlet_access;
		$staff->approve = 0;

		//Through Super Admin Dashboard
		if ($request->input('obo') == 'admin') {
			$staff->approve = 1;
		}

		$staff->department = $request->input('department');
		$staff->salary = $request->input('salary');
		$staff->salary = $user->outlet_id;

		//Assign Working Hour
    	$workingHours = $request->input('schedules');
		
		$totaltime = 0;
		foreach ($workingHours as $hour) {
			if ($hour['disabled'] == false) {
				$schedule = new Schedule;
				$schedule->user_id = $user->id;
				$schedule->day = $hour['day'];
				$schedule->start = $hour['start'];
				$schedule->end = $hour['end'];
				$schedule->save();

				//calculate working hours
				$start = Carbon::parse($schedule->start);
		        $end = Carbon::parse($schedule->end);
		        $hours = $end->diffInHours($start);
		        $totaltime = $totaltime + $hours;
			}
		}

		$staff->workingHour = $totaltime;	//store working hours
		$staff->contract = $request->input('contract');
		$staff->signature = $request->input('signature');
		$staff->leave = $request->input('leave');


		$staff->save();

		$coach = new Coach;
		$coach->user_id = $user->id;
		$coach->type = $request->input('type');
		$coach->save();


		// Assign Module
		if ($request->input('module_id')) {
			$moduleId = $request->input('module_id');
			$user->modules()->attach($moduleId);
		}

		return $user;
	}

	public function getCoach($approval)
	{
		// $check = app('App\Http\Controllers\CheckPermissionController')->index($this->coachAccess);
  //       if ($check == 'false') {
  //           return response()->json(array(
  //               'code'      =>  401,
  //               'message'   =>  'You cannot access.'
  //           ), 401);
  //       }
        
		$coaches = User::where([['role_id', '4'],['status','active']])->get();

		foreach ($coaches as $keys => $coach){	
			$coach->staffdetails = Staff::with('designations')->where('user_id', $coach->id)->first();

			if(!$coach->staffdetails){
				$coaches->forget($keys);
			}else if($coach->staffdetails->approve > 0){
				$coaches->forget($keys);
			}
		}
		//dd($coaches);
		foreach ($coaches as $coach) {
			$object = new stdClass;
			$object->name = $coach->id;
			$object->name = $coach->name;
			$object->email = $coach->email;
			$object->phone = $coach->phone;
			$object->designation = $coach->staffdetails->designations->name;
		}

		return [(array) $object];
		
	}

	public function getCoachDetail($coach_id){
		$coach = User::with('coach','staff')->where('id', $coach_id)->first();
		$designation = Designation::find($coach->staff->designation_id);
		if($designation){
			$coach->designation = $designation->id;	
		}
		
		$coach->approve = $coach->staff->approve;
		$coach->department = $coach->staff->department;
		$coach->salary = $coach->staff->salary;
		$coach->outlet_access = $coach->staff->outlet_access;
		$coach->schedule = Schedule::where('user_id',$coach->id)->get();

		return $coach;
	}

	public function getAllCoach($outlet, $filter_status)
	{
		$check = app('App\Http\Controllers\CheckPermissionController')->index($this->coachAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

		$users_raw = User::all()->where('role_id',4)->where('outlet_id', $outlet)->where('status', $filter_status);


		$users = [];
		
		foreach ($users_raw as $user) {

            $coach = new stdClass;
            $coach->id = $user->id;
            $coach->avatar = $user->avatar;
            $coach->name = $user->name;
            $coach->contact = $user->contact;
            $coach->type = $user->type;
            $coach->status = $user->status;
            $coach->class = $user->schedules;

			array_push($users, $coach);
		}

		return $users;
	}

	public function deleteCoach(User $coach){
		if ($coach->role_id == 4) {
			$classrooms = Classroom::with('coaches')->where('user_id', $coach->id)->get();
			//dd($classrooms);
			if ($classrooms->count() == '0') {
				$coach->status = 'Inactive';
				//dd('hi',$coach);
				$coach->save();
				return $classrooms;
			}
			else{
				return response()->json(array(
		            'code'      =>  500,
		            'message'   =>  'This coach have classrooms'
		        ), 500);
			}
		}else{
			return "This user not a Coach";
		}
	}


	public function updateCoach(User $staff, Request $request)
	{
		//dd($request->approve, $request->department, $request->salary, $request->outlet_access);
		$schedule = Schedule::where('user_id', $staff->id)->delete();

		$staff->name = $request->input('name');
		$staff->contact = $request->input('contact');
		$staff->icNumber = $request->input('icNumber');
		$staff->status = $request->input('status');
		$staff->email = $request->input('email');
		$staff->birthday = $request->input('birthday');
		$staff->gender = $request->input('gender');
		$staff->street1 = $request->input('street1');
		$staff->street2 = $request->input('street2');
      	$staff->city = $request->input('city');
      	$staff->postcode = $request->input('postcode');
      	$staff->state = $request->input('state');
      	$staff->country = $request->input('country');
		$staff->type = $request->input('type');
		$staff->remarks = $request->input('remarks');

		$designation_id = $request->input('designation');
		$approve = $request->input('approve');
		$department = $request->input('department');
		$salary = $request->input('salary');
		// $workingHour = $request->input('workingHour');
		$status = $request->input('status');
    	$workingHours = $request->input('schedule'); //Assign Working Hour
		$outlet_access = $request->input('outlet_access');

		$totaltime = 0;
		foreach ($workingHours as $hour) {
			if ($hour['disabled'] == false) {
				$schedule = new Schedule;
				$schedule->user_id = $staff->id;
				$schedule->day = $hour['day'];
				$schedule->start = $hour['start'];
				$schedule->end = $hour['end'];
				$schedule->save();

				//calculate working hours
				$start = Carbon::parse($schedule->start);
		        $end = Carbon::parse($schedule->end);
		        $hours = $end->diffInHours($start);
		        $totaltime = $totaltime + $hours;
			}
		}

        $updateStaff = Staff::where('user_id', $staff->id)->first();
        $updateStaff->designation_id = $designation_id;
        $updateStaff->approve = $approve;
        $updateStaff->department = $department;
        $updateStaff->salary = $salary;
        $updateStaff->workingHour = $totaltime;
		$updateStaff->outlet_access = $outlet_access;
        $updateStaff->save();

        $coach = Coach::where('user_id', $staff->id)->first();
		$coach->type = $request->input('type');
		$coach->save();

		$staff->save();

		return $staff;
	}

	public function getHigherCoach(Request $request){
		$user = $request->user();
		// return DB::table('staffs')
        //             ->where([
        //                 ['role_id', '=', 4],
        //                 ['id', '!=', $user->id]
        //             ])
        //             ->where('designation_id', '=', '1')
        //             ->orWhere('designation_id', '=', '2')
        //             ->orWhere('designation_id', '=', '3')
		// 			->get();
		
        $statemet = 'select * , u.name, d.name "designation" from
					users u, staffs s, designations d
					where (u.role_id = 4
					and u.id = s.user_id)
					and (s.designation_id = 2
					or s.designation_id = 3
					or s.designation_id = 4) 
					and (d.id = s.designation_id)';
        return DB::select(DB::raw($statemet));
	}

	public function getAvailableCoach(Request $request)
	{
		//dd($request->date);
		$outlet_id = $request->outlet;
		$class = Classroom::find($request->class);

		$overlaps = Classroom::with('coaches')
			->where(function ($query) use ($class){
                $query->where('day', $class->day)
					->where('start', $class->start)
					->where('end', $class->end);
            })
			->where('outlet_id', $class->outlet_id)
			->get();

		$replacements = CoachReplacement::where([['outlet_id',$outlet_id],['date',$request->date]])->get();
		//dd($replacements);
		$coaches = User::whereHas('coach')->get();

		foreach ($replacements as $key => $reps) {
			$replaced_coach = [$reps->coach_id];
		}

		foreach ($coaches as $keys => $coach) {

			if(in_array($coach->id, $replaced_coach)){
				
				$coaches->forget($keys);

			}else{
	
	            foreach ($overlaps as $key => $value) {
	                if ($coach->id == $value->coaches->id) {
	                    $coaches->forget($keys);
	                }
	            }
			}

			
        }
        $coaches = $coaches->values();

        return $coaches;
	}
}

<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\Inventory;
use DB;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index()
    {
        return Supplier::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'company_name'  => 'required',
            'street1'  => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
            'country' => 'required',
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required|email|unique:suppliers'
        ];

        $this->validate($request, $rules);

        DB::beginTransaction();
        $supplier = new Supplier;
        $supplier->company_name = $request->company_name;
        $supplier->street1 = $request->street1;
        $supplier->street2 = $request->street2;
        $supplier->city = $request->city;
        $supplier->postcode = $request->postcode;
        $supplier->state = $request->state;
        $supplier->country = $request->country;
        $supplier->name = ucwords($request->name);
        $supplier->phone_number = $request->phone_number;
        $supplier->email = $request->email;
        $supplier->save();

        $inventories = Inventory::all();

        foreach ($inventories as $key => $inventory) {
            $supplier->inventories()->attach($inventory->id,
                [
                    'price' => $inventory->price,
                ]
            );
        }

        if (!$supplier) {
            DB::rollback();
        }

        DB::commit();
        return $supplier;
    }

    public function show(Supplier $supplier)
    {
        $supplier->inventories;
        return $supplier;
    }

    public function edit(Supplier $supplier)
    {
        //
    }

    public function update(Request $request, Supplier $supplier)
    {
        $rules = [
            'company_name'  => 'required',
            'street1'  => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'state' => 'required',
            'country' => 'required',
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required|email|unique:suppliers,email,'.$supplier->id
        ];

        $this->validate($request, $rules);

        $supplier->company_name = $request->company_name;
        $supplier->street1 = $request->street1;
        $supplier->street2 = $request->street2;
        $supplier->city = $request->city;
        $supplier->postcode = $request->postcode;
        $supplier->state = $request->state;
        $supplier->country = $request->country;
        $supplier->name = ucwords($request->name);
        $supplier->phone_number = $request->phone_number;
        $supplier->email = $request->email;
        $supplier->save();

        return $supplier;
    }

    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
    }
}

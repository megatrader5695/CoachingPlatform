<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;
use App\NoticeHistory;
use App\Role;
use App\User;
use App\Mail\SendNotice;
use Mail;
use StdClass;
use Auth;

class NoticeController extends Controller
{
    public function index() {
    	$notices =  Notice::all();

        $setNotice = [];
        foreach ($notices as $noti) {
            $setRoles = [];
            foreach ($noti->role_id as $role) {
                $getRoles = Role::findOrFail($role);
                array_push($setRoles, ucwords($getRoles->role));
            }
            $setItem = new StdClass;
            $setItem->id = $noti->id;
            $setItem->subject = $noti->subject;
            $setItem->role_id = $setRoles;
            array_push($setNotice, $setItem);
        }

        return $setNotice;
    }

    public function show(Notice $notice) {
    	return $notice;
    }

    public function store(Request $request) {
    	$notice = new Notice;
    	
        $notice->subject = $request->subject;
    	$notice->message = $request->message;
        $notice->role_id = $request->role_id;
        $notice->broadcast = $request->broadcast;
    	$notice->save();        

        $noticeHistory = new NoticeHistory;
        $noticeHistory->subject = $notice->subject;
        $noticeHistory->message = $notice->message;
        $noticeHistory->role_id = $notice->role_id;
        $noticeHistory->broadcast = $notice->broadcast;
        $noticeHistory->save();

        if ($notice->broadcast == 'email' || $notice->broadcast == 'all') {
            return $this->sendEmail($notice);
        }

    	return $notice;
    }

    public function update(Notice $notice, Request $request)
    {
        $notice->subject = $request->subject;
        $notice->message = $request->message;
        $notice->role_id = $request->role_id;
        $notice->broadcast = $request->broadcast;
    	$notice->save();

        $noticeHistory = new NoticeHistory;
        $noticeHistory->subject = $notice->subject;
        $noticeHistory->message = $notice->message;
        $noticeHistory->role_id = $notice->role_id;
        $noticeHistory->broadcast = $notice->broadcast;
        $noticeHistory->save();

        if ($notice->broadcast == 'email' || $notice->broadcast == 'all') {
            return $this->sendEmail($notice);
        }
    	return $notice;
    }

    public function delete(Notice $notice) {
    	$notice->delete();
    	return 'Notice has been deleted';
    }

    public function sendEmail(Notice $notice)
    {
        foreach ($notice->role_id as $key => $roles) {
            $users = User::where('role_id', $roles)->get();
            foreach ($users as $key => $user) {
                $message = $notice->message;

                $setVariable = [
                    '{user_name}' => $user->name,
                    '{package_name}' => $user->gender,
                    '{date}' => date("Y-m-d H:i:s")
                ];

                foreach($setVariable as $key => $value) {
                    $message = str_replace($key, $value, $message);
                }

                $data = [
                    'subject' => $notice->subject,
                    'message' => $message
                ];

                Mail::to($user->email)->queue(new SendNotice($data));
            }
        }
    }

     public function indexHistory()
    {
        $auth = Auth::user();

        $noticeHistory = NoticeHistory::where('broadcast', '!=', 'email')->get();

        $setHistory = [];
        foreach ($noticeHistory as $key => $notice) {
            $setVariable = [
                '{user_name}' => $auth->name,
            ];

            // replace string
            foreach($setVariable as $key => $value) {
                $notice->message = str_replace($key, $value, $notice->message);
            }

            foreach ($notice->role_id as $key => $value) {
                if ($value == $auth->role_id) {
                    array_push($setHistory, $notice);
                }
            }
        }
        return $setHistory;
    }
}

<?php

namespace App\Http\Controllers;

use App\Exams;
use App\SyllabusQuestion;
use App\QuestionOption;
use Illuminate\Http\Request;
use StdClass;

class ExamsQuestionController extends Controller
{
    public function index($id)
    {
        $questions = Exams::where('package_id', $id)->orderBy('sequence','ASC')->get();

        $setQues = [];
        foreach ($questions as $question) {
            $temp = new StdClass;
            $temp->id = $question->id;
            $temp->sequence = $question->sequence;
            $temp->question = $question->question;
            $temp->package_id = $question->package_id;
            $temp->options = [];

          array_push($setQues, $temp);
        }

        return response()->json(['data' => $setQues], 200);
    }

    public function store($id)
    {
        $this->validate(request(), [
            'title' => 'required|string'
        ]);

        $checkOrder = Exams::where('package_id', $id)->orderByDesc('sequence')->first();
        $value = empty($checkOrder) ? null : $checkOrder->sequence;

        $question = new Exams;
        $question->package_id = $id;
        $question->question = request('title');
        $question->sequence = $value != null ? $value + 1 : '1';
        $question->save();
    }

    public function show(SyllabusQuestion $id)
    {
        $questions = $id->options()->orderByDesc('created_at')->get();

        return response()->json(['data' => $questions], 200);
    }

    public function destroy($id)
    {
        $questions = Exams::find($id);
        $questions->delete();
    }

    public function showQuestion($id)
    {
        $question = Exams::where('id', $id)->first();
        return response()->json(['data' => $question], 200);
    }

    public function updateQuestion($id)
    {
        $this->validate(request(), [
            'title' => 'required|string',
            // 'sequence' => 'required|integer|unique:syllabus_questions,sequence,'.$id
        ]);
        // dd(request('title'),request('sequence'));

        $questions = Exams::find($id);

        $swapSequence = Exams::where('sequence', request('sequence'))->where('package_id', $questions->package_id)->first();
        if (!empty($swapSequence)) {
            $swapSequence->sequence = $questions->sequence;
            $swapSequence->save();
        }

        $questions->question = request('title');
        $questions->sequence = request('sequence');
        $questions->save();
        return response()->json(['data' => $questions], 200);
    }
}

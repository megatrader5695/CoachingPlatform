<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    public function index() {
    	return Banner::all();
    }

    public function show(Banner $banner) {
    	return $banner;
    }

    public function store(Request $request) {

    	$banner = new Banner;
    	
    	$banner->title = $request->title;
    	$banner->image = $request->image;
    	$banner->description = $request->description;

    	$banner->save();
    	return $banner;
    }

    public function update(Banner $banner, Request $request) {

    	$banner->title = $request->title;
    	$banner->image = $request->image;
    	$banner->description = $request->description;

    	$banner->save();
    	return $banner;

    }

    public function delete(Banner $banner) {
    	$banner->delete();
    	return 'Banner has been deleted';
    }
}

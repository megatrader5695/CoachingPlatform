<?php

namespace App\Http\Controllers;

use App\SupplierOrder;
use Illuminate\Http\Request;
use App\Supplier;
use App\Outlet;
use App\Mail\EmailSupplierOrder;
use Mail;
use DB;
use StdClass;
use Carbon\Carbon;
use PDF;

class SupplierOrderController extends Controller
{
    public function index()
    {
        return SupplierOrder::with('supplier')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $rules = [
            'supplier'  => 'required',
            'listItem'  => 'required',
        ];

        $this->validate($request, $rules);

        $outlet_id = auth()->user()->outlet_id;

        $outlet = Outlet::find($outlet_id);

        $item = new SupplierOrder;
        $item->supplier_id = $request->supplier;
        $item->outlet_id = $outlet_id;
        $item->total_quantity = $request->total_quantity;
        $item->remarks = $request->remarks;
        $item->save();

        if (!$item) {
            DB::rollback();
        }

        foreach ($request->listItem as $key => $listItem) {
            $item->inventory()->attach($listItem['item']['id'],[
                'quantity' => $listItem['quantity']
            ]);
        }
        DB::commit();
        return $item;
    }

    public function sentEmail(SupplierOrder $supplierOrder)
    {
        $data = new StdClass();
        $data->company_name = $supplierOrder->supplier->company_name;
        $data->phone_number = $supplierOrder->supplier->phone_number;
        $data->address      = $supplierOrder->supplier->street1 . ' ' . $supplierOrder->supplier->street2 . ' ' .$supplierOrder->supplier->city . ' ' . $supplierOrder->supplier->postcode . ' ' . $supplierOrder->supplier->state . ' ' . $supplierOrder->supplier->country;
        $data->listItem     = $supplierOrder->inventory;
        $data->remarks      = $supplierOrder->remarks;
        $data->outlet       = $supplierOrder->outlet;
        $data->total_quantity = $supplierOrder->total_quantity;
        $data->date         = Carbon::parse($supplierOrder->created_at)->format('d-M-Y') ;

        Mail::to($supplierOrder->supplier->email)->queue(new EmailSupplierOrder($data));

        return response()->json($data);
    }

    public function PDF(SupplierOrder $supplierOrder)
    {
        $data = [
            'company_name'  => $supplierOrder->supplier->company_name,
            'phone_number'  => $supplierOrder->supplier->phone_number,
            'address'       => $supplierOrder->supplier->street1 . ' ' . $supplierOrder->supplier->street2 . ' ' .$supplierOrder->supplier->city . ' ' . $supplierOrder->supplier->postcode . ' ' . $supplierOrder->supplier->state . ' ' . $supplierOrder->supplier->country,
            'listItem'      => $supplierOrder->inventory,
            'remarks'       => $supplierOrder->remarks,
            'outlet'        => $supplierOrder->outlet,
            'total_quantity'  => $supplierOrder->total_quantity,
            'date'          => Carbon::parse($supplierOrder->created_at)->format('d-M-Y'),
        ];

        $pdf = PDF::loadView('supplier.pdf', $data);
        return $pdf->output();
        return $pdf->stream('invoice.pdf');
    }

    public function show(SupplierOrder $supplierOrder)
    {
        $supplierOrder->supplier;
        $supplierOrder->inventory;
        $supplierOrder->outlet;
        return $supplierOrder;
    }

    public function edit(SupplierOrder $supplierOrder)
    {
        //
    }

    public function update(Request $request, SupplierOrder $supplierOrder)
    {
        //
    }

    public function destroy(SupplierOrder $supplierOrder)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Rating;
use App\Questionnaire;
use App\QuestionnaireOption;
use App\MasterDataPackageCategory;
use Illuminate\Http\Request;
use StdClass;
use Auth;

class RatingController extends Controller
{
    public function index()
    {
        $package_category = MasterDataPackageCategory::where('name', MasterDataPackageCategory::OUTLET)->first();

        $questionnaire = Questionnaire::where('package_category_id', $package_category->id)->where('status', 'active')->orderByDesc('created_at')->get();

        $setQues = [];
        foreach ($questionnaire as $question) {
            $temp = new StdClass;
            $temp->id = $question->id;
            $temp->question = $question->question;
            $temp->package_category_id = $question->package_category_id;

            if (isset($question->options)) {
                $temp->options = $question->options;
            }
            array_push($setQues, $temp);
        }

        return $setQues;
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $rating = new Rating;
        $rating->outlet_id  = $request->outlet_id;
        $rating->user_id       = $user->id;
        $rating->remark     = $request->remark;
        $rating->rate       = $request->rate;
        $rating->save();

        $answers = $request->answer;
        foreach ($answers as $key => $answer) {
            $quesoptions = QuestionnaireOption::find($answer);

            $insertQues = $rating->options()->attach($quesoptions->id, [
                'question_id'   => $quesoptions->questionnaire_id,
            ]);
        }
    }

    public function show(Rating $rating)
    {
        $temp = new StdClass;
        $temp->user = $rating->user;
        $temp->rate = $rating->rate;
        $temp->remark = $rating->remark;
        $temp->options = [];

        foreach ($rating->options as $key => $value) {
            $interim = new StdClass;
            $interim->questions = $value->questions;
            $interim->options = $value->options;

            array_push($temp->options, $interim);
        }
        return response()->json($temp);
    }

    public function edit(Rating $rating)
    {
        //
    }

    public function update(Request $request, Rating $rating)
    {
        //
    }

    public function destroy(Rating $rating)
    {
        //
    }

    public function listAll()
    {
        return Rating::with('options', 'user')->get();
    }
}

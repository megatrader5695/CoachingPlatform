<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\ClaimUsers;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ClaimCollection;
use Carbon\Carbon;

class ClaimController extends Controller
{
    private $claimAccess = 'claim';

    public function index(){

        $claims = Claim::with('roles')->get();
        return $claims;
    }

    public function store(Request $request)
    {
        $claim = new Claim;
        $claim->type = ucwords($request->type);
        $claim->save();

        $roles = $this->listRoles();

        foreach ($roles as $key => $role) {
            $role->claims()->attach($claim->id, [
                'limit' => 0.00
            ]);
        }

        return $claim;
    }

    public function destroy(Claim $claim)
    {
        $users = $claim->users;
        foreach ($users as $user) {
            $claim->users()->detach($user->id);
        }

        $roles = $claim->roles;
        foreach ($roles as $role) {
            $claim->roles()->detach($role->id);
        }

        $claim->delete();

        return;
    }

    public function showByRole(Claim $claim){

        // $claims = DB::table('claim_role')
        //         ->join('roles', 'roles.id', '=', 'claim_role.role_id')
        //         ->join('claims', 'claims.id', '=', 'claim_role.claim_id')
        //         ->select('claim_role.id', 'claim_role.limit', 'roles.role', 'claims.type')
        //         ->where('claim_role.claim_id', $id)
        //         ->get();

        return $claim;

    }

    public function addClaimEntitlement(Claim $claim, Request $request){
        
        $limit = $request->limit;
        $role_id = $request->role_id;

        $claim->roles()->attach([ $role_id => [
            'limit' => $limit
        ] ]);

        return $claim;
    }

    public function updateClaimEntitlement(Claim $claim, Request $request)
    {
        $claim->type = $request->type;
        $claim->save();

        return $claim;
    }

    public function claimByUser($id){
        $user = User::find($id);
        $claims = $user->claims->groupBy('id');

        return $claims;
    }

    public function claimUser(Request $request, User $user){
        $amount = $request->amount;
        $remarks = $request->remarks;
        $claim_id = $request->claim_id;
        //$document = $request->file('document')->store('claim');

        if(auth()->user()->role_id == '4' || auth()->user()->role_id == '9'){
            $user->claims()->attach([ $claim_id => [
                        'amount' => $amount,
                        'remarks' => $remarks,
                        'firstApproval' => 1,
                        'secondApproval' => 0,
                    ]]);
        }else{
            $user->claims()->attach([ $claim_id => [
                        'amount' => $amount,
                        'remarks' => $remarks,
                        'firstApproval' => 1,
                        'secondApproval' => 1,
                    ]]);
        }

        

        //return $user->claims;

        $bridge = DB::table('claim_user')
                    ->where([
                        ['amount', '=', $amount],
                        ['remarks', '=', $remarks],
                        ['user_id', '=', $user->id],
                        ['claim_id', '=', $claim_id],
                        ['approve', '=', 0]
                    ])->get();


        return $bridge;
    }

    // Upload supported dcument
    public function uploadClaimDoc(Request $request, $claim_user_id){
        if($request->file('attachments')){
            
            $photo_url = $request->file('attachments')->store('leave');
        
            $leave = DB::table('claim_user')
                ->where('claim_user.id', $claim_user_id)
                ->update(['document' => $photo_url]);
        }

        return;

    }

    // public function indexClaimApplication(){
    //     $claims = DB::table('claim_user')->get();
    //     return $claims;

    // }

    public function showClaimApplication($id)
    {
        $check = app('App\Http\Controllers\CheckPermissionController')->index($this->claimAccess);
        if ($check == 'false') {
            return response()->json(array(
                'code'      =>  401,
                'message'   =>  'You cannot access.'
            ), 401);
        }

        $role = Role::find(auth()->user()->role_id);
        $leave = DB::table('claim_user')
                ->join('users', 'users.id', '=', 'claim_user.user_id')
                ->join('claims', 'claims.id', '=', 'claim_user.claim_id')
                ->select('claim_user.user_id', 'claim_user.id', 'claim_user.amount', 'claim_user.remarks', 'claim_user.document', 'claim_user.approve', 'claim_user.reason','users.name', 'users.email', 'claims.type as claim')
                ->where('claim_user.id', $id)
                ->get();
        if(auth()->user()->id == $leave[0]->user_id){
            $own_application = true;
        }else{
            $own_application = false;
        }
        return response()->json(compact('leave', 'role', 'own_application'));
    }

    public function approveClaimApplication($id, $status, Request $request){
        // Initialize
        $claim_user = DB::table('claim_user')
            ->where('id', $id)->first();
            
        $user = $request->user(); 
        $role = Role::find($user->role_id);

        if ($status == 'approve') {
            if ($role->role == 'finance') {
                $claim_user = DB::table('claim_user')
                ->where('id', $id)->update([
                    'approve' => '1',
                    'thirdApproval' => '1',
                ]);
            }else if($role->role == 'branch manager'){
                $claim_user = DB::table('claim_user')
                ->where('id', $id)->update([
                    'approve' => '0',
                    'secondApproval' => '1',
                ]);
            }
        }
        else{
            if ($role->role == 'finance') {
            $claim_user = DB::table('claim_user')
                ->where('id', $id)->update([
                    'approve' => '2',
                    'thirdApproval' => '1',
                    'reason' => $request->remark,
                ]);
            }else if($role->role == 'branch manager'){
            $claim_user = DB::table('claim_user')
                ->where('id', $id)->update([
                    'approve' => '2',
                    'secondApproval' => '1',
                    'reason' => $request->remark,
                ]);   
            }
        }

        return $claim_user;
    }

    public function indexApplication(){
        //$claims = Claim::with('users')->get();
        //return $claims;

        return "testing";
    }
    
    public function listRoles()
    {
        $notSelected = [
            'super admin',
            'system admin',
            'customer',
            'gym member',
            'swim member'
        ];

        $roles = Role::whereNotIn('role', $notSelected)->get();

        return $roles;
    }

    public function assignClaim(Role $role, Request $request)
    {        
        foreach ($request->claim as $key => $claim) {
            $role->claims()->updateExistingPivot($claim['id'], [
                'limit' => $claim['pivot']['limit']
            ]); //isset($claim['pivot']['limit']) ? $claim['pivot']['limit'] : 0.00
        }
    }

    public function getAssignClaim(Role $role)
    {
        return $role->claims;
    }

    public function claimApproved($month, $year)
    {
        $auth = auth()->user()->role;

        if($auth->role == 'branch manager'){

            $dateObj   = Carbon::createFromFormat('!m', $month + 1);
            $monthName = $dateObj->format('F');

            $first = Carbon::parse('first day of '.$monthName. ' '.$year)->format('Y-m-d');
            $last = Carbon::parse('last day of '.$monthName. ' '.$year)->format('Y-m-d');

            $leaveUser = ClaimUsers::with('user', 'claim')->where('firstApproval', 1)->whereIn('approve',['1','2','4','5'])->where(function ($q) use ($last, $first){
                $q->whereBetween('created_at', [$first, $last]);
            })->get();
        //dd($leaveUser);
        }else if($auth->role == 'finance'){

            $dateObj   = Carbon::createFromFormat('!m', $month + 1);
            $monthName = $dateObj->format('F');

            $first = Carbon::parse('first day of '.$monthName. ' '.$year)->format('Y-m-d');
            $last = Carbon::parse('last day of '.$monthName. ' '.$year)->format('Y-m-d');

            $leaveUser = ClaimUsers::with('user', 'claim')->where('secondApproval', 1)->whereIn('approve', ['1','2'])->where(function ($q) use ($last, $first){
                $q->whereBetween('created_at', [$first, $last]);
            })->get();
        }

        // $permission = ['super admin', 'it', 'finance', 'human resource', 'branch manager'];
        $permission = ['human resource', 'branch manager'];
        
        if (in_array($auth->role, $permission)) {   
            return $data = [
                    'permission' => true,
                    'leaveUser' => $leaveUser
                ];
        }
        else{
            return $data = [
                    'permission' => false,
                    'leaveUser' => $leaveUser
                ];
        }
    }

    public function approveClaim($id, Request $request)
    {
        //dd($id,$request->approve);
        $user = auth()->user()->role;
       //dd($user->role);
        if ($request->approve == '2') {
            $claimUser = ClaimUsers::find($id);
            
            if($user->role == 'finance'){

                $claimUser->thirdApproval = '2';
                $claimUser->approve = 2;
                $claimUser->reason = $request->reason;

            }elseif($user->role == 'branch manager'){

                $claimUser->secondApproval = '2';
                $claimUser->approve = 5;
                $claimUser->reason = $request->reason;
            }
            $claimUser->save();

            return $claimUser;

        }else{
            // Initialize

            $claimUser = ClaimUsers::find($id);
            
            if($user->role == 'finance'){
                $claimUser->thirdApproval = 1;
                $claimUser->approve = 1;
                $claimUser->reason = '';
            }elseif($user->role == 'branch manager'){
                $claimUser->secondApproval = 1;
                $claimUser->approve = 4;
                $claimUser->reason = '';
            }
            $claimUser->save();
            //dd($claimUser);
            return $claimUser;
           
        }

    }
}

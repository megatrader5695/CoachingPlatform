<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\Staff;
use App\Contract;
use App\CurrentLogin;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthenticateController extends Controller
{
	public function authenticate(Request $request)
	{
		$rules = [
			'email'    => 'required|email',
			'password' => 'required'
		];

		$this->validate($request, $rules);

		$credentials = $request->only('email', 'password');

		try {
			if(!$token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'Invalid login credential'], 401);
			}
		} catch(JWTException $e) {
			return response()->json(['error' => 'Could not create token'], 500);
		}
        
        if($user = $request->user()->status == 'active'){
            $user = $request->user();
            $staff = $request->user()->staff;
            $sessionId = Session::getId();
            $status = $request->user()->status;

            $condition = [
                'user_id' => $user->id
            ];

            $insert = [
                'outlet_id' => $user->outlet_id,
                'session_id' => $sessionId
            ];
            $session = CurrentLogin::updateOrCreate($condition,$insert);

            return response()->json(compact('token', 'user', 'status'));

        }else{

            return response()->json(['error' => 'Invalid login credential'], 401);

        }
		
	}

	public function getMember() {
		if (Auth::user()){
			return "User authorized";
		} else{
			return "User tak authorized";
		}
	}
	
	//Socialite

	public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
	}
	
	public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        if ($authUser->first_time_login == 0) {
            return redirect($this->redirectTo());
        }
        else{
            return redirect("/staff/signatures/".$authUser->id);
        }
	}
	
	public function findOrCreateUser($user, $provider)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        $contract = Contract::where('type', 'staff')->latest('created_at')->first();
        if ($authUser) {
            return $authUser;
        }

        $staffs = new User;
        $staffs->name = $user->name;
        $staffs->email = $user->email;
        $staffs->provider = $provider;
        $staffs->provider_id = $user->id;
        $staffs->outlet_id = 24;
        $staffs->role_id = 3;
        $staffs->first_time_login = 1;
        $staffs->password = bcrypt('1q2e3t4y5i');
        $staffs->save();

        $labour = new Staff;
        $labour->user_id = $staffs->id;
        $labour->outlet_id = $staffs->outlet_id;
        $labour->approve = 0;
        $labour->salary = 0.00;
        $labour->contract = $contract->id;
        $labour->save();

        return $staffs;
    }

    public function redirectTo()
    {
        return '/socialauth';
    }

    public function SocialLogin()
    {
    	$users = Auth::user();
    	return response()->json($users);
    }
}
<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Designation;
use DB;
use App\Contract;

class StaffResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => 'MEM' . $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'ic' => $this->ic,
            'outlet_id' => $this->outlet_id,
            'icNumber' => $this->icNumber,
            'passportNumber' => $this->passportNumber,
            'contact' => $this->contact,
            'gender' => $this->gender,
            'designation'   => $this->staff ? $this->staff->designations : null,
            'approve'       => $this->staff ? $this->staff->approve : null,
            'department' => $this->staff ? $this->staff->departments : null,
            'contract' => $this->staff ? Contract::find($this->staff->contract) : null,
            'signature' => $this->signature,
            'street1' => $this->street1,
            'street2' => $this->street2,
            'city' => $this->city,
            'postcode' => $this->postcode,
            'state' => $this->state,
            'country' => $this->country,
            'claims' => $this->claims,
            'salary' => $this->staff ? $this->staff->salary : null,
            'status' => $this->status,
            'workingHour' => $this->staff ? $this->staff->workingHour : null,
            'type' => $this->type,
            'birthday' => $this->birthday,
            'leave' => $this->staff ? $this->staff->leave : null,
            'outlet_access' => $this->staff ? $this->staff->outlet_access : null,
            // 'leaves' => DB::table('leave_user')->where('user_id', $this->id)->get(),
            'avatar' => $this->avatar,
            'schedule' => $this->schedules,
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PackageResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'photo' => $this->photo,
            'category' => $this->category,
            'status' => $this->status,
            'type' => $this->type,
            'payment_type' => $this->payment_type,
            'access' => $this->access,
            'subscription' => $this->subscription,
            'validityFrom' => $this->validityFrom,
            'validityTo' => $this->validityTo,
            'price' => number_format((float)$this->price, 2, '.', ''),
            'registration_fee' => number_format((float)$this->registration_fee, 2, '.', ''),
            'deposit' => number_format((float)$this->deposit, 2, '.', ''),
            'validity' => $this->validity,
            'outlets' => $this->outlets,
            'gateAccess' => $this->gateAccess,
            'minAge' => $this->minAge,
            'maxAge' => $this->maxAge,
            'schedules' => $this->schedules,
            'auto_upgrade' => $this->auto_upgrade,
            'upgrade_to' => $this->upgrade_to
        ];
    }
}

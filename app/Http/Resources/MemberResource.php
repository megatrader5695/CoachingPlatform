<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\User;
use App\Health;
use DB;
class MemberResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rawId' => $this->id,
            'name' => $this->name,
            'birthday' => $this->birthday,
            'gender' => $this->gender,
            'email' => $this->email,
            'contact' => $this->contact,
            'healths' => $this->health,
            'emergency_name1' => $this->emergency_name1,
            'emergency_relationship1' => $this->emergency_relationship1,
            'emergency_contact1' => $this->emergency_contact1,
            'emergency_name2' => $this->emergency_name2,
            'emergency_relationship2' => $this->emergency_relationship2,
            'emergency_contact2' => $this->emergency_contact2,
            'parent' => $this->parent,
            // 'designation' => $this->staff->designation_id,
            'user_id' => $this->user_id,
            'package' => $this->packages,
            'icNumber' => $this->icNumber,
            'passportNumber' => $this->passportNumber,
            'avatar' => $this->avatar,
            'street1' => $this->street1,
            'street2' => $this->street2,
            'postcode' => $this->postcode,
            'city' => $this->city,
            'state' => $this->state,
            'country' => $this->country,
            'remark' => $this->remarks,
            'status' => $this->status,
            'join_date'    => $this->join_date,
            'children'    => $this->children,
            'outlet_id'    => $this->outlet_id,
            // 'start_date' => DB::table('package_user')->where('user_id', $this->id)->get(),
            'classroom_id' => DB::table('classroom_user')->where('user_id', $this->id)->get(),
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LeaveResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'minimumDay' => $this->minimumDay,
            'continuous' => $this->continuous,
            'users' => $this->users
        ];
    }
}

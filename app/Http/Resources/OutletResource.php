<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OutletResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'address' => $this->address,
            'street1' => $this->street1,
            'street2' => $this->street2,
            'postcode' => $this->postcode,
            'city' => $this->city,
            'state' => $this->state,
            'country' => $this->country,
            'telephone' => $this->telephone,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'radius' => $this->radius,
            'superadmin' => $this->users,
            'gstNumber' => $this->gstNumber,
            'gstpercentage' => $this->gstpercentage,
            'outletlogo' => $this->outletlogo,
            'currency' => $this->currency,
            'receipts' => $this->receipts,
            'prefix' => $this->prefix,
            'receiptmessage' => $this->receipt_message,
            'taxlabel' => $this->tax_label,
            'url' => 'https://www.google.com/maps/embed/v1/place?key=AIzaSyCQu29sgpwGUcpZye79Y3Ks4U1W_lOzhCg&q='.$this->latitude.','.$this->longitude,
            'map_param' => $this->latitude.','.$this->longitude,
            'company_name' => $this->company_name,
            'payment' => $this->payment,
            'facial_recognitions' => $this->facial_recognitions,
            'receipt_type' => $this->receipt_type,
            'receipt_size' => $this->receipt_size,
        ];

    }
}

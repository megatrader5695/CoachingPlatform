<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'contact' => $this->contact,
            'status' => $this->status,
            'designation' => $this->staff->designation_id,
            'outlets' => $this->staff->outlet_id,
            'gender' => $this->gender,
            'icNumber' => $this->icNumber,
            'leaves' => $this->leaves,
            'avatar' => $this->avatar
        ];
    }
}

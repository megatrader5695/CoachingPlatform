<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\User;
use DB;

class ClassResources extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start' => $this->start,
            'end' => $this->end,
            'day' => $this->day,
            'coach' => User::find($this->user_id),
            'level' => $this->level,
            'status' => $this->status,
            'type' => $this->type,
            'maximum' => $this->maximum
        ];
    }
}

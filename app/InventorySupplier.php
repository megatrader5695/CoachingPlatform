<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventorySupplier extends Model
{
    protected $table = 'inventory_supplier';

    public function inventory()
    {
    	return $this->belongsTo('App\Inventory');
    }
}

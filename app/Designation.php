<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    public function users(){
        return $this->hasMany('App\User');
    }

    public function tiers(){
        return $this->belongsToMany('App\Tiers')->withPivot('hours_from','hours_to', 'rate');
    }
}

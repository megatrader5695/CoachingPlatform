<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $fillable = ['invoice_no','order_id','product_id','quantity','sub_total','sst_total','package_id','product_category','item_price','package_depo','reg_fee','created_at','updated_at'];

    protected $table = 'transaction_detail';

    public function voucher()
    {
        return $this->belongsTo('App\TransactionDiscount','transaction_discount_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\TransactionInvoice','invoice_no','invoice_no');
    }

    public function packagelinks()
    {
        return $this->belongsTo('App\Package','package_id');
    }

    public function orderlinks()
    {
        return $this->belongsTo('App\Order','order_id');
    }

    public function productlinks()
    {
        return $this->belongsTo('App\Inventory','product_id');
    }

    public function categorylinks()
    {
        return $this->belongsTo('App\Category','product_category');
    }

    // public function customers()
    // {
    //     return $this->belongsTo('App\User','cust_id');
    // }
}

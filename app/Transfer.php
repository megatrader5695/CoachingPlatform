<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transfer extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function classrooms()
    {
        return $this->belongsToMany('App\Classroom');
    }

    public function member()
    {
        return $this->belongsTo('App\User', 'member_id');
    }

    public function class()
    {
        return $this->belongsTo('App\Classroom', 'toClassroom');
    }
}

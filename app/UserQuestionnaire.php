<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuestionnaire extends Model
{
    protected $fillable = ['question_id', 'option_id', 'package_user_id'];
}

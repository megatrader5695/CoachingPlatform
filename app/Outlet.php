<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $casts = [
        'payment' => 'array',
    ];

    // public function scopeActive($query)
    // {
    //     return $query->where('facial_regonitions', '1');
    // }

    //protected $fillable = ['name', 'type', 'address', 'gps', 'clock'];
    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function packages(){
        return $this->belongsToMany('App\Package');
    }

    public function payments(){
        return $this->belongsToMany('App\Payment');
    }

    public function receipts(){
        return $this->belongsToMany('App\Receipt');
    }

    public function inventorys(){
        return $this->belongsToMany('App\Inventory');
    }
}

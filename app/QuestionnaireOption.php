<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionnaireOption extends Model
{
    protected $fillable = ['options'];

    public function questions()
    {
    	return $this->belongsTo('App\Questionnaire', 'questionnaire_id');
    }
}

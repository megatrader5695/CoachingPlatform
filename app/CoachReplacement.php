<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoachReplacement extends Model
{
    use SoftDeletes;

    public function coach()
    {
    	return $this->belongsTo('App\User', 'coach_id');
    }

    public function class()
    {
    	return $this->belongsTo('App\Classroom', 'class_id');
    }
}

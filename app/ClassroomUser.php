<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassroomUser extends Model
{
    protected $table = 'classroom_user';

    protected $fillable = [
        'classroom_id', 'user_id', 'package_id', 'package_user_id', 'status'
    ];

    public function classes()
    {
    	return $this->belongsTo('App\Classroom', 'classroom_id');
    }

    public function packageUsers()
    {
    	return $this->belongsTo('App\PackageUser', 'package_user_id');
    }

}

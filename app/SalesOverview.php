<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOverview extends Model
{
    protected $fillable = [
        'outlet_id',
        'category',
        'payment_type',
        'item',
        'month',
        'year',
        'total_sales',
        'created_at',
        'updated_at'
    ];

    protected $table = 'sales_overview';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    public function roles(){
        return $this->belongsToMany('App\Role')->withPivot('limit')->withTimestamps();
    }

    public function users(){
        return $this->belongsToMany('App\User')->withPivot('id', 'amount', 'remarks', 'document', 'approve');
    }
}

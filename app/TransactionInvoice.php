<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionInvoice extends Model
{
    protected $fillable = ['cust_id','outlet_id','casheir_id','invoice_total','invoice_sst_total','rounding_adj','amount_paid','change_returned','payment_method','status','invoice_no','remarks','max_transaction_id','created_at','updated_at'];

    protected $table = 'transaction_invoice';

    // public function product()
    // {
    //     return $this->belongsTo('App\Inventory','product_id');
    // }

    public function transactions()
    {
        return $this->hasMany('App\TransactionDetail','invoice_no','invoice_no');
    }

    public function customers()
    {
        return $this->belongsTo('App\User','cust_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    protected $table = 'attendances';

    protected $fillable = [
    	'user_id', 
    	'class_id', 
    	'package_id', 
    	'clockIn', 
    	'status', 
    	'week', 
    	'replacement_id', 
    	'outlet_id', 
    	'clock_in_start', 
    	'clock_in_end'
    ];

    public function scopeClassNull($query)
    {
        return $query->whereNull('class_id');
    }

    public function scopeAbsent($query)
    {
        return $query->where('status', 'absent');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

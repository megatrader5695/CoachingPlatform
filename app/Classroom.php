<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    public function scopeActive($query)
    {
        return $query->where('status', '1');
    }
    
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('id','remarks', 'status', 'package_user_id', 'package_id');
    }

    public function classroomUser()
    {
        return $this->hasMany('App\ClassroomUser', 'classroom_id');
    }

    public function fromClass()
    {
        return $this->hasMany('App\Transfer', 'fromClassroom');
    }

    public function toClass()
    {
        return $this->hasMany('App\Transfer', 'toClassroom');
    }

    public function coaches()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

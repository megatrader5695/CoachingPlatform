<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function outlets(){
        return $this->belongsToMany('App\Outlet');
    }

    public function timeslots(){
        return $this->belongsToMany('App\Timeslot');
    }

    public function payments(){
        return $this->belongsToMany('App\Payment');
    }

    public function question(){
        return $this->belongsTo('App\Question');
    }
    public function schedules(){
        return $this->hasMany('App\Schedule');
    }
}

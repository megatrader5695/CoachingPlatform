<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('login/{provider}', 'AuthenticateController@redirectToProvider');
Route::get('login/{provider}/callback', 'AuthenticateController@handleProviderCallback');
Route::get('socialLogin', 'AuthenticateController@SocialLogin');

Route::post('authenticate', 'AuthenticateController@authenticate');

//get current attendance for device to call
Route::get('attendanceDevice','AttendanceDeviceController@index');
//

Route::group(['middleware' => 'jwt.auth'], function()
{
    Route::get('user', 'UserController@show');
    Route::get('user/role', 'UserController@role');
    Route::post('user/profile/update', 'UserController@updateProfile');
    Route::post('user/password/update', 'UserController@updatePassword');
    Route::post('user/password/change/{id}', 'UserController@changePassword');
});

Route::put('user/profile/register', 'UserController@registerProfile');
Route::put('/staff/signup', 'StaffController@registerSignupStaff');

//SuperAdmin
Route::group(['prefix' => '/superadmin', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'superadmin', 'uses' => 'UserController@getAllSuperAdmin']);
    Route::get('/{superadmin}', ['as' => 'superadmin.show', 'uses' => 'UserController@getSuperAdmin']);
    Route::post('/{superadmin}/edit', ['as' => 'superadmin.edit', 'uses' => 'UserController@updateSuperAdmin']);
    Route::put('/create', ['as' => 'superadmin.register', 'uses' => 'StaffController@registerProfile']);
    Route::delete('/{superadmin}/delete', ['as' => 'superadmin.delete', 'uses' => 'UserController@deleteSuperAdmin']);
});

//Outlet Management
Route::group(['namespace' => 'Outlets', 'prefix' => '/outlets', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'outlets', 'uses' => 'OutletsController@index']);
    Route::put('/', ['as' => 'outlets.store', 'uses' => 'OutletsController@store']);
    Route::get('/{outlet}', ['as' => 'outlets.show', 'uses' => 'OutletsController@show']);
    Route::post('/{outlet}', ['as' => 'outlets.update', 'uses' => 'OutletsController@update']);
    Route::delete('/{outlet}', ['as' => 'outlets.destroy', 'uses' => 'OutletsController@destroy']);
    Route::post('/{outlet}/outletlogo', ['as' => 'outlets.upload', 'uses' => 'OutletsController@uploadOutletLogo']);
    Route::put('/{superadmin}/outlet', ['as' => 'superadmin.outlet', 'uses' => 'OutletsController@assignOutlet']);
});

//Admin Staff
Route::group(['prefix' => '/staff', 'middleware' => 'jwt.auth'], function(){
    Route::get('/{status}/{filter_status}', ['as' => 'staff', 'uses' => 'StaffController@getAllStaffAdmin']);
    Route::post('/register', ['as' => 'staff.register', 'uses' => 'StaffController@registerStaff']);
    Route::post('/{staff}/approve', ['as' => 'staff.approve', 'uses' => 'StaffController@approveStaff']);
    Route::get('/{staff}', ['as' => 'staff.show', 'uses' => 'StaffController@getStaff']);
    Route::delete('/{staff}', ['as' => 'staff.destroy', 'uses' => 'StaffController@deleteStaff']);
    

    Route::post('/{staff}/edit', ['as' => 'staff.update', 'uses' => 'StaffController@updateStaff']);
});

//Coach
Route::group(['prefix' => '/coach', 'middleware' => 'jwt.auth'], function(){
    Route::get('/config', ['as' => 'coach.config', 'uses' => 'DesignationController@index']);
    Route::get('/config/{config}', ['as' => 'coach.config', 'uses' => 'DesignationController@getDesignation']);
    Route::put('/config', ['as' => 'coach.config.create', 'uses' => 'DesignationController@store']);
    Route::post('/config/{config}', ['as' => 'coach.config.edit', 'uses' => 'DesignationController@update']);
    Route::get('/{designation_id}/designation', ['as' => 'coach.config.show', 'uses' => 'DesignationController@show']);
    Route::get('/{outlet}/{filter_status}/outlet', ['as' => 'coach', 'uses' => 'CoachController@getAllCoach']);
    Route::get('/status/{approval}', ['as' => 'coach.show', 'uses' => 'CoachController@getCoach']);
    Route::get('/replacement', ['as' => 'coach.replacement', 'uses' => 'CoachController@getHigherCoach']);
    Route::put('/register', ['as' => 'coach.register', 'uses' => 'CoachController@registerCoach']);
    Route::post('/{staff}/approve', ['as' => 'staff.approve', 'uses' => 'UserController@approveStaff']);
    Route::post('/{staff}/edit', ['as' => 'coach.edit', 'uses' => 'CoachController@updateCoach']);
    Route::delete('/{coach}', ['as' => 'coach.destroy', 'uses' => 'CoachController@deleteCoach']);
    Route::get('get/{coach_id}', ['as' => 'coach.replacement', 'uses' => 'CoachController@getCoachDetail']);

    // coach replacement
    Route::get('/{outlet}/replacement/{class}/{date}', ['as' => '', 'uses' => 'CoachController@getAvailableCoach']);
});

// Coach Replacement
Route::group(['prefix' => '/coach-replacement'], function(){
    Route::get('/', ['as' => 'coach-replacement.index', 'uses' => 'CoachReplacementController@index']);
    Route::put('/', ['as' => 'coach-replacement.store', 'uses' => 'CoachReplacementController@store']);
    Route::get('/{class}/{month}/{year}', ['as' => 'coach-replacement.show', 'uses' => 'CoachReplacementController@show']);
    Route::post('/{coach-replacement}', ['as' => 'coach-replacement.update', 'uses' => 'CoachReplacementController@update']);
    Route::delete('/{coach-replacement}', ['as' => 'coach-replacement.delete', 'uses' => 'CoachReplacementController@destroy']);
});

// To Do List Management
Route::group(['prefix' => '/todolist', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'todolist.index', 'uses' => 'ToDoListController@index']);
    Route::get('/dashboard', ['as' => '', 'uses' => 'ToDoListController@indexDashboard']);
    Route::put('/', ['as' => 'todolist.store', 'uses' => 'ToDoListController@store']);
    Route::get('/{todolist}', ['as' => 'todolist.show', 'uses' => 'ToDoListController@show']);
    Route::post('/{todolist}', ['as' => 'todolist.update', 'uses' => 'ToDoListController@update']);
    Route::delete('/{todolist}', ['as' => 'todolist.delete', 'uses' => 'ToDoListController@destroy']);
});

//Leave Management
Route::group(['prefix' => '/leave', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'leave', 'uses' => 'LeaveController@index']);
    Route::put('/', ['as' => 'leave.create', 'uses' => 'LeaveController@store']);
    Route::post('/{leave}/edit', ['as' => 'leave.edit', 'uses' => 'LeaveController@update']);
    Route::get('/{leave}', ['as' => 'leave.show', 'uses' => 'LeaveController@show']);
    Route::delete('/{leave}', ['as' => 'leave.destroy', 'uses' => 'LeaveController@destroy']);
    Route::put('/{user}/assign', ['as' => 'leave.assign', 'uses' => 'LeaveController@assignLeave']);

    Route::post('/{leave_user_id}/upload', ['as' => 'leave.assign.upload', 'uses' => 'LeaveController@uploadLeaveDoc']);
    Route::post('student/{leave_user_id}/upload', ['as' => 'Student_leave.assign.upload', 'uses' => 'LeaveController@uploadStudentLeaveDoc']);

    Route::put('/{user}/assign/{leave}', ['as' => 'leave.assignUpdate', 'uses' => 'LeaveController@updateLeaveUser']);
    //Route::get('/application', ['as' => 'leave.application', 'uses' => 'LeaveController@getSingleApplication']);
    Route::get('/application/{id}', ['as' => 'leave.application.view', 'uses' => 'LeaveController@showSingleApplication']);
    Route::get('/application', ['as' => 'leave.application.index', 'uses' => 'LeaveController@getSingleApplication']);
    Route::put('/application/{id}/approve', ['as' => 'leave.application.approve', 'uses' => 'LeaveController@approveLeave']);


    Route::get('/application/test', ['as' => 'leave.application.test', 'uses' => 'LeaveController@indexLeaveApplication']);

    Route::get('/{user}/count', ['as' => 'leave', 'uses' => 'LeaveController@leaveBalance']);
    Route::get('/{month}/{year}/approve', ['as' => 'history.leave', 'uses' => 'LeaveController@leaveApproved']);
    Route::get('/staff/list', ['as' => '', 'uses' => 'LeaveController@listStaffs']);
    Route::put('/assign/{user}', ['as' => '', 'uses' => 'LeaveUserDetailController@store']);
    Route::get('/assign/{user}', ['as' => '', 'uses' => 'LeaveUserDetailController@index']);
});

//Package Management
Route::group(['prefix' => '/package', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'package', 'uses' => 'PackageController@index']);
    Route::get('/getallpackage', ['as' => 'package', 'uses' => 'PackageController@getallpackage']);
    Route::get('/get', ['as' => 'package', 'uses' => 'PackageController@getPackages']);
    Route::get('/inactive', ['as' => 'package.inactive', 'uses' => 'PackageController@inactivePackage']);
    Route::put('/', ['as' => 'package.create', 'uses' => 'PackageController@store']);
    Route::post('/{package}/edit', ['as' => 'package.edit', 'uses' => 'PackageController@update']);
    Route::get('/{package}', ['as' => 'package.show', 'uses' => 'PackageController@show']);
    Route::delete('/{package}', ['as' => 'package.destroy', 'uses' => 'PackageController@destroy']);
    Route::post('/{package}/image', ['as' => 'package.upload', 'uses' => 'PackageController@uploadPackageImage']);
});

//Inventory Management
Route::group(['prefix' => '/inventory'], function(){
    Route::get('/product', ['as' => '', 'uses' => 'InventoryController@searchProduct']); //search product

    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('/', ['as' => 'inventory', 'uses' => 'InventoryController@index']);
        Route::put('/', ['as' => 'inventory.create', 'uses' => 'InventoryController@create']);
        // Route::get('/outlet/{id}', ['as' => '', 'uses' => 'InventoryController@getOutlet']);
        Route::post('/{inventory}', ['as' => 'inventory.edit', 'uses' => 'InventoryController@update']);
        Route::get('/{inventory}', ['as' => 'inventory.show', 'uses' => 'InventoryController@show']);
        Route::delete('/{inventory}', ['as' => 'inventory.destroy', 'uses' => 'InventoryController@destroy']);
        // Route::post('/{package}/image', ['as' => 'package.upload', 'uses' => 'InventoryController@uploadPackageImage']);
        Route::put('/receive/{outlet}', ['as' => 'inventory.createReceive', 'uses' => 'InventoryController@createReceive']);
        Route::put('/return/{outlet}', ['as' => 'inventory.createReturn', 'uses' => 'InventoryController@createReturn']);
        Route::get('/receive/{outlet}', ['as' => '', 'uses' => 'InventoryController@showReceiveOutlet']);
        Route::get('/receive/history/{id}', ['as' => '', 'uses' => 'InventoryController@showHistoryOutlet']);
    }); 
});

Route::group(['prefix' => '/inventory-category'], function(){
    Route::get('/', ['as' => 'inventory.category', 'uses' => 'CategoryController@index']);
    Route::put('/', ['as' => 'inventory.category.create', 'uses' => 'CategoryController@create']);
    Route::get('/{category}', ['as' => 'inventory.category.view', 'uses' => 'CategoryController@show']);
    Route::post('/{category}', ['as' => 'inventory.category.edit', 'uses' => 'CategoryController@update']);
    Route::delete('/{category}', ['as' => 'inventory.category.destroy', 'uses' => 'CategoryController@destroy']);
});

//Timeslot
Route::group(['prefix' => '/timeslot'], function(){
    Route::get('/', ['as' => 'timeslot', 'uses' => 'TimeslotController@index']);
    Route::put('/', ['as' => 'timeslot.create', 'uses' => 'TimeslotController@store']);
    // Route::post('/{staff}/approve', ['as' => 'staff.approve', 'uses' => 'UserController@approveStaff']);
    // Route::get('/{coach}', ['as' => 'coach.show', 'uses' => 'UserController@getCoach']);
    // Route::delete('/{coach}', ['as' => 'coach.destroy', 'uses' => 'UserController@deleteCoach']);
});

//Question
Route::group(['prefix' => '/question'], function(){
    // Route::get('/', ['as' => 'coach', 'uses' => 'UserController@getAllCoach']);
    Route::put('/', ['as' => 'question.create', 'uses' => 'QuestionController@store']);
    // Route::post('/{staff}/approve', ['as' => 'staff.approve', 'uses' => 'UserController@approveStaff']);
    // Route::get('/{coach}', ['as' => 'coach.show', 'uses' => 'UserController@getCoach']);
    // Route::delete('/{coach}', ['as' => 'coach.destroy', 'uses' => 'UserController@deleteCoach']);
});

//Payments
Route::group(['prefix' => '/payment'], function(){
    Route::get('/', ['as' => 'payment', 'uses' => 'PaymentController@index']);
    //Route::put('/', ['as' => 'question.create', 'uses' => 'QuestionController@store']);
    // Route::post('/{staff}/approve', ['as' => 'staff.approve', 'uses' => 'UserController@approveStaff']);
    // Route::get('/{coach}', ['as' => 'coach.show', 'uses' => 'UserController@getCoach']);
    // Route::delete('/{coach}', ['as' => 'coach.destroy', 'uses' => 'UserController@deleteCoach']);
});

//Claim Management
Route::group(['prefix' => '/claim', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'claim', 'uses' => 'ClaimController@index']);
    Route::get('/{claim}', ['as' => 'claim.show', 'uses' => 'ClaimController@showByRole']);
    Route::delete('/{claim}', ['as' => 'claim.delete', 'uses' => 'ClaimController@destroy']);
    Route::put('/', ['as' => 'claim.create', 'uses' => 'ClaimController@store']);
    Route::put('/{claim}/rules', ['as' => 'claim.rules', 'uses' => 'ClaimController@addClaimEntitlement']);
    Route::post('/{claim}/rules', ['as' => 'claim.rulesUpdate', 'uses' => 'ClaimController@updateClaimEntitlement']);

    Route::get('/user/{id}/amount', ['as' => 'claim.byUser', 'uses' => 'ClaimController@claimByUser']);

    Route::post('/user/{user}/', ['as' => 'claim.userClaim', 'uses' => 'ClaimController@claimUser']);
    Route::post('/{claim_user_id}/upload', ['as' => 'claim.userClaim.upload', 'uses' => 'ClaimController@uploadClaimDoc']);

    Route::get('/application', ['as' => 'claim.application.index', 'uses' => 'ClaimController@indexClaimApplication']);

    Route::get('/test', ['as' => 'claim.test', 'uses' => 'ClaimController@indexApplication']);

    Route::get('/application/{id}', ['as' => 'claim.application.show', 'uses' => 'ClaimController@showClaimApplication']);
    //Route::put('/application/{id}/{status}', ['as' => 'claim.application.approve', 'uses' => 'ClaimController@approveClaimApplication']);
    Route::get('/{month}/{year}/approve', ['as' => 'history.leave', 'uses' => 'ClaimController@claimApproved']);
    Route::put('/application/{id}/approve', ['as' => 'claim.application.approve', 'uses' => 'ClaimController@approveClaim']);
    // Route::post('/{staff}/approve', ['as' => 'staff.approve', 'uses' => 'UserController@approveStaff']);
    // Route::get('/{coach}', ['as' => 'coach.show', 'uses' => 'UserController@getCoach']);
    // Route::delete('/{coach}', ['as' => 'coach.destroy', 'uses' => 'UserController@deleteCoach']);

    // assign
    Route::get('/role/list', ['as' => '', 'uses' => 'ClaimController@listRoles']);
    Route::get('/assign/{role}', ['as' => '', 'uses' => 'ClaimController@getAssignClaim']);
    Route::put('/assign/{role}', ['as' => '', 'uses' => 'ClaimController@assignClaim']);
});

// Dashboard
Route::group(['prefix' => '/dashboard', 'middleware' => 'jwt.auth'], function(){
    Route::get('/replacement', ['as' => '', 'uses' => 'DashboardController@replacement']);
    Route::get('/freezed/{date}', ['as' => '', 'uses' => 'DashboardController@freezed']);
    Route::get('/terminated/{date}', ['as' => '', 'uses' => 'DashboardController@terminated']);
    Route::get('/data/{month}/{year}', ['as' => '', 'uses' => 'DashboardController@dashboardData']);
    Route::post('/data/{amount}', ['as' => '', 'uses' => 'DashboardController@salesTarget']);
    Route::get('/attendance', ['as' => '', 'uses' => 'DashboardController@attendance']);
    Route::get('/monthlysales', ['as' => '', 'uses' => 'DashboardController@monthly_sales']);
    Route::get('/invoice/{invoice}', ['as' => '', 'uses' => 'DashboardController@invoice']);
    Route::get('/newMember/{date}', ['as' => '', 'uses' => 'DashboardController@NewSwimMember']);
    Route::get('/sales', ['as' => '', 'uses' => 'DashboardController@sales']);
    Route::get('/salesoverview/{outlet}/{year}', ['as' => '', 'uses' => 'DashboardController@salesOverview']);
    Route::get('/manualSalesUpdate/{month}/{year}/{outlet}', ['as' => '', 'uses' => 'DashboardController@manualSalesUpdate']);
    Route::get('/status_chart/{year}', ['as' => '', 'uses' => 'DashboardController@statusChart']);
    Route::get('/totalsales/{date}', ['as' => '', 'uses' => 'DashboardController@totalsales']);
    Route::get('/feedback', ['as' => '', 'uses' => 'DashboardController@feedback']);
    Route::get('/memberOutsanding/{outlet}', ['as' => '', 'uses' => 'DashboardController@memberOutsanding']);
    Route::get('/checkUnpaidOrder/{outlet}/{date}', ['as' => '', 'uses' => 'DashboardController@checkUnpaidOrder']);
    Route::post('/current/{user}', 'DashboardController@currentlogin'); // update outlet login
    Route::get('/current', 'DashboardController@getcurrentlogin'); // get current login
    Route::put('/attendance', ['as' => 'dashboard.attendance', 'uses' => 'DashboardController@swim_attendance']);//swim_attendance
});

//History
Route::group(['prefix' => '/history', 'middleware' => 'jwt.auth'], function(){
    Route::get('/leave', ['as' => 'history.leave', 'uses' => 'DashboardController@leaveHistory']);
    Route::get('/leave/record/{month}/{year}/{status}', ['as' => 'history.leave.record', 'uses' => 'DashboardController@leaveHistoryUser']);
    Route::get('/claim/record/{month}/{year}/{status}', ['as' => 'history.claim.record', 'uses' => 'DashboardController@claimHistoryUser']);
    Route::get('/claim', ['as' => 'history.claim', 'uses' => 'DashboardController@claimHistory']);
});

// Customer
Route::group(['prefix' => '/customer', 'middleware' => 'jwt.auth'], function(){
    Route::get('/{outlet}/outlet', ['as' => 'customer.index', 'uses' => 'UserController@getAllCustomer']);
    Route::get('/{customer}/payment', ['as' => 'customer.show.payment', 'uses' => 'UserController@getCustomerPayment']);
    Route::put('/', ['as' => 'customer.create', 'uses' => 'UserController@registerCustomer']);
    Route::get('/{customer}', ['as' => 'customer.view', 'uses' => 'UserController@getCustomer']);
    Route::post('/{customer}/edit', ['as' => 'customer.edit', 'uses' => 'UserController@updateCustomer']);
    Route::delete('/{customer}', ['as' => 'customer.delete', 'uses' => 'UserController@deleteCustomer']);
});

//Swim Member
Route::group(['prefix' => '/user/swim'], function(){
    Route::get('/', ['as' => 'swim.index', 'uses' => 'UserController@getAllSwimMember']);
    Route::put('/', ['as' => 'swim.create', 'uses' => 'UserController@registerSwimMember']);
    Route::get('/{swim}', ['as' => 'swim.show', 'uses' => 'UserController@getSwimMember']);
    Route::get('/package/{package_user}', ['as' => 'swim.show', 'uses' => 'UserController@getPackageUser']);
    Route::post('/{swim}/edit', ['as' => 'swim.edit', 'uses' => 'UserController@updateSwimMember']);
    Route::delete('/{swim}', ['as' => 'swim.delete', 'uses' => 'UserController@deleteSwimMember']);
});

//Gym Member
Route::group(['prefix' => '/user/gym'], function(){
    Route::get('/', ['as' => 'gym.index', 'uses' => 'UserController@getAllGymMember']);
    Route::put('/', ['as' => 'gym.create', 'uses' => 'UserController@registerGymMember']);
    Route::get('/{gym}', ['as' => 'gym.show', 'uses' => 'UserController@getGymMember']);
    Route::post('/{gym}/edit', ['as' => 'gym.edit', 'uses' => 'UserController@updateGymMember']);
    Route::delete('/{gym}', ['as' => 'gym.delete', 'uses' => 'UserController@deleteGymMember']);
});

// Event Calendar
Route::group(['prefix' => '/event'], function(){
    Route::post('/filter', ['as' => 'event.index', 'uses' => 'EventController@index']);
    Route::put('/', ['as' => 'event.create', 'uses' => 'EventController@store']);
    Route::get('/{event}', ['as' => 'event.show', 'uses' => 'EventController@show']);
    Route::post('/{event}', ['as' => 'event.update', 'uses' => 'EventController@update']);
    Route::delete('/{event}', ['as' => 'event.delete', 'uses' => 'EventController@delete']);
});

// Syllabus Management
Route::group(['prefix' => '/syllabus'], function(){
    Route::get('/', ['as' => 'syllabus.index', 'uses' => 'SyllabusController@index']);
    Route::put('/', ['as' => 'syllabus.create', 'uses' => 'SyllabusController@store']);
    Route::get('/{syllabus}', ['as' => 'syllabus.show', 'uses' => 'SyllabusController@show']);
    Route::post('/{syllabus}', ['as' => 'syllabus.update', 'uses' => 'SyllabusController@update']);
    Route::delete('/{syllabus}', ['as' => 'syllabus.delete', 'uses' => 'SyllabusController@delete']);
});

// classroom
Route::group(['prefix' => '/class', 'middleware' => 'jwt.auth'], function(){
    Route::put('/active-class', ['as' => 'class.waiting', 'uses' => 'ClassroomController@updateWaitingToActive']);
    Route::get('/get-all/{member}', ['as' => 'class.all', 'uses' => 'ClassroomController@getAllClass'] );
    Route::get('/get-all/{member}/{day}', ['as' => 'class.all', 'uses' => 'ClassroomController@getAllClassDetails'] );
    Route::get('/get-syllabus', ['as' => 'class.syllabus', 'uses' => 'ClassroomController@getAllSyllabus'] );
    Route::get('/user', ['as' => 'class.showByUser', 'uses' => 'ClassroomController@showSessionByUser']);
    Route::get('/package_user_class/{package_user_id}', ['as' => 'class.showByPackageUserId', 'uses' => 'ClassroomController@getSessionByUser']);
    Route::put('/replacement', ['as' => 'class.replacement', 'uses' => 'ClassroomController@addReplacementClass']);
    Route::put('/replacement_credit', ['as' => 'class.replacement.credit', 'uses' => 'ClassroomController@addReplacementCredit']);
    Route::get('/check_replacement_credit/{package_user_id}', ['as' => 'class.check.replacement.credit', 'uses' => 'ClassroomController@getReplacementCredit']);
    Route::get('/{class}', ['as' => 'class.view', 'uses' => 'ClassroomController@getClassInformation']);
    Route::put('/{classroom}', ['as' => 'class.update', 'uses' => 'ClassroomController@update']);

    Route::get('/{class}/log/', ['as' => 'class.log.view', 'uses' => 'ClassroomController@getLogRecord']);
    Route::get('/classsummary/{outlet}/{day}', ['as' => 'class.summary', 'uses' => 'ClassroomController@classsummary']);
    Route::put('/{class}/log/', ['as' => 'class.log.add', 'uses' => 'ClassroomController@addLogRecord']);
    Route::post('/{class}/log', ['as' => 'class.log.view.day', 'uses' => 'ClassroomController@getLogRecordByDay']);

    Route::post('/{class}/log/comment', ['as' => 'class.log.update', 'uses' => 'ClassroomController@updateLogRecord']);

    Route::get('/{outlet}/outlet', ['as' => 'class.index', 'uses' => 'ClassroomController@index']);
    Route::put('/', ['as' => 'class.create', 'uses' => 'ClassroomController@store']);
    Route::get('/day/{outlet}/{day}/{status}', ['as' => 'class.day', 'uses' => 'ClassroomController@showByDay']);
    Route::get('/student/{class}', ['as' => 'class.student', 'uses' => 'ClassroomController@showStudentByClass']);
    Route::get('/transfer/{class}', ['as' => '', 'uses' => 'ClassroomController@showStudentTransferByClass']);
    Route::get('/coach/{class}/{memberId}', ['as' => 'class.coach', 'uses' => 'ClassroomController@showStudentByCoach']);
    Route::put('/student/{class}/add', ['as' => 'class.student.add', 'uses' => 'ClassroomController@addStudent']);
    Route::put('/student/{class}/remove', ['as' => 'class.student.remove', 'uses' => 'ClassroomController@removeStudent']);
    Route::get('/replacement/{id}', ['as' => 'class.replacement.all', 'uses' => 'ClassroomController@getAllReplacementClass']);
    Route::get('/{class}/student-replacement', ['as' => 'class.replacement.student', 'uses' => 'ClassroomController@getAllReplacementStudent'] );

    Route::post('/replacements', ['as' => 'class.replacement.student', 'uses' => 'ClassroomController@classReplacementStudent']);
    Route::get('/check_replacements', ['as' => 'class.replacement.student', 'uses' => 'ClassroomController@checkClassReplacement']);
    Route::get('/countTransfer/{date}/{id}', ['as' => '', 'uses' => 'ClassroomController@countTransfer']);
    Route::get('/deleteReplace/{replacement_id}/{id}/{class}', ['as' => '', 'uses' => 'ClassroomController@deleteReplace']);
    Route::post('/transferStudent/{id}/{class}/{classId}', ['as' => '', 'uses' => 'ClassroomController@transferStudent']);
    Route::get('/searchStudent/{student}', ['as' => '', 'uses' => 'ClassroomController@searchStudent']);

    // Class Questionnaire
    Route::get('/questionnaire/{level}', ['as' => '', 'uses' => 'ClassroomController@classQuestionnaire']);
    Route::post('/syllabus/{id}', ['as' => '', 'uses' => 'ClassroomController@storeClassSyllabus']);
    Route::get('/syllabus/{id}', ['as' => '', 'uses' => 'ClassroomController@showClassSyllabus']);
    Route::get('/createAttendance/{outletId}', ['as' => '', 'uses' => 'ClassroomController@createAttendance']);
});

// Member
Route::group(['prefix' => '/member'], function(){
    Route::get('/{outlet}/parent', ['as' => 'member.parent', 'uses' => 'MemberController@parent']);
    // Route::get('/students', ['as' => 'member.student', 'uses' => 'MemberController@students']);
    Route::get('/students/{outlet}', ['as' => 'member.student', 'uses' => 'MemberController@students']);
    Route::get('/students/class/{outlet}', ['as' => 'member.student', 'uses' => 'MemberController@studentsClass']);
    Route::get('/coaches', ['as' => 'member.coach', 'uses' => 'MemberController@coaches']);
    Route::put('/package/disable/auto_upgrade/', ['as' => '', 'uses' => 'MemberController@disableAutoUpgrade']);
    Route::post('/package/{package_user}/edit', ['as' => '', 'uses' => 'MemberController@updateMemberPackage']);

    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('/status/{status}/{outlet}', ['as' => 'member.index', 'uses' => 'MemberController@index']);
        Route::get('/status/{status}/{outlet}/CSV', ['as' => 'member.index', 'uses' => 'MemberController@csvData']);
        Route::put('/', ['as' => 'member.create', 'uses' => 'MemberController@store']);
        Route::get('/{user}', ['as' => 'member.show', 'uses' => 'MemberController@show']);
        //Route::get('/{order}', ['as' => 'member.show', 'uses' => 'MemberController@show']);
        Route::get('/member_order/{id}', ['as' => 'member.showOrder', 'uses' => 'MemberController@showOrder']);
        Route::post('/{user}/{parent}/edit', ['as' => 'member.edit', 'uses' => 'MemberController@update']);
        Route::post('package_user_remarks/{packageUser}', ['as' => 'member.remarks', 'uses' => 'MemberController@updateRemarks']);
        Route::post('/{id}/badge', ['as' => 'member.badge', 'uses' => 'MemberController@updateBadge']);
        Route::post('/{id}/emergency', ['as' => 'member.emergency', 'uses' => 'MemberController@assignEmergency']);
        Route::put('/', ['as' => 'member.create', 'uses' => 'MemberController@store']);
        Route::get('/package/{id}/{classId}', ['as' => 'member.package', 'uses' => 'MemberController@packageMember']); //delete member package
        Route::get('/date/{id}/{date}/{userid}', ['as' => 'member.date', 'uses' => 'MemberController@editDate']); //edit start date
        Route::get('/replacement/{classId}/{id}', ['as' => 'member.replacement', 'uses' => 'MemberController@checkReplacement']); //check replacement credit
        Route::get('/package/{id}', ['as' => '', 'uses' => 'MemberController@viewMemberPackage']); // view member package
         // update member package
        Route::post('/package/{package_user}/edit_month', ['as' => '', 'uses' => 'MemberController@updateMemberPackageMonth']);
        // update member package join date
        Route::get('/package_user/{user_id}', ['as' => '', 'uses' => 'MemberController@getPackageUserId']); // update member package join date
        Route::get('/package_users/{user_id}', ['as' => '', 'uses' => 'MemberController@getPackageUserIds']); // get package user details for badge
        Route::post('/package/{id}/editstatus/{class}/{memberid}', ['as' => '', 'uses' => 'MemberController@updateMemberPackageStatus']); // update member pack status
        Route::get('/order/{id}', ['as' => '', 'uses' => 'MemberController@viewOrderMember']); // view last member paid order

        Route::post('/payment_log/package_user/{id}', ['as' => '', 'uses' => 'MemberController@updatePaymentLog']);
        
        Route::get('/latest_order/{id}', ['as' => '', 'uses' => 'MemberController@LatestMemberOrder']); // view latest member order
        Route::get('/member_package_user/{id}', ['as' => '', 'uses' => 'MemberController@MemberPackageUser']); //  latest member order
        Route::get('/member_ordersubscription/{id}', ['as' => '', 'uses' => 'MemberController@MemberOrderSub']); //  latest member order

        Route::get('/newpackage/{id}/{package}', ['as' => '', 'uses' => 'MemberController@getNewPackage']); // get new package
        Route::get('/newpackage/{id}', ['as' => '', 'uses' => 'MemberController@deleteNewPackage']); // delete new package
        Route::post('/addClass/{id}', ['as' => 'member.addClass', 'uses' => 'MemberController@addClass']); // addClass
        Route::post('/order/{user}/{package_user}', ['as' => '', 'uses' => 'MemberController@createOrder']); // create new record order
        Route::get('/changes_package/{package_user}', ['as' => '', 'uses' => 'MemberController@userChangePackage']); // user change package
        Route::post('/update_freeze/{package_user}', ['as' => '', 'uses' => 'MemberController@update_freeze']); // user change package
        Route::get('/questionnaire/{package_user}', ['as' => '', 'uses' => 'MemberController@userQuestionnaire']); // user questionnaure
        Route::post('/questionnaire/{package_user}/submit', ['as' => '', 'uses' => 'MemberController@usersubmitQuestionnaire']); //

        Route::get('/progress/{classId}', ['as' => '', 'uses' => 'MemberController@userProgress']); // user progress
        Route::get('/package_user/{id}', ['as' => '', 'uses' => 'MemberController@getPackageUser']); // get package user
        Route::get('/hard_terminate/{id}', ['as' => '', 'uses' => 'MemberController@hardTerminate']); // get package user
        Route::get('/check_payment/{packageUser}/{date}', ['as' => '', 'uses' => 'MemberController@checkPrePayment']); // get package user
        Route::post('/update_payment/{packageUser}/{date}', ['as' => '', 'uses' => 'MemberController@updatePrePayment']); // get package user

        Route::get('/searchStudent/{student}', ['as' => '', 'uses' => 'MemberController@searchStudent']);
        Route::get('/all/class/{packageUserId}', ['as' => '', 'uses' => 'MemberController@showAllClass']);
    });
});


// Email Management
Route::group(['prefix' => '/email'], function(){
    Route::get('/', ['as' => 'email.index', 'uses' => 'EmailController@index']);
    Route::get('/variable', ['as' => '', 'uses' => 'EmailController@getVariable']);
    Route::put('/', ['as' => 'email.store', 'uses' => 'EmailController@store']);
    Route::get('/{email}', ['as' => 'email.show', 'uses' => 'EmailController@show']);
    Route::post('/{email}', ['as' => 'email.update', 'uses' => 'EmailController@update']);
    Route::delete('/{email}', ['as' => 'email.delete', 'uses' => 'EmailController@delete']);
});


// Voucher Management
Route::group(['prefix' => '/voucher'], function(){
    Route::get('/', ['as' => 'voucher.index', 'uses' => 'VoucherController@index']);
    Route::put('/', ['as' => 'voucher.store', 'uses' => 'VoucherController@store']);
    Route::get('/{voucher}', ['as' => 'voucher.show', 'uses' => 'VoucherController@show']);
    Route::post('/{voucher}', ['as' => 'voucher.update', 'uses' => 'VoucherController@update']);
    Route::delete('/{voucher}', ['as' => 'voucher.delete', 'uses' => 'VoucherController@delete']);
});


// Notice Management
Route::group(['prefix' => '/notice'], function(){
    Route::get('/', ['as' => 'notice.index', 'uses' => 'NoticeController@index']);
    Route::put('/', ['as' => 'notice.store', 'uses' => 'NoticeController@store']);
    Route::get('/{notice}', ['as' => 'notice.show', 'uses' => 'NoticeController@show']);
    Route::post('/{notice}', ['as' => 'notice.update', 'uses' => 'NoticeController@update']);
    Route::delete('/{notice}', ['as' => 'notice.delete', 'uses' => 'NoticeController@delete']);

    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('/history/board', ['as' => '', 'uses' => 'NoticeController@indexHistory']);
    });
});

// Banner Management
Route::group(['prefix' => '/banner'], function(){
    Route::get('/', ['as' => 'banner.index', 'uses' => 'BannerController@index']);
    Route::put('/', ['as' => 'banner.store', 'uses' => 'BannerController@store']);
    Route::get('/{banner}', ['as' => 'banner.show', 'uses' => 'BannerController@show']);
    Route::post('/{banner}', ['as' => 'banner.update', 'uses' => 'BannerController@update']);
    Route::delete('/{banner}', ['as' => 'banner.delete', 'uses' => 'BannerController@delete']);
});


// Contract Management
Route::group(['prefix' => '/contract'], function(){
    Route::get('/', ['as' => 'contract.index', 'uses' => 'ContractController@index']);
    Route::get('/{types}/filter', ['as' => '', 'uses' => 'ContractController@indexFilter']);
    Route::put('/', ['as' => 'contract.store', 'uses' => 'ContractController@store']);
    Route::get('/{contract}', ['as' => 'contract.show', 'uses' => 'ContractController@show']);
    Route::post('/{contract}', ['as' => 'contract.update', 'uses' => 'ContractController@update']);
    Route::delete('/{contract}', ['as' => 'contract.delete', 'uses' => 'ContractController@delete']);
});

// Upload REST
Route::group(['prefix' => '/upload'], function(){
    Route::post('/user/{user}/ic', ['as' => 'member.upload.ic', 'uses' => 'UploadController@ic']);
    Route::get('/user/email/{email}', ['as' => 'member.check.email', 'uses' => 'UploadController@email']);
    Route::get('/user/email/{email}/{user}', ['as' => 'member.check.email.user', 'uses' => 'UploadController@customer_email']);
    Route::post('/user/{user}/avatar', ['as' => 'member.upload.avatar', 'uses' => 'UploadController@avatar']);
    Route::post('/banner/{banner}/banner', ['as' => 'banner.upload.banner', 'uses' => 'UploadController@banner']);
    Route::post('/badge/{badge}/badge', ['as' => 'member.upload.badge', 'uses' => 'UploadController@badge']);
    Route::post('/logo/{outlet}/logo', ['as' => 'member.upload.logo', 'uses' => 'UploadController@logo']);
    Route::post('/tnc/{user}', ['as' => 'staff.upload.tnc', 'uses' => 'UploadController@tnc']);
});



// Health Management
Route::group(['prefix' => '/health'], function(){
    Route::get('/', ['as' => 'health.index', 'uses' => 'HealthController@index']);
    Route::put('/', ['as' => 'health.store', 'uses' => 'HealthController@store']);
    Route::get('/{health}', ['as' => 'health.show', 'uses' => 'HealthController@show']);
    Route::post('/{health}', ['as' => 'health.update', 'uses' => 'HealthController@update']);
    Route::delete('/{health}', ['as' => 'health.delete', 'uses' => 'HealthController@delete']);
});

// Badge Management
Route::group(['prefix' => '/badge'], function(){
    Route::get('/', ['as' => 'badge.index', 'uses' => 'BadgeController@index']);
    Route::put('/', ['as' => 'badge.store', 'uses' => 'BadgeController@store']);
    Route::get('/{badge}', ['as' => 'badge.show', 'uses' => 'BadgeController@show']);
    Route::post('/{badge}', ['as' => 'badge.update', 'uses' => 'BadgeController@update']);
    Route::delete('/{badge}', ['as' => 'badge.delete', 'uses' => 'BadgeController@delete']);
});

// Department Management
Route::group(['prefix' => '/department'], function(){
    Route::get('/', ['as' => 'department.index', 'uses' => 'DepartmentController@index']);
    Route::get('/active', ['as' => '', 'uses' => 'DepartmentController@indexActive']);
    Route::put('/', ['as' => 'department.store', 'uses' => 'DepartmentController@store']);
    Route::get('/{department}', ['as' => 'department.show', 'uses' => 'DepartmentController@show']);
    Route::post('/{department}', ['as' => 'department.update', 'uses' => 'DepartmentController@update']);
    Route::delete('/{department}', ['as' => 'department.delete', 'uses' => 'DepartmentController@delete']);
});

// Role and Permission Management
Route::group(['prefix' => '/rolespermission'], function(){
    Route::get('/', ['as' => 'rolespermission.index', 'uses' => 'RolePermissionController@index']);
    Route::put('/', ['as' => 'rolespermission.store', 'uses' => 'RolePermissionController@store']);
    Route::get('/{rolespermission}', ['as' => 'rolespermission.show', 'uses' => 'RolePermissionController@show']);
    Route::post('/{rolespermission}', ['as' => 'rolespermission.update', 'uses' => 'RolePermissionController@update']);
    Route::delete('/{rolespermission}', ['as' => 'rolespermission.delete', 'uses' => 'RolePermissionController@delete']);
});

// Calendar config Management
Route::group(['prefix' => '/calendar'], function(){
    Route::get('/', ['as' => 'calendar.index', 'uses' => 'CalendarController@index']);
    Route::put('/', ['as' => 'calendar.store', 'uses' => 'CalendarController@store']);
    Route::get('/{calendar}', ['as' => 'calendar.show', 'uses' => 'CalendarController@show']);
    Route::post('/{calendar}', ['as' => 'calendar.update', 'uses' => 'CalendarController@update']);
    Route::delete('/{calendar}', ['as' => 'calendar.delete', 'uses' => 'CalendarController@delete']);
});

// Role Management
Route::group(['prefix' => '/role'], function(){
    // Route::get('/', ['as' => 'role.index', 'uses' => 'RoleController@index']);
    // Route::put('/', ['as' => 'role.store', 'uses' => 'RoleController@store']);
    Route::get('/{role}', ['as' => 'role.show', 'uses' => 'RoleController@show']);
    // Route::post('/{role}', ['as' => 'role.update', 'uses' => 'RoleController@update']);
    // Route::delete('/{role}', ['as' => 'role.delete', 'uses' => 'RoleController@delete']);
});


// Transaction
Route::group(['prefix' => '/transaction'], function(){
    Route::get('/', ['as' => 'transaction.index', 'uses' => 'TransactionController@index']);
    Route::put('/{member}', ['as' => 'transaction.store', 'uses' => 'TransactionController@store']);
    Route::get('/{transaction}', ['as' => 'transaction.view', 'uses' => 'TransactionController@view']);
});

// Master Data Management
Route::group(['prefix' => '/master-data'], function(){
    Route::get('/maximum-student', ['as' => 'master-data.student', 'uses' => 'MasterDataController@getStudentLimit']);
    Route::put('/maximum-student', ['as' => 'master-data.student.update', 'uses' => 'MasterDataController@updateStudentLimit']);

    Route::get('/package-type', ['as' => 'master-data.package-type', 'uses' => 'MasterDataController@listPackageType']);
    Route::post('/package-type', ['as' => 'master-data.package-type.creaate', 'uses' => 'MasterDataController@insertPackageType']);
    Route::put('/package-type/{id}', ['as' => 'master-data.package-type.update', 'uses' => 'MasterDataController@updatePackageType']);
    Route::delete('/package-type/{id}', ['as' => 'master-data.package-type.delete', 'uses' => 'MasterDataController@deletePackageType']);

    Route::get('/package-category', ['as' => 'master-data.package-category', 'uses' => 'MasterDataController@listPackageCategory']);
    Route::get('/package-category/{category}', ['as' => 'master-data.package-category-show', 'uses' => 'MasterDataController@showPackageCategory']);
    Route::post('/package-category', ['as' => 'master-data.package-category.creaate', 'uses' => 'MasterDataController@insertPackageCategory']);
    Route::put('/package-category/{id}', ['as' => 'master-data.package-category.update', 'uses' => 'MasterDataController@updatePackageCategory']);
    Route::delete('/package-category/{id}', ['as' => 'master-data.package-category.delete', 'uses' => 'MasterDataController@deletePackageCategory']);

    Route::post('/other-category', ['as' => '', 'uses' => 'MasterDataController@insertOtherCategory']);
    Route::get('/other-category', ['as' => '', 'uses' => 'MasterDataController@getOtherCategory']);


    Route::get('/syllabus', 'SyllabusController@index');
    // Route::get('/syllabus/{id}/edit', 'SyllabusController@indexSyllabus');
    
    Route::post('/syllabus/createSyllabus', 'SyllabusController@createSyllabus');
    Route::get('/syllabus/{id}/ques/index', 'SyllabusQuestionController@index');
    Route::post('/syllabus/{id}/update', 'SyllabusController@update');
    Route::post('/syllabus/{id}/ques/create', 'SyllabusQuestionController@store');
    Route::get('/syllabus/{id}', 'SyllabusController@show');
    Route::get('/syllabus/{id}/question', 'SyllabusQuestionController@showQuestion');
    Route::put('/syllabus/{id}/question', 'SyllabusQuestionController@updateQuestion');
    Route::post('/syllabus', ['as' => 'master-data.package-category.creaate', 'uses' => 'MasterDataController@insertPackageCategory']);
    Route::put('/syllabus/{id}', ['as' => 'master-data.package-category.update', 'uses' => 'MasterDataController@updatePackageCategory']);

    Route::post('/exams/{id}/ques/create', 'ExamsQuestionController@store');
    Route::get('/exams/{id}/ques/index', 'ExamsQuestionController@index');
    Route::get('/exams/{id}/question', 'ExamsQuestionController@showQuestion');
    Route::put('/exams/{id}/question', 'ExamsQuestionController@updateQuestion');
    Route::delete('/exams/ques/{id}/delete', 'ExamsQuestionController@destroy');

    Route::get('/ques/{id}/index', 'QuestionOptionController@index');
    Route::delete('/ques/{id}/delete', 'SyllabusQuestionController@destroy');
    Route::post('/ques/{id}/option/create', 'QuestionOptionController@store');
    Route::delete('/option/{id}/delete', 'QuestionOptionController@destroy');
    Route::get('/option/{id}', 'QuestionOptionController@show');
    Route::post('/option/{id}', 'QuestionOptionController@update');
    Route::get('/ques/{id}', 'SyllabusQuestionController@show');
});


// Attendance
Route::group(['prefix' => '/attendance', 'middleware' => 'jwt.auth'], function(){
    Route::put('/', ['as' => 'attendance.store', 'uses' => 'AttendanceController@store']);
    Route::put('/add', ['as' => 'attendance.add', 'uses' => 'AttendanceController@addStudentAttendance']);
    Route::delete('/{attendance}', ['as' => 'attendance.delete', 'uses' => 'AttendanceController@destroy']);
    Route::get('/', ['as' => 'attendance.get', 'uses' => 'AttendanceController@get']);
    Route::get('/student_package/{memberId}', ['as' => 'attendance.student_package', 'uses' => 'AttendanceController@student_package']);
    Route::get('/week/{classroom}', ['as' => 'attendance.getweek', 'uses' => 'AttendanceController@getWeek']);
    Route::get('/{week}/{classroom}', ['as' => 'attendance.getbyweek', 'uses' => 'AttendanceController@getByWeek']);
    Route::get('/month/{month}/{classroom}', ['as' => 'attendance.getbyweek', 'uses' => 'AttendanceController@getByMonth']);
    Route::get('/{id}/{class}/{package}', ['as' => 'attendance.getweek', 'uses' => 'AttendanceController@getAttendanceByStudents']);
    Route::get('/replacemet_attachments/{package_user_id}', ['as' => 'attendance.replacement_attachments', 'uses' => 'AttendanceController@ReplacemetAttachments']);
    Route::post('/{id}/update', ['as' => 'attendance.update', 'uses' => 'AttendanceController@update']);

    Route::post('/staff/clockIn', ['as' => '', 'uses' => 'AttendanceController@ClockInStaffAttendance']);
    Route::post('/staff/clockOut', ['as' => '', 'uses' => 'AttendanceController@ClockOutStaffAttendance']);
    Route::get('/staff', ['as' => '', 'uses' => 'AttendanceController@getStaffAttendance']);

    Route::get('/dashboard/{type}', ['as' => '', 'uses' => 'AttendanceController@dashboard']);
});

// Questionnaire Management
Route::group(['prefix' => '/questionnaire'], function(){
    Route::get('/{package_user_id}', ['as' => 'questionnaire.show', 'uses' => 'QuestionnaireController@show']);
    Route::put('/', ['as' => 'questionnaire.store', 'uses' => 'QuestionnaireController@store']);
    Route::get('/{questionnaire}/category', ['as' => 'questionnaire.showAll', 'uses' => 'QuestionnaireController@showAll']);
    Route::get('/{questionnaire}/options', ['as' => 'questionnaire.showOptions', 'uses' => 'QuestionnaireController@showOptions']);
    Route::put('/options', ['as' => 'questionnaire.storeOption', 'uses' => 'QuestionnaireController@storeOption']);
    Route::get('/{options}/option', ['as' => 'questionnaire.showSingleOption', 'uses' => 'QuestionnaireController@showSingleOption']);
    Route::post('/{options}/update', ['as' => 'questionnaire.updateOption', 'uses' => 'QuestionnaireController@updateOption']);
    Route::delete('/{questionnaire}', ['as' => 'questionnaire.delete', 'uses' => 'QuestionnaireController@destroy']);
    Route::delete('/{options}/option', ['as' => 'questionnaire.deleteOption', 'uses' => 'QuestionnaireController@deleteOption']);

    // for rating
    Route::group(['middleware' => 'jwt.auth'], function(){
        Route::get('/rating/outlet', ['as' => '', 'uses' => 'RatingController@index']);
        Route::get('/rating/all', ['as' => '', 'uses' => 'RatingController@listAll']);
        Route::get('/rating/{rating}', ['as' => '', 'uses' => 'RatingController@show']);
        Route::put('/rating/outlet', ['as' => '', 'uses' => 'RatingController@store']);
    });
});

// Schedule
Route::group(['prefix' => '/schedule', 'middleware' => 'jwt.auth'], function(){
    Route::get('/today', ['as' => 'schedule.index', 'uses' => 'ScheduleController@index']);
    Route::put('/', ['as' => 'schedule.create', 'uses' => 'ScheduleController@store']);
    Route::get('/{schedule}', ['as' => 'schedule.show', 'uses' => 'ScheduleController@show']);
    Route::post('/{schedule}', ['as' => 'schedule.update', 'uses' => 'ScheduleController@update']);
    Route::delete('/{schedule}', ['as' => 'schedule.delete', 'uses' => 'ScheduleController@delete']);
});

// Feedback
Route::group(['prefix' => '/feedback', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'feedback.index', 'uses' => 'FeedbackController@index']);
    Route::get('/management/dashboard', ['as' => 'feedback.index', 'uses' => 'FeedbackController@indexDashboard']);
    Route::put('/', ['as' => 'feedback.create', 'uses' => 'FeedbackController@store']);
    Route::get('/{feedback}', ['as' => 'feedback.show', 'uses' => 'FeedbackController@show']);
    Route::post('/{feedback}', ['as' => 'feedback.update', 'uses' => 'FeedbackController@update']);
    Route::delete('/{feedback}', ['as' => 'feedback.delete', 'uses' => 'FeedbackController@destroy']);
});

// Supplier Management
Route::group(['prefix' => '/supplier', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'supplier.index', 'uses' => 'SupplierController@index']);
    Route::put('/', ['as' => 'supplier.create', 'uses' => 'SupplierController@store']);
    Route::get('/{supplier}', ['as' => 'supplier.show', 'uses' => 'SupplierController@show']);
    Route::post('/{supplier}', ['as' => 'supplier.update', 'uses' => 'SupplierController@update']);
    Route::delete('/{supplier}', ['as' => 'supplier.delete', 'uses' => 'SupplierController@destroy']);
});

// Supplier Item Management
Route::group(['prefix' => '/item', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'item.index', 'uses' => 'ItemController@index']);
    Route::put('/', ['as' => 'item.create', 'uses' => 'ItemController@store']);
    Route::get('/{item}', ['as' => 'item.show', 'uses' => 'ItemController@show']);
    Route::post('/{item}', ['as' => 'item.update', 'uses' => 'ItemController@update']);
    Route::delete('/{item}', ['as' => 'item.delete', 'uses' => 'ItemController@destroy']);
});

// Supplier Order Management
Route::group(['prefix' => '/supplier-order', 'middleware' => 'jwt.auth'], function(){
    Route::get('/', ['as' => 'supplier-order.index', 'uses' => 'SupplierOrderController@index']);
    Route::put('/', ['as' => 'supplier-order.create', 'uses' => 'SupplierOrderController@store']);
    Route::get('/{supplierOrder}', ['as' => 'supplier-order.show', 'uses' => 'SupplierOrderController@show']);
    Route::post('/{supplierOrder}', ['as' => 'supplier-order.update', 'uses' => 'SupplierOrderController@update']);
    Route::post('/{supplierOrder}/email', ['as' => 'supplier-order.update', 'uses' => 'SupplierOrderController@sentEmail']);
    Route::post('/{supplierOrder}/pdf', ['as' => 'supplier-order.update', 'uses' => 'SupplierOrderController@PDF']);
    Route::delete('/{supplierOrder}', ['as' => 'supplier-order.delete', 'uses' => 'SupplierOrderController@destroy']);
});

// Supplier Inventory
Route::group(['prefix' => '/supplier-inventory', 'middleware' => 'jwt.auth'], function(){
    Route::get('/{supplier}/filter', ['as' => 'supplier-inventory.index', 'uses' => 'InventorySupplierController@index']);
    Route::put('/', ['as' => 'supplier-inventory.create', 'uses' => 'InventorySupplierController@store']);
    Route::get('/{inventorySupplier}', ['as' => 'supplier-inventory.show', 'uses' => 'InventorySupplierController@show']);
    Route::post('/{inventorySupplier}', ['as' => 'supplier-inventory.update', 'uses' => 'InventorySupplierController@update']);
    Route::delete('/{inventorySupplier}', ['as' => 'supplier-inventory.delete', 'uses' => 'InventorySupplierController@destroy']);
});

